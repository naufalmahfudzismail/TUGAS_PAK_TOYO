let state = {
  pageLoading: true,
  moduls: {
    rumahsehat: [
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" }
        ],
        url: "rumahsehat",
        id: 36,
        name: "Rumah Sehat",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Tidak Ada Jentik" },
          { icon: "dislike-s", name: "Ada Jentik" }
        ],
        url: "jentikberkala",
        id: 37,
        name: "PJB",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "terbuka-s", name: "Terbuka" },
          { icon: "tertutup-s", name: "Tertutup" }
        ],
        url: "spal",
        id: 38,
        name: "SPAL",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "pompa-s", name: "Pompa" },
          { icon: "sumur-s", name: "Sumur Gali" },
          { icon: "sumurplus-s", name: "Sumur Gali Plus" },
          { icon: "pipa-s", name: "Perpipaan / PDAM" },
          { icon: "mataair-s", name: "Mata Air" }
        ],
        url: "sab",
        id: 39,
        name: "SAB",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "danau-s", name: "Koya / Empang" },
          { icon: "sungaikali-s", name: "Kali" },
          { icon: "helikopter-s", name: "Helikopter" },
          { icon: "septictank-s", name: "Septic Tank" }
        ],
        url: "jamban",
        id: 40,
        name: "Jamban",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Dipilah" },
          { icon: "dislike-s", name: "Tidak Dipilah" }
        ],
        url: "sampah",
        id: 41,
        name: "Sampah",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "sewa-s", name: "Kontrak" },
          { icon: "rumahsendiri-s", name: "Rumah Sendiri" }
        ],
        url: "statustempattinggal",
        id: 42,
        name: "Status Tempat Tinggal",
        selected: []
      }
    ],
    tempatumum: [
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Layak" },
          { icon: "dislike-s", name: "Tidak Layak" }
        ],
        url: "tempatibadah",
        id: 24,
        name: "Tempat Ibadah",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" }
        ],
        url: "pasar",
        id: 15,
        name: "Pasar",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" },
          { icon: "pns-s", name: "PNS" },
          { icon: "honorer-s", name: "Honorer" },
          { icon: "tetap", name: "Tetap" }
        ],
        url: "sekolah",
        id: 6,
        name: "Sekolah",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" },
          { icon: "pns-s", name: "PNS" },
          { icon: "honorer-s", name: "Honorer" },
          { icon: "tetap", name: "Tetap" }
        ],
        url: "pesantren",
        id: 7,
        name: "Pesantren",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Layak" },
          { icon: "dislike-s", name: "Tidak Layak" }
        ],
        url: "kolamrenang",
        id: 43,
        name: "Kolam Renang",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" }
        ],
        url: "puskesmas",
        id: 4,
        name: "Puskesmas",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" }
        ],
        url: "rumahsakit",
        id: 2,
        name: "Rumah Sakit",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Sehat" },
          { icon: "dislike-s", name: "Tidak Sehat" }
        ],
        url: "klinik",
        id: 3,
        name: "Klinik",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Layak" },
          { icon: "dislike-s", name: "Tidak Layak" }
        ],
        url: "hotel",
        id: 8,
        name: "Hotel",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Layak" },
          { icon: "dislike-s", name: "Tidak Layak" }
        ],
        url: "hotelmelati",
        id: 9,
        name: "Hotel Melati",
        selected: []
      }
    ],

    tempatpengolahanmakanan: [
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Penyimpangan Sedikit" },
          { icon: "dislike-s", name: "Penyimpangan Banyak" }
        ],
        url: "pemeriksaanjasaboga",
        id: 40,
        name: "Jasa Boga",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Laik Hygiene Sanitasi" },
          { icon: "dislike-s", name: "Tidak Laik Hygiene Sanitasi" }
        ],
        url: "rumahmakanrestaurant",
        id: 44,
        name: "Restoran",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "like-s", name: "Memenuhi Persyaratan" },
          { icon: "dislike-s", name: "Belum Memenuhi Persyaratan" }
        ],
        url: "depotairminum",
        id: 42,
        name: "Depot Air Minum",
        selected: []
      }
    ],

    lainnya: [
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "total", name: "Dalam Gedung" },
          { icon: "total", name: "Luar Gedung" }
        ],
        url: "kesehatanlingkungan",
        id: 10,
        name: "Pelayanan Kesehatan",
        selected: []
      },
      {
        indikator: [
          { icon: "total", name: "Total" },
          { icon: "total", name: "Dalam Gedung" },
          { icon: "total", name: "Luar Gedung" }
        ],
        url: "kesehatanlingkungan",
        id: 10,
        name: "PHBS",
        selected: []
      }
    ]
  }
};
export default state;
