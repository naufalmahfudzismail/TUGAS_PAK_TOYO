const state = {
  moduls: {
    kartukeluarga: [
      {
        url: "lakilakis",
        name: "Laki - Laki",
        id: 1000,
        alias: "penduduk",
        byPass: true
      },
      {
        url: "perempuan-s",
        name: "Perempuan",
        id: 1001,
        alias: "penduduk",
        byPass: true
      }
    ],
    rumahsehat: [
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          }
        ],
        url: "rumahsehat",
        id: 36,
        name: "Rumah Sehat",
        alias: "rumah_sehat",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Bebas Jentik",
            alias: "tidak_ada_jentik"
          },
          {
            icon: "dislike-s",
            name: "Ada Jentik",
            alias: "ada_jentik"
          }
        ],
        url: "jentikberkala",
        id: 36,
        name: "PJB",
        alias: "pjb",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "terbuka-s",
            name: "Terbuka",
            alias: "terbuka"
          },
          {
            icon: "tertutup-s",
            name: "Tertutup",
            alias: "tertutup"
          }
        ],
        url: "spal",
        id: 36,
        name: "SPAL",
        alias: "spal",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "pompa-s",
            name: "Pompa",
            alias: "pompa"
          },
          {
            icon: "sumur-s",
            name: "Sumur Gali",
            alias: "sumur_gali"
          },
          {
            icon: "sumurplus-s",
            name: "Sumur Gali Plus",
            alias: "sumur_gali_plus"
          },
          {
            icon: "pipa-s",
            name: "Perpipaan / PDAM",
            alias: "perpipaan_pdam"
          },
          {
            icon: "mataair-s",
            name: "Mata Air",
            alias: "mata_air"
          },
          {
            icon: "total",
            name: "Rendah (R)",
            alias: "rendah"
          },
          {
            icon: "total",
            name: "Sedang (S)",
            alias: "sedang"
          },
          {
            icon: "total",
            name: "Tinggi (T)",
            alias: "tinggi"
          },
          {
            icon: "total",
            name: "Amat Tinggi(AT)",
            alias: "amat_tinggi"
          }
        ],
        url: "sab",
        id: 11,
        name: "SAB",
        alias: "sab",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "danau-s",
            name: "Koya / Empang",
            alias: "koya"
          },
          {
            icon: "sungaikali-s",
            name: "Kali",
            alias: "kali"
          },
          {
            icon: "helikopter-s",
            name: "Helikopter",
            alias: "helikopter"
          },
          {
            icon: "septictank-s",
            name: "Septic Tank",
            alias: "septik_tank"
          }
        ],
        url: "jamban",
        id: 36,
        name: "Jamban",
        alias: "jamban",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Dipilah",
            alias: "dipilah"
          },
          {
            icon: "dislike-s",
            name: "Tidak Dipilah",
            alias: "tidak_dipilah"
          }
        ],
        url: "sampah",
        id: 36,
        name: "Sampah",
        alias: "sampah",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "sewa-s",
            name: "Kontrak",
            alias: "kontrak"
          },
          {
            icon: "rumahsendiri-s",
            name: "Rumah Sendiri",
            alias: "rumah_sendiri"
          }
        ],
        url: "statustempattinggal",
        id: 36,
        name: "Status Tempat Tinggal",
        alias: "stt",
        selected: [],
        byPass: false,
        total: 0
      }
    ],
    tempatumum: [
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "tempatibadah",
        id: 24,
        name: "Tempat Ibadah",
        alias: "tempat_ibadah",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "pasar",
        id: 15,
        name: "Pasar",
        alias: "pasar",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "sekolah",
        id: 6,
        name: "Sekolah",
        alias: "sekolah",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "pesantren",
        id: 7,
        name: "Pesantren",
        alias: "pesantren",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "kolamrenang",
        id: 43,
        name: "Kolam Renang",
        alias: "kolam_renang",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "puskesmas",
        id: 4,
        name: "Puskesmas",
        alias: "puskesmas",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "rumahsakit",
        id: 2,
        name: "Rumah Sakit",
        alias: "rumah_sakit",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "klinik",
        id: 3,
        name: "Klinik",
        alias: "klinik",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "hotel",
        id: 8,
        name: "Hotel",
        alias: "hotel",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "hotelmelati",
        id: 9,
        name: "Hotel Melati",
        alias: "hotel_melati",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Sehat",
            alias: "sehat"
          },
          {
            icon: "dislike-s",
            name: "Tidak Sehat",
            alias: "tidak_sehat"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "salon",
        id: 45,
        name: "Salon",
        alias: "salon",
        selected: [],
        byPass: false,
        total: 0
      }
    ],
    tempatpengolahanmakanan: [
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Laik Hygiene",
            alias: "laik_hygiene_sanitasi"
          },
          {
            icon: "dislike-s",
            name: "Tidak Laik Hygiene",
            alias: "tidak_laik_hygiene_sanitasi"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "pemeriksaanjasaboga",
        id: 40,
        name: "Jasa Boga",
        alias: "jasa_boga",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Laik Hygiene",
            alias: "laik_hygiene_sanitasi"
          },
          {
            icon: "dislike-s",
            name: "Tidak Laik Hygiene",
            alias: "tidak_laik_hygiene_sanitasi"
          },
          {
            icon: "like-s",
            name: "Memiliki izin Usaha",
            alias: "memiliki_izin_usaha"
          },
          {
            icon: "dislike-s",
            name: "Tidak Memiliki izin Usaha",
            alias: "tidak_memiliki_izin_usaha"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "rumahmakanrestaurant",
        id: 44,
        name: "Restoran",
        alias: "restoran",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Laik Hygiene",
            alias: "laik_hygiene_sanitasi"
          },
          {
            icon: "dislike-s",
            name: "Tidak Laik Hygiene ",
            alias: "tidak_laik_hygiene_sanitasi"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "depotairminum",
        id: 42,
        name: "Depot Air Minum",
        alias: "dam",
        selected: [],
        byPass: false,
        total: 0
      }
    ],
    lainnya: [
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "didalamgedung-s",
            name: "Dalam Gedung",
            alias: "dalam_gedung"
          },
          {
            icon: "diluargedung-s",
            name: "Luar Gedung",
            alias: "luar_gedung"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "kesehatanlingkungan",
        id: 10,
        name: "Yankesling",
        alias: "pelayanan_kesehatan",
        selected: [],
        byPass: false,
        total: 0
      },
      {
        indikator: [
          {
            icon: "total",
            name: "Total",
            alias: "total"
          },
          {
            icon: "like-s",
            name: "Rumah Tangga Ber PHBS",
            alias: "laik_hygiene_sanitasi"
          },
          {
            icon: "dislike-s",
            name: "Rumah Tangga Tidak Ber PHBS",
            alias: "tidak_laik_hygiene_sanitasi"
          },
          {
            icon: "ban-s",
            name: "Belum Di inspeksi",
            alias: "belum_dijamah"
          }
        ],
        url: "phbs-s",
        alias: "phbs",
        id: 46,
        name: "PHBS",
        selected: [],
        byPass: false,
        total: 0
      }
    ]
  },
  kategori: ["RS", "TTU", "TPM", "LAINNYA"],
  headers: {
    rumahsehat: {
      icon: "rumahsehat",
      title: "Rumah Sehat"
    },
    tempatumum: {
      icon: "tempatumum",
      title: "Tempat - Tempat Umum"
    },
    tempatpengolahanmakanan: {
      icon: "pemeriksaanjasaboga",
      title: "Tempat Pengolahan Makanan"
    },
    lainnya: {
      icon: "kesehatanlingkungan",
      title: "Lain - Lain"
    }
  },
  filter: {
    kecamatan: "",
    kelurahan: "",
    rw: "",
    rt: "",
    tanggal: ""
  }
};

const getters = {
  getModuls: state => {
    return state.moduls;
  },
  getModulsByKategori: state => kategori => {
    switch (kategori) {
      case "rs":
        return state.moduls.rumahsehat;
      case "ttu":
        return state.moduls.tempatumum;
      case "tpm":
        return state.moduls.tempatpengolahanmakanan;
      case "lainnya":
        return state.moduls.lainnya;
    }
  },
  getHeadersByKategori: state => kategori => {
    switch (kategori) {
      case "rs":
        return state.headers.rumahsehat;
      case "ttu":
        return state.headers.tempatumum;
      case "tpm":
        return state.headers.tempatpengolahanmakanan;
      case "lainnya":
        return state.headers.lainnya;
    }
  },
  getFilter: state => {
    return state.filter;
  }
};

const actions = {};

const mutations = {
  filterModule(state, payload) {
    state.filter = payload;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
