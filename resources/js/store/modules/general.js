const state = {
  pageLoading  : false,
  serverError  : false,
  filterLoading: false,
}

const getters = {
  getLoading: (state) => {
    return state.pageLoading
  },
  getServerError: (state) => {
    return state.serverError
  },
  getFilterLoading: (state) => {
    return state.filterLoading
  },
}

const actions = {}

const mutations = {
  STILL_LOADING (state) {
    state.pageLoading = true
  },
  NOT_LOADING (state) {
    state.pageLoading = false
  },
  SERVER_ERROR (state) {
    state.serverError = true
  },
  SERVER_SUCCESS (state) {
    state.serverError = false
  },
  FILTER_LOADING (state) {
    state.filterLoading = true
  },
  REMOVE_FILTER_LOADING (state) {
    state.filterLoading = false
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}
