/* eslint-disable eqeqeq */
/* eslint-disable import/no-duplicates */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'es6-promise/auto'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Datepicker from 'vuejs-datepicker'
import { id } from 'vuejs-datepicker/dist/locale'
import Notifications from 'vue-notification'
import Sweetalert from 'vue-sweetalert2'
import Vuelidate from 'vuelidate'
import Loading from './components/Loading'
import Select2 from './components/Select'
import App from './App'
import routes from './router'
import store from '../store'
import Vuetify from 'vuetify'
import Highcharts from 'highcharts'
// import Prefix from "prefix";
import stockInit from 'highcharts/modules/stock'
import mapInit from 'highcharts/modules/map'
import HighchartsVue from 'highcharts-vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import { Form,
  HasError,
  AlertError,
  AlertErrors,
  AlertSuccess } from 'vform'
import axios from 'axios'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import auth from '../auth'
import swal from 'sweetalert2'
import moment from 'moment'
import 'babel-polyfill'
import 'es6-promise/auto'
import * as VueGoogleMaps from 'vue2-google-maps'

moment.locale('id')

// set to global
window.Vue             = Vue
window.Fire            = new Vue()
Vue.config.performance = true

Vue.router = routes
Vue.use(VueRouter)

Vue.use(Vuetify)
Vue.use(BootstrapVue)
Vue.use(Notifications)
Vue.use(Sweetalert)
Vue.use(Vuelidate)
Vue.use(HighchartsVue)
Vue.use(Highcharts)
// Vue.use(Prefix);
stockInit(Highcharts)
mapInit(Highcharts)

// Vue.use(NProgress)

const VueScrollactive = require('vue-scrollactive')
Vue.use(VueScrollactive)

// sweet alert
window.swal = swal

const toast = swal.mixin({
  toast            : true,
  position         : 'top-end',
  showConfirmButton: false,
  timer            : 3000,
})

window.toast = toast

window.Form = Form

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertErrors.name, AlertErrors)
Vue.component(AlertSuccess.name, AlertSuccess)

Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`

Vue.use(VueAuth, auth)

Vue.use(VueGoogleMaps, {
  load: {
    key      : 'AIzaSyCQ-moahgm54VLY_YoF8HjoO6HDSZo7ORM',
    libraries: 'places, geocoding', // necessary for places input
  },
})

Vue.component(
  'passport-clients',
  require('./../components/passport/Clients.vue').default
)

Vue.component(
  'passport-authorized-clients',
  require('./../components/passport/AuthorizedClients.vue').default
)

Vue.component(
  'passport-personal-access-tokens',
  require('./../components/passport/PersonalAccessTokens.vue').default
)

Vue.filter('state', (value, dirtyOnly = true) => {
  if (dirtyOnly)
    if (!value.$dirty) return null
  return !value.$invalid ? 'valid' : 'invalid'
})

Vue.component('b-loading', Loading)
Vue.component('b-select-2', Select2)
Vue.component('b-datepicker', {
  extends: Datepicker,
  props  : {
    bootstrapStyling: {
      type   : Boolean,
      default: true,
    },
    language: {
      type   : Object,
      default: () => id,
    },
  },
})

Vue.mixin({
  methods: {
    PercentConverter: (value, total) => {
      const getValue = value / total
      if (isNaN(getValue))
        return '0%'
      else {
        const total = Math.round(getValue * 100)
        return `${total}%`
      }
    },
    IndonesiaDateFormat: (date) => {
      if (date != '0')
        return moment(date).format('LLLL')
      else
        return 'Tidak ada data'
    },
    Maintenance: () => {
      swal.fire('Fitur ini masih dalam pengembangan')
    },
    IndonesiaFilterFormat: (date) => {
      if (date != '0')
        return moment(date).format('LL')
      else
        return 'Tidak ada data'
    },
  },
})

export default new Vue({
  el        : '#app',
  router    : routes,
  store,
  components: { App },
  template  : '<App/>',
})
