export default class Gate {
  constructor (user) {
    this.user = user
  }

  isAdmin () {
    return this.user.role === 0
  }

  isOPD () {
    return this.user.role === 1
  }

  isUPT () {
    return this.user.role === 2
  }

  isUPF () {
    return this.user.role === 3
  }
}
