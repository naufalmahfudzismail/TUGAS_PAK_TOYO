export default {
  items: {
    UPT: [
      {
        name: "Dashboard",
        icon: "fa fa-tachometer",
        url: "/dashboard",
        badge: {
          variant: "primary"
        },
        children: [
          {
            name: "Dashboard Utama",
            url: "/dashboard",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Modul",
            url: "/modul",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Peta",
            url: "/dashboard-peta",
            icon: "fa fa-map"
          }
        ]
      },
      {
        name: "Data User",
        icon: "fa fa-user",
        children: [
          {
            name: "Data Admin",
            url: "/admin/data-admin",
            icon: "fa fa-user"
          },
          {
            name: "Data Kader",
            url: "/kader/data-kader",
            icon: "fa fa-user"
          }
        ]
      },

      {
        name: "Pesan",
        url: "/coming-soon",
        icon: "fa fa-envelope"
      },
      {
        name: "Guide Book",
        url: "/guide-book",
        icon: "fa fa-book"
      }
    ],
    admin: [
      {
        name: "Dashboard",
        icon: "fa fa-tachometer",
        url: "/dashboard",
        badge: {
          variant: "primary"
        },
        children: [
          {
            name: "Dashboard Utama",
            url: "/dashboard",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Modul",
            url: "/modul",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Peta",
            url: "/dashboard-peta",
            icon: "fa fa-map"
          }
        ]
      },
      {
        name: "Tambah Objek",
        url: "/tambah-data",
        icon: "fa fa-plus"
      },
      {
        name: "Manage Konten",
        url: "/manage-konten",
        icon: "fa fa-cog"
      },
      {
        name: "Data User",
        icon: "fa fa-user",
        children: [
          {
            name: "Data Admin",
            url: "/admin/data-admin",
            icon: "fa fa-user"
          },
          {
            name: "Data Kader",
            url: "/kader/data-kader",
            icon: "fa fa-user"
          }
        ]
      },
      {
        name: "Pesan",
        url: "/coming-soon",
        icon: "fa fa-envelope"
      },
      {
        name: "Penyesuaian Data",
        url: "/penyesuaian-data",
        icon: "fa fa-wrench"
      },
      {
        name: "Report",
        url: "/report",
        icon: "fa fa-info"
      },
      {
        name: "Validasi Data",
        url: "/validasi-data",
        icon: "fa fa-low-vision"
      },
      {
        name: "Trash",
        url: "/trash",
        icon: "fa fa-trash"
      },
      {
        name: "Guide Book",
        url: "/guide-book",
        icon: "fa fa-book"
      }
    ],
    OPD: [
      {
        name: "Dashboard",
        icon: "fa fa-tachometer",
        url: "/dashboard",
        badge: {
          variant: "primary"
        },
        children: [
          {
            name: "Dashboard Utama",
            url: "/dashboard",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Peta",
            url: "/dashboard-peta",
            icon: "fa fa-map"
          }
        ]
      },
      {
        name: "Guide Book",
        url: "/guide-book",
        icon: "fa fa-book"
      }
    ],
    UPF: [
      {
        name: "Dashboard",
        icon: "fa fa-tachometer",
        url: "/dashboard",
        badge: {
          variant: "primary"
        },
        children: [
          {
            name: "Dashboard Utama",
            url: "/dashboard",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Modul",
            url: "/modul",
            icon: "fa fa-tachometer",
            badge: {
              variant: "primary",
              text: ""
            }
          },
          {
            name: "Dashboard Peta",
            url: "/dashboard-peta",
            icon: "fa fa-map"
          }
        ]
      },
      {
        name: "Data Kader",
        url: "/kader/data-kader",
        icon: "fa fa-user"
      },
      {
        name: "Pesan",
        url: "/coming-soon",
        icon: "fa fa-envelope"
      },
      {
        name: "Guide Book",
        url: "/guide-book",
        icon: "fa fa-book"
      }
    ]
  }
};
