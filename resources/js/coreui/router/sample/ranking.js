import Ranking from "@/views/sample/Ranking";
import RankRumahSehat from "@/views/sample/ranking/RankRumahSehat";
import SemuaRank from "@/views/sample/ranking/SemuaRank";
import RankHotel from "@/views/sample/ranking/RankHotel";
import RankHotelMelati from "@/views/sample/ranking/RankHotelMelati";
import RankJasaBoga from "@/views/sample/ranking/RankJasaBoga";
import RankKlinik from "@/views/sample/ranking/RankKlinik";
import RankKolamRenang from "@/views/sample/ranking/RankKolamRenang";
import RankPasar from "@/views/sample/ranking/RankPasar";
import RankKesling from "@/views/sample/ranking/RankPelayananKesehatan";
import RankPesantren from "@/views/sample/ranking/RankPesantren";
import RankPuskesmas from "@/views/sample/ranking/RankPuskesmas";
import RankRestoran from "@/views/sample/ranking/RankRestoran";
import RankRumahSakit from "@/views/sample/ranking/RankRumahSakit";
import RankSekolah from "@/views/sample/ranking/RankSekolah";
import RankTempatIbadah from "@/views/sample/ranking/RankTempatIbadah";
import RankDAM from "@/views/sample/ranking/RankDepotAirMinum";

export default {
  path: "/ranking",
  name: "Ranking",
  redirect: "/ranking/semua",
  component: Ranking,
  meta: {
    auth: true,
    authRedirect: {
      path: "/pages/login"
    },
    hideFilter: true
  },
  children: [
    {
      path: "semua",
      name: "Semua",
      component: SemuaRank,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "rumah-sehat",
      name: " Rumah Sehat",
      component: RankRumahSehat,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "hotel",
      name: " Hotel",
      component: RankHotel,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "hotel-melati",
      name: " Hotel Melati",
      component: RankHotelMelati,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "jasa-boga",
      name: " Jasa Boga",
      component: RankJasaBoga,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "klinik",
      name: " Klinik",
      component: RankKlinik,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "kolam-renang",
      name: " Kolam Renang",
      component: RankKolamRenang,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "pasar",
      name: " Pasar",
      component: RankPasar,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "pelayanan-kesehatan",
      name: " Pelayanan Kesehatan Lingkungan",
      component: RankKesling,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "pesantren",
      name: " Pesantren",
      component: RankPesantren,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "puskesmas",
      name: " Puskesmas",
      component: RankPuskesmas,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "restoran",
      name: " Restoran",
      component: RankRestoran,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "rumah-sakit",
      name: " Rumah Sakit",
      component: RankRumahSakit,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "sekolah",
      name: " Sekolah",
      component: RankSekolah,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "tempat-ibadah",
      name: " Tempat Ibadah",
      component: RankTempatIbadah,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "depot-air-minum",
      name: " Depot Air Minum",
      component: RankDAM,
      meta: {
        hideFilter: true
      }
    }
  ]
};
