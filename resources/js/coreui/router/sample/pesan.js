// Views - Components
import Pesan from '@/views/sample/Pesan'

export default {
  path     : 'pesan',
  redirect : '/theme/pesan',
  name     : 'Pesan',
  meta: {
    auth: true,
    authRedirect: {
      path: '/pages/login'
    }
  },
  component: {
    render (c) {
      return c('router-view')
    },
  },
  children: [
    {
      path     : 'pesan',
      name     : 'Pesan',
      component: Pesan,
    },
  ],
}
