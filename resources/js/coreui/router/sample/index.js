import Charts from "@/views/sample/Charts";
import Widgets from "@/views/sample/Widgets";
import Loading from "@/views/sample/Loading";
import Pesan from "@/views/sample/Pesan";
import Managekonten from "@/views/sample/Managekonten";
import TambahData from "@/views/sample/form/TambahData";
import Penyesuaiandata from "@/views/sample/Penyesuaiandata";
import Trash from "@/views/sample/Trash";
import ProfilPetugas from "@/views/sample/ProfilPetugas";
import SearchResult from "@/views/sample/Searchresult";
import Detaildepokhealthycity from "@/views/sample/Detaildepokhealthycity";
import HistoriSemua from "@/views/sample/HistoriSemua";
import Report from "@/views/sample/Report";
import ValidasiData from "@/views/sample/ValidasiData";
import Baddata from "@/views/sample/Baddata";

import admin from "./admin";
import kader from "./kader";
import modul from "./modul";
import base from "./base";
import buttons from "./buttons";
import icons from "./icons";
import notifications from "./notifications";
import theme from "./theme";
import ranking from "./ranking";

export default [
  modul,
  base,
  buttons,
  icons,
  notifications,
  theme,
  admin,
  kader,
  ranking,
  {
    path: "charts",
    name: "Charts",
    component: Charts
  },
  {
    path: "report",
    name: "Report",
    component: Report,
    meta: {
      auth: {
        roles: 1,
        forbiddenRedirect: "/pages/403"
      },
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true
    }
  },
  {
    path: "widgets",
    name: "Widgets",
    component: Widgets
  },
  {
    path: "loading",
    name: "Loading",
    component: Loading
  },
  {
    path: "tambah-data",
    name: "Tambah Data",
    component: TambahData,
    meta: {
      auth: {
        roles: 1,
        redirect: { name: "Penyesuaian Data" },
        forbiddenRedirect: "/pages/403"
      },
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true,
      disabledLoadingCircle: true
    }
  },
  {
    path: "manage-konten",
    name: "Manage Konten",
    component: Managekonten,
    meta: {
      auth: {
        roles: 1,
        redirect: { name: "Penyesuaian Data" },
        forbiddenRedirect: "/pages/403"
      },
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true,
      disabledLoadingCircle: true
    }
  },
  {
    path: "pesan",
    name: "Pesan",
    component: Pesan,
    meta: {
      auth: true,
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true,
      disabledLoadingCircle: true
    }
  },
  {
    path: "penyesuaian-data",
    name: "Penyesuaian Data",
    meta: {
      auth: {
        roles: 1,
        redirect: { name: "Penyesuaian Data" },
        forbiddenRedirect: "/pages/403"
      },
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true,
      disabledLoadingCircle: true
    },
    component: Penyesuaiandata
  },
  {
    path: "trash",
    name: "Trash",
    component: Trash,
    meta: {
      auth: {
        roles: 1,
        redirect: { name: "Penyesuaian Data" },
        forbiddenRedirect: "/pages/403"
      },
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true,
      disabledLoadingCircle: true
    }
  },

  {
    path: "profil",
    name: "Profil Saya",
    component: ProfilPetugas,
    meta: {
      hideFilter: true,
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  },

  {
    path: "profil/:usertype",
    component: ProfilPetugas,
    name: "Profil",
    meta: {
      hideFilter: true,
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  },
  {
    path: "search-result/:modul/:query",
    name: "Search Result",
    component: SearchResult,
    meta: {
      auth: true,
      authRedirect: {
        path: "/pages/login"
      },
      hideFilter: true
    }
  },
  {
    path: "depokhealthycity/detail",
    name: "Depok Healthy City",
    component: Detaildepokhealthycity,
    meta: {
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  },
  {
    path: "histori/semua",
    name: "Histori",
    component: HistoriSemua,
    meta: {
      hideFilter: true,
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  },
  {
    path: "guide-book",
    beforeEnter() {
      if (confirm("Anda akan diarahkan keluar website")) {
        location.href =
          "https://drive.google.com/file/d/1woppp9xPijuU2qSjHPKO3bPfDfkFR7W_/view?ts=5dbafafb";
        target = "_blank";
      }
    }
  },
  {
    path: "validasi-data",
    name: "Validasi Data",
    component: ValidasiData,
    meta: {
      hideFilter: true,
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  },
  {
    path: "validasi-data/:cat/:username",
    name: " Validasi Data",
    component: Baddata,
    meta: {
      hideFilter: true,
      auth: true,
      authRedirect: {
        path: "/pages/login"
      }
    }
  }
];
