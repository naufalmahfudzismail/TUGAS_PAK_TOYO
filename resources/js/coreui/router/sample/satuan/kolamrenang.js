import KolamRenang from "@/views/sample/modul/Kolamrenang";
import DataKolamRenang from "@/views/sample/modul/satuan/DataKolamRenang";

export default {
  path: "kolam-renang",
  name: "Kolam Renang",
  component: KolamRenang,
  children: [
    {
      path: ":id/",
      name: "Data Kolam Renang",
      component: DataKolamRenang
    }
  ]
};
