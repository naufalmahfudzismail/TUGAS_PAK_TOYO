import Puskesmas from "@/views/sample/modul/Puskesmas";
import DataPuskesmas from "@/views/sample/modul/satuan/DataPuskesmas";

export default {
  path: "puskesmas",
  name: "Puskesmas",
  component: Puskesmas,
  children: [
    {
      path: ":id/",
      name: "Data Puskesmas",
      component: DataPuskesmas
    }
  ]
};
