import Restoran from "@/views/sample/modul/Restoran";
import DataRestoran from "@/views/sample/modul/satuan/DataRestoran";

export default {
  path: "restoran",
  name: "Restoran",
  component: Restoran,
  children: [
    {
      path: ":id/",
      name: "Data Restoran",
      component: DataRestoran
    }
  ]
};
