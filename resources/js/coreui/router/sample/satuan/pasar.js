import Pasar from "@/views/sample/modul/Pasar";
import DataPasar from "@/views/sample/modul/satuan/DataPasar";

export default {
  path: "pasar",
  name: "Pasar",
  component: Pasar,
  children: [
    {
      path: ":id/",
      name: "Data Pasar",
      component: DataPasar
    }
  ]
};
