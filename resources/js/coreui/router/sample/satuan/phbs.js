import Phbs from "@/views/sample/modul/Phbs";
import DataPhbs from "@/views/sample/modul/satuan/DataPhbs";

export default {
  path: "phbs",
  name: "PHBS",
  component: Phbs,
  children: [
    {
      path: ":id/",
      name: "Data PHBS",
      component: DataPhbs
    }
  ]
};
