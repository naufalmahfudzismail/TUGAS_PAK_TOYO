import JasaBoga from "@/views/sample/modul/Jasaboga";
import DataJasaBoga from "@/views/sample/modul/satuan/DataJasaBoga";

export default {
  path: "jasa-boga",
  name: "Jasa Boga",
  component: JasaBoga,
  children: [
    {
      path: ":id/",
      name: "Data Jasa Boga",
      component: DataJasaBoga
    }
  ]
};
