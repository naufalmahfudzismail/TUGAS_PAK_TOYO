import Klinik from "@/views/sample/modul/Klinik";
import DataKlinik from "@/views/sample/modul/satuan/DataKlinik";

export default {
  path: "klinik",
  name: "Klinik",
  component: Klinik,
  children: [
    {
      path: ":id/",
      name: "Data Klinik",
      component: DataKlinik
    }
  ]
};
