import Pesantren from "@/views/sample/modul/Pesantren";
import DataPesantren from "@/views/sample/modul/satuan/DataPesantren";

export default {
  path: "pesantren",
  name: "Pesantren",
  component: Pesantren,
  children: [
    {
      path: ":id/",
      name: "Data Pesantren",
      component: DataPesantren
    }
  ]
};
