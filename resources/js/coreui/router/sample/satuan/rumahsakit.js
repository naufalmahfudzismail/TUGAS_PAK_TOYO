import RumahSakit from "@/views/sample/modul/Rumahsakit";
import DataRumahSakit from "@/views/sample/modul/satuan/DataRumahSakit";

export default {
  path: "rumah-sakit",
  name: "Rumah Sakit",
  component: RumahSakit,
  children: [
    {
      path: ":id/",
      name: "Data Rumah Sakit",
      component: DataRumahSakit
    }
  ]
};
