import RumahSehat from "@/views/sample/modul/Rumahsehat";
import DataRumahSehat from "@/views/sample/modul/satuan/DataRumahSehat";

export default {
  path: "rumah-sehat",
  name: "Rumah Sehat",
  alias: ["pjb", "spal", "sab", "jamban", "sampah", "status-tempat-tinggal"],
  component: RumahSehat,
  children: [
    {
      path: ":id/",
      name: "Data Rumah Sehat",
      component: DataRumahSehat
    }
  ]
};
