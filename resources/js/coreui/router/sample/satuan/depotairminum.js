import DepotAirMinum from "@/views/sample/modul/Depotairminum";
import DataDepotAirMinum from "@/views/sample/modul/satuan/DataDepotAirMinum";

export default {
  path: "depot-air-minum",
  name: "Depot Air Minum",
  component: DepotAirMinum,
  children: [
    {
      path: ":id/",
      name: "Data Depot Air Minum",
      component: DataDepotAirMinum
    }
  ]
};
