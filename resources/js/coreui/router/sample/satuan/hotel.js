import Hotel from "@/views/sample/modul/Hotel";
import DataHotel from "@/views/sample/modul/satuan/DataHotel";

export default {
  path: "hotel",
  name: "Hotel",
  component: Hotel,
  children: [
    {
      path: ":id/",
      name: "Data Hotel",
      component: DataHotel
    }
  ]
};
