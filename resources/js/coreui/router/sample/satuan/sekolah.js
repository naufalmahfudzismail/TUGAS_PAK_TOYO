import Sekolah from "@/views/sample/modul/Sekolah";
import DataSekolah from "@/views/sample/modul/satuan/DataSekolah";

export default {
  path: "sekolah",
  name: "Sekolah",
  component: Sekolah,
  children: [
    {
      path: ":id/",
      name: "Data Sekolah",
      component: DataSekolah
    }
  ]
};
