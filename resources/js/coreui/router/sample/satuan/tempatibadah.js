import TempatIbadah from "@/views/sample/modul/Tempatibadah";
import DataTempatIbadah from "@/views/sample/modul/satuan/DataTempatIbadah";

export default {
  path: "tempat-ibadah",
  name: "Tempat Ibadah",
  component: TempatIbadah,
  children: [
    {
      path: ":id/",
      name: "Data Tempat Ibadah",
      component: DataTempatIbadah
    }
  ]
};
