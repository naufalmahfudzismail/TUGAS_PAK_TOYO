import PelayananKesehatan from "@/views/sample/modul/Pelayanankesehatan";
import DataPelayananKesehatan from "@/views/sample/modul/satuan/DataPelayananKesehatan";

export default {
  path: "yankesling",
  name: "Pelayanan Kesehatan Lingkungan",
  component: PelayananKesehatan,
  children: [
    {
      path: ":id/",
      name: "Data Pelayanan Kesehatan",
      component: DataPelayananKesehatan
    }
  ]
};
