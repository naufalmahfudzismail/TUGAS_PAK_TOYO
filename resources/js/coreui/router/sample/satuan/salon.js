import Salon from "@/views/sample/modul/Salon";
import DataSalon from "@/views/sample/modul/satuan/DataSalon";

export default {
  path: "salon",
  name: "Salon",
  component: Salon,
  children: [
    {
      path: ":id/",
      name: "Data Salon",
      component: DataSalon
    }
  ]
};
