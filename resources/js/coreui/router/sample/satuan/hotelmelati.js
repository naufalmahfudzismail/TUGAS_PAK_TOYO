import HotelMelati from "@/views/sample/modul/Hotelmelati";
import DataHotelMelati from "@/views/sample/modul/satuan/DataHotelMelati";

export default {
  path: "hotel-melati",
  name: "Hotel Melati",
  component: HotelMelati,
  children: [
    {
      path: ":id/",
      name: "Data Hotel Melati",
      component: DataHotelMelati
    }
  ]
};
