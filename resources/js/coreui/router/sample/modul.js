// Views - Buttons
import Modul from "@/views/sample/modul/Modul";
import KartuKeluarga from "@/views/sample/KartuKeluarga";

import rumahsehat from "./satuan/rumahsehat";
import jasaboga from "./satuan/jasaboga";
import depotairminum from "./satuan/depotairminum";
import hotel from "./satuan/hotel";
import hotelmelati from "./satuan/hotelmelati";
import klinik from "./satuan/klinik";
import kolamrenang from "./satuan/kolamrenang";
import pasar from "./satuan/pasar";
import pelayanankesehatan from "./satuan/pelayanankesehatan";
import pesantren from "./satuan/pesantren";
import puskesmas from "./satuan/puskesmas";
import restoran from "./satuan/restoran";
import rumahsakit from "./satuan/rumahsakit";
import sekolah from "./satuan/sekolah";
import tempatibadah from "./satuan/tempatibadah";
import salon from "./satuan/salon";
import phbs from "./satuan/phbs";

export default {
  path: "modul",
  name: "Modul",
  redirect: "/modul",
  meta: {
    auth: true,
    authRedirect: {
      path: "/pages/login"
    }
  },
  component: {
    render(c) {
      return c("router-view");
    }
  },

  // component: Modul,
  children: [{
      path: "/",
      name: "Dashboard Modul",
      component: Modul,
      meta: {
        hideFilter: true
      }
    },
    {
      path: "kartu-keluarga",
      name: "Kartu Keluarga",
      alias: ["Laki---Laki", "Perempuan"],
      // redirect: "kartu-keluarga",
      component: KartuKeluarga
    },
    rumahsehat,
    jasaboga,
    depotairminum,
    hotel,
    hotelmelati,
    klinik,
    kolamrenang,
    pasar,
    pelayanankesehatan,
    pesantren,
    puskesmas,
    restoran,
    rumahsakit,
    sekolah,
    tempatibadah,
    salon,
    phbs
  ]
};
