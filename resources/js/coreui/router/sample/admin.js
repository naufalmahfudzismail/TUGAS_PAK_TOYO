// Route Data Admin
import DataAdmin from "@/views/sample/Dataadmin";
import TambahAdmin from "@/views/sample/form/TambahDataAdmin";
import EditAdmin from "@/views/sample/form/EditDataAdmin";

export default {
  path: "admin",
  redirect: "/admin/data-admin",
  name: "Admin",
  component: {
    render(c) {
      return c("router-view");
    }
  },
  meta: {
    hideFilter: true,
    auth: true,
    disabledLoadingCircle: true,
    hideServerError: true,
    authRedirect: {
      path: "/pages/login"
    }
  },
  children: [
    {
      path: "data-admin",
      name: "Data Admin",
      component: DataAdmin,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true,
        hideServerError: true
      }
    },
    {
      path: "tambah-admin",
      name: "Tambah Admin",
      component: TambahAdmin,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true,
        hideServerError: true
      }
    },
    {
      path: ":id/edit",
      name: "Edit Admin",
      component: EditAdmin,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true,
        hideServerError: true
      }
    }
  ]
};
