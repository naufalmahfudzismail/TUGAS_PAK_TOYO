// Route Data Kader
import DataKader from "@/views/sample/Datakader";
import TambahKader from "@/views/sample/form/TambahDataKader";
import EditKader from "@/views/sample/form/EditDataKader";
import importCsvKader from "@/views/sample/form/ImportCsvKader";

export default {
  path: "kader",
  redirect: "/kader/data-kader",
  name: "Kader",
  meta: {
    auth: true,
    authRedirect: {
      path: "/pages/login"
    }
  },
  component: {
    render(c) {
      return c("router-view");
    }
  },
  children: [
    {
      path: "data-kader",
      name: "Data Kader",
      component: DataKader,
      meta: {
        disabledLoadingCircle: true
      }
    },
    {
      path: "tambah-kader",
      name: "Tambah Kader",
      component: TambahKader,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true
      }
    },
    {
      path: ":id/edit",
      name: "Edit Kader",
      component: EditKader,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true
      }
    },
    {
      path: "import-csv",
      name: "Import CSV Kader",
      component: importCsvKader,
      meta: {
        hideFilter: true,
        disabledLoadingCircle: true
      }
    }
  ]
};
