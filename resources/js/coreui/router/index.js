import Vue from "vue";
import Router from "vue-router";
import "vuetify/dist/vuetify.min.css";

// Containers
import Full from "@/containers/Full";

// Views
import Dashboard from "@/views/sample/Dashboard";
import LandingPage from "@/views/landingpage/LandingPage";
import Dokumentasi from "@/views/landingpage/Dokumentasi";
import Guide from "@/views/landingpage/Guide";
import SelectAPI from "@/views/OpenAPI/SelectAPI";
import ComingSoon from "@/views/pages/ComingSoon";

// Views - Pages
import Page404 from "@/views/pages/Page404";
import Page403 from "@/views/pages/Page403";
import Page500 from "@/views/pages/Page500";
import Login from "@/views/pages/Login";
import Register from "@/views/pages/Register";
import OPA from "@/views/pages/OnePageApplication";
import Dashboardpeta from "@/views/pages/Dashboardpeta";

// Sample route
import sample from "@/router/sample";

Vue.use(Router);

export default new Router({
  mode: "history",
  linkActiveClass: "open active",
  scrollBehavior: () => ({
    y: 0
  }),
  routes: [
    {
      path: "/",
      redirect: "/home",
      name: "Home",
      component: Full,
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
          meta: {
            auth: true,
            authRedirect: {
              path: "/pages/login"
            }
          }
        },
        ...sample
      ]
    },
    {
      path: "/coming-soon",
      name: "Coming Soon",
      component: ComingSoon
    },
    {
      path: "/home",
      name: "Landing-Page",
      component: LandingPage
    },
    {
      path: "/select-api",
      name: "Select API",
      component: SelectAPI
    },
    {
      path: "/dokumentasi",
      name: "Dokumentasi",
      component: Dokumentasi
    },
    {
      path: "/guide",
      name: "Guide",
      component: Guide
    },
    {
      path: "/pages",
      redirect: "/pages/404",
      name: "Pages",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          path: "404",
          name: "Page404",
          component: Page404
        },
        {
          path: "403",
          name: "Page403",
          component: Page403
        },
        {
          path: "500",
          name: "Page500",
          component: Page500
        },
        {
          path: "login",
          name: "Login",
          component: Login,
          meta: {
            auth: undefined
          }
        },
        {
          path: "register",
          name: "Register",
          component: Register,
          meta: {
            auth: undefined
          }
        }
      ]
    },
    {
      path: "/dashboard-peta",
      name: "DashboardPeta",
      component: Dashboardpeta,
      target: "_blank"
    },
    {
      path: "*",
      name: "404",
      component: Page404
    }
  ]
});
