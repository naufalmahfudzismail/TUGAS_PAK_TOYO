<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIPPKLING | Sistem Pemetaan Profil Lingkungan Kota Depok</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
        <i class="nav-icon fa fa-power-off" style="color: red;"></i>
        <p>
        Logout
        </p>
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</body>
</html>