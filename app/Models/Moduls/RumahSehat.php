<?php

namespace App\Models\Moduls;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;
use Carbon\Carbon;
use App\Scopes\ModulsFilterScope;

class RumahSehat extends Model
{
    use SoftDeletes, Dataviewer;
    
    protected $dates = ['deleted_at'];
    protected $guarded = array();

    protected static function boot(){
        parent::boot();
        
        static::addGlobalScope(new ModulsFilterScope);
    }

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }

    public function sab(){
        return $this->belongsTo('App\Models\Moduls\Sab');
    }

}
