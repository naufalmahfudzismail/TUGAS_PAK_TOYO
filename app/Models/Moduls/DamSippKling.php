<?php

namespace App\Models\Moduls;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class DamSippKling extends Model
{
    use SoftDeletes, Dataviewer;
    protected $dates = ['deleted_at'];
    protected $guarded = array();
    //protected $fillable = ['nama_pemilik', 'nilais', 'petugas_id', 'nama_depot', 'jam_operasional', 'pajak', 'foto', 'no_telp', 'alamat', 'koordinat'];
    

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }
}
