<?php

namespace App\Models\Moduls;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class JasaBoga extends Model
{
    use SoftDeletes, Dataviewer;
    protected $guarded = array();
    protected $dates = ['deleted_at'];
    

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }
}
