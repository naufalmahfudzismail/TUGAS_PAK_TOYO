<?php

namespace App\Models\Moduls;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class HotelMelati extends Model
{
    use SoftDeletes, Dataviewer;
    protected $dates = ['deleted_at'];
    protected $guarded = array();
    protected $table = 'hotel_melatis';
    

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }
}
