<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class History extends Model
{
    use SoftDeletes, Dataviewer;
    protected $dates = ['deleted_at'];

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }

    public function datas(){
        return $this->hasOne('App\Models\Data', 'id', 'data_id');
    }
}
