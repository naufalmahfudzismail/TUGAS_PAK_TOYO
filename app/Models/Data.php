<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Data extends Model
{
    use SoftDeletes;
    protected $table = 'datas';
    protected $dates = ['deleted_at'];
    

    public function history(){
        return $this->belongsTo(
            'App\Models\History'
        );
    }

    public function users(){
        return $this->belongsToMany(
            'App\Models\User'
        );
    }
}
