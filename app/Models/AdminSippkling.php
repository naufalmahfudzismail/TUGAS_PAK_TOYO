<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminSippkling extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    // 

    /**
     * get data admin with role spesification
     * 0 = super admin
     * 1 = OPD
     * 2 = UPT
     * 3 = UPF
     */
    public function scopeAdmin($query)
    {
        return $query->select(
            'nama',
            'email',
            'password',
            'role'
        );
    }

    public function scopeAdminByRole($query, $role)
    {
        return $query->select(
            'nama',
            'email',
            'password',
            'role'
        )->where('role', $role);
    }

    public function scopeAdminByMultipleRole($query, $role)
    {
        return $query->select(
            'nama',
            'email',
            'password',
            'role'
        )->whereIn('role', $role);
    }
}
