<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class Kecamatan extends Model
{
    use SoftDeletes, Dataviewer;
    protected $dates = ['deleted_at'];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nama', 'alamat', 'nama_camat', 'koordinat', 'no_telp', 'data_id', 'created_by'
    ];

    public function scopeGetNameKecamatan($query)
    {
        return $query->select('nama');
    }

    // defining relationship
    // has
    public function kelurahans(){
        return $this->hasMany('App\Models\Kelurahan', 'kecamatan_id');
    }

    // belongs
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function petugas()
    {
        return $this->belongsTo('App\Models\PetugasSippkling');
    }
}
