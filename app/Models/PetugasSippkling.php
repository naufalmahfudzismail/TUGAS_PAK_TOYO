<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Kelurahan;
use Illuminate\Support\Facades\Auth;
use App\Support\Dataviewer;
use App\Scopes\KaderFilterScope;

class PetugasSippkling extends Model
{

    use SoftDeletes, Dataviewer;

    protected $dates = ['deleted_at'];
    protected $guarded = array();
    

    // global scope
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new KaderFilterScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'username', 'password', 'kecamatan_id', 'kelurahan_id', 'create_by', 'role', 'foto'
    ];

    protected $hidden = [
        'remember_token'
    ];

    public function scopePetugas($query)
    {
        return $query->select(
            'nama',
            'username',
            'password',
            'kecamatan',
            'password'
        );
    }

    public function kecamatan()
    {
        return $this->hasOne(
            'App\Models\Kecamatan',
            'id',
            'kecamatan_id'
        );
    }

    public function kelurahan()
    {
        return $this->hasOne(
            'App\Models\Kelurahan',
            'id',
            'kelurahan_id'
        );
    }

    // belongsto

    public function history()
    {
        return $this->belongsTo(
            'App\Models\History'
        );
    }
}
