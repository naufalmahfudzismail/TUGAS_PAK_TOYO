<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Support\Dataviewer;

class KartuKeluarga extends Model
{
    use SoftDeletes, Dataviewer;
    protected $dates = ['deleted_at'];
    protected $guarded = array();

    public function petugas(){
        return $this->hasOne('App\Models\PetugasSippkling', 'id', 'petugas_id');
    }
    
}
