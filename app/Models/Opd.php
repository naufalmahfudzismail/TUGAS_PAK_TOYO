<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    use SoftDeletes;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nama','data_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function datas(){
        return $this->hasMany('App\Models\Data', 'data_id');
    }

    public function users(){
        return $this->hasMany('App\Models\User', 'user_id');
    }
}
