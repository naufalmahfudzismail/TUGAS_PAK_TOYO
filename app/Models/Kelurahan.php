<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelurahan extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nama', 'alamat', 'nama_lurah', 'koordinat', 'no_telp', 'kecamatan_id', 'data_id'
    ];

    //defining relationship
    public function kecamatan()
    {
        return $this->belongsTo('App\Models\Kecamatan');
    }

    // belongsto
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
