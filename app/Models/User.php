<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable implements JWTSubject
{
    
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'users';

    protected $guarded = ['id'];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nama', 'username', 'password', 'nip', 'alamat', 'no_telp', 'role', 'created_by','kecamatan_id', 'kelurahan_id','update_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function rules($update = false, $id = null){
        $rules = [
            'nama' => 'required|string|max:35',
            'username' => [
                'required', Rule::unique('users')->ignore($id)
            ],
            'username' => 'max:50',
            'password' => 'required|string|min:6',
            'role' => 'required',
            'alamat' => 'nullable|max:150',
            'no_telp' => 'nullable|max:15',
            'created_by' => 'required'
        ];

        if($update){
            return $rules;
        }

        return array_merge($rules, [
            'username'         => 'required|unique:users,username',
        ]);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    // defining relationship
    public function kecamatan(){
        return $this->hasOne(
            'App\Models\Kecamatan', 'id', 'kecamatan_id'
        );
    }

    public function kelurahan(){
        return $this->hasOne(
            'App\Models\Kelurahan', 'id', 'kelurahan_id'
        );
    }

    public function datas(){
        return $this->belongsToMany(
            'App\Models\Data'
        );
    }

    public function scopeHideSuperAdmin($query){
        return $query->where('role', '!=', '1');
    }
}
