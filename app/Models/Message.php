<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    

    protected $fillable = ['subject', 'message', 'private', 'sender_id', 'receiver_id', 'completed'];
}
