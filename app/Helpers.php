<?php
/**
 * @return mixed
 * Custom function SIPP KLING made by zerocryptor
 */

if (! function_exists('changeStatusUser')) {
    function changeStatusUser($param){
        switch ($param) {
            case 1:
                return "Super Admin";
                break;
            
            case 2:
                return "Admin OPD";
                break;
            
            case 3:
                return "Admin UPT";
                break;
        
            case 4:
                return "Admin UPF";
                break;
            
            default:
                return "this role doesn't has a value";
                break;
        }
    }
}

if (! function_exists('changeIdToUsername')) {
    function changeIdToUsername($param){
        return \App\Models\User::where('id', $param)->select('username')->get()[0]->username;
    }
}

if (! function_exists('changeTypeMessages')) {
    function changeTypeMessages($param){
        $param ? 'Private' : 'Broadcast';
    }
}

if (! function_exists('getValueFromQuery')) {
    function getValueFromQuery($query, $key){
        $final = [];
        for ($i=0; $i < count($query); $i++) { 
            $final[$i] = $query[$i]->{$key};
        }
        return $final;
    }
}

if (! function_exists('getKecamatanNameById')) {
    function getKecamatanNameById($param){
        if($param != null){
            return \App\Models\Kecamatan::select('nama')->where('id', $param)->first()->nama;
        } else {
            return "";
        }
    }
}

if (! function_exists('getKelurahanNameById')) {
    function getKelurahanNameById($param){
        if($param != null){
            return \App\Models\Kelurahan::select('nama')->where('id', $param)->first()->nama;
        } else {
            return "";
        }
    }
}

if (! function_exists('optimizeUsernameWord')) {
    function optimizeUsernameWord($param, $char){
        return strtolower(str_replace(' ', $char, $param));
    }
}

if (! function_exists('getIdKelurahanByName')) {
    function getIdKelurahanByName($param){
        if($param != null){
            return \App\Models\Kelurahan::select('id')->where('nama', $param)->first()->id;
        } else {
            return null;
        }
    }
}

if (! function_exists('getIdKecamatanByName')) {
    function getIdKecamatanByName($param){
        if($param != null){
            return \App\Models\Kecamatan::select('id')->where('nama', $param)->first()->id;
        } else {
            return null;
        }
    }
}

if (! function_exists('getStatusImb')) {
    function getStatusImb($param){
        $splitParam = explode(", ", $param);
        
        return [
            'imb' => $splitParam[0],
            'nip' => $splitParam[1],
            'pajak' => $splitParam[2]
        ];
    }
}

if (! function_exists('changeKategoriSab')) {
    function changeKategoriSab($param){
        switch ($param) {
            case 'A':
                return "Pompa";
                break;
            
            case 'B':
                return "Sumur Gali";
                break;
            
            case 'C':
                return "Sumur Gali Plus";
                break;
            
            case 'D':
                return "Perpipaan PDAM";
                break;
            
            case 'E':
                return "Mata Air";
                break;

            default:
                return "-";
                break;
        }
    }
}

if (! function_exists('changeStatusSab')) {
    function changeStatusSab($param){
        switch ($param) {
            case 'AT':
                return "Amat Tinggi (AT)";
                break;
            
            case 'T':
                return "Tinggi (T)";
                break;
            
            case 'S':
                return "Sedang (S)";
                break;
            
            case 'R':
                return "Rendah (R)";
                break;

            default:
                return "-";
                break;
        }
    }
}

if (! function_exists('getNamePetugasSippklingById')) {
    function getNamePetugasSippklingById($param){
        if($param != null){
            return \App\Models\PetugasSippkling::select('nama')->where('id', $param)->first()->nama;
        } else {
            return null;
        }
    }
}

if (! function_exists('getNameUserById')) {
    function getNameUserById($param){
        if($param != null){
            return \App\Models\User::select('nama')->where('id', $param)->first()->nama;
        } else {
            return null;
        }
    }
}

if (! function_exists('getNomorPajak')) {
    function getNomorPajak($param){
        
        $nomor = explode(", ", $param)[1];

        if($nomor == "" || $nomor == null || $nomor == 'null' || $nomor == '0' || $nomor == '000000000000000000' || $nomor == 'o'){
            
            return "-";
        
        } else {
        
            return $nomor;
        
        }

    }
}