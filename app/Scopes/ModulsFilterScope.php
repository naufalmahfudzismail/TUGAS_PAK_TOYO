<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class ModulsFilterScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        try {
            
            $role = Auth::user();
            if($role != null){
                switch($role->role){
                    case 1:
                    break;
                    case 2:
                    break;
                    case 3:
                        // $builder
                        // ->leftJoin('petugas_sippklings', 'rumah_sehats.petugas_id', 'petugas_sippklings.id')
                        // ->where('petugas_sippklings.kecamatan_id', Auth::user()->kecamatan_id);          
                        $builder->whereHas('petugas', function($query){
                            $query->where('kecamatan_id', Auth::user()->kecamatan_id);
                        });    
                    break;
                    case 4:
                        // $builder
                        // ->leftJoin('petugas_sippklings', 'rumah_sehats.petugas_id', 'petugas_sippklings.id')
                        // ->where('petugas_sippklings.kecamatan_id', Auth::user()->kecamatan_id)      
                        // ->where('petugas_sippklings.kelurahan_id', Auth::user()->kelurahan_id);
                        $builder->whereHas('petugas', function($query){
                            $query->where('kecamatan_id', Auth::user()->kecamatan_id)
                                ->where('kelurahan_id', Auth::user()->kelurahan_id);
                        });          
                    break;
                    default:
                }
            }

        } catch (\Throwable $th) {

            throw new \Exception("Disable penggunaan Scope pada model yang terkait, atau gunakan pengecualian (withoutGlobalScope) pada Eloquent");
        
        }
    }
}
