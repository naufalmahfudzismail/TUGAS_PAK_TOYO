<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class KaderFilterScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        try {
            $role = "";
            $role = Auth::user();
            if ($role != null) {
                switch ($role->role) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        $builder
                            ->orderBy('id', 'ASC')
                            ->where('kecamatan_id', Auth::user()->kecamatan_id);
                        break;
                    case 4:
                        $builder->orderBy('id', 'ASC')
                            ->where('kecamatan_id', Auth::user()->kecamatan_id)
                            ->where('kelurahan_id', Auth::user()->kelurahan_id);
                        break;
                    default:
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
