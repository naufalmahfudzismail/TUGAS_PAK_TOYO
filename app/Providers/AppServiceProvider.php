<?php

namespace App\Providers;

// use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    /**
     * Personal access client created successfully.
     * Client ID: 1
     * Client secret: r57N3LZmQvdKs55O5KUpNvaQQy7yPaX29fbhAGIr
     * Password grant client created successfully.
     * Client ID: 2
     * Client secret: I6MNBkRndnBg8HuRTPwmUY6smF9LrYRVOujftDa6
     */ 
    public function boot()
    {
        Schema::defaultStringLength('191');
        Resource::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        // $this->app->singleton(Client::class, function ($app) {
        //     $headers = [
        //         'Content-Type' => 'application/json',
        //         'Accept'       => 'application/json',
        //     ];

        //     if (session()->has('auth') && isset(session('auth')->access_token)) {
        //         $headers['Authorization'] = 'Bearer ' . session('auth')->access_token;
        //     }

        //     return new Client([
        //         'base_uri' => config('services.api.url'),
        //         'headers'  => $headers,
        //         'auth'     => [
        //             config('services.api.username'),
        //             config('services.api.password'),
        //         ],
        //     ]);
        // });
    }
}
