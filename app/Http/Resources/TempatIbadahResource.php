<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TempatIbadahResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Layak";
        } else if ($this->status == false){
            $this->status = 'Tidak Layak';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'nama_tempat' => $this->nama_tempat,
            'nama_pengurus' => $this->nama_pengurus,
            'jumlah_jamaah' => $this->jumlah_jamaah,
            'jam_operasional' => $this->jam_operasional,
            'no_telp' => $this->no_telp,
            'koordinat' => $this->koordinat,
            'waktu' => $this->waktu,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'skor' => $this->total_nilai,
            'status' => $this->status,
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
