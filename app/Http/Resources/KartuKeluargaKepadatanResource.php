<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KartuKeluargaKepadatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $finalData = [];

        $kecamatanDatas = [
            'id' => $this->id,
            'nama' => $this->nama,
        ];

        $dataModules = $this->getDataByKecamatan($this->id);

        $kelurahan = [
            'kelurahan' => $this->getKecamatanChildExtend($this->id)
        ];

        $data = [$kecamatanDatas, $dataModules, $kelurahan];

        $finalData = array_merge($finalData, $data[0]);
        $finalData = array_merge($finalData, $data[1]);
        $finalData = array_merge($finalData, $data[2]);

        return $finalData;
    }

    private function getDataByKecamatan($param){
        $data = [
            'luas_wilayah' => 0,
            'jumlah_penduduk' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->sum('jumlah_anggota'),
            'jumlah_keluarga' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'jumlah_laki_laki' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->sum('jumlah_laki_laki'),
            'jumlah_perempuan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->sum('jumlah_perempuan')
        ];

        return $data;
    }

    private function getDataByKelurahan($param){
        $data = [
            'luas_wilayah' => 0,
            'jumlah_penduduk' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->sum('jumlah_anggota'),
            'jumlah_keluarga' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'jumlah_laki_laki' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->sum('jumlah_laki_laki'),
            'jumlah_perempuan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->sum('jumlah_perempuan')
        ];

        return $data;
    }

    private function getKecamatanChildExtend($id_kecamatan){
        $finalData = [];

        $data_kelurahan = \App\Models\Kelurahan::select('id','nama')->where('kecamatan_id', $id_kecamatan)->get()->toArray();
        $dataModules = [
            'rumah_sehat' => 0
        ];

        for ($i=0; $i < count($data_kelurahan); $i++) { 
            $data_kelurahan[$i] = array_merge($data_kelurahan[$i], $this->getDataByKelurahan($data_kelurahan[$i]['id']));
        }

        return $data_kelurahan;
    }
}
