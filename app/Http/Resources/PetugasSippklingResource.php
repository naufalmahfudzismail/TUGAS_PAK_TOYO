<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PetugasSippklingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Petugas Sippkling (Kader)',
            'id' => (string) $this->id,
            'attributes' => [
                'nama' => $this->nama,
                'foto' => $this->foto !== null ? $this->foto : 'empty.jpg',
                'username' => $this->username,
                'kecamatan' => $this->kecamatan['id'],
                'kelurahan' => $this->kelurahan['id'],
                'kecamatan_name' => $this->kecamatan['nama'],
                'kelurahan_name' => $this->kelurahan['nama']
            ]
        ];
    }
}
