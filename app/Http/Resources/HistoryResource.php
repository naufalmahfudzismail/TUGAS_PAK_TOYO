<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
            'petugas_username' => $this->petugas->username,
            'deleted_by' => $this->deleted_by,
            'aktifitas' => $this->aktifitas,
            'tipe_petugas' => $this->tipe_petugas,
            'key_histories' => $this->key_histories,
            'waktu_input' => $this->created_at->toDateTimeString(),
            'data' => $this->datas->nama_data,
            'update_by' => getNamePetugasSippklingById($this->update_by) == null ? $this->petugas->nama : $this->update_by,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'waktu_history' => $this->waktu_history,
            'objek_id' => $this->objek_id,
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id)
        ];
    }
}
