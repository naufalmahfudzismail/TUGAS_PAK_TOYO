<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OpdAccessModuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data_id' => $this->data_id,
            'user_id' => $this->user_id,
            'user_name' => getNameUserById($this->user_id)
        ];
    }
}
