<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PenyesuaianDataLoginResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Admin Sippkling',
            'id' => $this->id,
            'role' => $this->role,
            'nama' => $this->nama,
            'username' => $this->username,
            'alamat' => $this->alamat !== null ? $this->alamat : null,
            'no_telp' => (int) $this->no_telp !== null ? $this->no_telp : null,
            'nip' => $this->nip !== null ? $this->nip : null,
            'kecamatan_id' => $this->kecamatan['id'],
            'kelurahan_id' => $this->kelurahan['id'],
            'kecamatan' => $this->kecamatan['nama'] !== null ? $this->kecamatan['nama'] : null,
            'kelurahan' => $this->kelurahan['nama'] !== null ? $this->kelurahan['nama'] : null,
            'role' => $this->role,
            'data_access' => $this->role != 2 ? : PenyesuaianDataLoginResource::collection($this->datas),
            'role_by_name' => changeStatusUser($this->role),
            'links' => [
                'self' => route('users.show', ['user' => $this->id])
            ]
        ];
    }
}
