<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \App\Models\Moduls\RumahSehat;
use \App\Models\History;

class ValidasiDataKaderPerluDiPerhatikanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'kelurahan' => getKelurahanNameById($this->kelurahan_id),
            'jumlah_pendataan' => $this->getJumlahPendataan($this->id),
            'jumlah_riwayat' => $this->jumlahRiwayat($this->id),
            'terakhir_pendataan' => $this->terakhirPendataan($this->id),
            'lama_tidak_pendataan' => $this->lamaTidakPendataan($this->id),
            'status' => $this->status($this->id)
        ];
    }

    private function status($id){
        $class = [RumahSehat::class];
        $count = [];

        for ($i=0; $i < count($class); $i++) { 
            $count[$i] = $class[$i]::where('petugas_id', $id)->count();
        }

        $total = array_sum($count);

        if($total <= 30 && $total >= 1){

            return "Sedikit Aktifitas";

        } else if ($total == 0){

            return "Tidak Ada Aktifitas";

        }
    }

    private function lamaTidakPendataan($id){
        $latestTime = RumahSehat::where('petugas_id', $id)->latest('waktu')->select('waktu')->first();
        $now = \Carbon\Carbon::now();

        if($latestTime != null){
            $date = $latestTime->waktu;
        } else {
            $date = 0;
        }

        return $now->diffInDays($date);
    }
    
    private function terakhirPendataan($id){
        $data = RumahSehat::where('petugas_id', $id)->latest('waktu')->select('waktu')->first();

        if($data != null){
            return $data->waktu;
        } else {
            return 0;
        }
    }

    private function jumlahRiwayat($id){
        $class = [History::class];
        $count = [];

        for ($i=0; $i < count($class); $i++) { 
            $count[$i] = $class[$i]::where('petugas_id', $id)->count();
        }

        return array_sum($count);
    }

    private function getJumlahPendataan($id){
        $class = [RumahSehat::class];
        $count = [];

        for ($i=0; $i < count($class); $i++) { 
            $count[$i] = $class[$i]::where('petugas_id', $id)->count();
        }

        return array_sum($count);
    }
}
