<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenyesuaianDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'data_id' => $this->data_id,
            'user_id' => $this->user_id,
            'nama_data' => \App\Models\Data::select('nama_data')->where('id', $this->data_id)->first()->nama_data,
            'nama_user' => \App\Models\User::select('nama')->where('id', $this->user_id)->first()->nama
        ];
    }
}
