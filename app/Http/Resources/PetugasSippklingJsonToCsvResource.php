<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PetugasSippklingJsonToCsvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nama' => $this->nama,
            'username' => $this->username,
            'kecamatan' => getKecamatanNameById($this->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->kelurahan_id)
        ];
    }
}
