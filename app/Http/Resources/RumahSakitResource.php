<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RumahSakitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Sehat";
        } else if ($this->status == false){
            $this->status = 'Tidak Sehat';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'jumlah_tempat_tidur' => $this->jumlah_tempat_tidur,
            'koordinat' => $this->koordinat,
            'kelas_rs' => $this->kelas_rs,
            'nama_tempat' => $this->nama_tempat,
            'no_telp' => $this->no_telp,
            'penyakit_terbanyak' => $this->penyakit_terbanyak,
            'rata_rata_rawat_inap' => $this->rata_rata_rawat_inap,
            'jam_operasional' => $this->jam_operasional,
            'status' => $this->status,
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'waktu' => $this->waktu,
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
