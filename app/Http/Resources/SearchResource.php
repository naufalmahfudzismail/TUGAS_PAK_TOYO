<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // if ($this->status == true) {
        //     $this->status = "Sehat";
        // } else if ($this->status == false) {
        //     $this->status = 'Tidak Sehat';
        // } else {
        //     $this->status = "Belum diinspeksi";
        // }

        return [
            'id' => $this->id,
            'nama_kk' => $this->nama_kk,
            'nama_pengurus' => $this->nama_pengurus,
            'nama_pkl' => $this->nama_pkl,
            'nama_pengelola' => $this->nama_pengelola,
            'nama_tempat' => $this->nama_tempat,
            'nama_depot' => $this->nama_depot,
            'nama_pemimpin' => $this->nama_pemimpin,
            'nama_pemilik' => $this->nama_pemilik,
            'nama_pengusaha' => $this->nama_pengusaha,
            'nama_kepala_sekolah' => $this->nama_kepala_sekolah,
            'modul' => $this->modul,
            'nmr_kk' => $this->nmr_kk,
            'waktu' => $this->waktu,
            'foto' => $this->foto,
            'rt' => $this->rt,
            'rw' => $this->rw,
            'no_rumah' => $this->no_rumah,
            'status' => $this->status,
            'alamat' => $this->alamat,
            'modul' => $this->modul,
            'kecamatan' => getKecamatanNameById($this->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->kelurahan_id),
        ];
    }
}
