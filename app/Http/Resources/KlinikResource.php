<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KlinikResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Sehat";
        } else if ($this->status == false){
            $this->status = 'Tidak Sehat';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'nama_tempat' => $this->nama_tempat,
            'nama_pimpinan' => $this->nama_pimpinan,
            'gambaran_umum' => $this->gambaran_umum,
            'koordinat' => $this->koordinat,
            'no_telp' => $this->no_telp,
            'jam_operasional' => $this->jam_operasional,
            'status' => $this->status,
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'waktu' => $this->waktu,
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
