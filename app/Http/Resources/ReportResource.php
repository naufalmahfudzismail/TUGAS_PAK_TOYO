<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'petugas_username' => getNamePetugasSippklingById($this->petugas_id),
            'judul' => $this->judul,
            'nama_pengirim' => $this->nama_pengirim,
            'created_at' => $this->created_at,
            'merk_hp' => $this->merk_hp,
            'model_hp' => $this->model_hp,
            'versi_android' => $this->versi_android,
            'foto' => $this->foto
        ];
    }
}
