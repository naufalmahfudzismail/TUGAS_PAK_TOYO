<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DB;
use \App\Models\Moduls\RumahSehat;

class ValidasiDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'kelurahan' => getKelurahanNameById($this->kelurahan_id),
            'abnormal_time_input' => $this->abnormalTimeInput($this->id),
            'empty_sab' => $this->noSab($this->id),
            'empty_kk' => $this->emptyKk($this->id),
            'abnormal_jml_anggota' => $this->abnormalJmlAnggota($this->id),
            'total' => $this->total
        ];
    }

    private function abnormalJmlAnggota($id){
        $zeroCount = \App\Models\KartuKeluarga::where('petugas_id', $id)
        ->where('jumlah_anggota', '=', 0);

        $data = \App\Models\KartuKeluarga::where('petugas_id', $id)
        ->where('jumlah_anggota', '>=', 20)
        ->union($zeroCount)
        ->count();
        
        return $data;
    }

    private function emptyKk($id){
        $namaKK = \App\Models\KartuKeluarga::where('petugas_id', $id)
        ->whereIn('nama_kk', [0, null]);
        
        $data = \App\Models\KartuKeluarga::where('petugas_id', $id)
        ->whereIn('nmr_kk', [0, null, '0000000000000000', 'o'])
        ->union($namaKK)
        ->count();

        return $data;
    }

    private function noSab($id){
        $data = RumahSehat::leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
        ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
        ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
        ->where('rumah_sehats.petugas_id', $id)
        ->count();

        return $data;
    }

    private function abnormalTimeInput($id){
        $class = [RumahSehat::class];
        $collect_count = [];
        
        for ($i=0; $i < count($class); $i++) {
            $collect_count[$i] = $class[$i]::where('petugas_id', $id)
            ->whereNotBetween(DB::raw('extract(hour from created_at)'), array(4, 20))                  
            ->count();
        }

        return array_sum($collect_count);
    }
}
