<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SekolahResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Sehat";
        } else if ($this->status == false){
            $this->status = 'Tidak Sehat';
        } else {
            $this->status = "Belum Di jamah";
        }
        
        return [
            'id' => $this->id,
            'waktu' => $this->waktu,
            'koordinat' => $this->koordinat,
            'nama_tempat' => $this->nama_tempat,
            'nama_kepala_sekolah' => $this->nama_kepala_sekolah,
            'jumlah_murid_pertahun' => $this->jumlah_murid_pertahun,
            'jumlah_guru_pns' => $this->jumlah_guru_pns,
            'jumlah_guru_tetap_non_pns' => $this->jumlah_guru_tetap_non_pns,
            'jumlah_guru_honorer' => $this->jumlah_guru_honorer,
            'jam_operasional' => $this->jam_operasional,
            'luas_bangunan' => $this->luas_bangunan,
            'luas_tanah' => $this->luas_tanah,
            'tahun_berdiri' => $this->tahun_berdiri,
            'no_telp' => $this->no_telp,
            'puskesmas' => $this->puskesmas,
            'no_izin_operasional' => $this->no_izin_operasional,
            'status' => $this->status,
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
