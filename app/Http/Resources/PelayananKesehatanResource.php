<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PelayananKesehatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Dalam Gedung";
        } else if ($this->status == false){
            $this->status = 'Luar Gedung';
        } else {
            $this->status = "Belum Di jamah";
        }
        
        return [
            'id' => $this->id,
            'jenis' => $this->jenis,
            'jenis_kelamin' => $this->jenis_kelamin,
            'kasus' => $this->kasus,
            'nama_pkl' => $this->nama_pkl,
            'nama_tempat' => $this->nama_tempat,
            'umur' => $this->umur,
            'alamat' => $this->alamat,
            'koordinat' => $this->koordinat,
            'waktu' => $this->waktu,
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
        ];
    }
}
