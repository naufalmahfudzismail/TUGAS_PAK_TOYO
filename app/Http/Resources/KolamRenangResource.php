<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KolamRenangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Layak";
        } else if ($this->status == false){
            $this->status = 'Tidak Layak';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'jumlah_karyawan' => $this->jumlah_karyawan,
            'koordinat' => $this->koordinat,
            'nama_pengelola' => $this->nama_pengelola,
            'nama_tempat' => $this->nama_tempat,
            'no_izin_usaha' => $this->no_izin_usaha,
            'no_telp' => $this->no_telp,
            'jam_operasional' => $this->jam_operasional,
            'status' => $this->status,
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
