<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PesantrenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Sehat";
        } else if ($this->status == false){
            $this->status = 'Tidak Sehat';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'jumlah_santri' => $this->jumlah_santri,
            'koordinat' => $this->koordinat,
            'nama_pengelola' => $this->nama_pengelola,
            'nama_tempat' => $this->nama_tempat,
            'no_telp' => $this->no_telp,
            'jam_operasional' => $this->jam_operasional,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'jumlah_laki_laki' => $this->jumlah_laki_laki,            
            'jumlah_perempuan' => $this->jumlah_perempuan,            
            'waktu' => $this->waktu,            
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'status' => $this->status,            
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
