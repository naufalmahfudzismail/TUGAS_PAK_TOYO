<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DashboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Dashboard',
            'informations' => [
                'area' => '143.000 km²',
                'total_population' => '$',
                'families' => [
                    'has' => '$',
                    'total' => '$'
                ],
                'healthy' => [
                    'has' => '$',
                    'total' => '$'
                ],
                'tax' => [
                    'has' => '$',
                    'total' => '$'
                ],
                'imb' => [
                    'has' => '$', 
                    'total' => '$'
                ]
            ],
            'moduls' => [
                'rumah_sehat' => [  
                    'sehat' => '$',
                    'tidak_sehat' => '$'
                ],
                'pjb' => [
                    'tidak_ada_jentik' => '$',
                    'ada_jentik' => '$'
                ],
                'spal' => [
                    'terbuka' => '$',
                    'tertutup' => '$'
                ],
                'sab' => [
                    'sehat' => '$',
                    'tidak_sehat' => '$'
                ],
                'jamban' => [
                    'koya' => '$',
                    'kali' => '$',
                    'helikopter' => '$',
                    'septik_tank' => '$'
                ],
                'sampah' => [
                    'dipilah' => '$',
                    'dibuang' => '$'
                ]
            ],
            'history' => [

            ],
            'ranking' => [
                
            ]
        ];
    }
}
