<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PasarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->status == true){
            $this->status = "Sehat";
        } else if ($this->status == false){
            $this->status = 'Tidak Sehat';
        } else {
            $this->status = "Belum Di jamah";
        }

        return [
            'id' => $this->id,
            'jumlah_asosiasi' => $this->jumlah_asosiasi,
            'jumlah_kios' => $this->jumlah_kios,
            'jumlah_pedagang' => $this->jumlah_pedagang,
            'koordinat' => $this->koordinat,
            'nama_pengelola' => $this->nama_pengelola,
            'nama_tempat' => $this->nama_tempat,
            'no_telp' => $this->no_telp,
            'jam_operasional' => $this->jam_operasional,
            'jumlah_kios_kosong' => $this->jumlah_kios_kosong,
            'jumlah_kios_terisi' => $this->jumlah_kios_terisi,
            'waktu' => $this->waktu,
            'status' => $this->status,
            'skor' => $this->total_nilai,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => \getStatusImb($this->pajak)['nip'] == null ? '-' : \getStatusImb($this->pajak)['nip'],
            'alamat' => $this->alamat,
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}
