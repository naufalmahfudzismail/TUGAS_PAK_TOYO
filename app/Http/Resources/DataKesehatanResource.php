<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Support\Modules\GeneralInformation;

class DataKesehatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $finalData = [];

        $kecamatanDatas = [
            'id' => $this->id,
            'nama' => $this->nama,
        ];

        $dataModules = $this->getDataByKecamatan($this->id);
        $kelurahan = [
            'kelurahan' => $this->getKecamatanChildExtend($this->id)
        ];
        
        $data = [$kecamatanDatas, $dataModules, $kelurahan];

        $finalData = array_merge($finalData, $data[0]);
        $finalData = array_merge($finalData, $data[1]);
        $finalData = array_merge($finalData, $data[2]);

        return $finalData;
    }

    private function getDataByKecamatan($param){
        $data = [
            'rumah_sehat' => \App\Models\Moduls\RumahSehat::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'tempat_ibadah' => \App\Models\Moduls\TempatIbadah::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'pasar' => \App\Models\Moduls\Pasar::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'sekolah' => \App\Models\Moduls\Sekolah::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'pesantren' => \App\Models\Moduls\Pesantren::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'kolam_renang' => \App\Models\Moduls\KolamRenang::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'puskesmas' => \App\Models\Moduls\Puskesmas::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'rumah_sakit' => \App\Models\Moduls\RumahSakit::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'klinik' => \App\Models\Moduls\Klinik::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'hotel' => \App\Models\Moduls\Hotel::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'hotel_melati' => \App\Models\Moduls\HotelMelati::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'salon' => \App\Models\Moduls\Salon::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'jasa_boga' => \App\Models\Moduls\JasaBoga::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'kuliner' => \App\Models\Moduls\Kuliner::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'dam' => \App\Models\Moduls\DamSippKling::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'pelayanan_kesling' => \App\Models\Moduls\PelayananKesling::whereHas('petugas', function($query) use ($param){
                $query->where('kecamatan_id', $param);
            })->count(),
            'phbs' => 0
        ];

        return $data;
    }

    private function getDataByKelurahan($param){
        $data = [
            'rumah_sehat' => \App\Models\Moduls\RumahSehat::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'tempat_ibadah' => \App\Models\Moduls\TempatIbadah::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'pasar' => \App\Models\Moduls\Pasar::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'sekolah' => \App\Models\Moduls\Sekolah::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'pesantren' => \App\Models\Moduls\Pesantren::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'kolam_renang' => \App\Models\Moduls\KolamRenang::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'puskesmas' => \App\Models\Moduls\Puskesmas::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'rumah_sakit' => \App\Models\Moduls\RumahSakit::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'klinik' => \App\Models\Moduls\Klinik::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'hotel' => \App\Models\Moduls\Hotel::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'hotel_melati' => \App\Models\Moduls\HotelMelati::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'salon' => \App\Models\Moduls\Salon::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'jasa_boga' => \App\Models\Moduls\JasaBoga::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'kuliner' => \App\Models\Moduls\Kuliner::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'dam' => \App\Models\Moduls\DamSippKling::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'pelayanan_kesling' => \App\Models\Moduls\PelayananKesling::whereHas('petugas', function($query) use ($param){
                $query->where('kelurahan_id', $param);
            })->count(),
            'phbs' => 0
        ];

        return $data;
    }

    private function getKecamatanChildExtend($id_kecamatan){
        $finalData = [];

        $data_kelurahan = \App\Models\Kelurahan::select('id','nama')->where('kecamatan_id', $id_kecamatan)->get()->toArray();
        $dataModules = [
            'rumah_sehat' => 0
        ];

        for ($i=0; $i < count($data_kelurahan); $i++) { 
            $data_kelurahan[$i] = array_merge($data_kelurahan[$i], $this->getDataByKelurahan($data_kelurahan[$i]['id']));
        }

        return $data_kelurahan;
    }
}
