<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KartuKeluargaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->petugas == null){
            $data_kelurahan = "-";
            $nama_petugas = "-";
            $username_petugas = "-";
        } else {
            $data_kelurahan = getKelurahanNameById($this->petugas->kelurahan_id);
            $nama_petugas = $this->petugas->nama;
            $username_petugas = $this->petugas->username;
        }

        return [
            'id' => $this->id,
            'nama_kk' => $this->nama_kk,
            'nmr_kk' => $this->nmr_kk,
            'jumlah_laki_laki' => $this->jumlah_laki_laki,
            'jumlah_perempuan' => $this->jumlah_perempuan,
            'jumlah_anggota' => $this->jumlah_anggota,
            'kelurahan' => $data_kelurahan,
            'petugas' => $nama_petugas,
            'username_petugas' => $username_petugas
        ];
    }
}
