<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'Admin Sippkling',
            'id' => $this->id,
            'role' => $this->role,
            'nama' => $this->nama,
            'username' => $this->username,
            'alamat' => $this->alamat !== null ? $this->alamat : null,
            'no_telp' => (int) $this->no_telp !== null ? $this->no_telp : null,
            'nip' => $this->nip !== null ? $this->nip : null,
            'kecamatan' => $this->kecamatan['id'],
            'kelurahan' => $this->kelurahan['id'],
            'kecamatan_name' => $this->kecamatan['nama'] !== null ? $this->kecamatan['nama'] : null,
            'kelurahan_name' => $this->kelurahan['nama'] !== null ? $this->kelurahan['nama'] : null,
            'role' => $this->role,
            'opd' => $this->opd['id'],
            'opd_name' => $this->opd['nama'],
            'role_by_name' => changeStatusUser($this->role),
            'self' => route('users.show', ['user' => $this->id]),
            'deleted_by' => $this->deleted_by
        ];
    }
}
