<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RumahSehatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $getSab = \App\Models\Moduls\Sab::where('rumah_sehat_id', $this->id)->first();

        if ($getSab == null) {

            $status_sab = null;
            $jenis_sab = null;
            $pemilik_sarana = null;
            $total_nilai_sab = 0;

        } else {
            $jenis_sab = changeKategoriSab($getSab->kategori);

            if ($getSab->total_nilai == null) {
                $total_nilai_sab = 0;
            } else {
                $total_nilai_sab = $getSab->total_nilai;
            }

            if ($jenis_sab == 'Mata Air' || $jenis_sab == 'Perpipaan PDAM') {
                $status_sab = $jenis_sab;
            } else {
                $status_sab = changeStatusSab($getSab->status);
            }

            $pemilik_sarana = $getSab->pemilik_sarana;


        }

        return [
            'id' => $this->id,
            'nama_kk' => \App\Models\KartuKeluarga::select('nama_kk')->where('rumah_sehat_id', $this->id)->first() == null ? '-' : \App\Models\KartuKeluarga::select('nama_kk')->where('rumah_sehat_id', $this->id)->first()->nama_kk,
            'nmr_kk' => \App\Models\KartuKeluarga::select('nmr_kk')->where('rumah_sehat_id', $this->id)->first() == null ? '-' : \App\Models\KartuKeluarga::select('nmr_kk')->where('rumah_sehat_id', $this->id)->first()->nmr_kk,
            'nop' => "",
            'total_nilai' => $this->total_nilai,
            'total_nilai_sab' => $total_nilai_sab,
            'status' => $this->status ? 'Sehat' : 'Tidak Sehat',
            'pjb' => $this->pjb ? 'Ya' : 'Tidak',
            'sampah' => $this->sampah ? 'Dipilah' : 'Tidak Dipilah',
            'spal' => $this->spal ? 'Tertutup' : 'Terbuka',
            'jamban' => $this->jamban,
            'petugas' => $this->petugas->nama,
            'petugas_id' => $this->petugas->id,
'petugas_username' => $this->petugas->username,
            'imb' => $this->imb == null ? "-" : getNomorPajak($this->imb),
            'status_rumah' => $this->status_rumah ? 'Milik Sendiri' : 'Kontrak',
            'status_sab' => $status_sab,
            'jenis_sab' => $jenis_sab,
            'pemilik_sab' => $pemilik_sarana,
            'alamat' => addslashes($this->alamat),
            'kecamatan' => getKecamatanNameById($this->petugas->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->petugas->kelurahan_id),
            'deleted_by' => $this->deleted_by
        ];
    }
}