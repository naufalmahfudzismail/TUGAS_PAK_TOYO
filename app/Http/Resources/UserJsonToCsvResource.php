<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserJsonToCsvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nama' => $this->nama,
            'username' => $this->username,
            'role' => changeStatusUser($this->role),
            'nip' => $this->nip,
            'alamat' => $this->alamat,
            'no_telp' => $this->no_telp,
            'kecamatan' => getKecamatanNameById($this->kecamatan_id),
            'kelurahan' => getKelurahanNameById($this->kelurahan_id)
        ];
    }
}
