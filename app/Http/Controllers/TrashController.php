<?php

namespace App\Http\Controllers;

use App\Models\Moduls\DamSippKling;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\PelayananKesling;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\Salon;
use App\Models\Moduls\Sekolah;
use App\Models\Moduls\TempatIbadah;
use App\Models\PetugasSippkling;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $rs = RumahSehat::onlyTrashed()->orderBy('id', 'asc')->get();

        $rs = DB::table('rumah_sehats as a')
            ->whereNotNull('a.deleted_at')
            ->leftjoin('kartu_keluargas as b', 'b.rumah_sehat_id', 'a.id')
            ->leftjoin('petugas_sippklings as petugas', 'petugas.id', 'a.petugas_id')
            ->select('a.*', 'b.nama_kk')
            ->orderBy('a.id', 'desc')
            ->get();
        
        $tempatibadah = \App\Http\Resources\TempatIbadahResource::collection(TempatIbadah::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $pasar = \App\Http\Resources\PasarResource::collection(Pasar::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $sekolah = \App\Http\Resources\SekolahResource::collection(Sekolah::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $pesantren = \App\Http\Resources\PesantrenResource::collection(Pesantren::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $kolamrenang = \App\Http\Resources\KolamRenangResource::collection(KolamRenang::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $puskesmas = \App\Http\Resources\PuskesmasResource::collection(Puskesmas::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $rumahsakit = \App\Http\Resources\RumahSakitResource::collection(RumahSakit::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $klinik = \App\Http\Resources\KlinikResource::collection(Klinik::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $hotel = \App\Http\Resources\HotelResource::collection(Hotel::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $hotelmelati = \App\Http\Resources\HotelMelatiResource::collection(HotelMelati::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $salon = \App\Http\Resources\SalonResource::collection(Salon::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $jasaboga = \App\Http\Resources\JasaBogaResource::collection(JasaBoga::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $kuliner = \App\Http\Resources\RestoranResource::collection(Kuliner::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $dam = \App\Http\Resources\DepotAirMinumResource::collection(DamSippKling::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        $kesling = \App\Http\Resources\PelayananKesehatanResource::collection(PelayananKesling::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get());
        // $phbs = Phbs::onlyTrashed()->with('petugas')->orderBy('id', 'desc')->get();
        $admin = \App\Http\Resources\UsersTableResource::collection(User::onlyTrashed()->orderBy('id', 'desc')->get());
        $kader = \App\Http\Resources\PetugasSippklingsTableResource::collection(PetugasSippkling::onlyTrashed()->orderBy('id', 'desc')->get());

        $jml_rs = $rs->count();        
        $jml_tempatibadah = $tempatibadah->count();
        $jml_pasar = $pasar->count();
        $jml_sekolah = $sekolah->count();
        $jml_pesantren = $pesantren->count();
        $jml_kolamrenang= $kolamrenang->count();
        $jml_puskesmas = $puskesmas->count();
        $jml_rumahsakit = $rumahsakit->count();
        $jml_klinik = $klinik->count();
        $jml_hotel = $hotel->count();
        $jml_hotelmelati = $hotelmelati->count();
        $jml_salon = $salon->count();
        $jml_jasaboga = $jasaboga->count();
        $jml_kuliner = $kuliner->count();
        $jml_dam = $dam->count();
        $jml_kesling = $kesling->count();
        // $jml_phbs = $phbs->count();
        $jml_admin = $admin->count();
        $jml_kader = $kader->count();

        return response()->json([
            'rumahsehat' => $rs,
            'tempatibadah' => $tempatibadah,
            'pasar' => $pasar,
            'sekolah' => $sekolah,
            'pesantren' => $pesantren,
            'kolamrenang' => $kolamrenang,
            'puskesmas' => $puskesmas,
            'rumahsakit' => $rumahsakit,
            'klinik' => $klinik,
            'hotel' => $hotel,
            'hotelmelati' => $hotelmelati,
            'salon' => $salon,
            'jasaboga' => $jasaboga,
            'restoran' => $kuliner,
            'dam' => $dam,
            'kesling' => $kesling,
            // 'phbs' => $phbs,
            'admin' => $admin,
            'kader' => $kader,
            'counts' => [$jml_rs ,$jml_tempatibadah
            ,$jml_pasar 
            ,$jml_sekolah 
            ,$jml_pesantren
            ,$jml_kolamrenang
            ,$jml_puskesmas
            ,$jml_rumahsakit
            ,$jml_klinik
            ,$jml_hotel
            ,$jml_hotelmelati 
            ,$jml_salon 
            ,$jml_jasaboga
            ,$jml_kuliner
            ,$jml_dam 
            ,$jml_kesling 
            // ,$jml_phbs
            ,$jml_admin 
            ,$jml_kader 
    ]
        ]);
    }

    public function restoreData($kategori, $id)
    {

        if ($kategori == "Rumah Sehat") {
            $data = RumahSehat::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Tempat Ibadah") {
            $data = TempatIbadah::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Pasar") {
            $data = Pasar::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Sekolah") {
            $data = Sekolah::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Pesantren") {
            $data = Pesantren::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Kolam Renang") {
            $data = KolamRenang::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Puskesmas") {
            $data = Puskesmas::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Rumah Sakit") {
            $data = RumahSakit::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Klinik") {
            $data = Klinik::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Hotel") {
            $data = Hotel::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Hotel Melati") {
            $data = HotelMelati::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Salon") {
            $data = Salon::onlyTrashed()->find($id)->restore();
        } else if ($kategori == "Jasa Boga") {
            $data = JasaBoga::onlyTrashed()->find($id)->restore();

        } else if ($kategori == "Restoran") {
            $data = Kuliner::onlyTrashed()->find($id)->restore();

        } else if ($kategori == "Depot Air Minum") {
            $data = DamSippkling::onlyTrashed()->find($id)->restore();

        } else if ($kategori == "Pelayanan Kesehatan Lingkungan") {
            $data = PelayananKesling::onlyTrashed()->find($id)->restore();

        }
        // else if($kategori == "PHBS"){
        //     $data = PHBS::onlyTrashed()->find($id)->restore();

        // }
        else if ($kategori == "Admin") {
            $data = User::onlyTrashed()->find($id)->restore();

        } else if ($kategori == "Kader") {
            $data = PetugasSippkling::onlyTrashed()->find($id)->restore();
        }

        if ($data) {
            return [
                'status' => 200,
            ];
        } else {
            return [
                'status' => 201,
            ];
        }

    }

    public function hapusData($kategori, $id)
    {

        if ($kategori == "Rumah Sehat") {
            $data = RumahSehat::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Tempat Ibadah") {
            $data = TempatIbadah::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Pasar") {
            $data = Pasar::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Sekolah") {
            $data = Sekolah::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Pesantren") {
            $data = Pesantren::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Kolam Renang") {
            $data = KolamRenang::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Puskesmas") {
            $data = Puskesmas::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Rumah Sakit") {
            $data = RumahSakit::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Klinik") {
            $data = Klinik::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Hotel") {
            $data = Hotel::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Hotel Melati") {
            $data = HotelMelati::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Salon") {
            $data = Salon::onlyTrashed()->find($id)->forceDelete();
        } else if ($kategori == "Jasa Boga") {
            $data = JasaBoga::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Restoran") {
            $data = Kuliner::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Depot Air Minum") {
            $data = DamSippkling::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Pelayanan Kesehatan Lingkungan") {
            $data = PelayananKesling::onlyTrashed()->find($id)->forceDelete();

        }
        // else if($kategori == "PHBS"){
        //     $data = PHBS::onlyTrashed()->find($id)->forceDelete();

        // }
        else if ($kategori == "Admin") {
            $data = User::onlyTrashed()->find($id)->forceDelete();

        } else if ($kategori == "Kader") {
            $data = PetugasSippkling::onlyTrashed()->find($id)->forceDelete();
        }

        if ($data) {
            return [
                'status' => 200,
            ];
        } else {
            return [
                'status' => 201,
            ];
        }

    }

    public function kosongkanTrash($kategori)
    {

        if ($kategori == "Rumah Sehat") {
            $data = RumahSehat::onlyTrashed()->forceDelete();
        } else if ($kategori == "Tempat Ibadah") {
            $data = TempatIbadah::onlyTrashed()->forceDelete();
        } else if ($kategori == "Pasar") {
            $data = Pasar::onlyTrashed()->forceDelete();
        } else if ($kategori == "Sekolah") {
            $data = Sekolah::onlyTrashed()->forceDelete();
        } else if ($kategori == "Pesantren") {
            $data = Pesantren::onlyTrashed()->forceDelete();

        } else if ($kategori == "Kolam Renang") {
            $data = KolamRenang::onlyTrashed()->forceDelete();

        } else if ($kategori == "Puskesmas") {
            $data = Puskesmas::onlyTrashed()->forceDelete();

        } else if ($kategori == "Rumah Sakit") {
            $data = RumahSakit::onlyTrashed()->forceDelete();

        } else if ($kategori == "Klinik") {
            $data = Klinik::onlyTrashed()->forceDelete();

        } else if ($kategori == "Hotel") {
            $data = Hotel::onlyTrashed()->forceDelete();
        } else if ($kategori == "Hotel Melati") {
            $data = HotelMelati::onlyTrashed()->forceDelete();
        } else if ($kategori == "Salon") {
            $data = Salon::onlyTrashed()->forceDelete();
        } else if ($kategori == "Jasa Boga") {
            $data = JasaBoga::onlyTrashed()->forceDelete();

        } else if ($kategori == "Restoran") {
            $data = Kuliner::onlyTrashed()->forceDelete();

        } else if ($kategori == "Depot Air Minum") {
            $data = DamSippkling::onlyTrashed()->forceDelete();

        } else if ($kategori == "Pelayanan Kesehatan Lingkungan") {
            $data = PelayananKesling::onlyTrashed()->forceDelete();

        }
        // else if($kategori == "PHBS"){
        //     $data = PHBS::onlyTrashed()->forceDelete();

        // }
        else if ($kategori == "Admin") {
            $data = User::onlyTrashed()->forceDelete();

        } else if ($kategori == "Kader") {
            $data = PetugasSippkling::onlyTrashed()->forceDelete();
        }

        if ($data) {
            return [
                'status' => 200,
            ];
        } else {
            return [
                'status' => 201,
            ];
        }
    }

    public function restoreTrash($kategori)
    {
        if ($kategori == "Rumah Sehat") {
            $data = RumahSehat::onlyTrashed()->restore();
        } else if ($kategori == "Tempat Ibadah") {
            $data = TempatIbadah::onlyTrashed()->restore();
        } else if ($kategori == "Pasar") {
            $data = Pasar::onlyTrashed()->restore();
        } else if ($kategori == "Sekolah") {
            $data = Sekolah::onlyTrashed()->restore();
        } else if ($kategori == "Pesantren") {
            $data = Pesantren::onlyTrashed()->restore();

        } else if ($kategori == "Kolam Renang") {
            $data = KolamRenang::onlyTrashed()->restore();

        } else if ($kategori == "Puskesmas") {
            $data = Puskesmas::onlyTrashed()->restore();

        } else if ($kategori == "Rumah Sakit") {
            $data = RumahSakit::onlyTrashed()->restore();

        } else if ($kategori == "Klinik") {
            $data = Klinik::onlyTrashed()->restore();

        } else if ($kategori == "Hotel") {
            $data = Hotel::onlyTrashed()->restore();
        } else if ($kategori == "Hotel Melati") {
            $data = HotelMelati::onlyTrashed()->restore();
        } else if ($kategori == "Salon") {
            $data = Salon::onlyTrashed()->restore();
        } else if ($kategori == "Jasa Boga") {
            $data = JasaBoga::onlyTrashed()->restore();

        } else if ($kategori == "Restoran") {
            $data = Kuliner::onlyTrashed()->restore();

        } else if ($kategori == "Depot Air Minum") {
            $data = DamSippkling::onlyTrashed()->restore();

        } else if ($kategori == "Pelayanan Kesehatan Lingkungan") {
            $data = PelayananKesling::onlyTrashed()->restore();

        }
        // else if($kategori == "PHBS"){
        //     $data = PHBS::onlyTrashed()->restore();

        // }
        else if ($kategori == "Admin") {
            $data = User::onlyTrashed()->restore();

        } else if ($kategori == "Kader") {
            $data = PetugasSippkling::onlyTrashed()->restore();
        }

        if ($data) {
            return [
                'status' => 200,
            ];
        } else {
            return [
                'status' => 201,
            ];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
}
