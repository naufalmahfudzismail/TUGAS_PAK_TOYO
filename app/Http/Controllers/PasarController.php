<?php

namespace App\Http\Controllers;

use App\Pasar;
use Illuminate\Http\Request;

class PasarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pasar  $pasar
     * @return \Illuminate\Http\Response
     */
    public function show(Pasar $pasar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pasar  $pasar
     * @return \Illuminate\Http\Response
     */
    public function edit(Pasar $pasar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pasar  $pasar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pasar $pasar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pasar  $pasar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pasar $pasar)
    {
        //
    }
}
