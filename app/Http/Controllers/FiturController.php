<?php

namespace App\Http\Controllers;

use App\Models\Fitur;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class FiturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $fitur = Fitur::select('*')->orderBy('id', 'asc')->get();
        return response()->json([
            'fitur' => $fitur,
        ]);
    }
    public function getFitur()
    {
        //
        $fiturs = Fitur::select()->where('param', true)->orderBy('id', 'asc')->get();
        $outputs = Fitur::select()->where('param', false)->orderBy('id', 'asc')->get();
        return response()->json([
            'fiturs' => $fiturs,
            'outputs' => $outputs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $fitur = $this->validate(request(), [
            'gambar' => 'required',
            'namafitur' => 'required',
            'param' => 'required',
        ]);

        Fitur::create($fitur);

        if ($testimoni) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }

    public function addFitur(Request $request){
        $fitur = new Fitur();

        $param = $request->param;        
        $namafitur = $request->namafitur;
        if($request->foto){
            $gambar = $request->foto;
            $imageName = 'fitur_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/fitur/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);

            $fitur->gambar = $lokasiimage;
        }
        $fitur->param = $param;
        $fitur->namafitur = $namafitur;
        $fitur->save();

        if ($fitur) {
            return response()->json([
                "message" => "Berhasil Update achievement",
                "fitur" => $fitur
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Fitur::findOrFail($id);

        return response()->json([
            "data"->$data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $fitur = Fitur::findOrFail($id);
        $this->validate(request, [
            'gambar' => 'required',
            'namafitur' => 'required',
            'param' => 'required',
        ]);

        $fitur->gambar = $request->get('gambar');
        $fitur->namafitur = $request->get('namafitur');
        $fitur->param = $request->get('param');
        $update = $fitur->save();

        if ($update) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    public function updateFitur(Request $request, $id)
    {
        // return $request;

        $namafitur = $request->namafitur;
        $param = $request->param;
        $gambar = $request->foto;

        $fitur = Fitur::find($id);
        if ($request->foto != null) {
            $imageName = 'fitur_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/fitur/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);
            $fitur->gambar = $lokasiimage;
        }

        $fitur->param = $param;
        $fitur->namafitur = $namafitur;
        $fitur->save();

        if ($fitur) {
            return response()->json([
                "message" => "Berhasil Update fitur",
                "isinya" => $fitur,
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFitur($id)
    {
        //
        $fitur = Fitur::findOrFail($id);
        $delete = $fitur->delete();

        if ($delete) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
}
