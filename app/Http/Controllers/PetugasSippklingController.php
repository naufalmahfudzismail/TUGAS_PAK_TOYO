<?php

namespace App\Http\Controllers;

use App\Models\PetugasSippkling;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Resources\PetugasSippklingResource;

class PetugasSippklingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->all();

        return PetugasSippklingResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */
    public function show(PetugasSippkling $petugasSippkling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */
    public function edit(PetugasSippkling $petugasSippkling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PetugasSippkling $petugasSippkling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */
    public function destroy(PetugasSippkling $petugasSippkling)
    {
        //
    }
}
