<?php

namespace App\Http\Controllers;

use App\Models\Achievement;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class AchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $achievement = Achievement::select('*')->orderBy('id', 'asc')->get();

        return response()->json([
            'achievement' => $achievement,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $achievement = $this->validate(request(), [
            'icon' => 'required',
            'year' => 'required',
            'kategori' => 'required',
        ]);

        Achievement::create($achievement);

        if ($achievement) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    public function addAchievement(Request $request)
    {

        $achievement = new Achievement();

        $kategori = $request->kategori;

        if ($request->foto) {

            $gambar = $request->foto;
            $imageName = 'achievement_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/achievement/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);

            $achievement->icon = $lokasiimage;
        }

        $achievement->kategori = $kategori;

        $achievement->save();

        if ($achievement) {
            return response()->json([
                "message" => "Berhasil Update achievement",
                "achievement" => $achievement
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Achievement::findOrFail($id);

        return response()->json([
            "data" => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $achievement = Achievement::findOrFail($id);

        $this->validate(request(), [
            'icon' => 'required',
            'year' => 'required',
            'kategori' => 'required',
        ]);

        $achievement->icon = $request->get('icon');
        $achievement->year = $request->get('year');
        $achievement->kategori = $request->get('kategori');
        $update = $achievement->save();

        if ($update) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    public function updateAchievement(Request $request, $id)
    {
        // return $request;

        $kategori = $request->kategori;
        $achievement = Achievement::find($id);

        $gambar = $request->foto;
        $imageName = 'achievement_' . str_random(5) . '.png';
        $lokasiimage = 'images/landingpage/achievement/' . $imageName;
        Image::make($gambar)->save($lokasiimage, $imageName);

        $achievement->icon = $lokasiimage;
        $achievement->kategori = $kategori;

        $achievement->save();

        if ($achievement) {
            return response()->json([
                "message" => "Berhasil Update achievement",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAchievement($id)
    {
        //
        $achievement = Achievement::findOrFail($id);
        $delete = $achievement->delete();

        if ($delete) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
}
