<?php

namespace App\Http\Controllers;

use App\Sab;
use Illuminate\Http\Request;

class SabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sab  $sab
     * @return \Illuminate\Http\Response
     */
    public function show(Sab $sab)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sab  $sab
     * @return \Illuminate\Http\Response
     */
    public function edit(Sab $sab)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sab  $sab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sab $sab)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sab  $sab
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sab $sab)
    {
        //
    }
}
