<?php

namespace App\Http\Controllers;

use App\JasaBoga;
use Illuminate\Http\Request;

class JasaBogaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JasaBoga  $jasaBoga
     * @return \Illuminate\Http\Response
     */
    public function show(JasaBoga $jasaBoga)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JasaBoga  $jasaBoga
     * @return \Illuminate\Http\Response
     */
    public function edit(JasaBoga $jasaBoga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JasaBoga  $jasaBoga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JasaBoga $jasaBoga)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JasaBoga  $jasaBoga
     * @return \Illuminate\Http\Response
     */
    public function destroy(JasaBoga $jasaBoga)
    {
        //
    }
}
