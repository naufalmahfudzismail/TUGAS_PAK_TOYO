<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\LoginResource;

class AuthController extends Controller
{
    public function validation(Request $request){

        try {
            $this->validate($request, 
                [
                    'username' => 'required',
                    'password'  => 'required'
                ],
                [
                    'username.required' => ':attribute harus di isi',
                    'password.required' => ':attribute harus di isi'
                ],
                [
                    'username' => 'Username',
                    'password' => 'Password'
                ]
            );

            return response()->json([
                'username' => $request->username,
                'password' => $request->password
            ]);

        } catch (\Throwable $th) {

            return response()->json([
                'status' => 500,
                'message' => 'Internal Server Error'
            ]);
        
        }
    }

    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'username' => 'required|min:6|max:10|unique:users',
            'password'  => 'required|min:3|confirmed',
        ]);

        if ($v->fails())
        {
            return response()->json([
                'error' => 'registration_validation_error',
                'errors' => $v->errors()
            ], 422);
        }

        $user = new User;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if ( ! $token = JWTAuth::attempt($credentials)) {
                return response([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], 400);
        }
        return response([
                'status' => 'success',
                'token' => $token
            ])
            ->header('Authorization', $token);
    }

    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function user(Request $request)
    {
        $user = new LoginResource(User::find(Auth::user()->id));
        return response([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function refresh()
    {
        return response([
                'status' => 'success'
            ]);
    }

    private function guard()
    {
        return Auth::guard();
    }
}

