<?php

namespace App\Http\Controllers;

use App\DamSippKling;
use Illuminate\Http\Request;

class DamSippKlingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DamSippKling  $damSippKling
     * @return \Illuminate\Http\Response
     */
    public function show(DamSippKling $damSippKling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DamSippKling  $damSippKling
     * @return \Illuminate\Http\Response
     */
    public function edit(DamSippKling $damSippKling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DamSippKling  $damSippKling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DamSippKling $damSippKling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DamSippKling  $damSippKling
     * @return \Illuminate\Http\Response
     */
    public function destroy(DamSippKling $damSippKling)
    {
        //
    }
}
