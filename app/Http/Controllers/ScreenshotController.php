<?php

namespace App\Http\Controllers;

use App\Models\Screenshot;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class ScreenshotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $screenshot = Screenshot::select('*')->orderBy('id', 'asc')->get();
        return response()->json([
            'screenshot' => $screenshot,
        ]);
    }

    public function getScreenshot()
    {
        $hps = Screenshot::select()->where('param', true)->get();
        $laptops = Screenshot::select()->where('param', false)->get();
        return response()->json([
            'hps' => $hps,
            'laptops' => $laptops,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $screenshot = $this . validate(request(), [
            'gambar' => 'required',
            'alt' => 'required',
        ]);

        Screenshot::create($screenshot);
        return back()->with('success', 'Data telah berhasil ditambahkan');
    }

    public function addScreenshot(Request $request){
        $screenshot = new Screenshot();

        $param = $request->param;  
        $alt = $request->alt;
        if($request->foto){
            $gambar = $request->foto;
            $imageName = 'screenshot_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/screenshots/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);

            $screenshot->gambar = $lokasiimage;
        }

        $screenshot->alt = $alt;
        $screenshot->param = $param;
        $screenshot->save();

        if ($screenshot) {
            return response()->json([
                "message" => "Berhasil Update achievement",
                "screenshot" => $screenshot
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Screenshot::findOrFail($id);
        return response()->json([
            'data' => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $screenshot = Screenshot::findOrFail($id);
        $this->validate(request(), [
            'gambar' => 'required',
            'alt' => 'required',
        ]);

        $screenshot->gambar = $request->get('gambar');
        $screenshot->alt = $request->get('alt');
        $update = $screenshot->save();

        if ($update) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    public function updateScreenshot(Request $request, $id)
    {

        $home = Screenshot::find($id);
        $alt = $request->alt;
        $param = $request->param;
        if ($request->foto != null) {
            $gambar = $request->foto;
            $imageName = 'screenshot_' . $alt . "_" . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/screenshots/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);
            $home->gambar = $lokasiimage;
        }
        $home->alt = $alt;
        $home->param = $param;

        $home->save();

        if ($home) {
            return response()->json([
                "message" => "Berhasil Update Data",

            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyScreenshot($id)
    {
        //
        $screenshot = Screenshot::findOrFail($id);
        $delete = $screenshot->delete();

        if ($delete) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
}
