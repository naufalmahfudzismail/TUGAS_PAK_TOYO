<?php

namespace App\Http\Controllers;

use App\RumahSehat;
use Illuminate\Http\Request;

class RumahSehatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function show(RumahSehat $rumahSehat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function edit(RumahSehat $rumahSehat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RumahSehat $rumahSehat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function destroy(RumahSehat $rumahSehat)
    {
        //
    }
}
