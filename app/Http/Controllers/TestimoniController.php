<?php

namespace App\Http\Controllers;

use App\Models\Testimoni;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class TestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $testimoni = Testimoni::select('*')->orderBy('id', 'asc')->get();
        return response()->json([
            'testimoni' => $testimoni,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $testimoni = Testimoni::findOrFail($id);
        $this->validate(request(), [
            'gambar' => 'required',
            'judul' => 'required',
        ]);

        Testimoni::create($testimoni);

        if ($testimoni) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    public function addTestimoni(Request $request)
    {

        $home = new Testimoni();
        $judul = $request->judul;
        if($request->foto){

            $gambar = $request->foto;
            $imageName = 'testimoni_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/testimoni/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);
    
            $home->gambar = $lokasiimage;
        }

        $home->judul = $judul;

        $home->save();

        if ($home) {
            return response()->json([
                "message" => "Berhasil Update Data",
                "home" => $home
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Testimoni::findOrFail($id);

        return response()->json([
            'data' => $data,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $testimoni = Testimoni::findOrFail($id);
        $this->validate(request(), [
            'gambar' => 'required',
            'judul' => 'required',
        ]);

        $testimoni->gambar = $request->get('gambar');
        $testimoni->judul = $request->get('judul');
        $update = $testimoni->save();

        if ($update) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    public function updateTestimoni(Request $request, $id)
    {
        $judul = $request->judul;

        $home = Testimoni::find($id);
        if($request->foto){

            $gambar = $request->foto;
            $imageName = 'testimoni_' . str_random(5) . '.png';
            $lokasiimage = 'images/landingpage/testimoni/' . $imageName;
            Image::make($gambar)->save($lokasiimage, $imageName);
    
            $home->gambar = $lokasiimage;
        }
        $home->judul = $judul;

        $home->save();

        if ($home) {
            return response()->json([
                "message" => "Berhasil Update Data",
                'home' => $home
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyTestimoni($id)
    {
        //
        $testimoni = Testimoni::findOrFail($id);

        // $testimoni = User::destroy($id);
        $delete = $testimoni->delete();


        if ($delete) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
}
