<?php

namespace App\Http\Controllers;

use App\Pesantren;
use Illuminate\Http\Request;

class PesantrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pesantren  $pesantren
     * @return \Illuminate\Http\Response
     */
    public function show(Pesantren $pesantren)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pesantren  $pesantren
     * @return \Illuminate\Http\Response
     */
    public function edit(Pesantren $pesantren)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pesantren  $pesantren
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pesantren $pesantren)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pesantren  $pesantren
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pesantren $pesantren)
    {
        //
    }
}
