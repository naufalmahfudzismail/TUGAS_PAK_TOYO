<?php

namespace App\Http\Controllers\API;

use Intervention\Image\Facades\Image;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Resources\PetugasSippklingsTableResource;
use App\Http\Resources\PetugasSippklingResource;
use Illuminate\Support\Facades\Hash;
use App\Models\PetugasSippkling;
use App\Models\Data;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\History;
use App\Scopes\KaderFilterScope;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PetugasSippklingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PetugasSippkling::join('kelurahans as kel', 'petugas_sippklings.kelurahan_id', 'kel.id')
            ->join('kecamatans as kec', 'petugas_sippklings.kecamatan_id', 'kec.id')
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'kel.nama as kelurahan',
                'kec.nama as kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(a.petugas_id ) over (partition by petugas_sippklings.id) + count(b.petugas_id) over (partition by petugas_sippklings.id) + count(c.petugas_id) over (partition by petugas_sippklings.id)
            + count(d.petugas_id) over (partition by petugas_sippklings.id) + count(e.petugas_id) over (partition by petugas_sippklings.id) + count(f.petugas_id) over (partition by petugas_sippklings.id) + count(g.petugas_id)  over (partition by petugas_sippklings.id) + count(h.petugas_id) over (partition by petugas_sippklings.id)
            + count(i.petugas_id) over (partition by petugas_sippklings.id) + count(j.petugas_id) over (partition by petugas_sippklings.id) + count(k.petugas_id) over (partition by petugas_sippklings.id) + count(l.petugas_id) over (partition by petugas_sippklings.id) + count(m.petugas_id) over (partition by petugas_sippklings.id)
            + count(n.petugas_id) over (partition by petugas_sippklings.id) + count(o.petugas_id) over (partition by petugas_sippklings.id) + count(jb.petugas_id) over (partition by petugas_sippklings.id) as total_input ')
            )
            ->distinct()
            ->paginate(20);

        $jumlah_total = PetugasSippkling::count();
        return response()->json([
            'data' => $data,
            'total_data' => $jumlah_total

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'nama' => 'required|string',
                'username' => 'required|unique:petugas_sippklings',
                'password'  => $request->password !== '' ? 'required|string' : 'string',
                'kecamatan' => 'required',
                'kelurahan' => 'required'
            ],
            [],
            [
                'nama' => 'Nama',
                'username' => 'Username',
                'password' => 'Password',
                'kecamatan' => 'Kecamatan',
                'kelurahan' => 'Kelurahan'
            ]
        );

        $kader = new \App\Models\PetugasSippkling;
        $kader->nama = $request->nama;
        $kader->username = $request->username;
        $kader->password = Hash::make($request->password);
        $kader->kecamatan_id = $request->kecamatan;
        $kader->kelurahan_id = $request->kelurahan;
        $kader->role = true;
        $kader->created_by = changeIdToUsername($request->created_by);

        if (!$kader->save()) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'Good Request',
                'code' => 200,
            ];
        }
    }


    public function loginPetugas(Request $request)
    {

        $username = $request->username;
        $password = $request->password;

        $log_email = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->where('username', $username)->count();
        if ($log_email == null || $log_email == 0) {
            $message = "Akun anda tidak terdaftar !";
            $berhasil = false;
            $petugas = null;
            $kel = null;
            $kec = null;
        } else {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
                ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
                ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
                ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
                ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
                ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
                ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
                ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
                ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
                ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
                ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
                ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
                ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
                ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
                ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
                ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
                ->select(
                    'petugas_sippklings.*',
                    DB::raw('count(distinct a.id) as jumlah_input_rumah_sehat'),
                    DB::raw('count(distinct b.id ) as jumlah_input_rumah_sakit'),
                    DB::raw('count(distinct c.id ) as jumlah_input_puskesmas'),
                    DB::raw('count(distinct d.id ) as jumlah_input_klinik'),
                    DB::raw('count(distinct e.id ) as jumlah_input_hotel'),
                    DB::raw('count(distinct f.id ) as jumlah_input_hotel_melati'),
                    DB::raw('count(distinct g.id ) as jumlah_input_kuliner'),
                    DB::raw('count(distinct h.id ) as jumlah_input_kolam_renang'),
                    DB::raw('count(distinct i.id ) as jumlah_input_pasar'),
                    DB::raw('count(distinct j.id ) as jumlah_input_pesantren'),
                    DB::raw('count(distinct k.id ) as jumlah_input_sekolah'),
                    DB::raw('count(distinct l.id ) as jumlah_input_sab'),
                    DB::raw('count(distinct m.id ) as jumlah_input_tempat_ibadah'),
                    DB::raw('count(distinct n.id ) as jumlah_input_kesling'),
                    DB::raw('count(distinct o.id ) as jumlah_input_dam'),
                    DB::raw('count(distinct jb.id ) as jumlah_input_jasa_boga'),
                    DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
            + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
            + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
            + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
                )
                ->where('username', $username)
                ->groupBy('petugas_sippklings.id')
                ->first();

            if ($petugas->role == true) {
                $kel = Kelurahan::select('nama')->where('id', $petugas->kelurahan_id)->first()->nama;
                $kec = Kecamatan::select('nama')->where('id', $petugas->kecamatan_id)->first()->nama;
            } else {
                $kel = "null";
                if ($petugas->kecamatan_id != null && $petugas->kelurahan_id != null) {
                    $kec = Kecamatan::select('nama')->where('id', $petugas->kecamatan_id)->first()->nama;
                    $kel = Kelurahan::select('nama')->where('id', $petugas->kelurahan_id)->first()->nama;
                }
                else if($petugas->kecamatan_id != null && $petugas->kelurahan_id == null){
                    $kec = Kecamatan::select('nama')->where('id', $petugas->kecamatan_id)->first()->nama;
                }
                else{
                    $kec = "null";
                }
            }

            //dd($petugas->password);
            $check = Hash::check($password, $petugas->password);
            if ($check) {
                $message = "Login berhasil";
                $berhasil = true;
            } else {
                $message = "Password salah !";
                $berhasil = false;
                $petugas = null;
                $kel = null;
                $kec = null;
            }
        }

        return response()->json([
            "message" => $message,
            "error" => !$berhasil,
            "petugas" => $petugas,
            "kelurahan" => $kel,
            "kecamatan" => $kec
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $p = PetugasSippkling::leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'petugas_sippklings.foto as foto',
                'petugas_sippklings.kelurahan_id as id_kelurahan',
                'petugas_sippklings.kecamatan_id as id_kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                'petugas_sippklings.created_by as created_by',
                'petugas_sippklings.update_by as update_by',
                'petugas_sippklings.role as role',
                DB::raw('count(a.petugas_id ) + count(b.petugas_id) + count(c.petugas_id)
            + count(d.petugas_id) + count(e.petugas_id) + count(f.petugas_id) + count(g.petugas_id) + count(h.petugas_id)
            + count(i.petugas_id) + count(j.petugas_id) + count(k.petugas_id) + count(l.petugas_id) + count(m.petugas_id)
            + count(n.petugas_id) + count(o.petugas_id) + count(jb.petugas_id) as total_input ')
            )
            ->where('petugas_sippklings.id', $id)

            ->groupBy('petugas_sippklings.id')
            ->first();

        if ($p->role == true) {
            $kel = Kelurahan::select('nama')->where('id', $p->kelurahan_id)->first()->nama;
        } else {
            $kel = "null";
        }
        $kec = Kecamatan::select('nama')->where('id', $p->id_kecamatan)->first()->nama;


        $last_activity = History::select('waktu_history', 'aktifitas')
            ->where('petugas_id', $p->id)
            ->orderByDesc('waktu_history')
            ->first();

        if ($last_activity != null) {
            $histori = $last_activity->waktu_history;
            $aktifitas = $last_activity->aktifitas;
        } else {
            $histori = null;
            $aktifitas = null;
        }

        $result = [
            'id'  => $p->id,
            'old_id' => $p->old_id,
            'nama' => $p->nama,
            'username' => $p->username,
            'foto' => $p->foto,
            'kelurahan' => $kel,
            'kecamatan' => $kec,
            'date_regis' => $p->tgl_regis,
            'last_update' => $p->tgl_update,
            'last_activity' => $aktifitas,
            'date_last_activity' => $histori,
            'total_input' => $p->total_input
        ];

        return response()->json([
            'petugas' => $result
        ]);
    }

    public function showDPloginuser($username)
    {
        $dp = DB::table('petugas_sippklings as p')->select('foto')->where('p.username', $username)->first();

        return response()->json([
            'fotos' => $dp
        ]);
    }


    public function getPerAkunPetugasKecamatan($id)
    {

        $petugasKec = PetugasSippKling::withoutGlobalScope(KaderFilterScope::class)->select('username')->where('id', $id)->where('role', false)->first();
        $username = $petugasKec->username;

        $akun = PetugasSippKling::withoutGlobalScope(KaderFilterScope::class)
            ->join('kelurahans as k', 'petugas_sippklings.kelurahan_id', 'k.id')
            ->select('petugas_sippklings.*', 'k.nama as nama_kelurahan')
            ->where('username', 'like', $username . '_' . '%')
            ->where('role', false)
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'data' => $akun
        ]);
    }

    public function showByKelurahan($kelurahan)
    {

        $data = PetugasSippkling::join('kelurahans as kel', 'petugas_sippklings.kelurahan_id', 'kel.id')
            ->join('kecamatans as kec', 'petugas_sippklings.kecamatan_id', 'kec.id')
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'kel.nama as kelurahan',
                'kec.nama as kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(a.petugas_id ) over (partition by petugas_sippklings.id) + count(b.petugas_id) over (partition by petugas_sippklings.id) + count(c.petugas_id) over (partition by petugas_sippklings.id)
            + count(d.petugas_id) over (partition by petugas_sippklings.id) + count(e.petugas_id) over (partition by petugas_sippklings.id) + count(f.petugas_id) over (partition by petugas_sippklings.id) + count(g.petugas_id)  over (partition by petugas_sippklings.id) + count(h.petugas_id) over (partition by petugas_sippklings.id)
            + count(i.petugas_id) over (partition by petugas_sippklings.id) + count(j.petugas_id) over (partition by petugas_sippklings.id) + count(k.petugas_id) over (partition by petugas_sippklings.id) + count(l.petugas_id) over (partition by petugas_sippklings.id) + count(m.petugas_id) over (partition by petugas_sippklings.id)
            + count(n.petugas_id) over (partition by petugas_sippklings.id) + count(o.petugas_id) over (partition by petugas_sippklings.id) + count(jb.petugas_id) over (partition by petugas_sippklings.id) as total_input ')
            )
            ->where('kel.nama', $kelurahan)
            ->orWhere('petugas_sippklings.kelurahan_id', $kelurahan)
            ->distinct()
            ->paginate(20);

        $jumlah_total = PetugasSippkling::join('kelurahans as kel', 'petugas_sippklings.kelurahan_id', 'kel.id')
            ->where('kel.nama', $kelurahan)
            ->orWhere('petugas_sippklings.kelurahan_id', $kelurahan)
            ->count();

        return response()->json([
            'data' => $data,
            'total_data' => $jumlah_total

        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama',  $kecamatan)->first();
            $id = PetugasSippkling::select('id')->where('kecamatan_id', $kel->id)->get();
        }

        $data = PetugasSippkling::join('kelurahans as kel', 'petugas_sippklings.kelurahan_id', 'kel.id')
            ->join('kecamatans as kec', 'petugas_sippklings.kecamatan_id', 'kec.id')
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'kel.nama as kelurahan',
                'kec.nama as kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(a.petugas_id ) over (partition by petugas_sippklings.id) + count(b.petugas_id) over (partition by petugas_sippklings.id) + count(c.petugas_id) over (partition by petugas_sippklings.id)
            + count(d.petugas_id) over (partition by petugas_sippklings.id) + count(e.petugas_id) over (partition by petugas_sippklings.id) + count(f.petugas_id) over (partition by petugas_sippklings.id) + count(g.petugas_id)  over (partition by petugas_sippklings.id) + count(h.petugas_id) over (partition by petugas_sippklings.id)
            + count(i.petugas_id) over (partition by petugas_sippklings.id) + count(j.petugas_id) over (partition by petugas_sippklings.id) + count(k.petugas_id) over (partition by petugas_sippklings.id) + count(l.petugas_id) over (partition by petugas_sippklings.id) + count(m.petugas_id) over (partition by petugas_sippklings.id)
            + count(n.petugas_id) over (partition by petugas_sippklings.id) + count(o.petugas_id) over (partition by petugas_sippklings.id) + count(jb.petugas_id) over (partition by petugas_sippklings.id) as total_input ')
            )
            ->where('kec.nama', $kecamatan)
            ->orWhere('petugas_sippklings.kecamatan_id', $kecamatan)
            ->distinct()
            ->orderByDesc('total_input')
            ->paginate(20);

        $jumlah_total = PetugasSippkling::join('kecamatans as kec', 'petugas_sippklings.kecamatan_id', 'kec.id')
            ->where('kec.nama', $kecamatan)
            ->orWhere('petugas_sippklings.kecamatan_id', $kecamatan)
            ->count();

        return response()->json([
            'data' => $data,
            'total_data' => $jumlah_total

        ]);
    }


    public function showHistory($id)
    {

        $obj = History::join('datas as d', 'histories.data_id', 'd.id')
            ->select('histories.*', 'd.nama_data as nama_data')
            ->where('histories.petugas_id', $id)
            ->orderByDesc('histories.updated_at')
            ->paginate(20);

        return response()->json([
            'data' => $obj
        ]);
    }
    public function showHistorySemua($id)
    {

        $obj = History::join('datas as d', 'histories.data_id', 'd.id')
            ->select('histories.*', 'd.nama_data as nama_data')
            ->where('histories.petugas_id', $id)
            ->orderByDesc('histories.updated_at')->get();

        return response()->json([
            'data' => $obj,
        ]);
    }



    public function edit(PetugasSippkling $petugasSippkling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */


    public function uploadFotoPetugas(Request $request)
    {

        $obj = PetugasSippkling::findOrFail($request->id);
        $username = $obj->username;

        $this->validate($request, [
            'foto' => 'required|image|mimes:jpg,png,jpeg'
        ]);

        $fotoLama = $obj->foto;

        if ($fotoLama != null) {
            unlink(storage_path() . '/app/public/images/profil/' . $fotoLama);
        }

        $image = $request->file('foto');
        $thumbnailPath = storage_path() . '/app/public/images/profil/';
        $imageName = $username .'_'. time() . str_random(3) . '.' . $image->getClientOriginalExtension();
        $saveImage =  Image::make($image)->save($thumbnailPath . $imageName);

        $obj->update([
            "foto" => $imageName
        ]);

        if ($obj && $saveImage) {
            return response()->json([
                'error' => false,
                'imagePath' => $imageName,
                'error_message' => 'updated',
                'code' => 201,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'imagePath' => null,
                'error_message' => 'error',
                'code' => 400,
            ]);
        }
    }


    public function updateNama(Request $request, $id)
    {

        $nama  = $request->nama;

        if (strlen($nama) > 35) {
            $error = true;
            $message = "Nama tidak boleh lebih dari 35 karakter";
            $result = null;
        } else {

            $obj = PetugasSippkling::findOrFail($id);


            $update = $obj->update([
                "nama" => $nama
            ]);

            if ($update) {
                $error = false;
                $message = "Ganti nama berhasil";
                $result = $nama;
            } else {
                $error = true;
                $message = "Ganti nama gagal, terjadi kesalahan";
                $result = null;
            }
        }

        return response()->json([
            "error" => $error,
            "error_message" => $message,
            "result" => $result
        ]);
    }


    public function changePassword(Request $request, $id)
    {

        $obj = PetugasSippkling::findOrFail($id);
        $check = Hash::check($request->oldPassword, $obj->password);

        if ($check) {

            $password = $request->password;
            $hashedPassword = Hash::make($password);

            $changePass = $obj->update([
                "password" => $hashedPassword
            ]);

            if (!$obj || !$changePass) {
                return response()->json([
                    "error" => true,
                    "error_message" => "Terjadi kesalahan, password tidak dapat di ubah",
                    "code" => 400
                ]);
            } else {
                return response()->json([
                    "error" => false,
                    "error_message" => "Password berhasil di ubah",
                    "code" => 201
                ]);
            }
        } else {

            return response()->json([
                "error" => true,
                "error_message" => "Password lama anda tidak benar, ulangi kembali",
                "code" => 404
            ]);
        }
    }




    public function update(Request $request, $petugasSippkling)
    {
        $obj = PetugasSippkling::findOrFail($petugasSippkling);

        if ($request->file('foto')) {

            $photo_path = $request->file('foto');
            $new_name = $obj->id . '_' . Carbon::now()->timestamp . str_random(15) . $photo_path->getClientOriginalExtension();

            $old_name = $obj->foto;
            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink(storage_path('app/public/images/profil/'.$old_name));
            }

            $photo_path->move(storage_path('app/public/images/profil'), $new_name);   //it will be save your image on public/images folder
            $request->foto = $new_name;
        }


        $request->password = Hash::make($request->get('password'));

        $requestData = $request->all();
        $requestData["password"] = Hash::make($request->get('password'));
        //$requestData["foto"] = $new_name;

        $obj->update($requestData);

        if ($obj) {
            return response()->json([
                'error' => false,
                'error_message' => 'updated',
                'code' => 201,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'error_message' => 'error',
                'code' => 400,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PetugasSippkling  $petugasSippkling
     * @return \Illuminate\Http\Response
     */
    public function destroy($petugasSippkling)
    {
        $dataLogin = Auth::user();

        if($dataLogin != null){
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = PetugasSippkling::findOrFail($petugasSippkling);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $deleteHs = $obj->delete();

        if ($deleteHs) {

            return response()->json([
                'error' => false,
                'error_message' => 'deleted',
                'code' => 204,
            ]);

        } else {

            return  response()->json([
                'error' => true,
                'error_message' => 'cannot delete',
                'code' => 400,
            ]);

        }

    }

    public function getPetugasSippklings()
    {
        $data_kader = PetugasSippkling::orderBy('id', 'desc')
        ->advancedFilterWithoutStaticClass()
        ->where('role', true);

        $data_upf = PetugasSippkling::orderBy('id', 'desc')
        ->advancedFilterWithoutStaticClass()
        ->where('nama', 'LIKE', '%UPF%');

        $data = PetugasSippkling::orderBy('id', 'desc')
        ->advancedFilterWithoutStaticClass()
        ->where('username', '!=', 'admin')
        ->where('role', false)
        ->whereNull('kelurahan_id')
        ->union($data_kader)
        ->union($data_upf)
        ->get();

        return response()->json([
            "status" => 200,
            "data" => [
                "total" => [
                    "petugas" => \App\Models\PetugasSippkling::where('role', false)
                                ->advancedFilterWithoutStaticClass()
                                ->where('username', '!=', 'admin')
                                ->whereNull('kelurahan_id')
                                ->union($data_upf)                                
                                ->count(),
                    "kader" => \App\Models\PetugasSippkling::where('role', true)
                                ->advancedFilterWithoutStaticClass()
                                ->count()
                ],
                "query" => PetugasSippklingsTableResource::collection($data)
            ]
        ]);
    }

    public function showPetugasSippkling($id)
    {
        return new PetugasSippklingsTableResource(\App\Models\PetugasSippkling::find($id));
    }

    public function customUpdatePetugasSippkling(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'nama' => 'required|string',
                // 'username' => 'unique:petugas_sippklings, username,' . $id,
                // 'username' => [
                //     Rule::unique('petugas_sippklings')->ignore($id)
                // ],
                'password'  => $request->password == '' ? '' : 'required|string',
                'kecamatan' => 'required',
                'kelurahan' => 'required'
            ],
            [],
            [
                'nama' => 'Nama',
                'username' => 'Username',
                'password' => 'Password',
                'kecamatan' => 'Kecamatan',
                'kelurahan' => 'Kelurahan'
            ]
        );

        $kader = \App\Models\PetugasSippkling::find($id);
        $kader->nama = $request->nama;
        $kader->username = $request->username;

        if ($request->password !== null) {
            $kader->password = Hash::make($request->password);
        }

        $kader->kecamatan_id = $request->kecamatan;
        $kader->kelurahan_id = $request->kelurahan;
        $kader->update_by = Auth::user()->id;

        if (!$kader->update()) {
            return [
                'message' => 'Bad Request',
                'code' => 400,
            ];
        } else {
            return [
                'message' => 'OK',
                'code' => 201,
            ];
        }
    }

    public function getListNamePetugasSippkling()
    {
        $data = \App\Models\PetugasSippkling::select('id AS value', 'username AS text')->orderBy('id', 'desc')->get();

        return [
            'status' => 200,
            'data' => $data
        ];
    }

    public function getJsonToCsv()
    {
        $data = \App\Http\Resources\PetugasSippklingJsonToCsvResource::collection(\App\Models\PetugasSippkling::all());

        return $data;
    }

    public function importCsv(Request $request)
    {
        try {
            $getArraySuccess = [];

            $validate = $request->validate(
                [
                    'csv.*.nama' => 'required|string',
                    'csv.*.username' => 'required|string|unique:petugas_sippklings,username',
                    'csv.*.password'  => 'required|string',
                    'csv.*.kecamatan' => 'required',
                    'csv.*.kelurahan' => 'required'
                ]
            );

            $data = $this->super_unique($request->csv, "username");

            if($data['false'] != null){
                
                return [
                    'status_code' => 1000,
                    'message' => "Beberapa username dalam data csv terindikasi sama, maka data tersebut tidak dimasukan",
                    'data' => $data['false']
                ];

            } else {
                for ($i=0; $i < count($data['true']); $i++) { 

                    $kader = new \App\Models\PetugasSippkling;
                    $kader->nama = $data['true'][$i]['nama'];
                    $kader->username = $data['true'][$i]['username'];
                    $kader->password = Hash::make($data['true'][$i]['password']);
                    $kader->kecamatan_id = $data['true'][$i]['kecamatan'];
                    $kader->kelurahan_id = $data['true'][$i]['kelurahan'];
                    $kader->role = true;
                    $kader->created_by = \changeIdToUsername(Auth::user()->id);

                    $kader->save();
                }
                
                return [
                    'status_code' => 200,
                    'message' => "Import csv berhasil dilakukan"
                ];
                
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    private function super_unique($array,$key)
    {
       $temp_array = [];
       $temp_array_false = [];
       foreach ($array as &$v) {
           if (!isset($temp_array[$v[$key]])){
               $temp_array[$v[$key]] =& $v;
           } else {
               $temp_array_false[$v[$key]] = &$v;
           }
       }
       $array = array_values($temp_array);
       $array_false = array_values($temp_array_false);
       
       return [
           'true' => $array,
           'false' => $array_false
       ];
    }
}
