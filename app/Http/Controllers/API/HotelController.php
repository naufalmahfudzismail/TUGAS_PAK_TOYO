<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\Kecamatan;
use App\Models\Moduls\Hotel;
use App\Models\PetugasSippkling;
use App\Scopes\KaderFilterScope;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = Hotel::paginate(20);
        $jumlah_total = Hotel::count();
        $jumlah_sehat = Hotel::where('status', 'true')->count();
        $jumlah_tidak_sehat = Hotel::where('status', 'false')->count();
        $jumlah_unkown = Hotel::whereNull('status')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg',
            ]);

            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'hotel_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] = null;
        }

        $obj = Hotel::create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas = $obj->created_by;
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '8';
            $history->objek_id = $obj->id;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 201,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($hotel)
    {
        $obj = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->select(
                'hotels.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from petugas_sippklings as p where p.id = hotels.update_by) as update_by"),
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )
            ->where('hotels.id', $hotel)
            ->first();

        return response()->json([
            'data' => $obj,
        ]);
    }

    public function showByKelurahan($kelurahan)
    {

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();

        $obj = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->select(
                'hotels.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('hotels.waktu')
            ->paginate(20);

        $jumlah_total = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->where('hotels.status', 'true')->count();
        $jumlah_tidak_sehat = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('hotels.status', 'false')->count();
        $jumlah_unkown = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('status')

            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama', $kecamatan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kel->id)->get();
        }

        $obj = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->select(
                'hotels.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('hotels.waktu')
            ->paginate(20);

        $jumlah_total = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->where('hotels.status', 'true')->count();
        $jumlah_tidak_sehat = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('hotels.status', 'false')->count();
        $jumlah_unkown = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('status')

            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $Hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $Hotel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $Hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $hotel)
    {
        $obj = Hotel::findOrFail($hotel);
        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'hotel' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);

        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->petugas_id = $request->update_by;
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by = $request->update_by;
            $history->created_at = Carbon::now();

            $history->data_id = '8';
            $history->objek_id = $obj->id;
            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $Hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy($hotel)
    {
        $dataLogin = Auth::user();

        if ($dataLogin != null) {
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = Hotel::find($hotel);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $delete = $obj->delete();

        if (!$delete) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '8';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    public function isiTrash()
    {
        $obj = Hotel::whereNotNull('deleted_at')
            ->get();

        return response()->json([
            'data' => $obj,
        ]);
    }

    public function importCsv(Request $request)
    {
        try {
            $getArraySuccess = [];

            $validate = $request->validate(
                [
                    'csv.*.nama_tempat' => 'required|string',
                    'csv.*.nama_pemimpin' => 'required|string',
                    'csv.*.alamat' => 'required|string',
                    'csv.*.jam_operasional' => 'required|string',
                    'csv.*.no_telp' => 'required|string',
                    'csv.*.jumlah_karyawan' => 'required|string',
                    'csv.*.no_izin_usaha' => 'required|integer',
                    'csv.*.kecamatan_id' => 'required|integer',
                    'csv.*.kelurahan_id' => 'required|integer',
                ]
            );

            $data = $request->csv;

            // if ($data['false'] != null) {

            //     return [
            //         'status_code' => 1000,
            //         'message' => "Terjadi kesalahan",
            //         'data' => $data['false'],
            //     ];

            // } else {
            for ($i = 0; $i < count($data); $i++) {

                $item = new \App\Models\Moduls\Hotel;
                $item->nama_tempat = $data[$i]['nama_tempat'];
                $item->nama_pemimpin = $data[$i]['nama_pemimpin'];
                $item->alamat = $data[$i]['alamat'];
                $item->jumlah_karyawan = $data[$i]['jumlah_karyawan'];
                $item->no_telp = $data[$i]['no_telp'];
                $item->no_izin_usaha = $data[$i]['no_izin_usaha'];

                $item->jam_operasional = $data[$i]['jam_operasional'];
                $kecamatan_id = $data[$i]['kecamatan_id'];
                $kelurahan_id = $data[$i]['kelurahan_id'];

                $item->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $item->created_by = \changeIdToUsername(Auth::user()->id);
                $item->waktu = Carbon::now();
                $item->save();

                $history = new History();
                $history->aktifitas = 'input';
                $history->waktu_history = Carbon::now();

                $history->tipe_petugas = 'petugas';
                $history->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $history->data_id = '8';
                $history->objek_id = $item->id;
                $history->created_by = \changeIdToUsername(Auth::user()->id);
                $history->created_at = $item->created_at;
                $history->key_histories = $item->petugas_id . '-' . $history->data_id . '-' . $item->id . '-' . $history->id;

                $save = $history->save();

            }

            return [
                'status_code' => 200,
                'message' => "Import csv berhasil dilakukan",
            ];

            // }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
