<?php

namespace App\Http\Controllers\API;use App\Http\Controllers\Controller;
use App\Models\Kelurahan;
use DB;
use Hash;
// use App\Http\Resources\User\UserCollection;
// use App\Http\Resources\User\UserItem;
use Illuminate\Http\Request;

class KelurahanController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return new UserCollection(\App\Models\User::get());
        $data = Kelurahan::all();

        return response()->json([
            'kelurahan' => $data,
        ]);
    }
    public function indexKecKel()
    {

        $data = DB::table('kelurahans as a')->join('kecamatans as b', 'a.kecamatan_id', 'b.id')
            ->select('a.id as id_kelurahan', 'a.nama as nama_kelurahan', 'b.id as id_kecamatan', 'b.nama as nama_kecamatan')
            ->get();
        return response()->json([
            'data' => $data,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, \App\Models\User::rules(false));

        $user = new \App\Models\User;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->alamat = $request->alamat;
        $user->no_telp = $request->no_telp;
        $user->role = $request->role;
        $user->created_by = $request->created_by;

        if (!$user->save()) {
            return response()->json([
                'message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            return response()->json([
                'message' => 'OK',
                'code' => 200,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\App\Models\User $user)
    {
        return new UserItem($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Models\User $user)
    {
        $this->validate($request, \App\Models\User::rules(true, $user->id));

        if (!$user->update($request->all())) {
            return response()->json([
                'message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            return response()->json([
                'message' => 'OK',
                'code' => 201,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Models\User $user)
    {
        if ($user->deletes()) {
            return response()->json([
                'message' => 'OK',
                'code' => 204,
            ]);
        } else {
            return response()->json([
                'message' => 'Bad Request',
                'code' => 400,
            ]);
        }
    }

    public function getKelurahansName($kecamatan_id)
    {
        $data = \App\Models\Kelurahan::select('id', 'nama')
            ->where('kecamatan_id', $kecamatan_id)
            ->get();

        return [
            'status' => 200,
            'data' => $data,
        ];
    }

    public function getKelurahanForSelectFormByKecamatan($kecamatan_id)
    {
        $data = \App\Models\Kelurahan::select('id AS value', 'nama AS text')
            ->where('kecamatan_id', $kecamatan_id)
            ->get();

        $defaultSelectForm = new \stdClass();
        $defaultSelectForm->value = null;
        $defaultSelectForm->text = "Semua Kelurahan";

        $newKelurahan = array();
        for ($i = 0; $i < count($data) + 1; $i++) {
            $newKelurahan[$i] = new \stdClass();
            if ($i == 0) {
                $newKelurahan[$i]->value = $defaultSelectForm->value;
                $newKelurahan[$i]->text = $defaultSelectForm->text;
            } else {
                $newKelurahan[$i]->value = $data[$i - 1]->value;
                $newKelurahan[$i]->text = $data[$i - 1]->text;
            }

        }

        return [
            'status' => 200,
            'data' => $newKelurahan,
        ];
    }

    public function getRwByKelurahan()
    {
        // $dataRw = \App\Models\PetugasSippkling::select('rw')
    }
}
