<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;
use App\Models\History;
use App\Models\Moduls\KolamRenang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\PetugasSippkling;
use App\Scopes\KaderFilterScope;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;



class KolamRenangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = KolamRenang::paginate(20);
        $jumlah_total =  KolamRenang::count();
        $jumlah_sehat =  KolamRenang::where('status', 'true')->count();
        $jumlah_tidak_sehat = KolamRenang::where('status', 'false')->count();
        $jumlah_unkown =  KolamRenang::whereNull('status')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, KolamRenang::rules(false));

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg'
            ]);
            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'kolam_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] =  null;
        }

        $obj = KolamRenang::create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas = $obj->created_by;
            $history->petugas_id = $request->petugas_id;
            $history->data_id = '43';
            $history->objek_id = $obj->id;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 201,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($kolamrenang)
    {
        $obj = KolamRenang::join('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
            ->select(
                'kolam_renangs.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from petugas_sippklings as p where p.id = kolam_renangs.update_by) as update_by"),
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )

            ->where('kolam_renangs.id', $kolamrenang)
            ->first();

        return response()->json([
            'data' => $obj
        ]);
    }


    public function showByKelurahan($kelurahan)
    {

        if (is_numeric($kelurahan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();
        } else {
            $kel = Kelurahan::where('nama',  $kelurahan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', $kel->id)->get();
        }

        $obj = KolamRenang::join('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
            ->select(
                'kolam_renangs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('kolam_renangs.waktu')
            ->paginate(20);


        $jumlah_total =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('kolam_renangs.status', 'true')->count();
        $jumlah_tidak_sehat = KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('kolam_renangs.status', 'false')->count();
        $jumlah_unkown =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('status')

            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();

        $obj = KolamRenang::join('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
            ->select(
                'kolam_renangs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('kolam_renangs.waktu')
            ->paginate(20);


        $jumlah_total =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('kolam_renangs.status', 'true')->count();
        $jumlah_tidak_sehat = KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('kolam_renangs.status', 'false')->count();
        $jumlah_unkown =  KolamRenang::join('petugas_sippklings as  p',  'kolam_renangs.petugas_id',  'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('kolam_renangs.status')
            ->orWhere('kolam_renangs.status', 'null')
            ->orWhere('kolam_renangs.status', '')
            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KolamRenang  $KolamRenang
     * @return \Illuminate\Http\Response
     */
    public function edit(KolamRenang $KolamRenang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KolamRenang  $KolamRenang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $kolamrenang)
    {
        $obj = KolamRenang::findOrFail($kolamrenang);

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'kolam_' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);


        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by  = $request->update_by;
            $history->created_at = Carbon::now();
            $history->petugas_id = $request->update_by;
            $history->data_id = '43';
            $history->objek_id = $obj->id;

            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KolamRenang  $KolamRenang
     * @return \Illuminate\Http\Response
     */
    public function destroy($kolamrenang)
    {
        $dataLogin = Auth::user();

        if($dataLogin != null){
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = KolamRenang::find($kolamrenang);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $delete = $obj->delete();

        if (!$delete) {
            return  response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '43';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }
    
    public function importCsv(Request $request)
    {
        try {
            $getArraySuccess = [];

            $validate = $request->validate(
                [
                    'csv.*.nama_tempat' => 'required|string',
                    'csv.*.nama_pengelola' => 'required|string',
                    'csv.*.alamat' => 'required|string',
                    'csv.*.jam_operasional' => 'required|string',
                    'csv.*.no_telp' => 'required|string',
                    'csv.*.jumlah_karyawan' => 'required|string',
                    'csv.*.no_izin_usaha' => 'required|string',
                    'csv.*.kecamatan_id' => 'required|integer',
                    'csv.*.kelurahan_id' => 'required|integer',
                ]
            );

            $data = $request->csv;

            // if ($data['false'] != null) {

            //     return [
            //         'status_code' => 1000,
            //         'message' => "Terjadi kesalahan",
            //         'data' => $data['false'],
            //     ];

            // } else {
            for ($i = 0; $i < count($data); $i++) {

                $item = new \App\Models\Moduls\KolamRenang;
                $item->nama_tempat = $data[$i]['nama_tempat'];
                $item->nama_pengelola = $data[$i]['nama_pengelola'];
                $item->alamat = $data[$i]['alamat'];
                $item->jumlah_karyawan = $data[$i]['jumlah_karyawan'];
                $item->no_telp = $data[$i]['no_telp'];
                $item->jam_operasional = $data[$i]['jam_operasional'];
                $item->no_izin_usaha = $data[$i]['no_izin_usaha'];
                $kecamatan_id = $data[$i]['kecamatan_id'];
                $kelurahan_id = $data[$i]['kelurahan_id'];

                $item->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $item->created_by = \changeIdToUsername(Auth::user()->id);
                $item->waktu = Carbon::now();
                $item->save();

                $history = new History();
                $history->aktifitas = 'input';
                $history->waktu_history = Carbon::now();

                $history->tipe_petugas = 'petugas';
                $history->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $history->data_id = '43';
                $history->objek_id = $item->id;
                $history->created_by = \changeIdToUsername(Auth::user()->id);
                $history->created_at = $item->created_at;
                $history->key_histories = $item->petugas_id . '-' . $history->data_id . '-' . $item->id . '-' . $history->id;

                $save = $history->save();

            }

            return [
                'status_code' => 200,
                'message' => "Import csv berhasil dilakukan",
            ];

            // }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
