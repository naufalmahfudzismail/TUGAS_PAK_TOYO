<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;

use Carbon\Carbon;
use App\Scopes\KaderFilterScope;
use App\Http\Controllers\Controller;
use App\Models\History;
use Illuminate\Http\Request;
use DB;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = History::paginate(20);
        $jumlah_total = History::count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save  = History::create($request->all());

        if ($save) {
            return response()->json([
                'error' => false,
                'error_message' => 'created',
                'code' => 201,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'error_message' => 'error',
                'code' => 400,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function show($history)
    {
        $obj = History::where('id', $history);
        return response()->json([
            'data' => $obj
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function edit(History $history)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $history)
    {

        $obj = History::find($history);
        $obj->update($request->all());

        if ($obj) {
            return response()->json([
                'error' => false,
                'error_message' => 'updated',
                'code' => 201,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'error_message' => 'error',
                'code' => 400,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function destroy($history)
    {
        $obj = History::find($history);
        $deleteHs = $obj->delete();

        if ($deleteHs) {
            return response()->json([
                'error' => false,
                'error_message' => 'deleted',
                'code' => 204,
            ]);
        } else {
           return response()->json([
                'error' => true,
                'error_message' => 'cannot delete',
                'code' => 400,
            ]);
        }
    }

    public function getHistoryWithLimit($limit, $data_id){
        if($data_id == 0){

            $obj = \App\Http\Resources\HistoryResource::
            
            collection(
                History::autoFilterByRole()->advancedFilterWithoutRtRw()->limit($limit)->where('aktifitas', '!=', 'delete')->orderBy('created_at', 'desc')->get()
            );
            
        } else {
            $obj = \App\Http\Resources\HistoryResource::
            
            collection(
                History::autoFilterByRole()->advancedFilterWithoutRtRw()->limit($limit)->where('aktifitas', '!=', 'delete')->where('data_id', $data_id)->orderBy('created_at', 'desc')->get()
            );
        }
        
        return response()->json([
            'data' => $obj
        ]);
    }

    public function getHistoriSemua(){
        if(request('kecamatan') != null || request('petugas') != null){
            $obj = new \App\Http\Resources\HistoryCollection(
                History::autoFilterByRole()
                ->whereHas('petugas', function($query){
                    if(request('kecamatan') != null && request('kelurahan') != null){
                        $query->where('kecamatan_id', request('kecamatan'))
                            ->where('kelurahan_id', request('kelurahan'));
                    } else if (request('kecamatan') != null) {
                        $query->where('kecamatan_id', request('kecamatan'));
                    } else {
                        return $query;
                    }
                })
                ->orderBy('waktu_history', 'desc')
                ->whereHas('petugas', function($query){
                    if(request('petugas') != null){
                        $query->where('id', request('petugas'));
                    }
                })
                ->where(function($query){
                    if(request('tanggal') != null){
                        $query->where(DB::raw('date(created_at)'), '=', request('tanggal'));
                    }
                })
                ->where(function($query){
                    if(request('waktu') == 1){
                        $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                    } else if (request('waktu') == 2){
                        $query->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
                    } else {
    
                    }
                })
                ->get()
            );
        } else {
            $obj = new \App\Http\Resources\HistoryCollection(
                History::autoFilterByRole()
                ->whereHas('petugas', function($query){
                    if(request('kecamatan') != null && request('kelurahan') != null){
                        $query->where('kecamatan_id', request('kecamatan'))
                            ->where('kelurahan_id', request('kelurahan'));
                    } else if (request('kecamatan') != null) {
                        $query->where('kecamatan_id', request('kecamatan'));
                    } else {
                        return $query;
                    }
                })
                ->orderBy('waktu_history', 'desc')
                ->whereHas('petugas', function($query){
                    if(request('petugas') != null){
                        $query->where('id', request('petugas'));
                    }
                })
                ->where(function($query){
                    $query->where('created_at', '=', \Carbon\Carbon::now());
                })
                ->where(function($query){
                    if(request('waktu') == 1){
                        $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                    } else if (request('waktu') == 2){
                        $query->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
                    } else {
    
                    }
                })
                ->get()
            );
        }
        

        return response()->json([
            'data' => $obj
        ]);
    }
}
