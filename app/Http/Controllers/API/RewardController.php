<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\PetugasSippkling;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\History;
use App\Scopes\KaderFilterScope;
use Illuminate\Support\Facades\Auth;

class RewardController extends Controller
{
    public function allReward()
    {
        // joining history last input aktifiats input dan waktu history terbaru dalam 1 query
        $data = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username as username',
                'petugas_sippklings.role as role',
                DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            ->orderByDesc('total_input')
            ->groupBy('petugas_sippklings.id')
            ->limit(20)
            ->get();

        return response()->json([
            'data' => $data
        ]);
    }

    public function allRewardForWeb()
    {
        // joining history last input aktifiats input dan waktu history terbaru dalam 1 query
        $data = PetugasSippkling::advancedFilterWithoutStaticClass()
            ->withoutGlobalScope(KaderFilterScope::class)
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                // DB::raw("ROW_NUMBER() OVER(ORDER BY total_input desc) as rank"),
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username as username',
                'petugas_sippklings.foto as foto',
                'petugas_sippklings.role as role',
                DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            ->orderByDesc('total_input')
            ->groupBy('petugas_sippklings.id')
            ->where('username', '!=', 'admin')
            ->where('petugas_sippklings.role', true)
            ->limit(10)
            ->get();

        return response()->json([
            'data' => $data
        ]);
    }


    public function petugasRewardDetail($id)
    {

        $p = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'petugas_sippklings.foto as foto',
                'petugas_sippklings.kelurahan_id as id_kelurahan',
                'petugas_sippklings.kecamatan_id as id_kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                'petugas_sippklings.created_by as created_by',
                'petugas_sippklings.update_by as update_by',
                'petugas_sippklings.role as role',
                DB::raw('count(distinct a.id) as jumlah_input_rumah_sehat'),
                DB::raw('count(distinct b.id ) as jumlah_input_rumah_sakit'),
                DB::raw('count(distinct c.id ) as jumlah_input_puskesmas'),
                DB::raw('count(distinct d.id ) as jumlah_input_klinik'),
                DB::raw('count(distinct e.id ) as jumlah_input_hotel'),
                DB::raw('count(distinct f.id ) as jumlah_input_hotel_melati'),
                DB::raw('count(distinct g.id ) as jumlah_input_kuliner'),
                DB::raw('count(distinct h.id ) as jumlah_input_kolam_renang'),
                DB::raw('count(distinct i.id ) as jumlah_input_pasar'),
                DB::raw('count(distinct j.id ) as jumlah_input_pesantren'),
                DB::raw('count(distinct k.id ) as jumlah_input_sekolah'),
                DB::raw('count(distinct l.id ) as jumlah_input_sab'),
                DB::raw('count(distinct m.id ) as jumlah_input_tempat_ibadah'),
                DB::raw('count(distinct n.id ) as jumlah_input_kesling'),
                DB::raw('count(distinct o.id ) as jumlah_input_dam'),
                DB::raw('count(distinct jb.id ) as jumlah_input_jasa_boga'),
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
            + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
            + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
            + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            ->where('petugas_sippklings.id', $id)
            ->groupBy('petugas_sippklings.id')
            ->first();

        $kelurahan_id = $p->id_kelurahan;
        if ($p->role == true) {
            $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
        } else {
            if ($kelurahan_id != null || !is_null($kelurahan_id)) {
                $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
            } else {
                $kel = "null";
            }
        }
        $kec = Kecamatan::select('nama')->where('id', $p->id_kecamatan)->first()->nama;


        $last_activity = History::select('waktu_history', 'aktifitas')
            ->where('petugas_id', $p->id)
            ->orderByDesc('waktu_history')
            ->first();

        if ($last_activity != null) {
            $histori = $last_activity->waktu_history;
            $aktifitas = $last_activity->aktifitas;
        } else {
            $histori = null;
            $aktifitas = null;
        }

        $result = [
            'data_petugas' => $p,
            'kelurahan' => $kel,
            'kecamatan' => $kec,
            'last_activity' => $aktifitas,
            'date_last_iput' => $histori
        ];;

        return response()->json([
            'petugas' => $result
        ]);
    }

    public function webPetugasRewardDetail($username)
    {

        $p = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username',
                'petugas_sippklings.foto as foto',
                'petugas_sippklings.kelurahan_id as id_kelurahan',
                'petugas_sippklings.kecamatan_id as id_kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                'petugas_sippklings.created_by as created_by',
                'petugas_sippklings.update_by as update_by',
                'petugas_sippklings.role as role',
                DB::raw('count(distinct a.id) as jumlah_input_rumah_sehat'),
                DB::raw('count(distinct b.id ) as jumlah_input_rumah_sakit'),
                DB::raw('count(distinct c.id ) as jumlah_input_puskesmas'),
                DB::raw('count(distinct d.id ) as jumlah_input_klinik'),
                DB::raw('count(distinct e.id ) as jumlah_input_hotel'),
                DB::raw('count(distinct f.id ) as jumlah_input_hotel_melati'),
                DB::raw('count(distinct g.id ) as jumlah_input_kuliner'),
                DB::raw('count(distinct h.id ) as jumlah_input_kolam_renang'),
                DB::raw('count(distinct i.id ) as jumlah_input_pasar'),
                DB::raw('count(distinct j.id ) as jumlah_input_pesantren'),
                DB::raw('count(distinct k.id ) as jumlah_input_sekolah'),
                DB::raw('count(distinct l.id ) as jumlah_input_sab'),
                DB::raw('count(distinct m.id ) as jumlah_input_tempat_ibadah'),
                DB::raw('count(distinct n.id ) as jumlah_input_kesling'),
                DB::raw('count(distinct o.id ) as jumlah_input_dam'),
                DB::raw('count(distinct jb.id ) as jumlah_input_jasa_boga'),
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
            + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
            + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
            + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            // ->where('petugas_sippklings.id', $id)
            ->where('petugas_sippklings.username', $username)
            ->groupBy('petugas_sippklings.id')
            ->first();

        $kelurahan_id = $p->id_kelurahan;
        if ($p->role == true) {
            $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
        } else {
            if ($kelurahan_id != null || !is_null($kelurahan_id)) {
                $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
            } else {
                $kel = "null";
            }
        }
        $kec = Kecamatan::select('nama')->where('id', $p->id_kecamatan)->first()->nama;


        $last_activity = History::select('waktu_history', 'aktifitas')
            ->where('petugas_id', $p->id)
            ->orderByDesc('waktu_history')
            ->first();

        if ($last_activity != null) {
            $histori = $last_activity->waktu_history;
            $aktifitas = $last_activity->aktifitas;
        } else {
            $histori = null;
            $aktifitas = null;
        }

        $result = [
            'data_petugas' => $p,
            'kelurahan' => $kel,
            'kecamatan' => $kec,
            'last_activity' => $aktifitas,
            'date_last_iput' => $histori
        ];;


        return response()->json([
            'petugas' => $result
        ]);
    }


    //not use for a while
    public function petugasKecamatanDetail($id)
    {

        $petugasKec = PetugasSippKling::withoutGlobalScope(KaderFilterScope::class)
            ->select(
                'username',
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.foto as foto',
                'petugas_sippklings.kelurahan_id as id_kelurahan',
                'petugas_sippklings.kecamatan_id as id_kecamatan',
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                'petugas_sippklings.created_by as created_by',
                'petugas_sippklings.update_by as update_by',
                'petugas_sippklings.role as role'
            )
            ->where('id', $id)->where('role', false)->first();
        $username = $petugasKec->username;

        $akun = PetugasSippKling::withoutGlobalScope(KaderFilterScope::class)
            ->select('id')
            ->where('username', 'like', $username . '_' . '%')
            ->where('role', false)
            ->orderBy('id', 'ASC')
            ->get();



        $p = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                DB::raw('count(distinct a.id) as jumlah_input_rumah_sehat'),
                DB::raw('count(distinct b.id ) as jumlah_input_rumah_sakit'),
                DB::raw('count(distinct c.id ) as jumlah_input_puskesmas'),
                DB::raw('count(distinct d.id ) as jumlah_input_klinik'),
                DB::raw('count(distinct e.id ) as jumlah_input_hotel'),
                DB::raw('count(distinct f.id ) as jumlah_input_hotel_melati'),
                DB::raw('count(distinct g.id ) as jumlah_input_kuliner'),
                DB::raw('count(distinct h.id ) as jumlah_input_kolam_renang'),
                DB::raw('count(distinct i.id ) as jumlah_input_pasar'),
                DB::raw('count(distinct j.id ) as jumlah_input_pesantren'),
                DB::raw('count(distinct k.id ) as jumlah_input_sekolah'),
                DB::raw('count(distinct l.id ) as jumlah_input_sab'),
                DB::raw('count(distinct m.id ) as jumlah_input_tempat_ibadah'),
                DB::raw('count(distinct n.id ) as jumlah_input_kesling'),
                DB::raw('count(distinct o.id ) as jumlah_input_dam'),
                DB::raw('count(distinct jb.id ) as jumlah_input_jasa_boga'),
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
            + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
            + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
            + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            ->whereIn('petugas_sippklings.id', $akun)
            ->groupBy('petugas_sippklings.id')
            ->get();

        $kelurahan_id = $petugasKec->id_kelurahan;
        dd($kelurahan_id);

        if ($petugasKec->role == true) {
            $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
        } else {
            if ($kelurahan_id != null || !is_null($kelurahan_id)) {
                $kel = Kelurahan::select('nama')->where('id', $kelurahan_id)->first()->nama;
            } else {
                $kel = "null";
            }
        }
        $kec = Kecamatan::select('nama')->where('id', $petugasKec->id_kecamatan)->first()->nama;


        $last_activity = History::select('waktu_history', 'aktifitas')
            ->where('petugas_id', $p->id)
            ->orderByDesc('waktu_history')
            ->first();

        if ($last_activity != null) {
            $histori = $last_activity->waktu_history;
            $aktifitas = $last_activity->aktifitas;
        } else {
            $histori = null;
            $aktifitas = null;
        }

        $result = [
            'data_petugas' => $p,
            'kelurahan' => $kel,
            'kecamatan' => $kec,
            'last_activity' => $aktifitas,
            'date_last_iput' => $histori
        ];;

        return response()->json([
            'petugas' => $result
        ]);
    }



    public function rewardByKategori($table)
    {

        if ($table == 'rumahsehat') {
            $table = 'rumah_sehats';
        } else if ($table == 'rumahsakit') {
            $table = 'rumah_sakits';
        } else if ($table == 'klinik') {
            $table = 'kliniks';
        } else if ($table == 'dam') {
            $table = 'dam_sipp_klings';
        } else if ($table == 'tempatibadah') {
            $table = 'tempat_ibadahs';
        } else if ($table == 'hotel' ||  $table == 'Hotel') {
            $table = 'hotels';
        } else if ($table == 'hotelmelati') {
            $table = 'hotel_melatis';
        } else if ($table == 'jasaboga') {
            $table = 'jasa_bogas';
        } else  if ($table == 'kuliner') {
            $table = 'kuliners';
        } else if ($table == 'sab') {
            $table = 'sabs';
        } else if ($table == 'pk' || $table == 'pelayanankesling' ||  $table == 'kesling') {
            $table = 'pelayanan_keslings';
        } else if ($table == 'pesantren') {
            $table = 'pesantrens';
        } else if ($table == 'pasar') {
            $table = 'pasars';
        } else  if ($table == 'sekolah') {
            $table = 'sekolahs';
        } else if ($table == 'kolam_renang') {
            $table = "kolam_renangs";
        }

        $reward = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)
            ->leftjoin($table, 'petugas_sippklings.id',  $table . '.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username as username',
                'petugas_sippklings.role as role',
                DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw("count($table.petugas_id) over (partition by petugas_sippklings.id) as total_input")
            )
            ->distinct()
            ->orderByDesc('total_input')
            ->limit(20)
            ->get();


        return response()->json([
            'data' => $reward
        ]);
    }

    public function searchPetugas($key, $kecamatan = "null", $kelurahan = "null", $sort = "null")
    {

        if ($kecamatan == "null") {
            $kecamatan = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        } else {
            $kecamatan = explode(",", $kecamatan);
        }

        if ($kelurahan == "null") {

            $kelurahan = array(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30
            );
        } else {
            $kelurahan = explode(",", $kelurahan);
        }


        switch ($sort) {
            case 1:
                PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
                    ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
                    ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
                    ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
                    ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
                    ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
                    ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
                    ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
                    ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
                    ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
                    ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
                    ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
                    ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
                    ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
                    ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
                    ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
                    ->select(
                        'petugas_sippklings.id as id',
                        'petugas_sippklings.nama as nama',
                        'petugas_sippklings.username as username',
                        'petugas_sippklings.role as role',
                        DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                        DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                        'petugas_sippklings.created_at as tgl_regis',
                        'petugas_sippklings.updated_at as tgl_update',
                        DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                    + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                    + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                    + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
                    )
                    ->where(function ($query) use ($kelurahan, $kecamatan) {
                        $query->when($kelurahan, function ($query) use ($kelurahan) {
                            $query->whereIn('petugas_sippklings.kelurahan_id', $kelurahan);
                        })->when($kecamatan, function ($query) use ($kecamatan) {
                            $query->whereIn('petugas_sippklings.kecamatan', $kecamatan);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('petugas_sippklings.nama', 'ilike', '%' . $key . '%')
                            ->OrWhere('username', 'ilike', '%' . $key . '%');
                    })
                    ->orderBy('total_input', 'desc')
                    ->groupBy('petugas_sippklings.id')
                    ->paginate(20);
                break;
            case 2:
                PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
                    ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
                    ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
                    ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
                    ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
                    ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
                    ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
                    ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
                    ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
                    ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
                    ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
                    ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
                    ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
                    ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
                    ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
                    ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
                    ->select(
                        'petugas_sippklings.id as id',
                        'petugas_sippklings.nama as nama',
                        'petugas_sippklings.username as username',
                        'petugas_sippklings.role as role',
                        DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                        DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                        'petugas_sippklings.created_at as tgl_regis',
                        'petugas_sippklings.updated_at as tgl_update',
                        DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                        + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                        + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                        + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
                    )
                    ->where(function ($query) use ($kelurahan, $kecamatan) {
                        $query->when($kelurahan, function ($query) use ($kelurahan) {
                            $query->whereIn('petugas_sippklings.kelurahan_id', $kelurahan);
                        })->when($kecamatan, function ($query) use ($kecamatan) {
                            $query->whereIn('petugas_sippklings.kecamatan', $kecamatan);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('petugas_sippklings.nama', 'ilike', '%' . $key . '%')
                            ->OrWhere('username', 'ilike', '%' . $key . '%');
                    })
                    ->orderBy('total_input', 'asc')
                    ->groupBy('petugas_sippklings.id')
                    ->paginate(20);
                break;
            case 3:
                PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
                    ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
                    ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
                    ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
                    ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
                    ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
                    ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
                    ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
                    ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
                    ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
                    ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
                    ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
                    ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
                    ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
                    ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
                    ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
                    ->select(
                        'petugas_sippklings.id as id',
                        'petugas_sippklings.nama as nama',
                        'petugas_sippklings.username as username',
                        'petugas_sippklings.role as role',
                        DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                        DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                        'petugas_sippklings.created_at as tgl_regis',
                        'petugas_sippklings.updated_at as tgl_update',
                        DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                        + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                        + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                        + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
                    )
                    ->where(function ($query) use ($kelurahan, $kecamatan) {
                        $query->when($kelurahan, function ($query) use ($kelurahan) {
                            $query->whereIn('petugas_sippklings.kelurahan_id', $kelurahan);
                        })->when($kecamatan, function ($query) use ($kecamatan) {
                            $query->whereIn('petugas_sippklings.kecamatan', $kecamatan);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('petugas_sippklings.nama', 'ilike', '%' . $key . '%')
                            ->OrWhere('username', 'ilike', '%' . $key . '%');
                    })
                    ->orderBy('petugas_sippklings.created_at', 'desc')
                    ->groupBy('petugas_sippklings.id')
                    ->paginate(20);
                break;
            case 4:
                PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
                    ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
                    ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
                    ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
                    ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
                    ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
                    ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
                    ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
                    ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
                    ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
                    ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
                    ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
                    ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
                    ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
                    ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
                    ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
                    ->select(
                        'petugas_sippklings.id as id',
                        'petugas_sippklings.nama as nama',
                        'petugas_sippklings.username as username',
                        'petugas_sippklings.role as role',
                        DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                        DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                        'petugas_sippklings.created_at as tgl_regis',
                        'petugas_sippklings.updated_at as tgl_update',
                        DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                    + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                    + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                    + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
                    )
                    ->where(function ($query) use ($kelurahan, $kecamatan) {
                        $query->when($kelurahan, function ($query) use ($kelurahan) {
                            $query->whereIn('petugas_sippklings.kelurahan_id', $kelurahan);
                        })->when($kecamatan, function ($query) use ($kecamatan) {
                            $query->whereIn('petugas_sippklings.kecamatan', $kecamatan);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('petugas_sippklings.nama', 'ilike', '%' . $key . '%')
                            ->OrWhere('username', 'ilike', '%' . $key . '%');
                    })
                    ->orderBy('petugas_sippklings.created_at', 'asc')
                    ->groupBy('petugas_sippklings.id')
                    ->paginate(20);
                break;
            default:
                break;
        }
    }
}
