<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\TempatIbadah;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\Sekolah;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\Salon;
use App\Models\Moduls\DamSippKling;
use App\Models\Moduls\PelayananKesling;
use App\Http\Controllers\Controller;
use App\Models\KartuKeluarga;
use DB;

class DashboardMapsController extends Controller
{
  public function getCoordinate(){
    $data = RumahSehat::select('id', 'koordinat', 'status', 'total_nilai', 'alamat')
    ->advancedFilterWithoutRtRw();
    $dataWithoutFilter = RumahSehat::select('id', 'koordinat', 'status', 'total_nilai', 'alamat');

    return [
        'filter' => [
            'data' => $data->get(),
            'layak' =>RumahSehat::select('id','koordinat',  'status', 'total_nilai', 'alamat')
                ->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_layak' =>RumahSehat::select('id','koordinat',  'status', 'total_nilai', 'alamat')
                ->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' =>RumahSehat::select('id','koordinat',  'status', 'total_nilai', 'alamat')
                ->advancedFilterWithoutRtRw()->where('status', null)->count()
        ],
        'no-filter' => [
            'data' => $dataWithoutFilter->get(),
            'layak' => RumahSehat::select('id','koordinat',  'status', 'total_nilai', 'alamat')->where('status', true)->count(),
            'tidak_layak' => RumahSehat::select('id', 'koordinat', 'status', 'total_nilai', 'alamat')->where('status', false)->count(),
            'belum_dijamah' => RumahSehat::select('id', 'koordinat', 'status', 'total_nilai', 'alamat')->where('status', null)->count()
        ]
    ];
    
  }

  public function getCoordinates(){
    $data = RumahSehat::select('koordinat', 'id', 'no_rumah', 'nama_kk','status','status_rumah AS kepemilikan','total_nilai','alamat')
    ->advancedFilter()
    // ->limit(100)
    ->paginate(200)
    ->get()
    // ->count()
    ;

    return $data;
  }

  public function countRumahSehat($status){
    $countrs = RumahSehat::select('koordinat', 'id', 'no_rumah','status','status_rumah AS kepemilikan','total_nilai','alamat')
    ->where('status',$status)
    ->advancedFilter()
    ->paginate(200)
    ->get()
    ->count()
    ;
    // dd($countrs);
    return $countrs;
  }

  public function getTpm(){
    $jasaboga = JasaBoga::select('id', 'koordinat', 'alamat', 'nama_pengusaha',  'no_telp', 'status', 'total_nilai')->get();
    $restoran = Kuliner::select('id','koordinat')->get();
    $depotAirMinum;

    return $jasaboga;
  }

  public function getPopulasi(){
    $pop = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')
    ->select(DB::raw('count(*) as total'), 'p.kelurahan_id as kelurahan')
    ->where('status',true)
    ->groupBy('p.kelurahan_id')
    ->orderBy('p.kelurahan_id')
    ->get();

    return $pop;
  }

  public function getJumlahKeluarga(){
    $angkel = KartuKeluarga::join('petugas_sippklings as p','kartu_keluargas.petugas_id','=','p.id')
    ->select(DB::raw('SUM (jumlah_anggota) as populasi'),'p.kelurahan_id as kelurahan')
    ->groupBy('p.kelurahan_id')
    ->orderBy('p.kelurahan_id')
    ->get();

    return $angkel;
  }

  public function getLegendRumahSehat(){
    $lrskc = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when rumah_sehats.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when rumah_sehats.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $lrskl = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when rumah_sehats.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when rumah_sehats.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $lrskc->get(),
      'kelurahan' => $lrskl->get()
    ];
  }

  public function getLegendTempatIbadah(){
    $ltikc = TempatIbadah::join('petugas_sippklings as p','tempat_ibadahs.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when tempat_ibadahs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when tempat_ibadahs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $ltikl = TempatIbadah::join('petugas_sippklings as p','tempat_ibadahs.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when tempat_ibadahs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when tempat_ibadahs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $ltikc->get(),
      'kelurahan' => $ltikl->get()
    ];
  }

  public function getLegendPasar(){
    $lpkc = Pasar::join('petugas_sippklings as p','pasars.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when pasars.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when pasars.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $lpkl = Pasar::join('petugas_sippklings as p','pasar.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when pasars.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when pasars.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $lpkc->get(),
      'kelurahan' => $lpkl->get()
    ];
  }

  public function getLegendSekolah(){
    $lskc = Sekolah::join('petugas_sippklings as p','sekolahs.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when sekolahs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when sekolahs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $lskl = Sekolah::join('petugas_sippklings as p','sekolahs.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when sekolahs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when sekolahs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $lskc->get(),
      'kelurahan' => $lskl->get()
    ];
  }

  public function getLegendPesantren(){
    $lptkc = Pesantren::join('petugas_sippklings as p','pesantrens.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when pesantrens.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when pesantrens.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $lptkl = Pesantren::join('petugas_sippklings as p','pesantrens.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when pesantrens.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when pesantrens.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $lptkc->get(),
      'kelurahan' => $lptkl->get()
    ];
  }

  public function getLegendKolamRenang(){
    $lkrkc = KolamRenang::join('petugas_sippklings as p','kolam_renangs.petugas_id', '=','p.id')
    ->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select('p.kecamatan_id as kecamatan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when kolam_renangs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when kolam_renangs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');
    
    $lkrkl = KolamRenang::join('petugas_sippklings as p','kolam_renangs.petugas_id', '=','p.id')
    ->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select('p.kelurahan_id as kelurahan','k.nama',DB::raw('count(*) as total'),
    DB::raw('sum(case when kolam_renangs.status = true then 1 else 0 end) AS Sehat'),
    DB::raw('sum(case when kolam_renangs.status = false then 1 else 0 end) AS tidaksehat'))
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    return [
      'kecamatan' => $lkrkc->get(),
      'kelurahan' => $lkrkl->get()
    ];
  }

  public function getRumahSehat(){
    $sehat = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select(DB::raw('count(*) as total'), 'p.kelurahan_id as kelurahan', 'k.nama')
    ->where('status',true)
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');

    $sehatkec = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select(DB::raw('count(*) as total'), 'p.kecamatan_id as kecamatan','k.nama')
    ->where('status',true)
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');

    $tidaksehat = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')->join('kelurahans as k','p.kelurahan_id','=',"k.id")
    ->select(DB::raw('count(*) as total'), 'p.kelurahan_id as kelurahan','k.nama')
    ->where('status',false)
    ->groupBy('p.kelurahan_id','k.nama')
    ->orderBy('p.kelurahan_id');
    
    $tidaksehatkec = RumahSehat::join('petugas_sippklings as p','rumah_sehats.petugas_id', '=','p.id')->join('kecamatans as k','p.kecamatan_id','=',"k.id")
    ->select(DB::raw('count(*) as total'), 'p.kecamatan_id as kecamatan','k.nama')
    ->where('status',false)
    ->groupBy('p.kecamatan_id','k.nama')
    ->orderBy('p.kecamatan_id');

    return 
    [
      'sehat' => 
      [
        'kelurahan' => $sehat->get(),
        'kecamatan' => $sehatkec->get()
      ],
      'tidaksehat' => 
      [
        'kelurahan' => $tidaksehat -> get(),
        'kecamatan' => $tidaksehatkec -> get()
      ]
    ];
  }
}
