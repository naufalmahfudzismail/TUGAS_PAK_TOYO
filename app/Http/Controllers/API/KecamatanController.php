<?php

namespace App\Http\Controllers\API;use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $kecamatan = Kecamatan::all();

        return response()->json([
            'status' => 200,
            'data' => $kecamatan
        ]);

    }

    public function getKecamatanForSelectForm(){
        $kecamatan = Kecamatan::select('id AS value', 'nama AS text')->get();
        
        $defaultSelectForm = new \stdClass();
        $defaultSelectForm->value = null;
        $defaultSelectForm->text = "Semua Kecamatan";

        $newKecamatan = array();
        for ($i=0; $i < count($kecamatan) + 1; $i++) { 
            $newKecamatan[$i] = new \stdClass();
            if($i == 0){
                $newKecamatan[$i]->value = $defaultSelectForm->value;
                $newKecamatan[$i]->text = $defaultSelectForm->text;
            } else {
                $newKecamatan[$i]->value = $kecamatan[$i-1]->value;
                $newKecamatan[$i]->text = $kecamatan[$i-1]->text;
            }

        }

        return response()->json([
            'status' => 200,
            'data' => $newKecamatan
        ]);
    }
}
