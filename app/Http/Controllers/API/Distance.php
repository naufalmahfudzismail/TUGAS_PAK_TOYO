<?php


class Distance
{

    public $coor1;
    public $coor2;

    public function __construct($coor1, $coor2)
    {
        $this->coor1 = $coor1;
        $this->coor2 = $coor2;

    }

    public function distance($unit)
    {

        $latlong1 = explode(",", $this->coor1);
        $latlong2 = explode(",", $this->coor2);

        $lat1 = $latlong1[0];
        $lat2 = $latlong2[0];
        $lon1 = $latlong1[1];
        $lon2 = $latlong2[1];

        $theta = $lon1 - $lon2;

        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);

        $dist = rad2deg($dist);


        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
