<?php

namespace App\Http\Controllers\API;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Hash;
use App\Models\User;
use App\Http\Requests;
use Auth;
use Illuminate\Validation\Rule;
use App\Http\Resources\UsersTableResource;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $data = $this->getUsersByRole(Auth::user()->role);

        return response()->json([
            "status" => 200,
            "data" => UsersTableResource::collection($data)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        switch ($request->role) {
            case 2:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => 'required|unique:users|alpha_dash',
                        'password'  => 'required|string',
                        'role' => 'required|integer',
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon'
                    ]
                );
                break;

            case 3:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => 'required|unique:users|alpha_dash',
                        'password'  => 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'digits:16|nullable',
                        'kecamatan' => 'required'
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                        'kecamatan' => 'Kecamatan'
                    ]
                );
                break;

            case 4:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => 'required|unique:users|alpha_dash',
                        'password'  => 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'digits:16|nullable',
                        'kecamatan' => 'required',
                        'kelurahan' => 'required'
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                        'kecamatan' => 'Kecamatan',
                        'kelurahan' => 'Kelurahan'
                    ]
                );
                break;
            default:
                $this->validate($request, 
                    [
                        'nama' => 'required|string',
                        'username' => 'required|unique:users|alpha_dash',
                        'password'  => 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'digits:16|nullable',
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                    ]
                );
        }

        $roleName = "";
        if($request->role == 4){
            $roleName = "UPF ";
        } else if ($request->role == 3){
            $roleName = "UPT ";
        } else {
            $roleName = "OPD ";
        }

        $user = new \App\Models\User;
        $user->nama = $roleName . $request->nama;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->nip = $request->nip;
        $user->alamat = $request->alamat;
        $user->no_telp = $request->no_telp;
        $user->role = $request->role;
        $user->kecamatan_id = $request->kecamatan;
        $user->kelurahan_id = $request->kelurahan;
        $user->created_by = changeIdToUsername(Auth::user()->id);
        
        if($request->role == 4 || $request->role == 3){
            $rootKader = new \App\Models\PetugasSippkling;
            $rootKader->nama = $roleName . $request->nama;
            $rootKader->username = $request->username;
            $rootKader->password = Hash::make($request->password);
            $rootKader->kecamatan_id = $request->kecamatan;
            $rootKader->kelurahan_id = $request->kelurahan;
            $rootKader->role = false;
            $rootKader->created_by = changeIdToUsername(Auth::user()->id);
            $rootKader->save();
        }

        if($request->role == 3){

            $kelurahan = \App\Models\Kelurahan::select('nama','id')->where('kecamatan_id', $request->kecamatan)->get();
            
            for ($i=0; $i < count($kelurahan); $i++) {

                $kader = new \App\Models\PetugasSippkling;
                $kader->nama = $roleName . $request->nama;
                $kader->username = $request->username . '_' . \optimizeUsernameWord($kelurahan[$i]->nama, '');
                $kader->password = Hash::make($request->password);
                $kader->kecamatan_id = $request->kecamatan;
                $kader->kelurahan_id = $kelurahan[$i]->id;
                $kader->role = false;
                $kader->created_by = \changeIdToUsername(Auth::user()->id);
                $kader->save();

            }
        }
        
        if((!$user->save())){
            return  response()->json([
                'message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            return response()->json([
                'message' => 'Good Request',
                'code' => 200,
           ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\App\Models\User $user)
    {
        return new UsersTableResource($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(\App\Models\User $user)
    {
        return new UsersTableResource($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Models\User $user)
    {
        // return $request;
        switch ($request->role) {
            case 2:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => [
                            'required', Rule::unique('users')->ignore($user->id)
                        ],
                        'password'  => $request->password == '' ? '' : 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'nullable|numeric|digits:16',
                        // 'opd' => 'required'
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                        // 'opd' => 'OPD'
                    ]
                );
                break;

            case 3:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => [
                            'required', Rule::unique('users')->ignore($user->id)
                        ],
                        'password'  => $request->password == '' ? '' : 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'nullable|numeric|digits:16',
                        'kecamatan' => 'required'
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                        'kecamatan' => 'Kecamatan'
                    ]
                );
                break;

            case 4:
                $this->validate($request,
                    [
                        'nama' => 'required|string',
                        'username' => [
                            'required', Rule::unique('users')->ignore($user->id)
                        ],
                        'password'  => $request->password == '' ? '' : 'required|string',
                        'role' => 'required|integer',
                        'nip' => 'nullable|numeric|digits:16',
                        'kecamatan' => 'required',
                        'kelurahan' => 'required'
                    ],
                    [],
                    [
                        'nama' => 'Nama',
                        'username' => 'Username',
                        'password' => 'Password',
                        'role' => 'Role',
                        'no_telp' => 'Nomor Telpon',
                        'nip' => 'NIP',
                        'kecamatan' => 'Kecamatan',
                        'kelurahan' => 'Kelurahan'
                    ]
                );
                break;
        }
        
        $updateUser = \App\Models\User::find($user->id);
        
        $updateUser->nama = $request->nama;
        $updateUser->username = $request->username;
        if($request->password != null){
            $updateUser->password = Hash::make($request->password);
        }
        
        $updateUser->kecamatan_id = $request->kecamatan;
        $updateUser->kelurahan_id = $request->kelurahan;
        $updateUser->nip = $request->nip;
        $updateUser->no_telp = $request->no_telp;
        $updateUser->alamat = $request->alamat;
        $updateUser->update_by = Auth::user()->id;
        
        if(!$updateUser->update()){
            return  response()->json([
                'message' => 'Bad Request',
                'code' => 400,
           ]);
        } else {
            return    response()->json([
                'message' => 'OK',
                'code' => 201,
           ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Models\User $user)
    {
        if($user->role == 2){
            $chkData = DB::table('data_user')->get();

            if($chkData != null){
                DB::table('data_user')->where('user_id', $user->id)->delete();
            }
        }

        $dataLogin = Auth::user();

        if($dataLogin != null){
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $user->deleted_by = $usernameLogin;
        $user->save();

        if ($user->delete()) {
            return [
                'message' => 'OK',
                'code' => 204
           ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400
           ];
        }
    }

    /**
     * custom API
     */
    public function getUserByRoleandKecamatan($role, $kecamatan){
        $user = \App\Models\User::where('role', $role)->where('kecamatan', $kecamatan)->get();

        return response()->json($user);
    }

    private function getUsersByRole($role){
        switch($role){
            case 1:
                return \App\Models\User::where('role', '!=', 1)->orderBy('id', 'desc')->get();
            break;
            case 3:
                return \App\Models\User::where('kecamatan_id', Auth::user()->kecamatan_id)
                ->where('role', '!=', 1)
                ->where('role', '!=', 2)
                ->where('id', '!=', Auth::user()->id)
                ->orderBy('id', 'desc')
                ->get();
            break;
            case 4:
                return \App\Models\User::where('kecamatan_id', Auth::user()->kecamatan_id)      
                ->where('kelurahan_id', Auth::user()->kelurahan_id)
                ->where('role', '!=', 1)
                ->where('role', '!=', 2)
                ->where('id', '!=', Auth::user()->id)
                ->orderBy('id', 'desc')
                ->get();  
            break;
            default:
                return \App\Models\User::where('role', '!=', 1)->get();
        }
    }

    public function getJsonToCsv(){
        $user = \App\Http\Resources\UserJsonToCsvResource::collection(\App\Models\User::hideSuperAdmin()->get());

        return $user;
    }
}
