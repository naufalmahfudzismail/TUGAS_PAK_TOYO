<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\KartuKeluarga;
use DB;
use Illuminate\Http\Request;

class KartuKeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \App\Models\KartuKeluarga::all();
        return response()->json([
            'status' => 200,
            'data' => \App\Http\Resources\KartuKeluargaResource::collection($data)
        ]);
    }

    public function getKepadatan(){
        $kecamatan = \App\Models\Kecamatan::all();
        
        return response()->json([
            'status' => 200,
            'dataWithResource' => \App\Http\Resources\KartuKeluargaKepadatanResource::collection($kecamatan),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $create = KartuKeluarga::create(
            $request->all()
        );

        if (!$create) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            return response()->json([
                'error' => false,
                'error_message' => 'OK',
                'code' => 200,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {}

    public function showKKbyNmr($nmr)
    {
        $kk = KartuKeluarga::where('nmr_kk', $nmr)->first();
        if ($kk != null) {
            $status = true;
        } else {
            $status = false;
            $kk = null;
        }

        return response()->json([
            'status' => $status,
            'data' => $kk,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kk = KartuKeluarga::findOrFail($id);
        $update = $kk->update($request->all());

        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            return response()->json([
                'error' => false,
                'error_message' => 'OK',
                'code' => 201,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
