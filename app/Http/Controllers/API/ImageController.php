<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public function objekImage($fileName)
    {
        $path = storage_path() . '/app/public/images/objek/' . $fileName;
        return Response::download($path);
    }


    public function photoProfile($fileName)
    {
        $path = storage_path() . '/app/public/images/profil/' . $fileName;
        return Response::download($path);
    }

    public function downloadManualBook()
    {
        $path = public_path() . '/manualbook/guide_book.pdf';
        return response()->file($path);
    }


    public function listChar()
    {
        $files = File::files(public_path() . '/images/char');

        $name = array();
        foreach ($files as $file) {
            $name[] = $file->getFilename();
        }


        return response()->json(
            [
                'data' => $name
            ]
        );
    }

    public function downloadChar($fileName)
    {
        $path = public_path() . '/images/char/' . $fileName;
        return Response::download($path);
    }
}
