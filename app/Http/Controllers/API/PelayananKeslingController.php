<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;
use App\Models\History;
use App\Models\PelayananKesling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Scopes\KaderFilterScope;
use App\Models\Kelurahan;
use Carbon\Carbon;
use App\Models\Kecamatan;
use App\Models\PetugasSippkling;
use Illuminate\Support\Facades\Auth;


class PelayananKeslingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = PelayananKesling::paginate(20);
        $jumlah_total =  PelayananKesling::count();
        $jumlah_laki_laki =  PelayananKesling::where('pelayanan_keslings.jenis_kelamin', 'L')->count();
        $jumlah_perempuan = PelayananKesling::where('pelayanan_keslings.jenis_kelamin', 'P')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_laki_laki' => $jumlah_laki_laki,
            'total_perempuan' =>  $jumlah_perempuan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, PelayananKesling::rules(false));

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg'
            ]);
            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'kesling_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] =  null;
        }

        $obj = PelayananKesling::create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas = $request->created_by;
            $history->petugas_id = $request->petugas_id;
            $history->data_id = '10';
            $history->objek_id = $obj->id;
            $history->created_by = $request->created_by;
            $history->created_at = $obj->created_at;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 201,
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($pelayananKesling)
    {
        $obj = PelayananKesling::join('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
            ->select(
                'pelayanan_keslings.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from petugas_sippklings as p where p.id = pelayanan_keslings.update_by) as update_by"),
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )

            ->where('pelayanan_keslings.id', $pelayananKesling)
            ->first();

        return response()->json([
            'data' => $obj
        ]);
    }


    public function showByKelurahan($kelurahan)
    {


        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();


        $obj = PelayananKesling::join('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
            ->select('pelayanan_keslings.*', 'p.nama as nama_petugas')
            ->whereIn('p.id', $petugas)
            ->orderByDesc('pelayanan_keslings.waktu')
            ->paginate(20);


        $jumlah_total =  PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_laki_laki =  PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('pelayanan_keslings.jenis_kelamin', 'L')->count();
        $jumlah_perempuan = PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('pelayanan_keslings.jenis_kelamin', 'P')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_laki_laki' => $jumlah_laki_laki,
            'total_perempuan' =>  $jumlah_perempuan
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama',  $kecamatan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kel->id)->get();
        }

        $obj = PelayananKesling::join('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
            ->select('pelayanan_keslings.*', 'p.nama as nama_petugas')
            ->whereIn('p.id', $petugas)
            ->orderByDesc('pelayanan_keslings.waktu')
            ->paginate(20);

        $jumlah_total =  PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_laki_laki =  PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('pelayanan_keslings.jenis_kelamin', 'L')->count();
        $jumlah_perempuan = PelayananKesling::join('petugas_sippklings as  p',  'pelayanan_keslings.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('pelayanan_keslings.jenis_kelamin', 'P')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_laki_laki' => $jumlah_laki_laki,
            'total_perempuan' =>  $jumlah_perempuan
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PelayananKesling  $PelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function edit(PelayananKesling $pelayananKesling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PelayananKesling  $PelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $pelayananKesling)
    {
        $obj = PelayananKesling::findOrFail($pelayananKesling);

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'kesling_' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);

        if (!$update) {
            return   response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by  = $request->update_by;
            $history->created_at = Carbon::now();

            $history->petugas_id = $request->update_by;
            $history->data_id = '10';
            $history->objek_id = $obj->id;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return   response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PelayananKesling  $PelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function destroy($pelayanankesling)
    {
        $dataLogin = Auth::user();

        if ($dataLogin != null) {
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = PelayananKesling::find($pelayanankesling);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $delete = $obj->delete();

        if (!$delete) {
            return  response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '10';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }
}
