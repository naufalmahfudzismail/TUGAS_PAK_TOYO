<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\SearchResource;
use App\Models\Moduls\DamSippKling;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\PelayananKesling;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\Salon;
use App\Models\Moduls\Sekolah;
use App\Models\Moduls\TempatIbadah;
use Illuminate\Http\Request;

class SearchWebController extends Controller
{
    public function searchRS(Request $request)
    {
        $cari = $request->cari;
        $obj = SearchResource::collection(RumahSehat::join('petugas_sippklings', 'rumah_sehats.petugas_id', 'petugas_sippklings.id')->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                ->select('rumah_sehats.id', 'nama_kk', 'nmr_kk', 'waktu', 'petugas_sippklings.kecamatan_id', 'petugas_sippklings.kelurahan_id', 'rt', 'rw', 'no_rumah', 'status', 'alamat', 'rumah_sehats.foto')->where('k.nama_kk', 'ILIKE', '%' . $cari . '%')->get());
        return response()->json(['data' => $obj]);
    }

    public function searchTTU(Request $request)
    {
        $cari = $request->cari;
        $tempatibadah = SearchResource::collection(TempatIbadah::join('petugas_sippklings', 'tempat_ibadahs.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $pasar = SearchResource::collection(Pasar::join('petugas_sippklings', 'pasars.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $sekolah = SearchResource::collection(Sekolah::join('petugas_sippklings', 'sekolahs.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $pesantren = SearchResource::collection(Pesantren::join('petugas_sippklings', 'pesantrens.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $kolamrenang = SearchResource::collection(KolamRenang::join('petugas_sippklings', 'kolam_renangs.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $puskesmas = SearchResource::collection(Puskesmas::join('petugas_sippklings', 'puskesmas.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $rumahsakit = SearchResource::collection(RumahSakit::join('petugas_sippklings', 'rumah_sakits.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $klinik = SearchResource::collection(Klinik::join('petugas_sippklings', 'kliniks.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $hotel = SearchResource::collection(Hotel::join('petugas_sippklings', 'hotels.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $hotelmelati = SearchResource::collection(HotelMelati::join('petugas_sippklings', 'hotel_melatis.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $salon = SearchResource::collection(Salon::join('petugas_sippklings', 'salons.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());

        // $merged = $tempatibadah->merge($pasar)->merge($sekolah)->merge($pesantren); //->merge($kolamrenang)->merge($puskesmas)->merge($rumahsakit)->merge($klinik)->merge($hotel)->merge($hotelmelati)->merge($salon);
        // $tempatibadah->put("tempat-ibadah", $tempatibadah);

        return response()->json([
            'tempatibadah' => $tempatibadah,
            'pasar' => $pasar,
            'sekolah' => $sekolah,
            'pesantren' => $pesantren,
            'kolamrenang' => $kolamrenang,
            'puskesmas' => $puskesmas,
            'rumahsakit' => $rumahsakit,
            'klinik' => $klinik,
            'hotel' => $hotel,
            'hotelmelati' => $hotelmelati,
            'salon' => $salon,
        ]);
    }

    public function searchTPM(Request $request)
    {
        $cari = $request->cari;
        $jasaboga = SearchResource::collection(JasaBoga::join('petugas_sippklings', 'jasa_bogas.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $restoran = SearchResource::collection(Kuliner::join('petugas_sippklings', 'kuliners.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        $dam = SearchResource::collection(DamSippKling::join('petugas_sippklings', 'dam_sipp_klings.petugas_id', 'petugas_sippklings.id')->select()->where('nama_depot', 'ILIKE', '%' . $cari . '%')->get());

        return response()->json([
            'jasaboga' => $jasaboga,
            'restoran' => $restoran,
            'dam' => $dam,
        ]);
    }

    public function searchETC(Request $request)
    {
        $cari = $request->cari;
        $kesling = SearchResource::collection(PelayananKesling::select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());
        // $phbs = SearchResource::collection(Kuliner::join('petugas_sippklings', 'phbs.petugas_id', 'petugas_sippklings.id')->select()->where('nama_tempat', 'ILIKE', '%' . $cari . '%')->get());

        return response()->json([
            'kesling' => $kesling,
            // 'phbs' => $phbs
        ]);
    }

}
