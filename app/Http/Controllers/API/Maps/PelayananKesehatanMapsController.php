<?php

namespace App\Http\Controllers\API\Maps;
use App\Http\Controllers\Controller;
use App\Models\Moduls\PelayananKesling;
use Illuminate\Http\Request;

class PelayananKesehatanMapsController extends Controller
{
    public function getPelayananKesehatan()
    {
        $data = PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', true)->count(),
                'tidak_layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', false)->count(),
                'belum_dijamah' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', null)->count()
            ]
        ];
    }
}
