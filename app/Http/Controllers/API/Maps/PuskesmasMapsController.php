<?php

namespace App\Http\Controllers\API\Maps;


use App\Models\Moduls\Puskesmas;
use Illuminate\Http\Request;

class PuskesmasMapsController extends Controller
{
    public function getPuskesmas()
    {
        $data = Puskesmas::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')
        ->advancedFilterWithoutRtRw();
    $dataWithoutFilter = Puskesmas::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak');

    return [
        'filter' => [
            'data' => $data->get(),
            'layak' =>Puskesmas::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')
                ->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_layak' =>Puskesmas::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')
                ->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' =>Puskesmas::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')
                ->advancedFilterWithoutRtRw()->where('status', null)->count()
        ],
        'no-filter' => [
            'data' => $dataWithoutFilter->get(),
            'layak' => Puskesmas::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')->where('status', true)->count(),
            'tidak_layak' => Puskesmas::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')->where('status', false)->count(),
            'belum_dijamah' => Puskesmas::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','nama_pemimpin','pajak')->where('status', null)->count()
        ]
    ];
    }
}
