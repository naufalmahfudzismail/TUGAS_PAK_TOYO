<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\KolamRenang;
use Illuminate\Http\Request;

class KolamRenangMapsController extends Controller
{
    public function getKolamRenang(){
        $data = KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')->where('status', true)->count(),
                'tidak_layak' =>KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')->where('status', false)->count(),
                'belum_dijamah' =>KolamRenang::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','imb','no_izin_usaha')->where('status', null)->count()
            ]
        ];
    }
}
