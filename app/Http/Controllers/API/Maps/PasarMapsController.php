<?php

namespace App\Http\Controllers\API\Maps;
use App\Models\Moduls\Pasar;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PasarMapsController extends Controller
{
    public function getPasar()
    {
        $data = Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','pajak','total_nilai');

        return [
            'filter' => [
                'data' => $data->get(),
                'sehat' => Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_sehat' => Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' =>Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'sehat' => Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','pajak')->where('status', true)->count(),
                'tidak_sehat' => Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')->where('status', false)->count(),
                'belum_dijamah' => Pasar::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_pengelola','total_nilai','pajak')->where('status', null)->count()
            ]
        ];
    }
}
