<?php

namespace App\Http\Controllers\API\Maps;


use App\Models\Moduls\TempatIbadah;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TempatIbadahMapsController extends Controller
{
    public function getTempatIbadah()
    {
        $data = TempatIbadah::join('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->select('tempat_ibadahs.id', 'tempat_ibadahs.koordinat', 'tempat_ibadahs.nama_tempat', 'tempat_ibadahs.status', 'tempat_ibadahs.total_nilai', 'tempat_ibadahs.alamat', 'tempat_ibadahs.nama_pengurus', 'p.nama as nama_petugas')
            ->advancedFilterWithoutRtRw();

        $dataWithoutFilter = TempatIbadah::join('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
        ->select('tempat_ibadahs.id', 'tempat_ibadahs.koordinat', 'tempat_ibadahs.nama_tempat', 'tempat_ibadahs.status', 'tempat_ibadahs.total_nilai', 'tempat_ibadahs.alamat', 'tempat_ibadahs.nama_pengurus', 'p.nama as nama_petugas');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')->where('status', true)->count(),
                'tidak_layak' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')->where('status', false)->count(),
                'belum_dijamah' => TempatIbadah::select('id', 'koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat', 'nama_pengurus')->where('status', null)->count()
            ]
        ];
    }
}
