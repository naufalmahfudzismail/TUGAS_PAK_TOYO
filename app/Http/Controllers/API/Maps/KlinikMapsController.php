<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\Klinik;
use Illuminate\Http\Request;

class KlinikMapsController extends Controller
{
    public function getKlinik()
    {
        $data = Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')->where('status', true)->count(),
                'tidak_layak' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')->where('status', false)->count(),
                'belum_dijamah' => Klinik::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pimpinan')->where('status', null)->count()
            ]
        ];
    }
}
