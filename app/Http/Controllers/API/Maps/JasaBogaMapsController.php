<?php

namespace App\Http\Controllers\API\Maps;
use App\Http\Controllers\Controller;
use App\Models\Moduls\JasaBoga;
use Illuminate\Http\Request;

class JasaBogaMapsController extends Controller
{
    public function getJasaBoga()
    {
        $data = JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', true)->count(),
                'tidak_layak' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', false)->count(),
                'belum_dijamah' => JasaBoga::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', null)->count()
            ]
        ];
    }
}