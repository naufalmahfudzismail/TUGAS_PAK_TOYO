<?php

namespace App\Http\Controllers\API\Maps;
use App\Http\Controllers\Controller;
use App\Models\Moduls\HotelMelati;
use Illuminate\Http\Request;

class HotelMelatiMapsController extends Controller
{
    public function getHotelMelati()
    {
        $data = HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', true)->count(),
                'tidak_layak' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', false)->count(),
                'belum_dijamah' => HotelMelati::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', null)->count()
            ]
        ];
    }
}
