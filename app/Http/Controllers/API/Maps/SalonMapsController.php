<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\Salon;
use Illuminate\Http\Request;

class SalonMapsController extends Controller
{
    public function getSalon()
    {
        $data = Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')->where('status', true)->count(),
                'tidak_layak' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')->where('status', false)->count(),
                'belum_dijamah' => Salon::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha', 'jumlah_karyawan')->where('status', null)->count()
            ]
        ];
    }
}
