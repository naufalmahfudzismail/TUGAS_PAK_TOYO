<?php

namespace App\Http\Controllers\API\Maps;
use App\Http\Controllers\Controller;
use App\Models\Moduls\DamSippKling;
use Illuminate\Http\Request;

class DepotAirMinumMapsController extends Controller
{
    public function getDepotAirMinum()
    {
        $data = DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', true)->count(),
                'tidak_layak' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', false)->count(),
                'belum_dijamah' => DamSippKling::select('id', 'nama_tempat', 'koordinat', 'status', 'total_nilai', 'alamat', 'imb', 'nama_pemimpin', 'no_izin_usaha')->where('status', null)->count()
            ]
        ];
    }
}
