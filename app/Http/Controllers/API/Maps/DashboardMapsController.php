<?php

namespace App\Http\Controllers\API\Maps;

use Illuminate\Http\Request;
use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\TempatIbadah;
use App\Models\Moduls\Pasar;
use App\Http\Controllers\Controller;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\Salon;
use App\Models\Moduls\Sekolah;

class DashboardMapsController extends Controller
{
  public function getJasaBoga()
  {
    $data = JasaBoga::select('id', 'koordinat', 'nama_tempat', '')->get();
    return $data;
  }

  public function getCoordinates()
  {
    $data = RumahSehat::select('koordinat AS latlng', 'id')->get();

    return $data;
  }
  
  public function getPasar()
  {
    $data = Pasar::advancedFilter()->select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')
      ->get();

    return $data;
  }

  public function getSekolah()
  {
    $data = Sekolah::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getPesantren()
  {
    $data = Sekolah::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getKolamRenang()
  {
    $data = KolamRenang::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getPuskesmas()
  {
    $data = Puskesmas::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getRumahSakit()
  {
    $data = RumahSakit::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getKlinik()
  {
    $data = Klinik::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getHotel()
  {
    $data = Hotel::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getHotelMelati()
  {
    $data = HotelMelati::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }

  public function getSalon()
  {
    $data = Salon::select('id', 'nama_tempat', 'status', 'total_nilai', 'alamat')->get();

    return $data;
  }
}
