<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\PelayananKesling;
use Illuminate\Http\Request;

class PklMapsController extends Controller
{
    public function getPelayanan_Kesling()
    {
        $data = PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' =>PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')->where('status', true)->count(),
                'tidak_layak' => PelayananKesling::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')->where('status', false)->count(),
                'belum_dijamah' => Salon::select('id', 'nama_tempat', 'koordinat', 'status','nama_pkl', 'total_nilai', 'alamat', 'kasus', 'umur')->where('status', null)->count()
            ]
        ];
    }
}
