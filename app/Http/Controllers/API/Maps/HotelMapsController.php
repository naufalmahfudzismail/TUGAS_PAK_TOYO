<?php

namespace App\Http\Controllers\API\Maps;
// namespace App\Http\Controllers;

use App\Models\Moduls\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelMapsController extends Controller
{
    public function getHotel()
    {
        $data = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
            ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas');

        return [
            'filter' => [
                'data' => $data->get(),
                'layak' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_layak' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'layak' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')->where('status', true)->count(),
                'tidak_layak' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')->where('status', false)->count(),
                'belum_dijamah' => Hotel::join('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                    ->select('hotels.id', 'hotels.nama_tempat', 'hotels.koordinat', 'hotels.status', 'hotels.total_nilai', 'hotels.alamat', 'hotels.pajak', 'hotels.nama_pemimpin', 'hotels.no_izin_usaha', 'p.nama as nama_petugas')->where('status', null)->count()
            ]
        ];
    }
}
