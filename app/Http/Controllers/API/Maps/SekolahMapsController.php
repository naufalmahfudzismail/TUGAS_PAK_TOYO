<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\Sekolah;
use Illuminate\Http\Request;

class SekolahMapsController extends Controller
{
    public function getSekolah()
    {
        $data = Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')
            ->advancedFilterWithoutRtRw();
        $dataWithoutFilter = Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan');

        return [
            'filter' => [
                'data' => $data->get(),
                'sehat' => Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')
                    ->advancedFilterWithoutRtRw()->where('status', true)->count(),
                'tidak_sehat' => Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')
                    ->advancedFilterWithoutRtRw()->where('status', false)->count(),
                'belum_dijamah' =>Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')
                    ->advancedFilterWithoutRtRw()->where('status', null)->count()
            ],
            'no-filter' => [
                'data' => $dataWithoutFilter->get(),
                'sehat' => Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')->where('status', true)->count(),
                'tidak_sehat' => Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')->where('status', false)->count(),
                'belum_dijamah' => Sekolah::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','nama_kepala_sekolah','pajak','luas_bagunan')->where('status', null)->count()
            ]
        ];
    }
}
