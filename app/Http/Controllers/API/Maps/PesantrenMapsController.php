<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\Pesantren;
use Illuminate\Http\Request;

class PesantrenMapsController extends Controller
{
    public function getPesantren(){
        $data = Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')
        ->advancedFilterWithoutRtRw();
    $dataWithoutFilter = Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak');

    return [
        'filter' => [
            'data' => $data->get(),
            'layak' =>Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')
                ->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_layak' =>Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')
                ->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' =>Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')
                ->advancedFilterWithoutRtRw()->where('status', null)->count()
        ],
        'no-filter' => [
            'data' => $dataWithoutFilter->get(),
            'layak' => Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')->where('status', true)->count(),
            'tidak_layak' =>Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')->where('status', false)->count(),
            'belum_dijamah' => Pesantren::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','nama_pengelola','jumlah_santri','pajak')->where('status', null)->count()
        ]
    ];

    }
}
