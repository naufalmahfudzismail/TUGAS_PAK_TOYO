<?php

namespace App\Http\Controllers\API\Maps;

use App\Models\Moduls\RumahSakit;
use Illuminate\Http\Request;

class RumahSakitMapsController extends Controller
{
    public function getRumahSakit(){
        $data = RumahSakit::select('id', 'nama_tempat','koordinat', 'status', 'total_nilai', 'alamat','imb')
        ->advancedFilterWithoutRtRw();
    $dataWithoutFilter = RumahSakit::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','imb');

    return [
        'filter' => [
            'data' => $data->get(),
            'layak' =>RumahSakit::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','imb')
                ->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_layak' =>RumahSakit::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','imb')
                ->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' =>RumahSakit::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','imb')
                ->advancedFilterWithoutRtRw()->where('status', null)->count()
        ],
        'no-filter' => [
            'data' => $dataWithoutFilter->get(),
            'layak' => RumahSakit::select('id','koordinat', 'nama_tempat', 'status', 'total_nilai', 'alamat','imb')->where('status', true)->count(),
            'tidak_layak' => RumahSakit::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','imb')->where('status', false)->count(),
            'belum_dijamah' => RumahSakit::select('id', 'koordinat','nama_tempat', 'status', 'total_nilai', 'alamat','imb')->where('status', null)->count()
        ]
    ];
    }
}
