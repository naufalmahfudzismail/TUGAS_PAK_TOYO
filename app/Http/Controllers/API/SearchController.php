<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use App\Models\History;
use Illuminate\Http\Request;
use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\DamSippKling;
use App\Models\Moduls\TempatIbadah;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\Sab;
use App\Models\PelayananKesling;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\Puskesmas;

use App\Models\Moduls\Sekolah;
use App\Models\PetugasSippkling;
use App\Models\Moduls\KolamRenang;
use App\Models\KartuKeluarga;

use App\Models\Kelurahan;
use App\Models\Kecamatan;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Scopes\ModulsFilterScope;
use App\Scopes\KaderFilterScope;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //this query is so old, because the postreSQL version on cpanel wis old to


    public function getRsByFilter($kelurahan, $rt = "null", $rw = "null", $sort)
    {

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();

        if ($rt != "null") {
            $rts = explode(",", $rt);
        } else {
            $rts = array(
                '001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020',
                '021', '022', '023', '024', '025',
                '026', '027', '028', '029', '030'
            );
        }
        if ($rw != "null") {
            $rws =  explode(",", $rw);
        } else {
            $rws =  array(
                '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                '21', '22', '23', '24', '25',
                '26', '27', '28', '29', '30'
            );
        }

        switch ($sort) {
            case 1:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 2:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 3:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 4:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 5:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;

            case 6:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;

            default:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
        }

        $ids = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->select(
                'rumah_sehats.id as id'
            )
            ->whereIn('p.id', $petugas)
            ->where(function ($query) use ($rts, $rws) {
                $query->when($rts, function ($query) use ($rts) {
                    $query->whereIn('rumah_sehats.rt', $rts);
                })->when($rws, function ($query) use ($rws) {
                    $query->whereIn('rumah_sehats.rw', $rws);
                });
            })
            ->get();

        $jumlah_total = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->whereIn('id', $ids)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'true')->whereIn('id', $ids)->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'false')->whereIn('id', $ids)->count();

        return response()->json([
            'data' => $rs,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }



    public function filterSabNull($kelurahan, $rt = "null", $rw = "null", $sort)
    {
        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();

        if ($rt != "null") {
            $rts = explode(",", $rt);
        } else {
            $rts = array(
                '001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020',
                '021', '022', '023', '024', '025',
                '026', '027', '028', '029', '030'
            );
        }
        if ($rw != "null") {
            $rws =  explode(",", $rw);
        } else {
            $rws =  array(
                '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                '21', '22', '23', '24', '25',
                '26', '27', '28', '29', '30'
            );
        }

        switch ($sort) {
            case 1:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 2:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 3:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 4:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 5:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;

            case 6:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;

            default:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
        }

        $ids = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->select(
                'rumah_sehats.id as id'
            )
            ->whereIn('p.id', $petugas)
            ->whereNull(DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id)"))
            ->where(function ($query) use ($rts, $rws) {
                $query->when($rts, function ($query) use ($rts) {
                    $query->whereIn('rumah_sehats.rt', $rts);
                })->when($rws, function ($query) use ($rws) {
                    $query->whereIn('rumah_sehats.rw', $rws);
                });
            })
            ->get();

        $jumlah_total = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->whereIn('id', $ids)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'true')->whereIn('id', $ids)->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'false')->whereIn('id', $ids)->count();


        return response()->json([
            'data' => $rs,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }



    public function searchWithFilter($kelurahan, $key, $rt = "null", $rw = "null", $sort)
    {
        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();

        if ($rt != "null") {
            $rts = explode(",", $rt);
        } else {
            $rts = array(
                '001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020',
                '021', '022', '023', '024', '025',
                '026', '027', '028', '029', '030'
            );
        }
        if ($rw != "null") {
            $rws =  explode(",", $rw);
        } else {
            $rws =  array(
                '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                '21', '22', '23', '24', '25',
                '26', '27', '28', '29', '30'
            );
        }

        switch ($sort) {
            case 1:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where sab.rumah_sehats = sab.id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 2:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)
                    ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 3:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
                break;
            case 4:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'desc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;
            case 5:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;

            case 6:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('status', 'asc')
                    ->orderBy('waktu', 'asc')
                    ->paginate(20);
                break;

            default:
                $rs = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
                    ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')

                    ->select(
                        'rumah_sehats.id as id',
                        DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                        'rumah_sehats.alamat',
                        'rumah_sehats.rt',
                        'rumah_sehats.rw',
                        'rumah_sehats.status',
                        'rumah_sehats.koordinat',
                        'rumah_sehats.updated_at',
                        'rumah_sehats.created_at',
                        'rumah_sehats.waktu as waktu',
                        DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                        DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id")
                    )
                    ->whereIn('p.id', $petugas)
                    ->where(function ($query) use ($rts, $rws) {
                        $query->when($rts, function ($query) use ($rts) {
                            $query->whereIn('rumah_sehats.rt', $rts);
                        })->when($rws, function ($query) use ($rws) {
                            $query->whereIn('rumah_sehats.rw', $rws);
                        });
                    })
                    ->where(function ($query) use ($key) {
                        $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                            ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                            ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
                    })
                    ->groupBy("rumah_sehats.id")
                    ->orderBy('waktu', 'desc')
                    ->paginate(20);
        }

        $ids = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->select(
                'rumah_sehats.id as id'
            )
            ->whereIn('p.id', $petugas)
            ->where(function ($query) use ($rts, $rws) {
                $query->when($rts, function ($query) use ($rts) {
                    $query->whereIn('rumah_sehats.rt', $rts);
                })->when($rws, function ($query) use ($rws) {
                    $query->whereIn('rumah_sehats.rw', $rws);
                });
            })
            ->where(function ($query) use ($key) {
                $query->where('rumah_sehats.no_rumah', 'ilike', '%' . $key . '%')
                    ->OrWhere('rumah_sehats.alamat', 'ilike', '%' . $key . '%')
                    ->OrWhere('k.nama_kk', 'ilike', '%' . $key . '%')
                    ->OrWhere('k.nmr_kk', 'ilike', '%' . $key . '%');
            })
            ->get();


        $jumlah_total = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->whereIn('id', $ids)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'true')->whereIn('id', $ids)->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->where('rumah_sehats.status', 'false')->whereIn('id', $ids)->count();


        return response()->json([
            'data' => $rs,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }


    public function otherSearch($table, $kelurahan, $key, $sort)
    {

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();
        if ($table == 'puskesmas') {
            switch ($sort) {
                case 1:
                    $obj = Puskesmas::leftjoin('petugas_sippklings as p', 'puskesmas.petugas_id', 'p.id')
                        ->select(
                            'puskesmas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('puskesmas.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Puskesmas::leftjoin('petugas_sippklings as p', 'puskesmas.petugas_id', 'p.id')
                        ->select(
                            'puskesmas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('puskesmas.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Puskesmas::leftjoin('petugas_sippklings as p', 'puskesmas.petugas_id', 'p.id')
                        ->select(
                            'puskesmas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('puskesmas.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('puskesmas.alamat', 'ilike', '%' . $key . '%');;
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Puskesmas::leftjoin('petugas_sippklings as p', 'puskesmas.petugas_id', 'p.id')
                ->select(
                    'puskesmas.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('puskesmas.nama_pemimpin', 'ilike', '%' . $key . '%')
                        ->OrWhere('puskesmas.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('puskesmas.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Puskesmas::whereIn('id', $ids)->count();
            $jumlah_sehat = Puskesmas::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Puskesmas::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Puskesmas::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'rumahsakit') {
            switch ($sort) {
                case 1:
                    $obj = RumahSakit::leftjoin('petugas_sippklings as p', 'rumah_sakits.petugas_id', 'p.id')
                        ->select(
                            'rumah_sakits.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('rumah_sakits.penyakit_terbanyak', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = RumahSakit::leftjoin('petugas_sippklings as p', 'rumah_sakits.petugas_id', 'p.id')
                        ->select(
                            'rumah_sakits.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('rumah_sakits.penyakit_terbanyak', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = RumahSakit::leftjoin('petugas_sippklings as p', 'rumah_sakits.petugas_id', 'p.id')
                        ->select(
                            'rumah_sakits.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('rumah_sakits.penyakit_terbanyak', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('rumah_sakits.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = RumahSakit::leftjoin('petugas_sippklings as p', 'rumah_sakits.petugas_id', 'p.id')
                ->select(
                    'rumah_sakits.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('rumah_sakits.penyakit_terbanyak', 'ilike', '%' . $key . '%')
                        ->OrWhere('rumah_sakits.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('rumah_sakits.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = RumahSakit::whereIn('id', $ids)->count();
            $jumlah_sehat = RumahSakit::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = RumahSakit::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = RumahSakit::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'klinik') {
            switch ($sort) {
                case 1:
                    $obj = Klinik::leftjoin('petugas_sippklings as p', 'kliniks.petugas_id', 'p.id')
                        ->select(
                            'kliniks.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kliniks.nama_pimpinan', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Klinik::leftjoin('petugas_sippklings as p', 'kliniks.petugas_id', 'p.id')
                        ->select(
                            'kliniks.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kliniks.nama_pimpinan', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Klinik::leftjoin('petugas_sippklings as p', 'kliniks.petugas_id', 'p.id')
                        ->select(
                            'kliniks.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kliniks.nama_pimpinan', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kliniks.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }
            $ids = Klinik::leftjoin('petugas_sippklings as p', 'kliniks.petugas_id', 'p.id')
                ->select(
                    'kliniks.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('kliniks.nama_pimpinan', 'ilike', '%' . $key . '%')
                        ->OrWhere('kliniks.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('kliniks.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Klinik::whereIn('id', $ids)->count();
            $jumlah_sehat = Klinik::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Klinik::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Klinik::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'dam') {
            switch ($sort) {
                case 1:
                    $obj = DamSippKling::leftjoin('petugas_sippklings as p', 'dam_sipp_klings.petugas_id', 'p.id')
                        ->select(
                            'dam_sipp_klings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('dam_sipp_klings.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.nama_depot', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = DamSippKling::leftjoin('petugas_sippklings as p', 'dam_sipp_klings.petugas_id', 'p.id')
                        ->select(
                            'dam_sipp_klings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('dam_sipp_klings.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.nama_depot', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = DamSippKling::leftjoin('petugas_sippklings as p', 'dam_sipp_klings.petugas_id', 'p.id')
                        ->select(
                            'dam_sipp_klings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('dam_sipp_klings.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.nama_depot', 'ilike', '%' . $key . '%')
                                ->OrWhere('dam_sipp_klings.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = DamSippKling::leftjoin('petugas_sippklings as p', 'dam_sipp_klings.petugas_id', 'p.id')
                ->select(
                    'dam_sipp_klings.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('dam_sipp_klings.nama_pemilik', 'ilike', '%' . $key . '%')
                        ->OrWhere('dam_sipp_klings.nama_depot', 'ilike', '%' . $key . '%')
                        ->OrWhere('dam_sipp_klings.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = DamSippKling::whereIn('id', $ids)->count();
            $jumlah_sehat = DamSippKling::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = DamSippKling::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = DamSippKling::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'tempatibadah') {
            switch ($sort) {
                case 1:
                    $obj = TempatIbadah::leftjoin('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
                        ->select(
                            'tempat_ibadahs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('tempat_ibadahs.alamat', 'ilike', '%' . $key . '%')
                                ->OrWhere('tempat_ibadahs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = TempatIbadah::leftjoin('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
                        ->select(
                            'tempat_ibadahs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('tempat_ibadahs.alamat', 'ilike', '%' . $key . '%')
                                ->OrWhere('tempat_ibadahs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = TempatIbadah::leftjoin('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
                        ->select(
                            'tempat_ibadahs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('tempat_ibadahs.alamat', 'ilike', '%' . $key . '%')
                                ->OrWhere('tempat_ibadahs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = TempatIbadah::leftjoin('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
                ->select(
                    'tempat_ibadahs.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('tempat_ibadahs.alamat', 'ilike', '%' . $key . '%')
                        ->OrWhere('tempat_ibadahs.nama_tempat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = TempatIbadah::whereIn('id', $ids)->count();
            $jumlah_sehat = TempatIbadah::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = TempatIbadah::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = TempatIbadah::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'hotel' ||  $table == 'Hotel') {
            switch ($sort) {
                case 1:
                    $obj = Hotel::leftjoin('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                        ->select(
                            'hotels.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotels.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Hotel::leftjoin('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                        ->select(
                            'hotels.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotels.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Hotel::leftjoin('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                        ->select(
                            'hotels.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotels.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotels.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Hotel::leftjoin('petugas_sippklings as p', 'hotels.petugas_id', 'p.id')
                ->select(
                    'hotels.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('hotels.nama_pemimpin', 'ilike', '%' . $key . '%')
                        ->OrWhere('hotels.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('hotels.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Hotel::whereIn('id', $ids)->count();
            $jumlah_sehat = Hotel::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Hotel::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Hotel::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'hotelmelati') {
            switch ($sort) {
                case 1:
                    $obj = HotelMelati::leftjoin('petugas_sippklings as p', 'hotel_melatis.petugas_id', 'p.id')
                        ->select(
                            'hotel_melatis.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotel_melatis.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = HotelMelati::leftjoin('petugas_sippklings as p', 'hotel_melatis.petugas_id', 'p.id')
                        ->select(
                            'hotel_melatis.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotel_melatis.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = HotelMelati::leftjoin('petugas_sippklings as p', 'hotel_melatis.petugas_id', 'p.id')
                        ->select(
                            'hotel_melatis.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('hotel_melatis.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('hotel_melatis.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = HotelMelati::leftjoin('petugas_sippklings as p', 'hotel_melatis.petugas_id', 'p.id')
                ->select(
                    'hotel_melatis.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('hotel_melatis.nama_pemimpin', 'ilike', '%' . $key . '%')
                        ->OrWhere('hotel_melatis.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('hotel_melatis.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = HotelMelati::whereIn('id', $ids)->count();
            $jumlah_sehat = HotelMelati::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = HotelMelati::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = HotelMelati::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'jasaboga') {
            switch ($sort) {
                case 1:
                    $obj = JasaBoga::leftjoin('petugas_sippklings as p', 'jasa_bogas.petugas_id', 'p.id')
                        ->select(
                            'jasa_bogas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('jasa_bogas.nama_pengusaha', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = JasaBoga::leftjoin('petugas_sippklings as p', 'jasa_bogas.petugas_id', 'p.id')
                        ->select(
                            'jasa_bogas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('jasa_bogas.nama_pengusaha', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = JasaBoga::leftjoin('petugas_sippklings as p', 'jasa_bogas.petugas_id', 'p.id')
                        ->select(
                            'jasa_bogas.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('jasa_bogas.nama_pengusaha', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('jasa_bogas.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = JasaBoga::leftjoin('petugas_sippklings as p', 'jasa_bogas.petugas_id', 'p.id')
                ->select(
                    'jasa_bogas.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('jasa_bogas.nama_pengusaha', 'ilike', '%' . $key . '%')
                        ->OrWhere('jasa_bogas.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('jasa_bogas.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = JasaBoga::whereIn('id', $ids)->count();
            $jumlah_sehat = JasaBoga::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = JasaBoga::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = JasaBoga::whereIn('id', $ids)->whereNull('status')->count();
        } else  if ($table == 'kuliner') {
            switch ($sort) {
                case 1:
                    $obj = Kuliner::leftjoin('petugas_sippklings as p', 'kuliners.petugas_id', 'p.id')
                        ->select(
                            'kuliners.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kuliners.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Kuliner::leftjoin('petugas_sippklings as p', 'kuliners.petugas_id', 'p.id')
                        ->select(
                            'kuliners.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kuliners.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Kuliner::leftjoin('petugas_sippklings as p', 'kuliners.petugas_id', 'p.id')
                        ->select(
                            'kuliners.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kuliners.nama_pemilik', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kuliners.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Kuliner::leftjoin('petugas_sippklings as p', 'kuliners.petugas_id', 'p.id')
                ->select(
                    'kuliners.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('kuliners.nama_pemilik', 'ilike', '%' . $key . '%')
                        ->OrWhere('kuliners.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('kuliners.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Kuliner::whereIn('id', $ids)->count();
            $jumlah_sehat = Kuliner::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Kuliner::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Kuliner::whereIn('id', $ids)->whereNull('status')->count();
        } /*else if ($table == 'sab') {
            switch ($sort) {
                case 1:
                    $obj = Sab::leftjoin('petugas_sippklings as p', 'sab.petugas_id', 'p.id')
                        ->select(
                            'sabs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sabs.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('sabs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Sab::leftjoin('petugas_sippklings as p', 'sab.petugas_id', 'p.id')
                        ->select(
                            'sabs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sabs.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('sabs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Sab::leftjoin('petugas_sippklings as p', 'sab.petugas_id', 'p.id')
                        ->select(
                            'sabs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sabs.nama_pemimpin', 'ilike', '%' . $key . '%')
                                ->OrWhere('sabs.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Sab::leftjoin('petugas_sippklings as p', 'sab.petugas_id', 'p.id')
                ->select(
                    'sabs.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('sabs.nama_pemimpin', 'ilike', '%' . $key . '%')
                        ->OrWhere('sabs.nama_tempat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Sab::whereIn('id', $ids)->count();
            $jumlah_sehat = Sab::where('sabs.status', 'true')->whereIn('id', $ids)->count();
            $jumlah_tidak_sehat = Sab::whereNotNull('sabs.status', '!=', 'true')->whereIn('id', $ids)->count();
            $jumlah_belum_input = Sab::where('sabs.status', 'null')->whereIn('id', $ids)->count();

        } */ else if ($table == 'pk' || $table == 'pelayanankesling' ||  $table == 'kesling') {
            switch ($sort) {
                case 1:
                    $obj = PelayananKesling::leftjoin('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
                        ->select(
                            'pelayanan_keslings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pelayanan_keslings.nama_pkl', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_kasus', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_jenis', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = PelayananKesling::leftjoin('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
                        ->select(
                            'pelayanan_keslings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pelayanan_keslings.nama_pkl', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_kasus', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_jenis', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = PelayananKesling::leftjoin('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
                        ->select(
                            'pelayanan_keslings.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pelayanan_keslings.nama_pkl', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_kasus', 'ilike', '%' . $key . '%')
                                ->OrWhere('pelayanan_keslings.nama_jenis', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = PelayananKesling::leftjoin('petugas_sippklings as p', 'pelayanan_keslings.petugas_id', 'p.id')
                ->select(
                    'pelayanan_keslings.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('pelayanan_keslings.nama_pkl', 'ilike', '%' . $key . '%')
                        ->OrWhere('pelayanan_keslings.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('pelayanan_keslings.nama_kasus', 'ilike', '%' . $key . '%')
                        ->OrWhere('pelayanan_keslings.nama_jenis', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = PelayananKesling::whereIn('id', $ids)->count();
            $jumlah_sehat = PelayananKesling::where('pelayanan_keslings.jenis_kelamin', 'laki-laki')->whereIn('id', $ids)->count();
            $jumlah_tidak_sehat = PelayananKesling::whereNotNull('pelayanan_keslings.jenis_kelamin', '!=', 'laki-laki')->whereIn('id', $ids)->count();
            $jumlah_belum_input = PelayananKesling::whereNull('pelayanan_keslings.kasus')->whereIn('id', $ids)->count();
        } else if ($table == 'pesantren') {
            switch ($sort) {
                case 1:
                    $obj = Pesantren::leftjoin('petugas_sippklings as p', 'pesantrens.petugas_id', 'p.id')
                        ->select(
                            'pesantrens.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pesantrens.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Pesantren::leftjoin('petugas_sippklings as p', 'pesantrens.petugas_id', 'p.id')
                        ->select(
                            'pesantrens.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pesantrens.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Pesantren::leftjoin('petugas_sippklings as p', 'pesantrens.petugas_id', 'p.id')
                        ->select(
                            'pesantrens.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pesantrens.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pesantrens.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Pesantren::leftjoin('petugas_sippklings as p', 'pesantrens.petugas_id', 'p.id')
                ->select(
                    'pesantrens.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('pesantrens.nama_pengelola', 'ilike', '%' . $key . '%')
                        ->OrWhere('pesantrens.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('pesantrens.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Pesantren::whereIn('id', $ids)->count();
            $jumlah_sehat = Pesantren::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Pesantren::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Pesantren::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'pasar') {
            switch ($sort) {
                case 1:
                    $obj = Pasar::leftjoin('petugas_sippklings as p', 'pasars.petugas_id', 'p.id')
                        ->select(
                            'pasars.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pasars.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Pasar::leftjoin('petugas_sippklings as p', 'pasars.petugas_id', 'p.id')
                        ->select(
                            'pasars.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pasars.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Pasar::leftjoin('petugas_sippklings as p', 'pasars.petugas_id', 'p.id')
                        ->select(
                            'pasars.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('pasars.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('pasars.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Pasar::leftjoin('petugas_sippklings as p', 'pasars.petugas_id', 'p.id')
                ->select(
                    'pasars.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('pasars.nama_pengelola', 'ilike', '%' . $key . '%')
                        ->OrWhere('pasars.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('pasars.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Pasar::whereIn('id', $ids)->count();
            $jumlah_sehat = Pasar::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Pasar::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Pasar::whereIn('id', $ids)->whereNull('status')->count();
        } else  if ($table == 'sekolah') {
            switch ($sort) {
                case 1:
                    $obj = Sekolah::leftjoin('petugas_sippklings as p', 'sekolahs.petugas_id', 'p.id')
                        ->select(
                            'sekolah.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sekolah.nama_kepala_sekolah', 'ilike', '%' . $key . '%')
                                ->OrWhere('sekolah.nama_tempat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = Sekolah::leftjoin('petugas_sippklings as p', 'sekolahs.petugas_id', 'p.id')
                        ->select(
                            'sekolah.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sekolah.nama_kepala_sekolah', 'ilike', '%' . $key . '%')
                                ->OrWhere('sekolah.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('sekolah.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = Sekolah::leftjoin('petugas_sippklings as p', 'sekolahs.petugas_id', 'p.id')
                        ->select(
                            'sekolah.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('sekolah.nama_kepala_sekolah', 'ilike', '%' . $key . '%')
                                ->OrWhere('sekolah.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('sekolah.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = Sekolah::leftjoin('petugas_sippklings as p', 'sekolahs.petugas_id', 'p.id')
                ->select(
                    'sekolahs.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('sekolahs.nama_kepala_sekolah', 'ilike', '%' . $key . '%')
                        ->OrWhere('sekolahs.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('sekolah.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = Sekolah::whereIn('id', $ids)->count();
            $jumlah_sehat = Sekolah::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = Sekolah::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = Sekolah::whereIn('id', $ids)->whereNull('status')->count();
        } else if ($table == 'kolamrenang') {
            switch ($sort) {
                case 1:
                    $obj = KolamRenang::leftjoin('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
                        ->select(
                            'kolam_renangs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kolam_renangs.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
                    break;
                case 2:
                    $obj = KolamRenang::leftjoin('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
                        ->select(
                            'kolam_renangs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kolam_renangs.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'asc')
                        ->paginate(20);
                    break;
                default:
                    $obj = KolamRenang::leftjoin('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
                        ->select(
                            'kolam_renangs.*',
                            'p.nama as nama_petugas'
                        )
                        ->whereIn('p.id', $petugas)
                        ->where(function ($query) use ($key) {
                            $query->where('kolam_renangs.nama_pengelola', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.nama_tempat', 'ilike', '%' . $key . '%')
                                ->OrWhere('kolam_renangs.alamat', 'ilike', '%' . $key . '%');
                        })
                        ->orderBy('waktu', 'desc')
                        ->paginate(20);
            }

            $ids = KolamRenang::leftjoin('petugas_sippklings as p', 'kolam_renangs.petugas_id', 'p.id')
                ->select(
                    'kolam_renangs.id as id'
                )
                ->whereIn('p.id', $petugas)
                ->where(function ($query) use ($key) {
                    $query->where('kolam_renangs.nama_pengelola', 'ilike', '%' . $key . '%')
                        ->OrWhere('kolam_renangs.nama_tempat', 'ilike', '%' . $key . '%')
                        ->OrWhere('kolam_renangs.alamat', 'ilike', '%' . $key . '%');
                })
                ->get();

            $jumlah_total = KolamRenang::whereIn('id', $ids)->count();
            $jumlah_sehat = KolamRenang::whereIn('id', $ids)->where('status', 'true')->count();
            $jumlah_tidak_sehat = KolamRenang::whereIn('id', $ids)->where('status', 'false')->count();
            $jumlah_belum_input = KolamRenang::whereIn('id', $ids)->whereNull('status')->count();
        }

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_belum_input
        ]);
    }



    public function searchHistory($petugas, $key)
    {

        $history = History::join('datas as d', 'histories.data_id', 'd.id')
            ->select('histories.*', 'd.nama_data as nama_data')
            ->where('histories.petugas_id', $petugas)
            ->where(function ($query) use ($key) {
                $query->where('nama_data', 'ilike', '%' . $key . '%')
                    ->orWhere('aktifitas', 'ilike', '%' . $key . '%')
                    ->OrWhere('histories.waktu_history', 'ilike', '%' . $key . '%');
            })
            ->orderByDesc('histories.waktu_history')
            ->paginate(20);

        return response()->json([
            'data' => $history
        ]);
    }

    public function searchPetugas($key)
    {
        $data = PetugasSippkling::advancedFilterWithoutStaticClass()
            ->withoutGlobalScope(KaderFilterScope::class)
            ->leftjoin('rumah_sehats as a', 'petugas_sippklings.id', 'a.petugas_id')
            ->leftjoin('rumah_sakits as b', 'petugas_sippklings.id', 'b.petugas_id')
            ->leftjoin('puskesmas as c', 'petugas_sippklings.id', 'c.petugas_id')
            ->leftjoin('kliniks as d', 'petugas_sippklings.id', 'd.petugas_id')
            ->leftjoin('hotels as e', 'petugas_sippklings.id', 'e.petugas_id')
            ->leftjoin('hotel_melatis as f', 'petugas_sippklings.id', 'f.petugas_id')
            ->leftjoin('kuliners as g', 'petugas_sippklings.id', 'g.petugas_id')
            ->leftjoin('kolam_renangs as h', 'petugas_sippklings.id', 'h.petugas_id')
            ->leftjoin('pasars as i', 'petugas_sippklings.id', 'i.petugas_id')
            ->leftjoin('pesantrens as j', 'petugas_sippklings.id', 'j.petugas_id')
            ->leftjoin('sekolahs as k', 'petugas_sippklings.id', 'k.petugas_id')
            ->leftjoin('sabs as l', 'petugas_sippklings.id', 'l.petugas_id')
            ->leftjoin('tempat_ibadahs as m', 'petugas_sippklings.id', 'm.petugas_id')
            ->leftjoin('pelayanan_keslings as n', 'petugas_sippklings.id', 'n.petugas_id')
            ->leftjoin('dam_sipp_klings as o', 'petugas_sippklings.id', 'o.petugas_id')
            ->leftjoin('jasa_bogas as jb', 'petugas_sippklings.id', 'jb.petugas_id')
            ->select(
                'petugas_sippklings.id as id',
                'petugas_sippklings.nama as nama',
                'petugas_sippklings.username as username',
                'petugas_sippklings.role as role',
                DB::raw("(select nama from kelurahans where kelurahans.id = petugas_sippklings.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = petugas_sippklings.kecamatan_id) as kecamatan"),
                'petugas_sippklings.created_at as tgl_regis',
                'petugas_sippklings.updated_at as tgl_update',
                DB::raw('count(distinct a.id) + count(distinct b.id ) + count(distinct c.id )
                    + count(distinct d.id) + count(distinct e.id) + count(distinct f.id) + count(distinct g.id) + count(distinct h.id)
                    + count(distinct i.id) + count(distinct j.id) + count(distinct k.id) + count(distinct l.id) + count(distinct m.id)
                    + count(distinct n.id) + count(distinct o.id) + count(distinct jb.id) as total_input ')
            )
            ->where(function ($query) use ($key) {
                $query->where('petugas_sippklings.nama', 'ilike', '%' . $key . '%')
                    ->OrWhere('petugas_sippklings.username', 'ilike', '%' . $key . '%');
            })
            ->orderByDesc('total_input')
            ->groupBy('petugas_sippklings.id')
            ->get();

        return response()->json([
            'data' => $data
        ]);
    }
}
