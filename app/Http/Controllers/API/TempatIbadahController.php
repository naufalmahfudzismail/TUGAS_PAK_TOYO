<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\Kecamatan;
use App\Models\Moduls\TempatIbadah;
use App\Models\PetugasSippkling;
use App\Scopes\KaderFilterScope;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class TempatIbadahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = TempatIbadah::paginate(20);
        $jumlah_total = TempatIbadah::count();
        $jumlah_sehat = TempatIbadah::where('status', 'true')->count();
        $jumlah_tidak_sehat = TempatIbadah::where('status', 'false')->count();
        $jumlah_unkown = TempatIbadah::whereNull('status')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg',
            ]);
            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'tempatIbadah_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] = null;
        }

        $obj = TempatIbadah::create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas = $request->created_by;
            $history->petugas_id = $request->petugas_id;
            $history->data_id = '24';
            $history->objek_id = $obj->id;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($tempatibadah)
    {
        $obj = TempatIbadah::join('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->select(
                'tempat_ibadahs.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from petugas_sippklings as p where p.id = tempat_ibadahs.update_by) as update_by"),
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )

            ->where('tempat_ibadahs.id', $tempatibadah)
            ->first();

        return response()->json([
            'data' => $obj,
        ]);
    }

    public function showByKelurahan($kelurahan)
    {

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();
        $obj = TempatIbadah::join('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->select(
                'tempat_ibadahs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('tempat_ibadahs.waktu')
            ->paginate(20);

        $jumlah_total = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('status', 'true')->count();
        $jumlah_tidak_sehat = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('status', 'false')->count();
        $jumlah_unkown = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('status')
            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama', $kecamatan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kel->id)->get();
        }

        $obj = TempatIbadah::join('petugas_sippklings as p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->select(
                'tempat_ibadahs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('tempat_ibadahs.waktu')
            ->paginate(20);

        $jumlah_total = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('tempat_ibadahs.status', 'true')->count();
        $jumlah_tidak_sehat = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')->whereIn('p.id', $petugas)->where('tempat_ibadahs.status', 'false')->count();
        $jumlah_unkown = TempatIbadah::join('petugas_sippklings as  p', 'tempat_ibadahs.petugas_id', 'p.id')
            ->whereIn('p.id', $petugas)
            ->whereNull('status')

            ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown,
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TempatIbadah  $TempatIbadah
     * @return \Illuminate\Http\Response
     */
    public function edit(TempatIbadah $tempatibadah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TempatIbadah  $TempatIbadah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tempatibadah)
    {
        $obj = TempatIbadah::findOrFail($tempatibadah);

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'tempat_ibadah' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);

        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by = $request->update_by;
            $history->created_at = Carbon::now();

            $history->petugas_id = $request->update_by;
            $history->data_id = '24';
            $history->objek_id = $obj->id;

            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TempatIbadah  $TempatIbadah
     * @return \Illuminate\Http\Response
     */
    public function destroy($tempatibadah)
    {
        $dataLogin = Auth::user();

        if ($dataLogin != null) {
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = TempatIbadah::find($tempatibadah);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $delete = $obj->delete();

        if (!$delete) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '24';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories = $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    public function importCsv(Request $request)
    {
        try {
            $getArraySuccess = [];

            $validate = $request->validate(
                [
                    'csv.*.nama_tempat' => 'required|string',
                    'csv.*.nama_pengurus' => 'required|string',
                    'csv.*.alamat' => 'required|string',
                    'csv.*.jumlah_jamaah' => 'required|string',
                    'csv.*.no_telp' => 'required|string',
                    'csv.*.jam_operasional' => 'required|string',
                    'csv.*.kecamatan_id' => 'required|integer',
                    'csv.*.kelurahan_id' => 'required|integer',
                ]
            );

            $data = $request->csv;

            // if ($data['false'] != null) {

            //     return [
            //         'status_code' => 1000,
            //         'message' => "Terjadi kesalahan",
            //         'data' => $data['false'],
            //     ];

            // } else {
            for ($i = 0; $i < count($data); $i++) {

                $item = new \App\Models\Moduls\TempatIbadah;
                $item->nama_tempat = $data[$i]['nama_tempat'];
                $item->nama_pengurus = $data[$i]['nama_pengurus'];
                $item->alamat = $data[$i]['alamat'];
                $item->jumlah_jamaah = $data[$i]['jumlah_jamaah'];
                $item->no_telp = $data[$i]['no_telp'];
                $item->jam_operasional = $data[$i]['jam_operasional'];
                $kecamatan_id = $data[$i]['kecamatan_id'];
                $kelurahan_id = $data[$i]['kelurahan_id'];

                $item->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $item->created_by = \changeIdToUsername(Auth::user()->id);
                $item->waktu = Carbon::now();
                $item->save();

                $history = new History();
                $history->aktifitas = 'input';
                $history->waktu_history = Carbon::now();

                $history->tipe_petugas = 'petugas';
                $history->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
                $history->data_id = '24';
                $history->objek_id = $item->id;
                $history->created_by = \changeIdToUsername(Auth::user()->id);
                $history->created_at = $item->created_at;
                $history->key_histories = $item->petugas_id . '-' . $history->data_id . '-' . $item->id . '-' . $history->id;

                $save = $history->save();

            }

            return [
                'status_code' => 200,
                'message' => "Import csv berhasil dilakukan",
            ];

            // }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function storeweb(Request $request)
    {
        // $requestData = $request->all();

        $item = new \App\Models\Moduls\TempatIbadah;

        $item->nama_tempat = $request->nama_tempat;
        $item->nama_pengurus = $request->nama_pengurus;
        $item->alamat = $request->alamat;
        $item->jumlah_jamaah = $request->jumlah_jamaah;
        $item->no_telp = $request->no_telp;
        $item->jam_operasional = $request->jam_operasional;

        $kecamatan_id = $request->kecamatan;
        $kelurahan_id = $request->kelurahan;

        $item->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
        $item->created_by = Auth::user()->username;
        $item->waktu = Carbon::now();
        $item->save();
        // $obj = TempatIbadah::create($requestData);

        $history = new History();
        $history->aktifitas = 'input';
        $history->waktu_history = Carbon::now();

        $history->tipe_petugas = 'petugas';
        $history->petugas_id = PetugasSippkling::select('id')->where('kecamatan_id', $kecamatan_id)->where('kelurahan_id', $kelurahan_id)->where('role', 'false')->where('nama', 'admin')->first()->id;
        $history->data_id = '24';
        $history->objek_id = $item->id;
        $history->created_by = Auth::user()->username;
        $history->created_at = $item->created_at;
        $history->key_histories = $item->petugas_id . '-' . $history->data_id . '-' . $item->id . '-' . $history->id;

        $save = $history->save();
        if ($save) {
            return response()->json([
                'error' => false,
                'error_message' => 'OK',
                'code' => 200,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'error_message' => 'activity cannot be tracked',
                'code' => 400,
            ]);
        }

    }
}
