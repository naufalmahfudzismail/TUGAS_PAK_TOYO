<?php

namespace App\Http\Controllers\API;

use App\Models\Opd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OpdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataOpd = \App\Models\User::select('id','nama')->where('role', 2)->get();

        return [
            'status' => 200,
            'data' => $dataOpd
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $opd = new \App\Models\Opd;
        $opd->nama = $request->nama;

        if ($opd->save()) {
            return response()->json([
                'error' => false,
                'error_message' => 'created',
                'code' => 201,
            ]);
        } else {
            return response()->json([
                'error' => true,
                'error_message' => 'error',
                'code' => 400,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Opd  $opd
     * @return \Illuminate\Http\Response
     */
    public function show(Opd $opd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Opd  $opd
     * @return \Illuminate\Http\Response
     */
    public function edit(Opd $opd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Opd  $opd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opd $opd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Opd  $opd
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opd $opd)
    {
        //
    }

    public function listOpdRadioGroupSelected(){
        try {
            $availableModule = [
                [ "id" => 36 ],
                [ "id" => 24 ],
                [ "id" => 15 ],
                [ "id" => 6 ],
                [ "id" => 7 ],
                [ "id" => 43 ],
                [ "id" => 4 ],
                [ "id" => 2 ],
                [ "id" => 3 ],
                [ "id" => 8 ],
                [ "id" => 9 ],
                [ "id" => 46 ],
                [ "id" => 40 ],
                [ "id" => 44 ],
                [ "id" => 42 ],
                [ "id" => 10 ],
                [ "id" => 45 ]
            ];
    
            $finalArray = [];
    
            for ($i=0; $i < count($availableModule); $i++) { 
                $dataOpd = DB::table('data_user')->select('user_id')->where('data_id', @$availableModule[$i]['id'])->get(); 
    
                $finalArray[$i] = $dataOpd;
                if($dataOpd != null){
                    $opd = [];
                    for ($j=0; $j < count($dataOpd); $j++) { 
                        $opd[$j] = $dataOpd[$j]->user_id;
                    }
                    $finalArray[$i] = $opd;
                }
            }
            
            return $finalArray;
        } catch (\Throwable $th) {
            return [
                "gagal"
            ];
        }
    }

    public function changeOpdModulStatus($user_id, $data_id){
        $chkUserModulRelation = DB::table('data_user')
        ->where('data_id', $data_id)
        ->where('user_id', $user_id)
        ->get();

        if(count($chkUserModulRelation) == 0){
            
            try {

                DB::table('data_user')->insert([
                    'user_id' => $user_id,
                    'data_id' => $data_id
                ]);

                    return [
                        'status' => 200,
                        'data' => 'data berhasil diperbaharui1'
                    ];

            } catch (\Throwable $th) {

                throw $th;
            
            }

        } else {

            try {
            
                DB::table('data_user')
                    ->where('data_id', $data_id)
                    ->where('user_id', $user_id)
                    ->delete();

                return [
                    'status' => 200,
                    'data' => 'data berhasil diperbaharui2'
                ];
            
            } catch (\Throwable $th) {
                
                throw $th;
                
            }
        }
    }

    public function getOpdByData($data_id){
        try {
            //code...
            $getModul = \App\Http\Resources\PenyesuaianDataResource::collection(DB::table('data_user')->where('data_id', $data_id)->get());
        
            return [
                'status' => 200,
                'data' => $getModul
            ];
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
