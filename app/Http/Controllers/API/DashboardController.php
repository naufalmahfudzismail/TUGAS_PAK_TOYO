<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Support\Modules\RumahSehat;
use App\Support\Modules\GeneralInformation;
use App\Support\Modules\Lainnya;
use App\Support\Modules\TempatUmum;
use App\Support\Modules\TempatPengelolahanMakanan;
use App\Support\Modules\KartuKeluarga;


class DashboardController extends Controller
{
    protected $rs, $general, $lainnya, $tempat_umum, $tempat_pengelolahan_makanan, $kartukeluarga;
    public function __construct(RumahSehat $rs, GeneralInformation $general, Lainnya $lainnya, TempatUmum $tempat_umum, TempatPengelolahanMakanan $tempat_pengelolahan_makanan, KartuKeluarga $kartukeluarga)
    {
        $this->rs = $rs;
        $this->general = $general;
        $this->lainnya = $lainnya;
        $this->tempat_umum = $tempat_umum;
        $this->tempat_pengelolahan_makanan = $tempat_pengelolahan_makanan;
        $this->kartukeluarga = $kartukeluarga;
    }

    public function index(){
        return [
            'data' => [
                'general' => $this->general->index()
            ]
        ];
    }

    public function getModulesData($kategori){
        $data = [];
        if($kategori == 'all'){
            try {
                return [
                    'data' => [
                        'general' => $this->general->index(),
                        'rumah_sehat' => $this->rs->index(),
                        'tempat_umum' => $this->tempat_umum->index(),
                        'tempat_pengelolahan_makanan' => $this->tempat_pengelolahan_makanan->index(),
                        'lainnya' => $this->lainnya->index()
                    ]
                ];
            } catch (\Throwable $th) {
                throw $th;
            }
        }

        if($kategori == 'rs'){
            $data = array_merge($data, [
                $this->rs->index()
            ]);
        }

        if($kategori == 'ttu'){
            $data = array_merge($data, [
                $this->tempat_umum->index()
            ]);
        }

        if($kategori == 'tpm'){
            $data = array_merge($data, [
                $this->tempat_pengelolahan_makanan->index()
            ]);
        }

        if($kategori == 'lainnya'){
            $data = array_merge($data, [
                $this->lainnya->index()
            ]);
        }

        try {

            return [
                'status' => 200,
                'data' => $data[0]
            ];

        } catch (\Throwable $th) {

            throw $th;
            
        }

    }

    public function getModulesDataSatuan($kategori, $modules){
        if($kategori == 'rs'){

            try {

                return [
                    'status' => 200,
                    'data' => $this->rs->getRumahSehatChild($modules)
                ];

            } catch (\Throwable $th) {
                
                return [
                    'status' => 'error',
                    'data' => $th
                ];
            
            }

        } else if ($kategori == 'ttu'){

            try {

                return [
                    'status' => 200,
                    'data' => $this->tempat_umum->getTempatUmumChild($modules)
                ];

            } catch (\Throwable $th) {
                
                return [
                    'status' => 'error',
                    'data' => $th
                ];
            
            }

        } else if ($kategori == 'tpm'){

            try {

                return [
                    'status' => 200,
                    'data' => $this->tempat_pengelolahan_makanan->getTempatMakananUmumChild($modules)
                ];

            } catch (\Throwable $th) {
                
                return [
                    'status' => 'error',
                    'data' => $th
                ];
            
            }

        } else if ($kategori == 'lainnya'){

            try {

                return [
                    'status' => 200,
                    'data' => $this->lainnya->getLainnyaChild($modules)
                ];

            } catch (\Throwable $th) {
                
                return [
                    'status' => 'error',
                    'data' => $th
                ];
            
            }

        } else {

            try {
                
                return [
                    'status' => 200,
                    'data' => $this->kartukeluarga->getKartuKeluargaChild()
                ];

            } catch (\Throwable $th) {
                return [
                    'status' => 'error',
                    'data' => $th
                ];
            }

        }

    }

    public function getGrafikTabelData($kategori){
        
        if($kategori == 'rs'){

            $query = true;

            return [
                'data' => [
                    'general' => $this->rs->general($query)
                ]
            ];
        
        }

    }

    public function getGrafikStatsPajak($kategori, $modul, $grafik){
    
        if($kategori == 0 && $modul == 0){

            if($grafik == 'line'){
                
                $labels = [0, \Carbon\Carbon::now()->year];
            
            } else {

                $labels = $this->general->payloadParamFilterStructure(null, false);
            
            }

        } else {

            if($grafik == 'line'){
            
                $getClass = $this->general->getClass($kategori, $modul);

                $labels = $this->general->payloadParamFilterStructure($getClass, true);

                if($labels == null){
            
                    $labels = [0, \Carbon\Carbon::now()->year];
                
                }
            
            } else {
                
                $labels = $this->general->payloadParamFilterStructure(null, false);
            
            }
            
        }

        $datasets = $this->general->getDatasets($labels, $grafik);
        
        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => "Total Objek yang Memiliki IMB",
                        'backgroundColor'=> "#20a8d8",
                        'fill' => false,
                        'borderColor' => 'rgba(32, 168, 216, 0.77)',
                        'borderWidth' => 7,
                        'data' => $datasets
                    ]
                ]
            ]
        ]);
    }

    public function getGrafikStats($kategori, $modul, $grafik){
        $getClass = $this->general->getClass($kategori, $modul);

        if($grafik == 'bar'){
            
            $labels = $this->general->payloadParamFilterStructure(null, false);
        
        } else {
            
            $labels = $this->general->payloadParamFilterStructure($getClass, true);
            
            if($labels == null){

                $labels = [0, \Carbon\Carbon::now()->year];

            }
        
        }

        if($kategori == 'rs'){

            return $this->general->uniqueDatasetsRumahSehat($modul, $labels, $grafik);

        } else {

            $datasets = $this->general->getDatasetsStats($kategori, $modul, $getClass, $labels, $grafik);
        
            return response()->json([
                'data' => [
                    'labels' => $labels,
                    'datasets' => [
                        [
                            'label' => "Sehat",
                            'backgroundColor'=> "#329059",
                            'fill' => false,
                            'borderColor' => 'rgba(50, 144, 89, 0.96)',
                            'borderWidth' => 7,
                            'data' => $datasets['sehat']
                        ],
                        [
                            'label' => "Tidak Sehat",
                            'backgroundColor'=> "#f86c6b",
                            'fill' => false,
                            'borderColor' => 'rgba(255,99,132,0.2)',
                            'borderWidth' => 7,
                            'data' => $datasets['tidak_sehat']
                        ]
                    ]
                ]
            ]);
        }
    }

    public function getGrafikStructure(){
        return [
            'data' => $this->general->payloadParamFilterStructure()
        ];
    }

    public function getGrafikDashboardUtama(){
        $labels = ['Rumah Sehat', 'Pasar', 'Sekolah', 'Pesantren', 'Kolam Renang', 'Puskesmas', 'Rumah Sakit', 'Klinik', 'Hotel', 'Hotel Melati', 'Salon', 'Jasa Boga', 'Restoran', 'Depot Air Minum'];
        
        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => "Total Angka Perpajakan",
                        'backgroundColor'=> "#f86c6b",
                        'data' => $this->general->getTotalBayarPajak(true)
                    ]
                ]
            ]
        ]);
    }

    public function getTotalPajakImb($kategori, $modul){
        return response()->json([
            'data' => [
                'imb' => $this->general->getImbPajakWithKey('imb', $kategori, $modul),
                'pajak' => $this->general->getImbPajakWithKey('pajak', $kategori, $modul)
            ]
        ]);
    }

    public function getGeneralModuleData($kategori, $modul){
        $getStaticClass = $this->general->getClass($kategori, $modul);
        $getResourcesClass = $this->general->getResourcesClass($kategori, $modul);


        $latestTime = $getStaticClass::advancedFilterWithoutRtRw()->latest('waktu')->select('waktu')->first();
        
        if($latestTime != null){
            $latestTime = $getStaticClass::advancedFilterWithoutRtRw()->latest('waktu')->select('waktu')->first()->waktu;
            $date = date('Y-m-d', strtotime($latestTime));
        } else {
            $date = "";
            $latestTime = 0;
        }

        return [
            'data' => [
                'last_update' => $latestTime,
                'query' => $getResourcesClass::collection($getStaticClass::advancedFilterWithoutRtRw()->orderBy('id', 'desc')->get())
            ]
        ];

    }

    public function getSekolahTerbesar(){
        return $this->general->getSekolahTerbesar();
    }

    public function getGrafikStatsJumlah(){
        $labels = $this->general->getAllModules();
        $datasets = $this->general->getCountAllModule();

        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => "Jumlah Data Per Modul",
                        'backgroundColor'=> "#f86c6b",
                        'data' => $datasets
                    ]
                ]
            ]
        ]);
    }

    public function generateCountModule(){
        $data = [
            "data" => [
                [
                    "data_id" => 1000,
                    "total" => \App\Models\KartuKeluarga::advancedFilter()->autoFilterByRole()->sum('jumlah_laki_laki'),
                ],
                [
                    "data_id" => 1001,
                    "total" => \App\Models\KartuKeluarga::advancedFilter()->autoFilterByRole()->sum('jumlah_perempuan'),
                ],
                [
                    "data_id" => 36,
                    "total" => \App\Models\Moduls\RumahSehat::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 24,
                    "total" => \App\Models\Moduls\TempatIbadah::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 15,
                    "total" => \App\Models\Moduls\Pasar::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 6,
                    "total" => \App\Models\Moduls\Sekolah::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 43,
                    "total" => \App\Models\Moduls\KolamRenang::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 4,
                    "total" => \App\Models\Moduls\Puskesmas::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 2,
                    "total" => \App\Models\Moduls\RumahSakit::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 3,
                    "total" => \App\Models\Moduls\Klinik::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 8,
                    "total" => \App\Models\Moduls\Hotel::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 9,
                    "total" => \App\Models\Moduls\HotelMelati::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 45,
                    "total" => \App\Models\Moduls\Salon::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 40,
                    "total" => \App\Models\Moduls\JasaBoga::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 44,
                    "total" => \App\Models\Moduls\Kuliner::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 42,
                    "total" => \App\Models\Moduls\DamSippKling::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 10,
                    "total" => \App\Models\Moduls\PelayananKesling::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 11,
                    "total" => \App\Models\Moduls\Sab::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                ],
                [
                    "data_id" => 46,
                    // "total" => \App\Models\Moduls\PelayananKesling::advancedFilterWithoutRtRw()->autoFilterByRole()->count(),
                    "total" => 0
                ]
            ]
        ];

        return $data;
    }

    public function dataOpdAccessModule(){
        $data = \App\Http\Resources\OpdAccessModuleResource::collection(DB::table('data_user')->select('id', 'data_id', 'user_id')->get());

        return [
            'data' => $data
        ];
    }
}
