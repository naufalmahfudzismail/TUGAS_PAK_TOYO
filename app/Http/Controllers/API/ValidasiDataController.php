<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class ValidasiDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataValidasi = \App\Http\Resources\ValidasiDataResource::collection(
            \App\Models\PetugasSippkling::select('id', 'username', 'kelurahan_id', DB::raw('(select count(*) from rumah_sehats where rumah_sehats.petugas_id = petugas_sippklings.id) as total'))
                ->where('username', 'NOT LIKE', '%admin%')
                ->where('kelurahan_id', '!=', null)
                ->where(DB::raw('(select count(*) from rumah_sehats where rumah_sehats.petugas_id = petugas_sippklings.id)'), '>=', 200)
                ->get()
            );
        
        $dataPerluDiPerhatikan = \App\Http\Resources\ValidasiDataKaderPerluDiPerhatikanResource::collection(
            \App\Models\PetugasSippkling::select('id','username', 'kelurahan_id')
            ->where('username', 'NOT LIKE', '%admin%')
            ->where('kelurahan_id', '!=', null)
            ->where(DB::raw('(select count(*) from rumah_sehats where rumah_sehats.petugas_id = petugas_sippklings.id)'), '<=', 30)
            // ->limit(200)
            ->get()
        );
        
        return [
            'data' => [
                'validasi' => $dataValidasi,
                'watch' => $dataPerluDiPerhatikan
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
