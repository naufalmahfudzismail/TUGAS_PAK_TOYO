<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;
use App\Models\History;
use App\Models\Moduls\Sab;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Kelurahan;
use App\Scopes\KaderFilterScope;
use App\Models\Kecamatan;
use App\Models\PetugasSippkling;
use Carbon\Carbon;

class SabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obj = Sab::paginate(20);
        $jumlah_total =  Sab::count();
        $jumlah_sehat =  Sab::where('status', 'true')->count();
        $jumlah_tidak_sehat = Sab::where('status', 'false')->count();
        $jumlah_unkown =  Sab::whereNull('status')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, Sab::rules(false));

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg'
            ]);

            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'sab_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] =  null;
        }


        $obj = Sab::create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas = $requestData["created_by"];
            $history->petugas_id = $request->petugas_id;
            $history->data_id = '11';
            $history->objek_id = $obj->id;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($sab)
    {
        $obj = Sab::join('petugas_sippklings as p', 'sabs.petugas_id', 'p.id')
            ->select(
                'sabs.*',
                'p.nama as nama_petugas',
                DB::raw("(select nama from petugas_sippklings as p where p.id = sabs.update_by) as update_by"),
                DB::raw("(select nama from kelurahans where kelurahans.id = p.kelurahan_id) as kelurahan"),
                DB::raw("(select nama from kecamatans where kecamatans.id = p.kecamatan_id) as kecamatan")
            )

            ->where('sabs.id', $sab)
            ->first();

        return response()->json([
            'data' => $obj
        ]);
    }


    public function showByKelurahan($kelurahan)
    {


        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kelurahan_id', '=', $kelurahan)->get();

        $obj = Sab::join('petugas_sippklings as p', 'sabs.petugas_id', 'p.id')
            ->select(
                'sabs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('sabs.waktu')
            ->paginate(20);



        $jumlah_total =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('sabs.status', 'true')->count();
        $jumlah_tidak_sehat = Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('sabs.status', 'false')->count();
        $jumlah_unkown =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')
        ->whereIn('p.id', $petugas)
        ->whereNull('status')
       
        ->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama',  $kecamatan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kel->id)->get();
        }

        $obj = Sab::join('petugas_sippklings as p', 'sabs.petugas_id', 'p.id')
            ->select(
                'sabs.*',
                'p.nama as nama_petugas'
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('sabs.waktu')
            ->paginate(20);


        $jumlah_total =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('sabs.status', 'true')->count();
        $jumlah_tidak_sehat = Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('sabs.status', 'false')->count();
        $jumlah_unkown =  Sab::join('petugas_sippklings as  p',  'sabs.petugas_id',  'p.id')->whereIn('p.id', $petugas)->where('sabs.status', 'null')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat,
            'total_belum_input' => $jumlah_unkown
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sab  $Sab
     * @return \Illuminate\Http\Response
     */
    public function edit(Sab $sab)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sab  $Sab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sab)
    {
        $obj = Sab::findOrFail($sab);

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'sab_' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }
        $update = $obj->update($requestData);

        if (!$update) {
            return  response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by  = $request->update_by;
            $history->created_at = Carbon::now();

            $history->petugas_id = $request->update_by;
            $history->data_id = '11';
            $history->objek_id = $obj->id;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return   response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                ]);
            } else {
                return   response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sab  $Sab
     * @return \Illuminate\Http\Response
     */
    public function destroy($sab)
    {
        $obj = Sab::findOrFail($sab);
        $delete = $obj->delete();

        if (!$delete) {
            return   response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '11';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return   response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }
}
