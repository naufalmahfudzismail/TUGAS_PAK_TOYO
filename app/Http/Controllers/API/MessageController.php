<?php

namespace App\Http\Controllers\API;

use Illuminate\Events\Dispatcher;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Notifications\Notification;
use Pusher\PushNotifications\PushNotifications;

// use App\MessagesEvents\MessageCreated;
// use App\MessagesEvents\MessageRemoved;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    protected $beams;

    public function __construct(PushNotifications $beams, Dispatcher $events)
    {
        $this->beams = $beams;
        $this->events = $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if (Auth::user()->role == 1) {
                $data = \App\Models\Message::all();

                return [
                    'status' => 200,
                    'data' => $data
                ];
            } else {
                $data = \App\Models\Message::where('sender_id', Auth::user()->id)->get();

                return [
                    'status' => 200,
                    'data' => $data
                ];
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->private == false) {
            $this->validate(
                $request,
                [
                    'subject' => 'required',
                    'message' => 'required',
                    'sender_id' => 'required',
                    'private' => 'required'
                ],
                [],
                [
                    'subject' => 'Subjek',
                    'message' => 'Pesan',
                    'sender_id' => 'Pengirim'
                ]
            );
        } else {
            $this->validate(
                $request,
                [
                    'subject' => 'required',
                    'message' => 'required',
                    'private' => 'required'
                ],
                [],
                [
                    'subject' => 'Subjek',
                    'message' => 'Pesan',
                    'sender_id' => 'Pengirim'
                ]
            );
        }

        $newMessage = new \App\Models\Message;
        $newMessage->subject = $request->subject;
        $newMessage->message = $request->message;
        $newMessage->private = $request->private;
        $newMessage->sender_id = $request->sender_id;
        $newMessage->receiver_id = $request->receiver_id;

        if ($request->private == false) {

            $send = $this->send($request);
        } else {

            $send = $this->send($request, $request->receiver_id);
        }

        if (!$newMessage->save()) {
            return [
                'status' => 500,
                'message' => 'ada kesalahan pada koneksi ke server'
            ];
        } else {
            // broadcast(new MessageCreated($newMessage));

            return [
                'status' => 200,
                'message' => 'berhasil'
            ];
        }
    }

    public function send($request, $receiver_username = false)
    {
        try {
            if ($receiver_username == false) {
                $publishResponse = $this->beams->publishToInterests(
                    array("broadcast"),
                    array(
                        "fcm" => array(
                            "notification" => array(
                                "title" => $request->title,
                                "body" => $request->message
                            )
                        )
                    )
                );
            } else {
                $token = $beamsClient->generateToken($receiver_username);

                $publishResponse = $this->beams->publishToUsers(
                    array($request->receiver_id),
                    array(
                        "fcm" => array(
                            "notification" => array(
                                "title" => $request->title,
                                "body" => $request->message
                            )
                        )
                    )
                );
            }

            return $publishResponse;
        } catch (\Exception $e) {
            return [
                'status' => 'error',
                'message' => $e
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        if ($message->delete()) {
            // broadcast(new MessageRemoved($message));
            return [
                'message' => 'OK',
                'code' => 204
            ];
        } else {
            return [
                'message' => 'Bad Request',
                'code' => 400
            ];
        }
    }
}
