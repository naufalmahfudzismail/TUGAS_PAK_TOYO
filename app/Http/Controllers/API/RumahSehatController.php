<?php

namespace App\Http\Controllers\API;

use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Moduls\RumahSehat;
use App\Models\KartuKeluarga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Kelurahan;
use App\Models\Kecamatan;
use App\Models\PetugasSippkling;
use App\Models\Moduls\Sab;
use App\Models\History;
use App\Scopes\ModulsFilterScope;
use Carbon\Carbon;
use App\Scopes\KaderFilterScope;
use App\Support\Modules\RumahSehat as RumahSehatSupport;
use Illuminate\Support\Facades\Auth;

class RumahSehatController extends Controller
{
    protected $rs;
    public function __construct(RumahSehatSupport $rs)
    {
        $this->rs = $rs;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $obj = RumahSehat::withoutGlobalScope(\App\Scopes\ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->select(
                'rumah_sehats.*',
                DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                DB::raw("string_agg(k.jumlah_anggota::text, ',') as jumlah_anggota"),
                DB::raw("string_agg(k.jumlah_laki_laki::text, ',') as jumlah_laki_laki"),
                DB::raw("string_agg(k.jumlah_perempuan::text, ',') as jumlah_perempuan"),
                DB::raw("string_agg(k.nama_kk, ',') as nama_kk")
            )
            ->groupBy('rumah_sehats.id')
            ->orderByDesc('rumah_sehats.waktu')
            ->paginate(20);

        $jumlah_total = RumahSehat::withoutGlobalScope(\App\Scopes\ModulsFilterScope::class)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(\App\Scopes\ModulsFilterScope::class)->where('rumah_sehats.status', 'true')->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(\App\Scopes\ModulsFilterScope::class)->where('rumah_sehats.status', 'false')->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $this->validate($request, [
                'foto' => 'required|image|mimes:jpg,png,jpeg'
            ]);

            $image = $request->file('foto');
            $thumbnailPath = storage_path() . '/app/public/images/objek/';
            $imageName = 'rumah_sehat_' . time() . str_random(3) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save($thumbnailPath . $imageName);
            $requestData["foto"] = $imageName;
        } else {
            $requestData["foto"] =  null;
        }

        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->create($requestData);

        if (!$obj) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'input';
            $history->waktu_history = $request->waktu;
            $history->tipe_petugas =  $request->created_by;
            $history->petugas_id = $request->petugas_id;
            $history->data_id = '36';
            $history->objek_id = $obj->id;
            $history->created_by = $request->created_by;
            $history->created_at = $obj->created_at;
            $history->key_histories =  $request->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();


            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'rs_id' => $obj->id,
                    'code' => 200,
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'rs_id' => null,
                    'code' => 400,
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getNamaKK($id)
    {

        $nama = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)
        ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->select(
                DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk")
            )->where('rumah_sehats.id', $id)
            ->groupBy('rumah_sehats.id')
            ->first();

        return response()->json([
            'data' => $nama
        ]);
    }


    public function show($rumahSehat)
    {
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)
            ->leftjoin('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->select(
                'rumah_sehats.*',
                DB::raw("(select nama from petugas_sippklings as p where p.id = rumah_sehats.update_by) as  update_by"),
                DB::raw("(select id from sabs as sab where rumah_sehats.id = sab.rumah_sehat_id) as  sab_id"),
                DB::raw("string_agg(k.id::text, ',') as kk_id"),
                DB::raw("string_agg(k.nmr_kk::text, ',') as nmr_kk"),
                DB::raw("string_agg(k.jumlah_anggota::text, ',') as jumlah_anggota"),
                DB::raw("string_agg(k.jumlah_laki_laki::text, ',') as jumlah_laki_laki"),
                DB::raw("string_agg(k.jumlah_perempuan::text, ',') as jumlah_perempuan"),
                DB::raw("string_agg(k.depok::text, ',') as ktp_depok"),
                DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                DB::raw("string_agg(k.updated_at::text, ',') as tgl_update_kk")
            )->where('rumah_sehats.id', $rumahSehat)
            ->groupBy("rumah_sehats.id")
            ->first();

        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)
            ->select('nama', 'kelurahan_id', 'kecamatan_id')->where('id', '=', $obj->petugas_id)->first();
        $obj["kelurahan"] = Kelurahan::select('nama')->where('id', $petugas->kelurahan_id)->first()->nama;
        $obj["kecamatan"] = Kecamatan::select('nama')->where('id', $petugas->kecamatan_id)->first()->nama;
        $obj["nama_petugas"] = $petugas->nama;

        $sab = Sab::where('rumah_sehat_id', $obj->id)->limit(1)->first();

        if ($sab != null) {
            $obj["sab"] = $sab;
        } else {
            $obj["sab"] = null;
        }

        return response()->json([
            'data' => $obj
        ]);
    }

    public function showByKelurahan($kelurahan)
    {
        $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)
            ->select('id')
            ->where('kelurahan_id', '=', $kelurahan)->get();
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)
            ->withoutGlobalScope(ModulsFilterScope::class)
            ->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->leftjoin('sabs as sab', 'rumah_sehats.id', 'sab.rumah_sehat_id')
            ->select(
                'rumah_sehats.id',
                DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                'rumah_sehats.alamat',
                'rumah_sehats.rt',
                'rumah_sehats.rw',
                'rumah_sehats.status',
                'rumah_sehats.koordinat',
                'rumah_sehats.updated_at',
                'rumah_sehats.created_at',
                'rumah_sehats.waktu as waktu',
                DB::raw("string_agg(k.nmr_kk, ',') as nmr_kk"),
                'sab.id as sab_id'
            )
            ->whereIn('p.id', $petugas)
            ->groupBy('rumah_sehats.id')
            ->orderByDesc('rumah_sehats.waktu')
            ->paginate(20);


        $jumlah_total = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->where('rumah_sehats.status', 'true')->whereIn('p.id', $petugas)->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->where('rumah_sehats.status', 'false')->whereIn('p.id', $petugas)->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }

    public function showByKecamatan($kecamatan)
    {

        if (is_numeric($kecamatan)) {
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kecamatan)->get();
        } else {
            $kel = Kecamatan::where('nama',  $kecamatan)->first();
            $petugas = PetugasSippkling::withoutGlobalScope(KaderFilterScope::class)->select('id')->where('kecamatan_id', '=', $kel->id)->get();
        }
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')
            ->leftjoin('kartu_keluargas as k', 'rumah_sehats.id', 'k.rumah_sehat_id')
            ->select(
                'rumah_sehats.id',
                DB::raw("string_agg(k.nama_kk, ',') as nama_kk"),
                'rumah_sehats.alamat',
                'rumah_sehats.rt',
                'rumah_sehats.rw',
                'rumah_sehats.status',
                'rumah_sehats.koordinat',
                'rumah_sehats.updated_at',
                'rumah_sehats.created_at',
                'rumah_sehats.waktu as waktu',
                DB::raw("(GROUP_CONCAT(k.nmr_kk SEPARATOR ',')) as `nmr_kk`")
            )
            ->whereIn('p.id', $petugas)
            ->orderByDesc('rumah_sehats.waktu')
            ->paginate(20);


        $jumlah_total = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->whereIn('p.id', $petugas)->count();
        $jumlah_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->where('rumah_sehats.status', 'true')->whereIn('p.id', $petugas)->count();
        $jumlah_tidak_sehat = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->join('petugas_sippklings as p', 'rumah_sehats.petugas_id', 'p.id')->where('rumah_sehats.status', 'false')->whereIn('p.id', $petugas)->count();

        return response()->json([
            'data' => $obj,
            'total_data' => $jumlah_total,
            'total_sehat' => $jumlah_sehat,
            'total_tidak_sehat' => $jumlah_tidak_sehat
        ]);
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function edit(RumahSehat $rumahSehat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */

    public function updateWithImage(Request $request, $id)
    {
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->findOrFail($id);
        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'rumah_sehat' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);

            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);

        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {
            $lastHistory = History::orderBy('id', 'desc')->first();

            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $requestData["update_by"];
            $history->petugas_id = $requestData["update_by"];
            $history->data_id = '36';
            $history->objek_id = $obj->id;
            $history->created_by = $requestData["update_by"];
            $history->created_at = $obj->created_at;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();
            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                    'rs_id' => $obj->id
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                    'rs_id' => null
                ]);
            }
        }
    }
    public function update(Request $request, $rumahSehat)
    {
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->findOrFail($rumahSehat);
        $requestData = $request->all();

        if ($request->hasFile('foto')) {

            $thumbnailPath = storage_path() . '/app/public/images/objek/';

            $photo_path = $request->file('foto');
            $new_name = 'rumah_sehat' . time() . str_random(3) . $photo_path->getClientOriginalExtension();

            if ($obj->foto != null) {
                $old_name = $obj->foto;
                unlink($thumbnailPath . $old_name);
            }

            Image::make($photo_path)->save($thumbnailPath . $new_name);
            $requestData["foto"] = $new_name;
        }

        $update = $obj->update($requestData);

        if (!$update) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();

            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'update';

            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = $request->update_by;
            $history->created_by  = $request->update_by;
            $history->created_at = Carbon::now();

            $history->petugas_id = $requestData["update_by"];
            $history->data_id = '36';
            $history->objek_id = $obj->id;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;


            $save = $history->save();

            if ($save) {
                return response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 200,
                    'rs_id' => $obj->id
                ]);
            } else {
                return  response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                    'rs_id' => null
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RumahSehat  $rumahSehat
     * @return \Illuminate\Http\Response
     */
    public function destroy($rumahSehat)
    {
        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->find($rumahSehat);
        
        $delete = $obj->delete();

        if (!$delete) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '36';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->created_by = $obj->created_by;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }

    public function dashboardRumahSehat()
    {

        try {

            $waktu = \App\Models\Moduls\RumahSehat::advancedFilter()->latest('waktu')->first();

            if ($waktu != null) {

                $waktu = \App\Models\Moduls\RumahSehat::advancedFilter()->latest('waktu')->first()->waktu;
                $query = \App\Http\Resources\RumahSehatResource::collection(\App\Models\Moduls\RumahSehat::advancedFilter()->where('waktu', $waktu)->get());
            } else {

                $query = "";
            }

            return response()
                ->json([
                    'data' => [
                        'rumah_sehat' => [
                            'sehat' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('status', true)->count(),
                            'tidak_sehat' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('status', false)->count(),
                            'total' => \App\Models\Moduls\RumahSehat::advancedFilter()->count(),
                            'last_update' => $waktu,
                            'query' => $query
                        ],
                        'pjb' => [
                            'tidak_ada_jentik' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('pjb', true)->count(),
                            'ada_jentik' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('pjb', false)->count()
                        ],
                        'spal' => [
                            'tertutup' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('spal', true)->count(),
                            'terbuka' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('spal', false)->count()
                        ],
                        'sab' => [
                            'sehat' => 0,
                            'tidak_sehat' => 0
                        ],
                        'jamban' => [
                            'koya' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('jamban', "Koya / Empang")->count(),
                            'kali' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('jamban', "Kali")->count(),
                            'helikopter' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('jamban', "Helikopter")->count(),
                            'septik_tank' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('jamban', "Septik Tank")->count()
                        ],
                        'sampah' => [
                            'dipilah' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('sampah', true)->count(),
                            'tidak_dipilah' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('sampah', false)->count()
                        ],
                        'status_rumah' => [
                            'milik_sendiri' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('status_rumah', true)->count(),
                            'sewa' => \App\Models\Moduls\RumahSehat::advancedFilter()->where('status_rumah', false)->count()
                        ],
                        'grafik' => [
                            // 'labels' => $labels,
                            // 'datasets' => \App\Models\Moduls\RumahSehat::advancedFilter()->grafikFilter()
                        ]
                    ],
                ]);
        } catch (\Throwable $th) {

            throw $th;
            
        }
    }

    public function isiTrash()
    {
        $obj = RumahSehat::onlyTrashed()->get();

        return response()->json([
            'data' => $obj
        ]);
    }

    public function destroyFromWebsite($rumahSehat)
    {
        $dataLogin = Auth::user();

        if($dataLogin != null){
            $usernameLogin = Auth::user()->username;
        } else {
            $usernameLogin = "undefined";
        }

        $obj = RumahSehat::withoutGlobalScope(ModulsFilterScope::class)->find($rumahSehat);
        $obj->deleted_by = $usernameLogin;
        $obj->save();

        $sab = \App\Models\Moduls\Sab::where('rumah_sehat_id', $rumahSehat)->first();
        $sab->deleted_by = $usernameLogin;
        $sab->save();
        
        $delete = $obj->delete();
        $sabDelete = $sab->delete();
        
        if (!$delete && !$sabDelete) {
            return response()->json([
                'error' => true,
                'error_message' => 'Bad Request',
                'code' => 400,
            ]);
        } else {

            $lastHistory = History::orderBy('id', 'desc')->first();
            if ($lastHistory == null) {
                $id = 1;
            } else {
                $id = $lastHistory->id + 1;
            }

            $history = new History();
            $history->aktifitas = 'delete';
            $history->waktu_history = Carbon::now();
            $history->tipe_petugas = "kader";
            $history->petugas_id = $obj->petugas_id;
            $history->data_id = '36';
            $history->objek_id = $obj->id;
            $history->created_by = $obj->created_by;
            $history->created_at = $obj->created_at;
            $history->deleted_by = Auth::user()->username;
            $history->key_histories =  $history->petugas_id . '-' . $history->data_id . '-' . $obj->id . '-' . $id;

            $save = $history->save();

            if ($save) {
                return  response()->json([
                    'error' => false,
                    'error_message' => 'OK',
                    'code' => 204,
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'error_message' => 'activity cannot be tracked',
                    'code' => 400,
                ]);
            }
        }
    }
}
