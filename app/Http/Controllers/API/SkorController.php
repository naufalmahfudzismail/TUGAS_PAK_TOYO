<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Support\Modules\GeneralInformation;


class SkorController extends Controller
{
    protected $general;
    public function __construct(GeneralInformation $general)
    {
        $this->general = $general;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \App\Models\Skor::all();
        $count = \App\Models\Skor::sum('bobot');
        $skor_sehat = DB::table('general_sippkling_dashboards')->where('param', 'skor')->get()[0]->value;
        return [
            'total_bobot' => $count,
            'bobot' => getValueFromQuery($data, 'bobot'),
            'data' => $data,
            'skor_sehat' => $skor_sehat
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateSkor(Request $request){
        DB::table('skor_kesehatan_lingkungans')->truncate();
        
        for ($i=0; $i < count($request->skor); $i++) { 
            DB::table("skor_kesehatan_lingkungans")->insert([
                'objek' => $request->skor[$i]['objek'],
                'bobot' => $request->skor[$i]['bobot']
            ]);
        }

        return [
            'status' => 200,
            'error' => 'false'
        ];
    }

    public function getGrafikScore(){
        $labels = $this->general->payloadParamFilterStructure(null, false);
        $datasets = $this->general->getScoreAngkaKesehatan($labels);
        
        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => "Tingkat Kesehatan",
                        'backgroundColor'=> "#f86c6b",
                        'data' => $datasets
                    ]
                ]
            ]
        ]);
    }

    public function changeScore(Request $request){
        DB::table('general_sippkling_dashboards')->where('param', 'skor')->update(['value' => $request->skor]);
        
        return [
            'status' => 200,
            'error' => 'false'
        ];
    }

    public function getDataKesehatan(){
        $getData = \App\Http\Resources\DataKesehatanResource::collection(\App\Models\Kecamatan::all());

        return [
            'data' => $getData
        ];
    }

    public function getTotalKesehatanLingkungan(){
        return [
            'data' => $this->depokHealthy()
        ];
    }

    public function depokHealthy(){
        $bobot = DB::table('skor_kesehatan_lingkungans')->select('bobot')->get();
        
        $data = round(array_sum([
            ((\App\Models\Moduls\RumahSehat::where('status', true)->count() * \App\Models\Moduls\RumahSehat::count()) / 100) * $bobot[0]->bobot,
            ((\App\Models\Moduls\TempatIbadah::where('status', true)->count() * \App\Models\Moduls\TempatIbadah::count()) / 100) * $bobot[1]->bobot,
            ((\App\Models\Moduls\Pasar::where('status', true)->count() * \App\Models\Moduls\Pasar::count()) / 100) * $bobot[2]->bobot,
            ((\App\Models\Moduls\Sekolah::where('status', true)->count() * \App\Models\Moduls\Sekolah::count()) / 100) * $bobot[3]->bobot,
            ((\App\Models\Moduls\Pesantren::where('status', true)->count() * \App\Models\Moduls\Pesantren::count()) / 100) * $bobot[4]->bobot,
            ((\App\Models\Moduls\KolamRenang::where('status', true)->count() * \App\Models\Moduls\KolamRenang::count()) / 100) * $bobot[5]->bobot,
            ((\App\Models\Moduls\Puskesmas::where('status', true)->count() * \App\Models\Moduls\Puskesmas::count()) / 100) * $bobot[6]->bobot,
            ((\App\Models\Moduls\RumahSakit::where('status', true)->count() * \App\Models\Moduls\RumahSakit::count()) / 100) * $bobot[7]->bobot,
            ((\App\Models\Moduls\Klinik::where('status', true)->count() * \App\Models\Moduls\Klinik::count()) / 100) * $bobot[8]->bobot,
            ((\App\Models\Moduls\Hotel::where('status', true)->count() * \App\Models\Moduls\Hotel::count()) / 100) * $bobot[9]->bobot,
            ((\App\Models\Moduls\HotelMelati::where('status', true)->count() * \App\Models\Moduls\HotelMelati::count()) / 100) * $bobot[10]->bobot,
            ((\App\Models\Moduls\Salon::where('status', true)->count() * \App\Models\Moduls\Salon::count()) / 100) * $bobot[11]->bobot,
            ((\App\Models\Moduls\JasaBoga::where('status', true)->count() * \App\Models\Moduls\JasaBoga::count()) / 100) * $bobot[12]->bobot,
            ((\App\Models\Moduls\Kuliner::where('status', true)->count() * \App\Models\Moduls\Kuliner::count()) / 100) * $bobot[13]->bobot,
            ((\App\Models\Moduls\DamSippKling::where('status', true)->count() * \App\Models\Moduls\DamSippKling::count()) / 100) * $bobot[14]->bobot,
            ((\App\Models\Moduls\PelayananKesling::where('status', true)->count() * \App\Models\Moduls\PelayananKesling::count()) / 100) * $bobot[15]->bobot,
            0
        ]) / 10, 3);

        return $data;
    }

    public function getGrafikKesehatanLingkungan(){
        $labels = [0, 2019];
        $datasets = $this->general->getScoreAngkaKesehatanByYear($labels);

        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'label' => "Tingkat Kesehatan Per Tahun",
                        'backgroundColor'=> "#f86c6b",
                        'data' => $datasets
                    ]
                ]
            ]
        ]);
    }
}
