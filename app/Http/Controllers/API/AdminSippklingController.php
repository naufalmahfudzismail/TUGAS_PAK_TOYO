<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Http\Resources\User as UserResource;

class AdminSippklingController extends Controller
{
    /**
     * Display a listing of admin without Super Admin Role
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminSippkling  $adminSippkling
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminSippkling $adminSippkling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminSippkling  $adminSippkling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminSippkling $adminSippkling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminSippkling  $adminSippkling
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
