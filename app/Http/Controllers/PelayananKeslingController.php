<?php

namespace App\Http\Controllers;

use App\PelayananKesling;
use Illuminate\Http\Request;

class PelayananKeslingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PelayananKesling  $pelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function show(PelayananKesling $pelayananKesling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PelayananKesling  $pelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function edit(PelayananKesling $pelayananKesling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PelayananKesling  $pelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PelayananKesling $pelayananKesling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PelayananKesling  $pelayananKesling
     * @return \Illuminate\Http\Response
     */
    public function destroy(PelayananKesling $pelayananKesling)
    {
        //
    }
}
