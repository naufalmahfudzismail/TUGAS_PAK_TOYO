<?php

namespace App\Http\Controllers;

use App\Models\General;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $general = General::all();
        return response()->json([
            'general' => $general,
        ]);
    }
    public function getHome()
    {
        $home = General::select('id', 'title', 'subtitle', 'konten', 'link', 'gambar')->where('param', 'home')->get();
        return response()->json([
            'data' => $home,
        ]);
    }
    public function getLatarbelakang()
    {
        $latarbelakangs = General::select('id', 'title', 'subtitle', 'konten', 'gambar')->where('param', 'latbel')->orderBy('id', 'asc')->get();
        return response()->json([
            'latarbelakangs' => $latarbelakangs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $general = this . validate(request(), [
            'param' => 'required',
            'title' => 'required',
            'subtitle' => 'required',
            'konten' => 'required',
            'link' => 'required',
            'gambar' => 'required',
        ]);

        General::create($general);
        return back()->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = General::findOrFail($id);

        return response()->json([
            "data" => $data,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $general = General::findOrFail($id);
        $this->validate(request(), [
            'param' => 'required',
            'title' => 'required',
            'subtitle' => 'required',
            'konten' => 'required',
            'link' => 'required',
            'gambar' => 'required',
        ]);

        $general->param = $request->get('param');
        $general->title = $request->get('title');
        $general->subtitle = $request->get('subtitle');
        $general->konten = $request->get('konten');
        $general->link = $request->get('link');
        $general->gambar = $request->get('gambar');
        $update = $general->save();

        if ($update) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
    public function updateHome(Request $request, $id)
    {
        // return $request;

        $title = $request->title;
        $subtitle = $request->subtitle;
        $konten = $request->konten;
        $link = $request->link;
        $home = General::find($id);

        $gambar = $request->foto;
        
        if ($request->foto != null) {
        $imageName = 'home_' . str_random(5) . '.png';
        $lokasiimage = 'images/landingpage/home/' . $imageName;
        Image::make($gambar)->save($lokasiimage, $imageName);
        $home->gambar = $lokasiimage;}

        $home->title = $title;
        $home->subtitle = $subtitle;
        $home->konten = $konten;
        $home->link = $link;

        $home->save();

        if ($home) {
            return response()->json([
                "message" => "Berhasil Update Data",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $general = General::findOrFail($id);
        $delete = $general->delete();

        if ($delete) {
            return response()->json([
                "message" => "sukses",
            ]);
        } else {
            return response()->json([
                "message" => "gagal",
            ]);
        }
    }
}
