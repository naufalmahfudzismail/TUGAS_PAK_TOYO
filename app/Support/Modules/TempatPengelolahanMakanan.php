<?php

namespace App\Support\Modules;

use App\Support\Modules\GeneralInformation;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\DamSippKling;

class TempatPengelolahanMakanan extends ModulesStructure {
    protected $total;
    public function __construct(){
        $this->total = [
            'jasa_boga' => JasaBoga::autoFilterByRole()->count(),
            'kuliner' => Kuliner::autoFilterByRole()->count(),
            'dam' => DamSippKling::autoFilterByRole()->count()
        ];
    }
    
    public function index(){
        $tempat_pengelolahan_makanan = [
            'general' => $this->general(),
            'items' => $this->items()
        ];

        return $tempat_pengelolahan_makanan;
    }

    public function general(){
        $general = [
            'last_update' => '0'
        ];

        return $general;
    }

    public function items(){
        $items = [
            'jasa_boga' => $this->jasa_boga(),
            'restoran' => $this->restoran(),
            'dam' => $this->dam()
        ];

        return $items;
    }

    public function jasa_boga(){
        $value = [
            'laik_hygiene_sanitasi' => JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_laik_hygiene_sanitasi' => JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $jasa_boga = [
            'laik_hygiene_sanitasi' => $this->generalChildInitialize($value['laik_hygiene_sanitasi'], $this->total['jasa_boga']),
            'tidak_laik_hygiene_sanitasi' => $this->generalChildInitialize($value['tidak_laik_hygiene_sanitasi'], $this->total['jasa_boga']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['jasa_boga']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['jasa_boga'])
        ];

        $waktu = JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = JasaBoga::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $jasa_boga = array_merge($jasa_boga, [
            'last_update' => $waktu
        ]);

        return $jasa_boga;
    }   

    public function restoran(){
        $value = [
            'laik_hygiene_sanitasi' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_laik_hygiene_sanitasi' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'memiliki_izin_usaha' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->where('pajak', true)->count(),
            'tidak_memiliki_izin_usaha' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->where('pajak', false)->count(),
            'total' => Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $restoran = [
            'laik_hygiene_sanitasi' => $this->generalChildInitialize($value['laik_hygiene_sanitasi'], $this->total['kuliner']),
            'tidak_laik_hygiene_sanitasi' => $this->generalChildInitialize($value['tidak_laik_hygiene_sanitasi'], $this->total['kuliner']),
            'memiliki_izin_usaha' => $this->generalChildInitialize($value['memiliki_izin_usaha'], $this->total['kuliner']),
            'tidak_memiliki_izin_usaha' => $this->generalChildInitialize($value['tidak_memiliki_izin_usaha'], $this->total['kuliner']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['kuliner']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['kuliner'])
        ];

        $waktu = Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Kuliner::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $restoran = array_merge($restoran, [
            'last_update' => $waktu
        ]);

        return $restoran;
    }

    public function dam(){
        $value = [
            'laik_hygiene_sanitasi' => DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_laik_hygiene_sanitasi' => DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->count(),
        ];
        
        $dam = [
            'laik_hygiene_sanitasi' => $this->generalChildInitialize($value['laik_hygiene_sanitasi'], $this->total['dam']),
            'tidak_laik_hygiene_sanitasi' => $this->generalChildInitialize($value['tidak_laik_hygiene_sanitasi'], $this->total['dam']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['dam']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['dam'])
        ];

        $waktu = DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = DamSippKling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $dam = array_merge($dam, [
            'last_update' => $waktu
        ]);

        return $dam;
    }

    public function getTempatMakananUmumChild($param){
        switch ($param) {
            case 1:
                return $this->jasa_boga();
                break;
            
            case 2:
                return $this->restoran();
                break;
            
            case 3:
                return $this->dam();
                break;
            
            default:
                return [];
                break;
        }
    }

    private function getPercent($value, $total){

        if($total > 0){

            return [
                'value' => $value,
                'percent' => round(($value / $total) * 100). "%"
            ];

        } else {

            return [
                'value' => 0,
                'percent' => '0%'
            ];

        }
    }
}