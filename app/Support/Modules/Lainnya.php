<?php

namespace App\Support\Modules;

use App\Support\Modules\GeneralInformation;
use \App\Models\Moduls\PelayananKesling;
// use \App\Models\Moduls\PelayananKesling;

class Lainnya extends ModulesStructure {
    
    protected $total;
    public function __construct(){
        $this->total = [
            'pelayanan_kesling' => PelayananKesling::autoFilterByRole()->count(),
            'phbs' => 0
        ];
    }

    public function index(){
        $lainnya = [
            'general' => $this->general(),
            'items' => $this->items()
        ];

        return $lainnya;
    }

    public function items(){
        $items = [
            'pelayanan_kesehatan' => $this->pelayanan_kesehatan(),
            'phbs' => $this->phbs()
        ];

        return $items;
    }

    public function general(){
        $general = [
            'last_update' => '0'
        ];

        return $general;
    }

    public function pelayanan_kesehatan(){
        $value = [
            'dalam_gedung' => PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'luar_gedung' => PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $dam = [
            'dalam_gedung' => $this->generalChildInitialize($value['dalam_gedung'], $this->total['pelayanan_kesling']),
            'luar_gedung' => $this->generalChildInitialize($value['luar_gedung'], $this->total['pelayanan_kesling']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['pelayanan_kesling']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['pelayanan_kesling'])
        ];

        $waktu = PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = PelayananKesling::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $dam = array_merge($dam, [
            'last_update' => $waktu
        ]);

        return $dam;
    }

    public function phbs(){
        $value = [
            'laik_hygiene_sanitasi' => 0,
            'tidak_laik_hygiene_sanitasi' => 0,
            'belum_dijamah' => 0,
            'total' => 0
        ];
        
        $dam = [
            'laik_hygiene_sanitasi' => $this->generalChildInitialize($value['laik_hygiene_sanitasi'], $this->total['phbs']),
            'tidak_laik_hygiene_sanitasi' => $this->generalChildInitialize($value['tidak_laik_hygiene_sanitasi'], $this->total['phbs']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['phbs']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['phbs'])
        ];

        $waktu = \App\Models\Moduls\PelayananKesling::advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(\App\Models\Moduls\PelayananKesling::advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = \App\Models\Moduls\PelayananKesling::advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $dam = array_merge($dam, [
            'last_update' => $waktu
        ]);

        return $dam;
    }

    public function getLainnyaChild($param){

        switch($param){

            case 1:
                return $this->pelayanan_kesehatan();
                break;

            case 2:
                return $this->phbs();
                break;

            default:
                return 'belum di buat exception';
                break;

        }

    }

    private function getPercent($value, $total){

        if($total > 0){

            return [

                'value' => $value,
                'percent' => round(($value / $total) * 100). "%"
                
            ];

        } else {

            return [

                'value' => 0,
                'percent' => '0%'

            ];

        }
    }
}