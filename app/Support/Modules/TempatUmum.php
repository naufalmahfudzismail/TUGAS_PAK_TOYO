<?php

namespace App\Support\Modules;

use App\Support\Modules\GeneralInformation;
use App\Models\Moduls\TempatIbadah;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\Sekolah;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\Salon;

class TempatUmum extends ModulesStructure {
    protected $total;
    public function __construct(){
        $this->total = [
            'tempat_ibadah' => TempatIbadah::autoFilterByRole()->count(),
            'pasar' => Pasar::autoFilterByRole()->count(),
            'sekolah' => Sekolah::autoFilterByRole()->count(),
            'pesantren' => Pesantren::autoFilterByRole()->count(),
            'kolam_renang' => KolamRenang::autoFilterByRole()->count(),
            'puskesmas' => Puskesmas::autoFilterByRole()->count(),
            'rumah_sakit' => RumahSakit::autoFilterByRole()->count(),
            'klinik' => Klinik::autoFilterByRole()->count(),
            'hotel' => Hotel::autoFilterByRole()->count(),
            'hotel_melati' => HotelMelati::autoFilterByRole()->count(),
            'salon' => Salon::autoFilterByRole()->count()
        ];
    }

    public function index(){
        $tempat_umum = [
            'general' => $this->general(),
            'items' => $this->items()
        ];

        return $tempat_umum;
    }

    public function general(){
        $general = [
            'last_update' => '0'
        ];

        return $general;
    }

    public function items(){
        $items = [
            'tempat_ibadah' => $this->tempat_ibadah(),
            'pasar' => $this->pasar(),
            'sekolah' => $this->sekolah(),
            'pesantren' => $this->pesantren(),
            'kolam_renang' => $this->kolam_renang(),
            'puskesmas' => $this->puskesmas(),
            'rumah_sakit' => $this->rumah_sakit(),
            'klinik' => $this->klinik(),
            'hotel' => $this->hotel(),
            'hotel_melati' => $this->hotel_melati(),
            'salon' => $this->salon()
        ];

        return $items;
    }

    public function tempat_ibadah($query = false){
        $value = [
            'sehat' => TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $tempat_ibadah = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['tempat_ibadah']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['tempat_ibadah']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['tempat_ibadah']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['tempat_ibadah'])
        ];

        $waktu = TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = TempatIbadah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $tempat_ibadah = array_merge($tempat_ibadah, [
            'last_update' => $waktu
        ]);

        return $tempat_ibadah;
    }

    public function pasar($query = false){
        $value = [
            'sehat' => Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $pasar = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['pasar']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['pasar']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['pasar']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['pasar'])
        ];

        $waktu = Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Pasar::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $pasar = array_merge($pasar, [
            'last_update' => $waktu
        ]);

        return $pasar;
    }

    public function sekolah($query = false){
        $value = [
            'sehat' => Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $sekolah = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['sekolah']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['sekolah']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['sekolah']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['sekolah'])
        ];

        $waktu = Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Sekolah::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $sekolah = array_merge($sekolah, [
            'last_update' => $waktu
        ]);

        return $sekolah;
    }

    public function pesantren($query = false){

        $value = [
            'sehat' => Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->count(),
        ];
        
        $pesantren = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['pesantren']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['pesantren']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['pesantren']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['pesantren'])
        ];

        $waktu = Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Pesantren::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $pesantren = array_merge($pesantren, [
            'last_update' => $waktu
        ]);

        return $pesantren;
    }

    public function kolam_renang($query = false){
        $value = [
            'sehat' => KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $kolam_renang = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['kolam_renang']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['kolam_renang']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['kolam_renang']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['kolam_renang'])
        ];

        $waktu = KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = KolamRenang::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $kolam_renang = array_merge($kolam_renang, [
            'last_update' => $waktu
        ]);

        return $kolam_renang;
    }

    public function puskesmas($query = false){
        $value = [
            'sehat' => Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->count(),
        ];
        
        $puskesmas = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['puskesmas']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['puskesmas']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['puskesmas']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['puskesmas'])
        ];

        $waktu = Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Puskesmas::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $puskesmas = array_merge($puskesmas, [
            'last_update' => $waktu
        ]);

        return $puskesmas;
    }

    public function rumah_sakit(){

        $value = [
            'sehat' => RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->count(),
        ];
        
        $rumah_sakit = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['rumah_sakit']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['rumah_sakit']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['rumah_sakit']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sakit'])
        ];

        $waktu = RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = RumahSakit::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $rumah_sakit = array_merge($rumah_sakit, [
            'last_update' => $waktu
        ]);

        return $rumah_sakit;
    }

    public function klinik(){
        $value = [
            'sehat' => Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $klinik = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['klinik']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['klinik']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['klinik']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['klinik'])
        ];

        $waktu = Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Klinik::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $klinik = array_merge($klinik, [
            'last_update' => $waktu
        ]);

        return $klinik;
    }

    public function hotel(){
        $value = [
            'sehat' => Hotel::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Hotel::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Hotel::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Hotel::autoFilterByRole()->advancedFilterWithoutRtRw()->count(),
        ];
        
        $hotel = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['hotel']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['hotel']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['hotel']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['hotel'])
        ];

        $waktu = Hotel::advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Hotel::advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Hotel::advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $hotel = array_merge($hotel, [
            'last_update' => $waktu
        ]);

        return $hotel;
    }

    public function hotel_melati(){
        $value = [
            'sehat' => HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $hotel_melati = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['hotel_melati']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['hotel_melati']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['hotel_melati']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['hotel_melati'])
        ];

        $waktu = HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = HotelMelati::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $hotel_melati = array_merge($hotel_melati, [
            'last_update' => $waktu
        ]);

        return $hotel_melati;
    }

    public function salon(){
        $value = [
            'sehat' => Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', true)->count(),
            'tidak_sehat' => Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', false)->count(),
            'belum_dijamah' => Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->where('status', null)->count(),
            'total' => Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->count()
        ];
        
        $salon = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['salon']),
            'tidak_sehat' => $this->generalChildInitialize($value['tidak_sehat'], $this->total['salon']),
            'belum_dijamah' => $this->generalChildInitialize($value['belum_dijamah'], $this->total['salon']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['salon'])
        ];

        $waktu = Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first();

        if(Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first() != null){
            $waktu = Salon::autoFilterByRole()->advancedFilterWithoutRtRw()->select('waktu')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $salon = array_merge($salon, [
            'last_update' => $waktu
        ]);

        return $salon;

    }

    public function getTempatUmumChild($param){
        switch ($param) {
            case 1:
                return $this->tempat_ibadah();
                break;
            
            case 2:
                return $this->pasar();
                break;
            
            case 3:
                return $this->sekolah();
                break;
            
            case 4:
                return $this->pesantren();
                break;
            
            case 5:
                return $this->kolam_renang();
                break;

            case 6:
                return $this->puskesmas();
                break;
            
            case 7:
                return $this->rumah_sakit();
                break;
            
            case 8:
                return $this->klinik();
                break;

            case 9:
                return $this->hotel();
                break;

            case 10:
                return $this->hotel_melati();
                break;
            
            case 11:
                return $this->salon();
                break;
            
            default:
                return [];
                break;
        }
    }

    private function getPercent($value, $total){

        if($total > 0){

            return [
                'value' => $value,
                'percent' => round(($value / $total) * 100). "%"
            ];

        } else {

            return [
                'value' => 0,
                'percent' => '0%'
            ];

        }
    }
}