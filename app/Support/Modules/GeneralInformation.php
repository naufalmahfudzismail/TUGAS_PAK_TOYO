<?php

namespace App\Support\Modules;

use DB;

use \App\Models\KartuKeluarga;
use \App\Models\Moduls\RumahSehat;
use \App\Models\Moduls\TempatIbadah;
use \App\Models\Moduls\Pasar;
use \App\Models\Moduls\Sekolah;
use \App\Models\Moduls\Pesantren;
use \App\Models\Moduls\KolamRenang;
use \App\Models\Moduls\Puskesmas;
use \App\Models\Moduls\RumahSakit;
use \App\Models\Moduls\Klinik;
use \App\Models\Moduls\Hotel;
use \App\Models\Moduls\HotelMelati;
use \App\Models\Moduls\Salon;
use \App\Models\Moduls\JasaBoga;
use \App\Models\Moduls\Kuliner;
use \App\Models\Moduls\DamSippKling;
use \App\Models\Moduls\PelayananKesling;
use \App\Models\Moduls\Sab;

use \App\Support\Modules\ModulesStructure;

class GeneralInformation extends ModulesStructure{

    public function index(){
        $start = microtime(true);

        $informations = [
            'total_population' => \App\Models\KartuKeluarga::advancedFilter()->autoFilterByRole()->sum('jumlah_anggota'),
            'families' => \App\Models\KartuKeluarga::advancedFilter()->autoFilterByRole()->count(),
            'healthy' => [
                'total' => $this->depokHealthy()
            ],
            'imb' => [
                'tidak_ada' => $this->getTotalThereIsNoImb(),
                'ada' => $this->getTotalImb()
            ],
            'bayar_pajak' => [
                'tidak_ada' => $this->getTotalThereIsNoBayarPajak(),
                'ada' => $this->getTotalBayarPajak()
            ]
        ];

        $time = microtime(true) - $start;

        $informations = array_merge($informations, [
            'time' => $time
        ]);

        return $informations;
    }

    public function getTotalThereIsNoBayarPajak($array = null){
        $data = [
            $this->extractThereIsNoBayarPajak(RumahSehat::select('imb')->where('imb', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Pasar::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Sekolah::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Pesantren::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(KolamRenang::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Puskesmas::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(RumahSakit::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Klinik::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Hotel::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(HotelMelati::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Salon::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(JasaBoga::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(Kuliner::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractThereIsNoBayarPajak(DamSippKling::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get())
        ];

        if($array != null){
            return $data;
        } else {
            return array_sum($data);
        }

    }

    public function getTotalBayarPajak($array = null){
        $data = [
            $this->extractBayarPajak(RumahSehat::select('imb')->where('imb', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Pasar::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Sekolah::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Pesantren::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(KolamRenang::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Puskesmas::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(RumahSakit::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Klinik::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Hotel::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(HotelMelati::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Salon::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(JasaBoga::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(Kuliner::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get()),
            $this->extractBayarPajak(DamSippKling::select('pajak')->where('pajak', '!=', null)->advancedFilterWithoutRtRw()->autoFilterByRole()->get())
        ];

        if($array != null){
            return $data;
        } else {
            return array_sum($data);
        }

    }

    private function extractBayarPajak($param){
        $counter = 0;
        for ($i=0; $i < count($param); $i++) {
             
            if(isset($param[$i]->imb)){

                $status = explode(", ", $param[$i]->imb);
            
            } else {
            
                $status = explode(", ", $param[$i]->pajak);
            
            }
            
            if($status[0] == 'Ada'){

                if($status[2] == 'Sudah bayar'){
                    
                    $counter++;
                
                }

            }

        }

        return $counter;
    }

    private function extractThereIsNoBayarPajak($param){
        $counter = 0;
        for ($i=0; $i < count($param); $i++) {
             
            if(isset($param[$i]->imb)){

                $status = explode(", ", $param[$i]->imb);
            
            } else {
            
                $status = explode(", ", $param[$i]->pajak);
            
            }
            
            if($status[0] == 'Ada'){

                if($status[2] == 'Belum bayar'){
                    
                    $counter++;
                
                }

            }

        }

        return $counter;
    }
    

    private function getTotalImb($year = null){

        $data = [
            $this->extractImb(RumahSehat::advancedFilter()->autoFilterByRole()
            ->select('imb')
            ->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('imb', '!=', null)
            ->get()),
            $this->extractImb(Pasar::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Sekolah::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Pesantren::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(KolamRenang::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Puskesmas::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(RumahSakit::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Klinik::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Hotel::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(HotelMelati::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Salon::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(JasaBoga::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(Kuliner::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get()),
            $this->extractImb(DamSippKling::advancedFilterWithoutRtRw()->autoFilterByRole()
            ->select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })->where('pajak', '!=', null)->get())
        ];

        return array_sum($data);
    }

    private function getTotalThereIsNoImb($year = null){

        $data = [
            $this->extractThereIsNoImb(RumahSehat::select('imb')
            ->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('imb', '!=', null)
            ->autoFilterByRole()
            ->advancedFilter()
            ->get()),
            $this->extractThereIsNoImb(Pasar::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Sekolah::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Pesantren::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(KolamRenang::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Puskesmas::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(RumahSakit::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Klinik::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Hotel::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(HotelMelati::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Salon::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(JasaBoga::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(Kuliner::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractThereIsNoImb(DamSippKling::select('pajak')->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get())
        ];

        return array_sum($data);
    }

    private function getTotalImbBar($param = ""){
        $data = [
            $this->extractImb(RumahSehat::select('imb')
            ->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('imb', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Pasar::select('pajak')
            ->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Sekolah::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Pesantren::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(KolamRenang::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Puskesmas::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(RumahSakit::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Klinik::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Hotel::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(HotelMelati::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Salon::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(JasaBoga::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(Kuliner::select('pajak')->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get()),
            $this->extractImb(DamSippKling::select('pajak')
            ->whereHas('petugas', function($query) use ($param){
                if(request('kecamatan') != null){
                    $query->where('kelurahan_id', getIdKelurahanByName($param));
                } else {
                    $query->where('kecamatan_id', getIdKecamatanByName($param));
                }
            })
            ->where('pajak', '!=', null)
            ->autoFilterByRole()
            ->advancedFilterWithoutRtRw()
            ->get())
        ];

        return array_sum($data);
    }

    private function extractImb($param){
        $counter = 0;

        for ($i=0; $i < count($param); $i++) {
             
            if(isset($param[$i]->imb)){

                $status = explode(", ", $param[$i]->imb)[0];
            
            } else {
            
                $status = explode(", ", $param[$i]->pajak)[0];
            
            }
            
            if($status == 'Ada'){

                $counter++;

            }

        }

        return $counter;
    }

    private function extractThereIsNoImb($param){
        $counter = 0;

        for ($i=0; $i < count($param); $i++) {
             
            if(isset($param[$i]->imb)){

                $status = explode(", ", $param[$i]->imb)[0];
            
            } else {
            
                $status = explode(", ", $param[$i]->pajak)[0];
            
            }
            
            if($status == 'Tidak ada'){

                $counter++;

            }

        }

        return $counter;
    }

    private function getPercent($value, $total){
        if($total > 0){

            return [
                'value' => $value,
                'percent' => round(($value / $total) * 100) . "%"
            ];

        } else {
            
            return [
                'value' => 0,
                'percent' => 0 ."%"
            ];

        }
    }

    public function payloadParamFilterStructure($modul, $waktu = false){
        if($waktu == true){
            
            $tahun = $modul::select(DB::raw('extract(year from waktu) as year'))
            ->groupBy('year')
            ->orderBy('year', 'asc')
            ->get();

            if($tahun == null){
                return [0, \Carbon\Carbon::now()->year];
            } else {
                $initLineGraph = [0];

                $finalYear = getValueFromQuery($tahun, 'year');
            }
            
            return array_merge($initLineGraph, $finalYear);
        }

        if($modul == null && $waktu == false){
            if (request('kelurahan') != null && request('kecamatan') != null) {

                // $labels = array (
                //     '001', '002', '003', '004', '005', '006', '007', '008', '009', '010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020',
                //     '021', '022', '023', '024', '025',
                //     '026', '027', '028', '029', '030'
                // );

                $labels = getValueFromQuery(\App\Models\Kelurahan::select('nama AS labels')->where('kecamatan_id', request('kecamatan'))->get(), 'labels');
                
            } else if (request('kecamatan') != null) {
    
                $labels = getValueFromQuery(\App\Models\Kelurahan::select('nama AS labels')->where('kecamatan_id', request('kecamatan'))->get(), 'labels');
            
            } else {
    
                $labels = getValueFromQuery(\App\Models\Kecamatan::select('nama AS labels')->get(), 'labels');
            }
    
            return $labels;
        }
    }

    public function getDatasetsStats($kategori, $modul, $class, $label, $grafik){
        $finalData = [];
        if($grafik == 'line'){
            // default
            for ($i=0; $i < count($label); $i++) { 
                if($label[$i] != 0){

                    $finalData['sehat'][$i] = $this->getTotalStatusModules($class, $label[$i], true);
                    $finalData['tidak_sehat'][$i] = $this->getTotalStatusModules($class, $label[$i], false);
                
                } else {
                    $finalData['sehat'][$i] = 0;
                    $finalData['tidak_sehat'][$i] = 0;
                }
            }

        } else {
            
            // default
            for ($i=0; $i < count($label); $i++) { 
                $getNameLabel = $label[$i];
                $finalData['sehat'][$i] = 
                    $class::whereHas('petugas', function($query) use ($getNameLabel){
                        if(request("kecamatan") != null){
                            $query->where('kelurahan_id', getIdKelurahanByName($getNameLabel));
                        } else {
                            $query->where('kecamatan_id', getIdKecamatanByName($getNameLabel));
                        }
                    })->where('status', true)->count();
                $finalData['tidak_sehat'][$i] = 
                    $class::whereHas('petugas', function($query) use ($getNameLabel){
                        if(request("kecamatan") != null){
                            $query->where('kelurahan_id', getIdKelurahanByName($getNameLabel));
                        } else {
                            $query->where('kecamatan_id', getIdKecamatanByName($getNameLabel));
                        }
                    })->where('status', false)->count();
            }
        }

        return $finalData;
    }

    public function getDatasets($label, $grafik){
        if($grafik == 'line'){
            $finalData = [];
            
            for ($i=0; $i < count($label); $i++) { 
                if($label[$i] != 0){
                    $finalData[$i] = $this->getTotalImb($label[$i]);
                } else {
                    $finalData[$i] = 0;
                }
            }

            return $finalData;

        } else {
            $finalData = [];

            for ($i=0; $i < count($label); $i++) { 
                $finalData[$i] = $this->getTotalImbBar($label[$i]);
            }

            return $finalData;
        }
    }

    public function getClass($kategori, $modul){

        if($kategori == 'rs'){
            return RumahSehat::class;
        }

        if($kategori == 'ttu'){

            switch ($modul) {
                case 1:
                    return TempatIbadah::class;
                    break;
                
                case 2:
                    return Pasar::class;
                    break;
                
                case 3:
                    return Sekolah::class;
                    break;
                
                case 4:
                    return Pesantren::class;
                    break;
                
                case 5:
                    return KolamRenang::class;
                    break;
    
                case 6:
                    return Puskesmas::class;
                    break;
                
                case 7:
                    return RumahSakit::class;
                    break;
                
                case 8:
                    return Klinik::class;
                    break;
    
                case 9:
                    return Hotel::class;
                    break;

                case 10:
                    return HotelMelati::class;
                    break;
                
                case 11:
                    return Salon::class;
                    break;
                
                default:
                    return [];
                    break;
            }

        }

        if($kategori == 'tpm'){
            switch ($modul) {
                case 1:
                    return JasaBoga::class;
                    break;
                
                case 2:
                    return Kuliner::class;
                    break;
                
                case 3:
                    return DamSippKling::class;
                    break;
            }
        }

        if($kategori == 'lainnya'){
            switch ($modul) {
                case 1:
                    return PelayananKesling::class;
                    break;
                
                // case 2:
                //     return PHBS::class;
                //     break;
            }
        }
    }

    public function getImbPajakWithKey($type, $kategori, $modul){
        $getClass = $this->getClass($kategori, $modul);
        
        if($type == 'pajak'){
            if($kategori == 'rs'){

                return $this->extractBayarPajak(RumahSehat::advancedFilter()->autoFilterByRole()->select('imb')->where('imb', '!=', null)->get());            
            
            } else {

                if($kategori == 'ttu' && $modul == 1){

                } else if ($kategori == 'lainnya'){
                
                } else  {

                    return $this->extractBayarPajak($getClass::advancedFilterWithoutRtRw()->autoFilterByRole()->select('pajak')->where('pajak', '!=', null)->get());
                
                }
            }
            
        } else {

            if($kategori == 'rs'){

                return $this->extractImb(RumahSehat::select('imb')
                        ->where('imb', '!=', null)
                        ->get());            
            } else {

                if($kategori == 'ttu' && $modul == 1){

                } else if ($kategori == 'lainnya'){
                
                } else  {
                    
                return $this->extractImb($getClass::select('pajak')
                        ->where('pajak', '!=', null)
                        ->get());
                }
            }
        }
    }

    public function getSekolahTerbesar(){
        $data = Sekolah::advancedFilter()
        // ->max('luas_bangunan')
        // ->max('luas_tanah')
        // ->max('jumlah_murid_pertahun')
        ->limit(5)
        ->get();

        return [
            'data' => [
                'query' => $data
            ]
        ];
    }

    public function getResourcesClass($kategori, $modul){
        if($kategori == 'rs'){
            return \App\Http\Resources\RumahSehatResource::class;
        }

        if($kategori == 'ttu'){

            switch ($modul) {
                case 1:
                    return \App\Http\Resources\TempatIbadahResource::class;
                    break;
                
                case 2:
                    return \App\Http\Resources\PasarResource::class;
                    break;
                
                case 3:
                    return \App\Http\Resources\SekolahResource::class;
                    break;
                
                case 4:
                    return \App\Http\Resources\PesantrenResource::class;
                    break;
                
                case 5:
                    return \App\Http\Resources\KolamRenangResource::class;
                    break;
    
                case 6:
                    return \App\Http\Resources\PuskesmasResource::class;
                    break;
                
                case 7:
                    return \App\Http\Resources\RumahSakitResource::class;
                    break;
                
                case 8:
                    return \App\Http\Resources\KlinikResource::class;
                    break;
    
                case 9:
                    return \App\Http\Resources\HotelResource::class;
                    break;

                case 10:
                    return \App\Http\Resources\HotelMelatiResource::class;
                    break;
                
                case 11:
                    return \App\Http\Resources\SalonResource::class;
                    break;
                
                default:
                    return [];
                    break;
            }

        }

        if($kategori == 'tpm'){
            switch ($modul) {
                case 1:
                    return \App\Http\Resources\JasaBogaResource::class;
                    break;
                
                case 2:
                    return \App\Http\Resources\RestoranResource::class;
                    break;
                
                case 3:
                    return \App\Http\Resources\DepotAirMinumResource::class;
                    break;
            }
        }

        if($kategori == 'lainnya'){
            switch ($modul) {
                case 1:
                    return \App\Http\Resources\PelayananKesehatanResource::class;
                    break;
                
                // case 2:
                //     return \App\Http\Resources\PHBS::class;
                //     break;
            }
        }
    }

    public function getAllModules(){
        return [
            "Rumah Sehat",
            "Tempat Ibadah",
            "Pasar",
            "Sekolah",
            "Pesantren",
            "Kolam Renang",
            "Puskesmas",
            "Rumah Sakit",
            "Klinik",
            "Hotel",
            "Hotel Melati",
            "Salon",
            "Jasa Boga",
            "Restoran",
            "Depot Air Minum",
            "Pelayanan Kesehatan",
            "PHBS"
        ];
    }

    public function getCountAllModule(){
        $data = [
            RumahSehat::advancedFilter()->count(),
            TempatIbadah::advancedFilterWithoutRtRw()->count(),
            Pasar::advancedFilterWithoutRtRw()->count(),
            Sekolah::advancedFilterWithoutRtRw()->count(),
            Pesantren::advancedFilterWithoutRtRw()->count(),
            KolamRenang::advancedFilterWithoutRtRw()->count(),
            Puskesmas::advancedFilterWithoutRtRw()->count(),
            RumahSakit::advancedFilterWithoutRtRw()->count(),
            Klinik::advancedFilterWithoutRtRw()->count(),
            Hotel::advancedFilterWithoutRtRw()->count(),
            HotelMelati::advancedFilterWithoutRtRw()->count(),
            Salon::advancedFilterWithoutRtRw()->count(),
            JasaBoga::advancedFilterWithoutRtRw()->count(),
            Kuliner::advancedFilterWithoutRtRw()->count(),
            DamSippKling::advancedFilterWithoutRtRw()->count(),
            PelayananKesling::advancedFilterWithoutRtRw()->count(),
            0
        ];

        return $data;
    }

    public function getScoreAngkaKesehatan($labels){
        $bobot = DB::table('skor_kesehatan_lingkungans')->select('bobot')->get();
        for ($i=0; $i < count($labels); $i++) { 
            $label = $labels[$i];

            $data[$i] = round(array_sum([
                ((RumahSehat::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * RumahSehat::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[0]->bobot,
                ((TempatIbadah::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * TempatIbadah::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[1]->bobot,
                ((Pasar::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Pasar::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[2]->bobot,
                ((Sekolah::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Sekolah::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[3]->bobot,
                ((Pesantren::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Pesantren::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[4]->bobot,
                ((KolamRenang::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * KolamRenang::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[5]->bobot,
                ((Puskesmas::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Puskesmas::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[6]->bobot,
                ((RumahSakit::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * RumahSakit::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[7]->bobot,
                ((Klinik::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Klinik::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[8]->bobot,
                ((Hotel::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Hotel::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[9]->bobot,
                ((HotelMelati::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * HotelMelati::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[10]->bobot,
                ((Salon::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Salon::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[11]->bobot,
                ((JasaBoga::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * JasaBoga::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[12]->bobot,
                ((Kuliner::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * Kuliner::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[13]->bobot,
                ((DamSippKling::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * DamSippKling::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[14]->bobot,
                ((PelayananKesling::where('status', true)->whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count() * PelayananKesling::whereHas('petugas', function($query) use ($label){ $query->where('kecamatan_id', getIdKecamatanByName($label)); })->count()) / 100) * $bobot[15]->bobot,
                0
            ]) / 100, 3);
        }

        return $data;
    }

    public function getScoreAngkaKesehatanByYear($year){
        $bobot = DB::table('skor_kesehatan_lingkungans')->select('bobot')->get();
        for ($i=0; $i < count($year); $i++) { 
            $label = $year[$i];
            if($label == 0){
                $data[$i] = 0;
            } else {
                $data[$i] = round(array_sum([
                    ((RumahSehat::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * RumahSehat::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[0]->bobot,
                    ((TempatIbadah::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * TempatIbadah::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[1]->bobot,
                    ((Pasar::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Pasar::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[2]->bobot,
                    ((Sekolah::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Sekolah::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[3]->bobot,
                    ((Pesantren::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Pesantren::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[4]->bobot,
                    ((KolamRenang::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * KolamRenang::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[5]->bobot,
                    ((Puskesmas::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Puskesmas::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[6]->bobot,
                    ((RumahSakit::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * RumahSakit::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[7]->bobot,
                    ((Klinik::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Klinik::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[8]->bobot,
                    ((Hotel::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Hotel::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[9]->bobot,
                    ((HotelMelati::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * HotelMelati::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[10]->bobot,
                    ((Salon::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Salon::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[11]->bobot,
                    ((JasaBoga::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * JasaBoga::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[12]->bobot,
                    ((Kuliner::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * Kuliner::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[13]->bobot,
                    ((DamSippKling::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * DamSippKling::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[14]->bobot,
                    ((PelayananKesling::where('status', true)->where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count() * PelayananKesling::where(function($query) use ($label){ if($label != null){ $query->where(DB::raw('extract(year from created_at)'), '=', $label); } else { return $query; } })->count()) / 100) * $bobot[15]->bobot,
                    0
                ]) / 100, 3);
            }
        }

        return $data;
    }

    public function depokHealthy(){
        $bobot = DB::table('skor_kesehatan_lingkungans')->select('bobot')->get();
        
        $data = round(array_sum([
            ((RumahSehat::where('status', true)->count() * RumahSehat::count()) / 100) * $bobot[0]->bobot,
            ((TempatIbadah::where('status', true)->count() * TempatIbadah::count()) / 100) * $bobot[1]->bobot,
            ((Pasar::where('status', true)->count() * Pasar::count()) / 100) * $bobot[2]->bobot,
            ((Sekolah::where('status', true)->count() * Sekolah::count()) / 100) * $bobot[3]->bobot,
            ((Pesantren::where('status', true)->count() * Pesantren::count()) / 100) * $bobot[4]->bobot,
            ((KolamRenang::where('status', true)->count() * KolamRenang::count()) / 100) * $bobot[5]->bobot,
            ((Puskesmas::where('status', true)->count() * Puskesmas::count()) / 100) * $bobot[6]->bobot,
            ((RumahSakit::where('status', true)->count() * RumahSakit::count()) / 100) * $bobot[7]->bobot,
            ((Klinik::where('status', true)->count() * Klinik::count()) / 100) * $bobot[8]->bobot,
            ((Hotel::where('status', true)->count() * Hotel::count()) / 100) * $bobot[9]->bobot,
            ((HotelMelati::where('status', true)->count() * HotelMelati::count()) / 100) * $bobot[10]->bobot,
            ((Salon::where('status', true)->count() * Salon::count()) / 100) * $bobot[11]->bobot,
            ((JasaBoga::where('status', true)->count() * JasaBoga::count()) / 100) * $bobot[12]->bobot,
            ((Kuliner::where('status', true)->count() * Kuliner::count()) / 100) * $bobot[13]->bobot,
            ((DamSippKling::where('status', true)->count() * DamSippKling::count()) / 100) * $bobot[14]->bobot,
            ((PelayananKesling::where('status', true)->count() * PelayananKesling::count()) / 100) * $bobot[15]->bobot,
            0
        ]) / 100, 3);

        return $data;
    }

    private function getTotalStatusModules($class, $year = null, $status){

        $data = [
            $class::advancedFilter()->autoFilterByRole()
            ->where(function($query) use ($year){
                if($year != null){
                    $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                } else {
                    return $query;
                }
            })
            ->where('status', $status)
            ->count()
        ];

        return array_sum($data);
    }

    /**
     * green => ['#329059', 'rgba(50, 144, 89, 0.96)']
     * red => ['#f86c6b', 'rgba(255,99,132,0.2)']
     * yellow => ['#ffc107', 'rgba(255, 193, 7, 0.72)']
     * cyan => ['#63c2de', 'rgba(99, 194, 222, 0.73)']
     * purple => ['#6f42c1', 'rgba(111, 66, 193, 0.77)']
     */
    public function uniqueDatasetsRumahSehat($modul, $labels, $grafik){
        switch ($modul) {
            case 1:
                $conf = [
                    'kategori' => ['Sehat', 'Tidak Sehat'],
                    'alias' => [true, false],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'],
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];
                $col = 'status';
                break;
            
            case 2:
                $conf = [
                    'kategori' => ['Tidak Ada Jentik', 'Ada Jentik'],
                    'alias' => [true, false],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'],
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];

                $col = 'pjb';
                break;
            
            case 3:
                $conf = [
                    'kategori' => ['Dipilah', 'Tidak Dipilah'],
                    'alias' => [true, false],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'],
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];

                $col = 'spal';
                break;
            
            case 4:
                $conf = [
                    'kategori' => ['Rendah (R)', 'Sedang (S)', 'Tinggi (T)', 'Amat Tinggi (AT)'],
                    'alias' => ['R', 'S', 'T', 'AT'],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'], 
                        ['#63c2de', 'rgba(99, 194, 222, 0.73)'],
                        ['#ffc107', 'rgba(255, 193, 7, 0.72)'], 
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];

                $col = 'sab';
                break;
            
            case 5:
                $conf = [
                    'kategori' => ['Koya', 'Kali', 'Helikopter', 'Septic Tank'],
                    'alias' => ['Koya', 'Kali', 'Helikopter', 'Septic Tank'],
                    'color' => [
                        ['#f86c6b', 'rgba(255,99,132,0.2)'],
                        ['#ffc107', 'rgba(255, 193, 7, 0.72)'],
                        ['#6f42c1', 'rgba(111, 66, 193, 0.77)'], 
                        ['#63c2de', 'rgba(99, 194, 222, 0.73)']
                    ]
                ];
                
                $col = 'jamban';
                break;
            
            case 6:
                $conf = [
                    'kategori' => ['Dipilah', 'Tidak Dipilah'],
                    'alias' => [true, false],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'],
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];

                $col = 'sampah';
                break;
            
            case 7:
                $conf = [
                    'kategori' => ['Kontrak', 'Rumah Sendiri'],
                    'alias' => [false, true],
                    'color' => [
                        ['#329059', 'rgba(50, 144, 89, 0.96)'],
                        ['#f86c6b', 'rgba(255,99,132,0.2)']
                    ]
                ];

                $col = 'status_rumah';
                break;
        }

        $datasets = $this->createDatasetsGraphRumahSehat($labels, $col, $conf, $grafik);

        return response()->json([
            'data' => [
                'labels' => $labels,
                'datasets' => $datasets
            ]
        ]);
    }

    private function createDatasetsGraphRumahSehat($labels, $col, $conf, $grafik){        
        for ($i=0; $i < count($conf['alias']); $i++) { 
            $data[$i] = [
                'label' => $conf['kategori'][$i],
                'backgroundColor' => $conf['color'][$i][0],
                'fill' => false,
                'borderColor' => $conf['color'][$i][1],
                'borderWidth' => 7,
                'data' => $this->generateDatasetsRumahSehat($labels, $conf, $conf['alias'][$i], $col, $grafik)
            ];
        }

        return $data;
    }

    private function generateDatasetsRumahSehat($labels, $conf, $statement, $col, $grafik){
        if($col == 'sab'){
            $class = new Sab;
            $col = 'status';
        } else {
            $class = new RumahSehat;
        }

        if($grafik == 'line'){

            for ($i=0; $i < count($labels); $i++) { 

                $year = $labels[$i];
                if($year == '0'){
                    $data[$i] = 0;
                } else {
                    $data[$i] = $class::advancedFilter()->autoFilterByRole()
                    ->where(function($query) use ($year){
                        if($year != null){
                            $query->where(DB::raw('extract(year from waktu)'), '=', $year);
                        } else {
                            return $query;
                        }
                    })
                    ->where($col, $statement)->count();
                }
            }

        } else {

            for ($i=0; $i < count($labels); $i++) { 

                $getNameLabel = $labels[$i];
                $data[$i] = $class::advancedFilter()->autoFilterByRole()
                    ->whereHas('petugas', function($query) use ($getNameLabel){
                        $query->where('kecamatan_id', getIdKecamatanByName($getNameLabel));
                    })
                    ->where($col, $statement)
                    ->count();

            }

        }

        return $data;
    }
}