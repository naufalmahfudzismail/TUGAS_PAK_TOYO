<?php

namespace App\Support\Modules;

use App\Support\Modules\GeneralInformation;

class KartuKeluarga extends ModulesStructure {

    public function getKartuKeluargaChild(){
        // return 1;
        $total = [
            'no_filter' => \App\Models\KartuKeluarga::sum('jumlah_anggota'),
            'with_filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->sum('jumlah_anggota')
        ];

        $total_keluarga = [
            'no_filter' => \App\Models\KartuKeluarga::count(),
            'with_filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->count()
        ];
        
        $value = [
            'lakilaki' => [
                'filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->sum('jumlah_laki_laki'),
                'kecamatan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kecamatan_id', request('kecamatan'));
                })->sum('jumlah_laki_laki'),
                'kelurahan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kelurahan_id', request('kelurahan'));
                })->sum('jumlah_laki_laki')
            ],
            'perempuan' => [
                'filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->sum('jumlah_perempuan'),
                'kecamatan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kecamatan_id', request('kecamatan'));
                })->sum('jumlah_perempuan'),
                'kelurahan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kelurahan_id', request('kelurahan'));
                })->sum('jumlah_perempuan')
            ],
            'depok' => [
                'filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->where('depok', true)->count(),
                'kecamatan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kecamatan_id', request('kecamatan'));
                })->where('depok', true)->count(),
                'kelurahan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kelurahan_id', request('kelurahan'));
                })->where('depok', true)->count()
            ],
            'bukan_depok' => [
                'filter' => \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->where('depok', false)->count(),
                'kecamatan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kecamatan_id', request('kecamatan'));
                })->where('depok', false)->count(),
                'kelurahan' => \App\Models\KartuKeluarga::whereHas('petugas', function($query){
                    $query->where('kelurahan_id', request('kelurahan'));
                })->where('depok', false)->count()
            ]
        ];
        
        $kartukeluarga = [
            'lakilaki' => $this->generalChildInitialize($value['lakilaki'], $total),
            'perempuan' => $this->generalChildInitialize($value['perempuan'], $total),
            'depok' => $this->generalChildInitialize($value['depok'], $total_keluarga),
            'bukan_depok' => $this->generalChildInitialize($value['bukan_depok'], $total_keluarga),
            'total' => $this->generalChildInitialize($total['with_filter'], $total, true)
        ];

        $waktu = \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->select('created_at')->latest()->first();

        if(\App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->select('created_at')->latest()->first() != null){
            $waktu = \App\Models\KartuKeluarga::advancedFilterWithoutRtRw()->select('created_at')->latest()->first()->waktu;
        } else {
            $waktu = 0;
        }

        $kartukeluarga = array_merge($kartukeluarga, [
            'last_update' => $waktu
        ]);

        return $kartukeluarga;
    }

}