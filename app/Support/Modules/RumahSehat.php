<?php

namespace App\Support\Modules;

use App\Support\Modules\GeneralInformation;
use App\Models\Moduls\Sab;
use App\Models\Moduls\RumahSehat as RumahSehatModuls;
use DB;

class RumahSehat extends ModulesStructure {
    protected $total;
    public function __construct(RumahSehatModuls $rumah_sehat, Sab $sab){
        $this->total = [
            'rumah_sehat' => $rumah_sehat::autoFilterByRole()->count(),
            'sab' => $sab::autoFilterByRole()->count()
        ];
    }
    
    public function index($query = false){
        
        $rumahsehat = [
            'general' => $this->general($query),
            'items' => $this->items()
        ];

        return $rumahsehat;
    }

    public function items(){

        $items = [
            'rumah_sehat' => $this->rumah_sehat(),
            'pjb' => $this->pjb(),
            'spal' => $this->spal(),
            'sab' => $this->sab(),              
            'jamban' => $this->jamban(),
            'sampah' => $this->sampah(),
            'stt' => $this->statusTempatTinggal()
        ];

        return $items;

    }

    public function general($query = false){
        $latestTime = RumahSehatModuls::advancedFilter()->autoFilterByRole()->latest('waktu')->select('waktu')->first();
        
        if($latestTime != null){
            $latestTime = RumahSehatModuls::advancedFilter()->autoFilterByRole()->latest('waktu')->select('waktu')->first()->waktu;
            $date = date('Y-m-d', strtotime($latestTime));
        } else {
            $date = 0;
            $latestTime = 0;
        }

        $general = [
            'last_update' => $latestTime,
            'tanggal_terakhir' => $date
        ];

        if($query != false){

            if($date == 0 || $latestTime == 0){

                $general = array_merge($general, [

                    'query' => [],
                
                ]);

            } else {

                if(request('tanggal') != null){
                    
                    $general = array_merge($general, [

                        'query' => \App\Http\Resources\RumahSehatResource::collection(RumahSehatModuls::autoFilterByRole()->advancedFilter()->orderBy('id', 'desc')->get()),
                    
                    ]);

                } else {

                    $general = array_merge($general, [
                    
                        'query' => \App\Http\Resources\RumahSehatResource::collection(RumahSehatModuls::advancedFilter()->autoFilterByRole()->where(DB::raw('date(waktu)'), $date)->orderBy('id', 'desc')->get()),
                    
                    ]);

                }
            }
        }

        return $general;
    }

    public function rumah_sehat(){
        $value = [
            'sehat' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('status', true)->count(),
            'tidak_sehat' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('status', false)->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count()
        ];
        
        $rumah_sehat = [
            'sehat' => $this->generalChildInitialize($value['sehat'], $this->total['rumah_sehat']),
            'tidak_sehat' =>  $this->generalChildInitialize($value['tidak_sehat'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat']),
        ];

        $rumah_sehat = array_merge($rumah_sehat, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $rumah_sehat;
    }

    public function sab(){        
        $value = [
            'pompa' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('kategori', 'A')->count(),
            'sumur_gali' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('kategori', 'B')->count(),
            'sumur_gali_plus' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('kategori', 'C')->count(),
            'perpipaan_pdam' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('kategori', 'D')->count(),
            'mata_air' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('kategori', 'E')->count(),
            'amat_tinggi' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('status', 'AT')->count(),
            'tinggi' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('status', 'T')->count(),
            'sedang' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('status', 'S')->count(),
            'rendah' => Sab::sabAdvancedFilter()->autoFilterByRole()->where('status', 'R')->count(),
            'total' => Sab::sabAdvancedFilter()->autoFilterByRole()->count()
        ];

        $sab = [
            'pompa' => $this->generalChildInitialize($value['pompa'], $this->total['sab']),
            'sumur_gali' => $this->generalChildInitialize($value['sumur_gali'], $this->total['sab']),
            'sumur_gali_plus' => $this->generalChildInitialize($value['sumur_gali_plus'], $this->total['sab']),
            'perpipaan_pdam' => $this->generalChildInitialize($value['perpipaan_pdam'], $this->total['sab']),
            'mata_air' => $this->generalChildInitialize($value['mata_air'], $this->total['sab']),
            'amat_tinggi' => $this->generalChildInitialize($value['amat_tinggi'], $this->total['sab']),
            'tinggi' => $this->generalChildInitialize($value['tinggi'], $this->total['sab']),
            'sedang' => $this->generalChildInitialize($value['sedang'], $this->total['sab']),            
            'rendah' => $this->generalChildInitialize($value['rendah'], $this->total['sab']),            
            'total' => $this->generalChildInitialize($value['total'], $this->total['sab']),
        ];

        $sab = array_merge($sab, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $sab;
    }

    public function pjb(){
        $value = [
            'tidak_ada_jentik' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('pjb', true)->count(),
            'ada_jentik' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('pjb', false)->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count()
        ];

        $pjb = [
            'tidak_ada_jentik' => $this->generalChildInitialize($value['tidak_ada_jentik'], $this->total['rumah_sehat']),
            'ada_jentik' => $this->generalChildInitialize($value['ada_jentik'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat']),
        ];

        $pjb = array_merge($pjb, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $pjb;
    }

    public function jamban(){
        $value = [
            'koya' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('jamban', "Koya / Empang")->count(),
            'kali' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('jamban', "Kali")->count(),
            'helikopter' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('jamban', "Helikopter")->count(),
            'septik_tank' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('jamban', "Septik Tank")->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count()
        ];

        $jamban = [
            'koya' => $this->generalChildInitialize($value['koya'], $this->total['rumah_sehat']),
            'kali' => $this->generalChildInitialize($value['kali'], $this->total['rumah_sehat']),
            'helikopter' => $this->generalChildInitialize($value['helikopter'], $this->total['rumah_sehat']),
            'septik_tank' => $this->generalChildInitialize($value['septik_tank'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat'])
        ];

        $jamban = array_merge($jamban, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $jamban;
    
    }

    public function spal(){
        $value = [
            'tertutup' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('spal', true)->count(),
            'terbuka' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('spal', false)->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count(),
        ];
        
        $spal = [
            'tertutup' => $this->generalChildInitialize($value['tertutup'], $this->total['rumah_sehat']),
            'terbuka' => $this->generalChildInitialize($value['terbuka'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat']),
        ];

        $spal = array_merge($spal, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $spal;
    
    }

    public function statusTempatTinggal(){
        
        $value = [
            'rumah_sendiri' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('status_rumah', true)->count(),
            'kontrak' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('status_rumah', false)->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count()
        ];

        $statusTempatTinggal = [
            'rumah_sendiri' => $this->generalChildInitialize($value['rumah_sendiri'], $this->total['rumah_sehat']),
            'kontrak' => $this->generalChildInitialize($value['kontrak'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat']),
        ];

        $statusTempatTinggal = array_merge($statusTempatTinggal, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $statusTempatTinggal;
    }

    public function sampah(){
        $value = [
            'dipilah' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('sampah', true)->count(),
            'tidak_dipilah' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->where('sampah', true)->count(),
            'total' => RumahSehatModuls::advancedFilter()->autoFilterByRole()->count()
        ];

        $sampah = [
            'dipilah' => $this->generalChildInitialize($value['dipilah'], $this->total['rumah_sehat']),
            'tidak_dipilah' => $this->generalChildInitialize($value['tidak_dipilah'], $this->total['rumah_sehat']),
            'total' => $this->generalChildInitialize($value['total'], $this->total['rumah_sehat'])
        ];

        $sampah = array_merge($sampah, [
            'last_update' => $this->general(false)['last_update']
        ]);

        return $sampah;
    
    }

    public function getRumahSehatChild($param){
        switch ($param) {
            case 1:
                return $this->rumah_sehat();
                break;
            
            case 2:
                return $this->pjb();
                break;
            
            case 3:
                return $this->spal();
                break;
            
            case 4:
                return $this->sab();
                break;
            
            case 5:
                return $this->jamban();
                break;

            case 6:
                return $this->sampah();
                break;
            
            case 7:
                return $this->statusTempatTinggal();
                break;
            
            default:
                return [];
                break;
        }
    }

    public function getGrafikRumahSehat($type){
        if($type == 'bar'){

            return [
                [11,21,31,41,51,61,71,81,91,10,11],
                [11,21,31,41,51,61,71,81,91,10,11]
            ];

        } else {

            return [
                [11,21,31,41,51,61,71,81,91,10,11],
                [11,21,31,41,51,61,71,81,91,10,11]
            ];

        }
    }
}