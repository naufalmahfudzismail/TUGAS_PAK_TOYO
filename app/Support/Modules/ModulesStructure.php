<?php

namespace App\Support\Modules;

use App\Models\Moduls\RumahSehat;
use App\Models\Moduls\TempatIbadah;
use App\Models\Moduls\Pasar;
use App\Models\Moduls\Sekolah;
use App\Models\Moduls\Pesantren;
use App\Models\Moduls\KolamRenang;
use App\Models\Moduls\Puskesmas;
use App\Models\Moduls\RumahSakit;
use App\Models\Moduls\Klinik;
use App\Models\Moduls\Hotel;
use App\Models\Moduls\HotelMelati;
use App\Models\Moduls\Salon;
use App\Models\Moduls\JasaBoga;
use App\Models\Moduls\Kuliner;
use App\Models\Moduls\DamSippKling;
use App\Models\Moduls\PelayananKesling;
// use App\Models\Moduls\PHBS;

class ModulesStructure {

    protected $fcRumahSehat, $fcTempatIbadah, $fcPasar, $fcSekolah, $fcPesantren, $fcKolamRenang, $fcPuskesmas,$fcRumahSakit,$fcKlinik,$fcHotel, $fcHotelMelati, $fcSalon, $fcJasaBoga, $fcRestoran, $fcDepotAirMinum, $fcPelayananKesehatan, $fcPHBS;
    public function __construct()
    {
        $this->arrModulesGroup = [
            RumahSehat::advancedFilterWithoutRtRw()->autoFilterByRole(),
            TempatIbadah::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Pasar::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Sekolah::advancedFilterWithoutRtRw()->autoFilterByRole(),
            KolamRenang::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Puskesmas::advancedFilterWithoutRtRw()->autoFilterByRole(),
            RumahSakit::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Klinik::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Hotel::advancedFilterWithoutRtRw()->autoFilterByRole(),
            HotelMelati::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Salon::advancedFilterWithoutRtRw()->autoFilterByRole(),
            JasaBoga::advancedFilterWithoutRtRw()->autoFilterByRole(),
            Kuliner::advancedFilterWithoutRtRw()->autoFilterByRole(),
            DamSippKling::advancedFilterWithoutRtRw()->autoFilterByRole(),
            PelayananKesling::advancedFilterWithoutRtRw()->autoFilterByRole()
        ];
    }

    public function getClassWithFilter($className){
        switch ($className) {
            case 'RumahSehat':
                return $this->arrModulesGroup[0];
                break;
            
            case 'TempatIbadah':
                return $this->arrModulesGroup[1];
                break;

            case 'Pasar':
                return $this->arrModulesGroup[2];
                break;

            case 'Sekolah':
                return $this->arrModulesGroup[3];
                break;
            
            case 'KolamRenang':
                return $this->arrModulesGroup[4];
                break;
            
            case 'Puskesmas':
                return $this->arrModulesGroup[5];
                break;
            
            case 'RumahSakit':
                return $this->arrModulesGroup[6];
                break;
            
            case 'Klinik':
                return $this->arrModulesGroup[7];
                break;
            
            case 'Hotel':
                return $this->arrModulesGroup[8];
                break;
            
            case 'HotelMelati':
                return $this->arrModulesGroup[9];
                break;
            
            case 'Salon':
                return $this->arrModulesGroup[10];
                break;
            
            case 'JasaBoga':
                return $this->arrModulesGroup[11];
                break;
            
            case 'Kuliner':
                return $this->arrModulesGroup[12];
                break;
            
            case 'DamSippKling':
                return $this->arrModulesGroup[13];
                break;
            
            case 'PelayananKesling':
                return $this->arrModulesGroup[14];
                break;

            case 'All':
                return $this->arrModulesGroup;
                break;
                
            default:
                throw new \Exception("Class not registered");
            break;
        }

    }

    /**
     * @mixed
     * 1 => sehat, tidak sehat
     * 2 => layak, tidak layak
     */
    public function getGeneralStructure($data = null, $custom = null){

        if($data == null){
            return $this->getCustomStructure($custom);
        }

        if($data == 1){
            
            $structure = [

                'sehat' => ['status', true],
                'tidak_sehat' => ['status', false]
            
            ];

        } else {

            $structure = [
            
                'layak' => ['status', true],
                'tidak_layak' => ['status', false]
            
            ];

        }

        return $structure;
    }

    protected function generalChildInitialize($value, $total){
        if($total == 0){

            $childInitialize =  [
                'value' => $value,
                'percent' => "0%",
                'legend' => [
                    'Jumlah data '. $value
                ]
            ];

        } else {

            $childInitialize =  [
                'value' => $value,
                'percent' => round(($value / $total) * 100, 2). "%",
                'legend' => [
                    'Jumlah data '. $value
                ]
            ];

        }

        return $childInitialize;
    }
}