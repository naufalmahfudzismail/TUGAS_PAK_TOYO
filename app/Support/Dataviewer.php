<?php
namespace App\Support;
use Illuminate\Validation\ValidationException;
use DB;
use Auth;

trait Dataviewer {

    public function scopeAdvancedFilter($query){
        return $this->process($query, request()->all())
            ->whereHas('petugas', function($query){

                if(request('kecamatan') != null && request('kelurahan') != null){
                    $query->where('kecamatan_id', request('kecamatan'))
                        ->where('kelurahan_id', request('kelurahan'));
                } else if (request('kecamatan') != null) {
                    $query->where('kecamatan_id', request('kecamatan'));
                } else {
                    return $query;
                }

            })
            ->where(function($query){
                if(request('rw') != null && request('rt') != null){
                    $query->where('rw', request('rw'))
                        ->where('rt', request('rt'));
                } else if (request('rw') != null){
                    $query->where('rw', request('rw'));
                } else {
                    //
                }

                if(request('tanggal') != null){
                    $newTanggal = request('tanggal').' 00:00:00';

                    $query->where(DB::raw('date(waktu)'), '=', request('tanggal'));
                }
            });
    }

    public function scopeSabAdvancedFilter($query){
        return $this->process($query, request()->all())
            ->whereHas('petugas', function($query){
                if(request('kecamatan') != null){
                    $query->where('kecamatan_id', request('kecamatan'));                    
                } else if (request('kecamatan') != null && request('kelurahan') != null){
                    $query->where('kecamatan_id', request('kecamatan'))
                        ->where('kelurahan_id', request('kelurahan'));
                } else {
                    // do nothing
                }
            })
            ->whereHas('rumahSehat', function($query){
                if(request('rw') != null){
                    $query->where('rw', request('rw'));
                } else if (request('rw') != null && request('rt') != null){
                    $query->where('rw', request('rw'))
                        ->where('rt', request('rt'));
                } else {
                    // do nothing
                }
            });
    }

    public function scopeAdvancedFilterWithoutRtRw($query){
        return $this->process($query, request()->all())
            ->whereHas('petugas', function($query){
                if(request('kecamatan') != null && request('kelurahan') != null){
                    $query->where('kecamatan_id', request('kecamatan'))
                        ->where('kelurahan_id', request('kelurahan'));
                } else if (request('kecamatan') != null) {
                    $query->where('kecamatan_id', request('kecamatan'));
                } else {
                    return $query;
                }
            })
            ->where(function($query){
                if(request('tanggal') != null){
                    $newTanggal = request('tanggal').' 00:00:00';

                    $query->where(DB::raw('date(waktu)'), '=', request('tanggal'));
                }
            });
    }

    public function scopeAdvancedFilterWithoutStaticClass($query){
        return $this->process($query, request()->all())
        ->where(function($query){
            if(request('kecamatan') != null && request('kelurahan') != null){
                $query->where('petugas_sippklings.kecamatan_id', request('kecamatan'))
                    ->where('petugas_sippklings.kelurahan_id', request('kelurahan'));
            } else if (request('kecamatan') != null){
                $query->where('petugas_sippklings.kecamatan_id', request('kecamatan'));                
            } else {

            }
        });
    }

    public function scopeAutoFilterByRole($query){
        return $this->process($query, request()->all())
        ->whereHas('petugas', function($query){
            $role = Auth::user();
            if($role != null){
                switch($role->role){
                    case 1:
                    break;
                    case 2:
                    break;
                    case 3:
                        // $builder
                        // ->leftJoin('petugas_sippklings', 'rumah_sehats.petugas_id', 'petugas_sippklings.id')
                        // ->where('petugas_sippklings.kecamatan_id', Auth::user()->kecamatan_id);          
                            $query->where('kecamatan_id', Auth::user()->kecamatan_id);  
                    break;
                    case 4:
                        // $builder
                        // ->leftJoin('petugas_sippklings', 'rumah_sehats.petugas_id', 'petugas_sippklings.id')
                        // ->where('petugas_sippklings.kecamatan_id', Auth::user()->kecamatan_id)      
                        // ->where('petugas_sippklings.kelurahan_id', Auth::user()->kelurahan_id);
                            $query->where('kecamatan_id', Auth::user()->kecamatan_id)
                                ->where('kelurahan_id', Auth::user()->kelurahan_id);
                    break;
                    default:
                }
            }
        });
    }

    public function process($query, $data){
        return (new CustomQueryBuilder)->apply($query, $data);
    }
}
?>