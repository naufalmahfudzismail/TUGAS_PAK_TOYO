<?php

namespace App\Support;

class CustomQueryBuilder {
    public function apply($query, $data)
    {
        if(isset($data['f'])) {
            foreach($data['f'] as $filter) {
                $filter['match'] = isset($filter['filter_match']) ? $ $filter['filter_match'] : 'and';
                $this->makeFilter($query, $filter);
            }
        }

        return $query;
    }

    
}
?>