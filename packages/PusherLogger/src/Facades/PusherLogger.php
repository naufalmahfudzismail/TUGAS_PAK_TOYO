<?php // File: ./src/Facades/PusherLogger.php

    namespace SippPackage\PusherLogger\Facades;

    use Illuminate\Support\Facades\Facade;

    class PusherLogger extends Facade
    {
        protected static function getFacadeAccessor()
        {
            return 'pusher-logger';
        }
    }
