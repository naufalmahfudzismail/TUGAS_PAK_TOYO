<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function(){
//     return view('index');
// });

// Auth::routes();

// Route::get('/dashboard','AppController@index')->name('dashboard');

// Route::get('/{path}', 'AppController@index')
// ->where('path', '([A-z\d-\/_.]+)?');

// Route::get('/{vue_capture?}', function () {
//     return view('app');
// })->where('vue_capture', '[\/\w\.-]*');

// Route::get('/{vue_capture?}', 'AppController@index')->where('vue_capture', '[\/\w\.\,\-]*');

// Route::get('/{vue_capture?}', 'AppController@index')
//     ->middleware(['speed'])
//     ->where('vue_capture', '^(?!api\/)[\/\w\.-]*');
// '^(?!api\/)[\/\w\.-]*'
// '([A-z\d-\/_.]+)?'

// Auth::routes();

Route::get('/{any?}', function (){
    return view('app');
})->where('any', '^(?!api\/)[\/\w\.-]*');

// Route::post('/push', 'LogController')->name('log-message');
// Route::get('/', function () {
//     return view('welcome');
// });
// info('Testing');
