<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::post('validation', 'AuthController@validation');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });

    Route::group(['middleware' => 'jwt.refresh'], function () {
        Route::get('refresh', 'AuthController@refresh');
    });
});

Route::group(['middleware' => 'jwt.auth'], function () {});

/**
 * remember to move into middleware
 */
Route::apiResources(['users' => 'API\UserController']);
Route::get('users/custom/json-to-csv', 'API\UserController@getJsonToCsv');
Route::apiResources(['petugas-sippklings' => 'API\PetugasSippklingController']);
Route::get('petugas-sippklings/custom/get-petugas-sippklings', 'API\PetugasSippklingController@getPetugasSippklings');
Route::get('petugas-sippklings/custom/get-petugas-sippklings/{id}', 'API\PetugasSippklingController@showPetugasSippkling');
Route::put('petugas-sippklings/custom/update-petugas-sippklings/{id}', 'API\PetugasSippklingController@customUpdatePetugasSippkling');
Route::get('petugas-sippklings/custom/get-list-name-petugas-sippklings', 'API\PetugasSippklingController@getListNamePetugasSippkling');
Route::get('petugas-sippklings/custom/json-to-csv', 'API\PetugasSippklingController@getJsonToCsv');
Route::post('petugas-sippklings/custom/import-csv', 'API\PetugasSippklingController@importCsv');
Route::post('tempat-ibadah/custom/import-csv', 'API\TempatIbadahController@importCsv');
Route::post('pasar/custom/import-csv', 'API\PasarController@importCsv');
Route::post('sekolah/custom/import-csv', 'API\SekolahController@importCsv');
Route::post('pesantren/custom/import-csv', 'API\PesantrenController@importCsv');
Route::post('kolam-renang/custom/import-csv', 'API\KolamRenangController@importCsv');
Route::post('puskesmas/custom/import-csv', 'API\PuskesmasController@importCsv');
Route::post('rumah-sakit/custom/import-csv', 'API\RumahSakitController@importCsv');
Route::post('klinik/custom/import-csv', 'API\KlinikController@importCsv');
Route::post('hotel/custom/import-csv', 'API\HotelController@importCsv');
Route::post('hotel-melati/custom/import-csv', 'API\HotelMelatiController@importCsv');
Route::post('dam/custom/import-csv', 'API\DamSippKlingController@importCsv');
Route::post('jasa-boga/custom/import-csv', 'API\JasaBogaController@importCsv');

Route::apiResources(['kecamatans' => 'API\KecamatanController']);
Route::get('kecamatans/custom/kecamatans-select-option', 'API\KecamatanController@getKecamatanForSelectForm');
Route::get('kelurahans/custom/get-kelurahans-name/id/{kecamatan_id}', 'API\KelurahanController@getKelurahansName');
Route::get('kelurahans/custom/kelurahan-select-option-by-kecamatan/{kecamatan_id}', 'API\KelurahanController@getKelurahanForSelectFormByKecamatan');
Route::get('kelurahans/all', 'API\KelurahanController@indexKecKel');
Route::get('dashboards', 'API\DashboardController@index');
Route::get('rewards/all/web', 'API\RewardController@allRewardForWeb');
Route::apiResources(['messages' => 'API\MessageController']);
Route::get('rumahsehat/custom/dashboards', 'API\RumahSehatController@dashboardRumahSehat');
Route::delete('rumahsehat/custom/destroy-from-website/{id}', 'API\RumahSehatController@destroyFromWebsite');
Route::get('histori/custom/get-history-with-limit/{limit}/data/{data_id}', 'API\HistoryController@getHistoryWithLimit');
Route::get('histori/semua', 'API\HistoryController@getHistoriSemua');
Route::get('list-opd-user', 'API\OpdController@index');
Route::get('list-opd-radio-group-selected', 'API\OpdController@listOpdRadioGroupSelected');
Route::post('opds', 'API\OpdController@store');
Route::post('change-opd-modul-status/{user_id}/{data_id}', 'API\OpdController@changeOpdModulStatus');
Route::get('get-opd-by-data/{data_id}', 'API\OpdController@getOpdByData');
Route::get('get-modules-data-satuan/{kategori}/{modules}', 'API\DashboardController@getModulesDataSatuan');
Route::get('get-modules-data/{kategori}', 'API\DashboardController@getModulesData');
Route::get('get-grafik-tabel/{kategori}', 'API\DashboardController@getGrafikTabelData');
// Route::get('grafik-dashboard-utama', 'API\DashboardController@getGrafikDashboardUtama');
Route::get('grafik-stats-pajak/{kategori}/{modul}/{grafik}', 'API\DashboardController@getGrafikStatsPajak');
Route::get('grafik-stats/{kategori}/{modul}/{grafik}', 'API\DashboardController@getGrafikStats');
Route::get('grafik-structure', 'API\DashboardController@getGrafikStructure');
Route::get('get-total-pajak/{kategori}/{modul}', 'API\DashboardController@getTotalPajakImb');
Route::get('get-general-module-data/{kategori}/{modul}', 'API\DashboardController@getGeneralModuleData');
Route::get('get-sekolah-terbesar', 'API\DashboardController@getSekolahTerbesar');
Route::get('grafik-stats-jumlah', 'API\DashboardController@getGrafikStatsJumlah');
Route::get('generate-count-modul-data-id', 'API\DashboardController@generateCountModule');
Route::get('data-opd-access-module', 'API\DashboardController@dataOpdAccessModule');
Route::apiResources(['skor' => 'API\SkorController']);
Route::put('update-skor', 'API\SkorController@updateSkor');
Route::get('get-grafik-score', 'API\SkorController@getGrafikScore');
Route::post('update-skor-sehat', 'API\SkorController@changeScore');
Route::get('get-data-kesehatan', 'API\SkorController@getDataKesehatan');
Route::get('get-total-kesehatan-lingkungan', 'API\SkorController@getTotalKesehatanLingkungan');
Route::get('get-grafik-kesehatan-lingkungan', 'API\SkorController@getGrafikKesehatanLingkungan');
Route::get('kartu-keluarga', 'API\KartuKeluargaController@index');
Route::get('kepadatan-keluarga', 'API\KartuKeluargaController@getKepadatan');
Route::get('validasi-data', 'API\ValidasiDataController@index');
Route::get('report', 'API\ReportController@indexWeb');


Route::apiResources(['rumahsehat' => 'API\RumahSehatController']);
Route::apiResources(['rumahsakit' => 'API\RumahSakitController']);
Route::apiResources(['puskesmas' => 'API\PuskesmasController']);
Route::apiResources(['hotel' => 'API\HotelController']);
Route::apiResources(['hotelmelati' => 'API\HotelMelatiController']);
Route::apiResources(['jasaboga' => 'API\JasaBogaController']);
Route::apiResources(['klinik' => 'API\KlinikController']);
Route::apiResources(['kolamrenang' => 'API\KolamRenangController']);
Route::apiResources(['kuliner' => 'API\KulinerController']);
Route::apiResources(['pasar' => 'API\PasarController']);
Route::apiResources(['pelayanankesling' => 'API\PelayananKeslingController']);
Route::apiResources(['pesantren' => 'API\PesantrenController']);
Route::apiResources(['petugas' => 'API\PetugasSippklingController']);
Route::apiResources(['sab' => 'API\SabController']);
Route::apiResources(['dam' => 'API\DamSippKlingController']);
Route::apiResources(['sekolah' => 'API\SekolahController']);
Route::apiResources(['tempatibadah' => 'API\TempatIbadahController']);
Route::apiResources(['histori' => 'API\HistoryController']);
Route::apiResources(['kk' => 'API\KartuKeluargaController']);
Route::apiResources(['salon' => 'API\SalonController']);
Route::apiResources(['report' => 'API\ReportController']);

Route::post('update/rumahsakit/{id}', 'API\RumahSakitController@update');
Route::post('update/puskesmas/{id}', 'API\PuskesmasController@update');
Route::post('update/hotel/{id}', 'API\HotelController@update');
Route::post('update/hotelmelati/{id}', 'API\HotelMelatiController@update');
Route::post('update/jasaboga/{id}', 'API\JasaBogaController@update');
Route::post('update/klinik/{id}', 'API\KlinikController@update');
Route::post('update/kolamrenang/{id}', 'API\KolamRenangController@update');
Route::post('update/kuliner/{id}', 'API\KulinerController@update');
Route::post('update/pasar/{id}', 'API\PasarController@update');
Route::post('update/pelayanankesling/{id}', 'API\PelayananKeslingController@update');
Route::post('update/pesantren/{id}', 'API\PesantrenController@update');
Route::post('update/sab/{id}', 'API\SabController@update');
Route::post('update/dam/{id}', 'API\DamSippKlingController@update');
Route::post('update/sekolah/{id}', 'API\SekolahController@update');
Route::post('update/tempatibadah/{id}', 'API\TempatIbadahController@update');
Route::post('update/salon/{id}', 'API\SalonController@update');

Route::get('rumahsehat/kelurahan/{kelurahan}', 'API\RumahSehatController@showByKelurahan');
Route::get('rumahsehat/kecamatan/{kecamatan}', 'API\RumahSehatController@showByKecamatan');
Route::get('rumahsehat/search/{key}', 'API\RumahSehatController@searchBy');
Route::post('rumahsehat/update/{id}', 'API\RumahSehatController@updateWithImage');
Route::get('rumahsehat/namaKK/{id}', 'API\RumahSehatController@getNamaKK');
Route::get('kk/nomor/{nmr}', 'API\KartuKeluargaController@showKKbyNmr');
// Route::get('webkk', 'API\KartuKeluargaController@index');

Route::get('rumahsakit/kelurahan/{kelurahan}', 'API\RumahSakitController@showByKelurahan');
Route::get('rumahsakit/kecamatan/{kecamatan}', 'API\RumahSakitController@showByKecamatan');

Route::get('puskesmas/kelurahan/{kelurahan}', 'API\PuskesmasController@showByKelurahan');
Route::get('puskesmas/kecamatan/{kecamatan}', 'API\PuskesmasController@showByKecamatan');

Route::get('hotel/kelurahan/{kelurahan}', 'API\HotelController@showByKelurahan');
Route::get('hotel/kecamatan/{kecamatan}', 'API\HotelController@showByKecamatan');

Route::get('hotelmelati/kelurahan/{kelurahan}', 'API\HotelMelatiController@showByKelurahan');
Route::get('hotelmelati/kecamatan/{kecamatan}', 'API\HotelMelatiController@showByKecamatan');

Route::get('jasaboga/kelurahan/{kelurahan}', 'API\JasaBogaController@showByKelurahan');
Route::get('jasaboga/kecamatan/{kecamatan}', 'API\JasaBogaController@showByKecamatan');

Route::get('klinik/kelurahan/{kelurahan}', 'API\KlinikController@showByKelurahan');
Route::get('klinik/kecamatan/{kecamatan}', 'API\KlinikController@showByKecamatan');

Route::get('kolamrenang/kelurahan/{kelurahan}', 'API\KolamRenangController@showByKelurahan');
Route::get('kolamrenang/kecamatan/{kecamatan}', 'API\KolamRenangController@showByKecamatan');

Route::get('kuliner/kelurahan/{kelurahan}', 'API\KulinerController@showByKelurahan');
Route::get('kuliner/kecamatan/{kecamatan}', 'API\KulinerController@showByKecamatan');

Route::get('pasar/kelurahan/{kelurahan}', 'API\PasarController@showByKelurahan');
Route::get('pasar/kecamatan/{kecamatan}', 'API\PasarController@showByKecamatan');

Route::get('pelayanankesling/kelurahan/{kelurahan}', 'API\PelayananKeslingController@showByKelurahan');
Route::get('pelayanankesling/kecamatan/{kecamatan}', 'API\PelayananKeslingController@showByKecamatan');

Route::get('pesantren/kelurahan/{kelurahan}', 'API\PesantrenController@showByKelurahan');
Route::get('pesantren/kecamatan/{kecamatan}', 'API\PesantrenController@showByKecamatan');

Route::get('sab/kelurahan/{kelurahan}', 'API\SabController@showByKelurahan');
Route::get('sab/kecamatan/{kecamatan}', 'API\SabController@showByKecamatan');

Route::get('dam/kelurahan/{kelurahan}', 'API\DamSippKlingController@showByKelurahan');
Route::get('dam/kecamatan/{kecamatan}', 'API\DamSippKlingController@showByKecamatan');

Route::get('sekolah/kelurahan/{kelurahan}', 'API\SekolahController@showByKelurahan');
Route::get('sekolah/kecamatan/{kecamatan}', 'API\SekolahController@showByKecamatan');

Route::get('tempatibadah/kelurahan/{kelurahan}', 'API\TempatIbadahController@showByKelurahan');
Route::get('tempatibadah/kecamatan/{kecamatan}', 'API\TempatIbadahController@showByKecamatan');

Route::get('petugas/kelurahan/{kelurahan}', 'API\PetugasSippKlingController@showByKelurahan');
Route::get('petugas/kecamatan/{kecamatan}', 'API\PetugasSippklingController@showByKecamatan');
Route::get('petugas/histori/{id}', 'API\PetugasSippklingController@showHistory');
Route::get('petugas/historisemua/{id}', 'API\PetugasSippklingController@showHistorySemua');
Route::post('petugas/login', 'API\PetugasSippklingController@loginPetugas');
Route::post('petugas/upload/image', 'API\PetugasSippklingController@uploadFotoPetugas');
Route::put('petugas/change/name/{id}', 'API\PetugasSippklingController@updateNama');
Route::put('petugas/change/password/{id}', 'API\PetugasSippklingController@changePassword');

Route::get('petugas/kecamatan/perakun/{id}', 'API\PetugasSippklingController@getPerAkunPetugasKecamatan');

Route::get('reward/all', 'API\RewardController@allReward');
Route::get('reward/kategori/{table}', 'API\RewardController@rewardByKategori');
Route::get('reward/petugas/{id}', 'API\RewardController@petugasRewardDetail');
Route::get('reward/{username}', 'API\RewardController@webpetugasRewardDetail');

Route::get('search/kelurahan/{kelurahan}/{kategori}/{key}', 'API\SearchController@searchPerKelurahan');
Route::get('search/kecamatan/{kecamatan}/{kategori}/{key}', 'API\SearchController@searchPerKecamatan');

Route::get('search/objek/kelurahan/{kategori}/{kelurahan}/{key}/{sort}', 'API\SearchController@otherSearch');
Route::get('search/rumahsehat/kelurahan/{kelurahan}/{key}/{rt}/{rw}/{sort}', 'API\SearchController@searchWithFilter');

Route::get('search/histori/petugas/{petugas}/{key}', 'API\SearchController@searchHistory');
Route::get('search/petugas/{key}', 'API\SearchController@searchPetugas');
Route::get('rumahsehat/kelurahan/filter/{kelurahan}/{rt}/{rw}/{sort}', 'API\SearchController@getRsByFilter');
Route::get('rumahsehat/kelurahan/filter-sab/{kelurahan}/{rt}/{rw}/{sort}', 'API\SearchController@filterSabNull');

Route::get('search/web/rs', 'API\SearchWebController@searchRS');
Route::get('search/web/ttu', 'API\SearchWebController@searchTTU');
Route::get('search/web/tpm', 'API\SearchWebController@searchTPM');
Route::get('search/web/etc', 'API\SearchWebController@searchETC');

Route::get('download/manualbook', 'API\ImageController@downloadManualBook');
Route::get('download/image/objek/{path}', 'API\ImageController@objekImage');
Route::get('download/image/profile/{path}', 'API\ImageController@photoProfile');
Route::get('download/char/{path}', 'API\ImageController@downloadChar');
Route::get('list/char', 'API\ImageController@listChar');

Route::get('maps-jasaboga', 'API\DashboardMapsController@getJasaBoga');
Route::get('maps-pasar', 'API\DashboardMapsController@getPasar');
Route::get('maps-pasar', 'API\DashboardMapsController@getPasar');
Route::get('maps-sekolah', 'API\DashboardMapsController@getSekolah');
Route::get('maps-pesantren', 'API\DashboardMapsController@getPesantren');
Route::get('maps-kolam-renang', 'API\DashboardMapsController@getKolamRenang');
Route::get('maps-puskesmas', 'API\DashboardMapsController@getPuskesmas');
Route::get('maps-rumah-sakit', 'API\DashboardMapsController@getRumahSakit');
Route::get('maps-klinik', 'API\DashboardMapsController@getKlinik');
Route::get('maps-Hotel', 'API\DashboardMapsController@getHotel');
Route::get('maps-hotel-melati', 'API\DashboardMapsController@getKlinik');
Route::get('maps-salon', 'API\DashboardMapsController@getSalon');

Route::get('tempat-ibadah-maps', 'API\Maps\TempatIbadahMapsController@getTempatIbadah');
Route::get('pasar-maps', 'API\Maps\PasarMapsController@getpasar');
Route::get('sekolah-maps', 'API\Maps\SekolahMapsController@getsekolah');
Route::get('pesantren-maps', 'API\Maps\PesantrenMapsController@getPesantren');
Route::get('kolam-renang-maps', 'API\Maps\KolamRenangMapsController@getKolamRenang');
Route::get('puskesmas-maps', 'API\Maps\PuskesmasMapsController@getPesantren');
Route::get('rumah-sakit-maps', 'API\Maps\RumahSakitMapsController@getRumahSakit');
Route::get('klinik-maps', 'API\Maps\KlinikMapsController@getKlinik');
Route::get('hotel-maps', 'API\Maps\HotelMapsController@getHotel');
Route::get('hotel-melati-maps', 'API\Maps\HotelMelatiMapsController@getHotelMelati');
Route::get('salon-maps', 'API\Maps\SalonMapsController@getSalon');
Route::get('jasa-boga-maps', 'API\Maps\JasaBogaMapsController@getJasaBoga');
Route::get('restoran-maps', 'API\Maps\RestoranMapsController@getRestoran');
Route::get('depot-air-minum-maps', 'API\Maps\DepotAirMinumMapsController@getDAM');
Route::get('pelayanan-kesehatan-maps', 'API\Maps\PelayananKesehatanMapsController@getPK');
Route::get('phbs-maps', 'API\Maps\PHBSMapsController@getPHBS');

Route::get('coordinates', 'API\DashboardMapsController@getCoordinate');
Route::get('tpm', 'API\DashboardMapsController@getTpm');
Route::get('count-rumah-sehat/{status}', 'API\DashboardMapsController@countRumahSehat');
Route::get('pop', 'API\DashboardMapsController@getPopulasi');
Route::get('legendrumahsehat', 'API\DashboardMapsController@getLegendRumahSehat');
Route::get('legendtempatibadah','API\DashboardMapsController@getLegendTempatIbadah');
Route::get('sehat', 'API\DashboardMapsController@getRumahSehat');
Route::get('rame', 'API\DashboardMapsController@getJumlahKeluarga');

Route::get('trash', 'TrashController@index');
Route::post('trash/{kategori}/{id}', 'TrashController@restoreData');
Route::post('trash/hapus/{kategori}/{id}', 'TrashController@hapusData');
Route::post('trash/kosongkan/a/{kategori}', 'TrashController@kosongkanTrash');
Route::get('tpm', 'API\DashboardMapsController@getTpm');
Route::get('countrs', 'API\DashboardMapsController@countRumahSehat');

Route::get('landing-page/achievements', 'AchievementController@index');
Route::get('landing-page/testimonis', 'TestimoniController@index');
Route::get('landing-page/screenshots', 'ScreenshotController@index');
Route::get('landing-page/latarbelakang', 'GeneralController@getLatarbelakang');
Route::get('landing-page/home', 'GeneralController@getHome');
Route::get('landing-page/fitur', 'FiturController@getFitur');

Route::post('manage-konten/home/{id}', 'GeneralController@updateHome');

Route::post('manage-konten/achievement/{id}', 'AchievementController@updateAchievement');
Route::post('manage-konten/achievement/', 'AchievementController@addAchievement');
Route::delete('manage-konten/achievement/hapus/{id}', 'AchievementController@destroyAchievement');

Route::post('manage-konten/testimoni/{id}', 'TestimoniController@updateTestimoni');
Route::post('manage-konten/testimoni/', 'TestimoniController@addTestimoni');
Route::delete('manage-konten/testimoni/hapus/{id}', 'TestimoniController@destroyTestimoni');

Route::post('manage-konten/fitur/{id}', 'FiturController@updateFitur');
Route::post('manage-konten/fitur/', 'FiturController@addFitur');
Route::delete('manage-konten/fitur/hapus/{id}', 'FiturController@destroyFitur');

Route::post('manage-konten/screenshots/{id}', 'ScreenshotController@updateScreenshot');
Route::post('manage-konten/screenshot/', 'ScreenshotController@addScreenshot');
Route::delete('manage-konten/screenshots/hapus/{id}', 'ScreenshotController@destroyScreenshot');

Route::get('dp/{username}', 'API\PetugasSippklingController@showDPloginuser');

// Route::get('select-api/{namaController}/{namaFunction}/{query}',
// funtion ($namaController, $namaFunction, $query = null){
//     var_dump($namaController, $namaFunction, $query)
// });

Route::get('select-api/{table}', 'API\SelectAPIController@getTableColumn');

Route::post('tempatibadah/web/', 'API\TempatIbadahController@storeweb');

Route::get('tester-aja', function () {
});
