<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rumah_sehats', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, sabs, data_id, user_id
             */
            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('tempat_ibadahs', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('sabs', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, rumah_sehats
             */
            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('rumah_sehat_id')->unique();
            $table->foreign('rumah_sehat_id')->references('id')->on('rumah_sehats')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('rumah_sakits', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('sekolahs', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('puskesmas', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('pesantrens', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('pelayanan_keslings', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');



            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('pasars', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('moduls', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kuliners', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kolam_renangs', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kliniks', function (Blueprint $table) {
            /**
             * @foreign table kecamatans, datas
             */
            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('jasa_bogas', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('hotel_melatis', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('hotels', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('salons', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('dam_sipp_klings', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('jadwals', function (Blueprint $table) {
            /**
             * @foreign table kecamatans, kelurahans
             */

            $table->unsignedInteger('kecamatan_id');
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('kelurahan_id');
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kelurahans', function (Blueprint $table) {
            /**
             * @foreign table kecamatans
             */

            $table->unsignedInteger('kecamatan_id');
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('data_id');
            $table->foreign('data_id')->references('id')->on('datas')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kecamatans', function (Blueprint $table) {
            /**
             * @foreign table datas
             */

            $table->unsignedInteger('data_id');
            $table->foreign('data_id')->references('id')->on('datas')->onUpdate('cascade')->onDelete('cascade');


            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('datas', function (Blueprint $table) {
            /**
             * @foreign table kategoris
             */

            $table->unsignedInteger('kategori_id');
            $table->foreign('kategori_id')->references('id')->on('kategoris')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('kartu_keluargas', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, rumah_sehats
             */

            $table->unsignedInteger('petugas_id')->nullable();
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('rumah_sehat_id')->nullable();
            $table->foreign('rumah_sehat_id')->references('id')->on('rumah_sehats')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('histories', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, {{ objek_id }}
             */

            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('data_id');
            $table->foreign('data_id')->references('id')->on('datas')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('objek_id');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by')->nullable();
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            /**
             * @foreign table kecamatans, kelurahans
             */

            $table->unsignedInteger('kecamatan_id')->nullable();
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('kelurahan_id')->nullable();
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onUpdate('cascade')->onDelete('cascade');

            // $table->unsignedInteger('opd_id')->nullable();
            // $table->foreign('opd_id')->references('id')->on('opds')->onUpdate('cascade')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('petugas_sippklings', function (Blueprint $table) {
            /**
             * @foreign table kecamatans, kelurahans
             */

            $table->unsignedInteger('kecamatan_id')->nullable();
            $table->foreign('kecamatan_id')->references('id')->on('kecamatans')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('kelurahan_id')->nullable();
            $table->foreign('kelurahan_id')->references('id')->on('kelurahans')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });

        Schema::table('messages', function (Blueprint $table) {
            $table->unsignedInteger('sender_id');
            $table->foreign('sender_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedInteger('receiver_id')->nullable();
            $table->foreign('receiver_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('data_user', function(Blueprint $table){
            // data_id
            $table->unsignedInteger('data_id');
            $table->foreign('data_id')->references('id')->on('datas')->onUpdate('cascade')->onDelete('cascade');
            
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
            $table->softDeletes();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
