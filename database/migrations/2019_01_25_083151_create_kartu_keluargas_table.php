<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKartuKeluargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu_keluargas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jumlah_anggota')->nullable();
            $table->string('nama_kk', 50)->nullable();
            $table->string('nmr_kk', 100)->nullable()->unique();
            $table->unsignedInteger('jumlah_laki_laki')->nullable();
            $table->unsignedInteger('jumlah_perempuan')->nullable();
            $table->boolean("depok")->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu_keluargas');
    }
}
