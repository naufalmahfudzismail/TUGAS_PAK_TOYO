<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKulinersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuliners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alamat', 120);
            $table->smallInteger('jumlah_karyawan');
            $table->smallInteger('jumlah_penjamah');
            $table->string('koordinat', 40);
            $table->string('nama_pemilik', 25);
            $table->string('nama_tempat', 25);
            $table->string('no_izin_usaha', 50);
            $table->string('jam_operasional', 50)->nullable();
            $table->string('no_telp', 15)->nullable();
            $table->text('foto')->nullable();
            $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
            $table->tinyInteger('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuliners');
    }
}
