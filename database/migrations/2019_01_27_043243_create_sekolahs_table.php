<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tempat', 35);
            $table->string('nama_kepala_sekolah', 35);
            $table->string('alamat', 120);
            $table->smallInteger('jumlah_murid_pertahun');
            $table->string('tahun_berdiri', 6);
            $table->smallInteger('luas_bangunan');
            $table->smallInteger('luas_tanah');
            $table->smallInteger('jumlah_guru_pns');
            $table->smallInteger('jumlah_guru_tetap_non_pns');
            $table->smallInteger('jumlah_guru_honorer');
            $table->string('jam_operasional', 50);
            $table->string('no_telp', 16);
            $table->string('koordinat', 50);
            $table->text('foto')->nullable();
            $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->string('puskesmas', 100);
            $table->string('no_izin_operasional', 30);
            $table->boolean('status')->nullable();
            $table->text('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolahs');
    }
}
