<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetugasSippklingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas_sippklings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('old_id')->nullable();
            $table->string('nama', 100);
            $table->text('foto')->nullable();
            $table->string('username', 50)->unique();
            $table->string('password', 72);
            $table->boolean('role')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas_sippklings');
    }
}
