<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJasaBogasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jasa_bogas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tempat', 35);
            $table->string('alamat', 120);
            $table->string('nama_pengusaha', 35);
            $table->string('jam_operasional', 50);
            $table->string('no_telp', 15)->nullable();
            $table->string('koordinat', 40);
            $table->text('foto')->nullable();
            $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
            $table->tinyInteger('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jasa_bogas');
    }
}
