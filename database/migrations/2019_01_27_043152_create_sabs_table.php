<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sabs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alamat', 120);
            $table->string('golongan', 80);
            $table->string('kategori', 5);
            $table->string('kode_sampel', 30)->nullable();
            $table->string('kode_sarana', 30)->nullable();
            $table->string('koordinat', 40);
            $table->string('nama_puskesmas', 50)->nullable();
            $table->string('pemilik_sarana', 50)->nullable();
            $table->string('sudah_diambil', 40)->nullable();
            $table->string('foto', 40)->nullable();
            // $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->string('status', 30)->nullable();
            $table->tinyInteger('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sabs');
    }
}
