<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelayananKeslingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelayanan_keslings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alamat', 120);
            $table->string('jenis', 50);
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->text('kasus');
            $table->text('foto')->nullable();
            $table->string('koordinat', 40);
            $table->string('nama_pkl', 40);
            $table->string('nama_tempat', 40);
            $table->string('umur');
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelayanan_keslings');
    }
}
