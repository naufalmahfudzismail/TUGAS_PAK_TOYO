<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempatIbadahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tempat_ibadahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tempat', 35);
            $table->string('nama_pengurus');
            $table->string('alamat', 120);
            $table->smallInteger('jumlah_jamaah');
            $table->string('jam_operasional', 50);
            $table->string('no_telp', 16);
            $table->string('koordinat', 40)->nullable();
            $table->text('foto')->nullable();
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
            $table->tinyInteger('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tempat_ibadahs');
    }
}
