<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('waktu');
            $table->string('koordinat', 40);
            $table->string('nama_tempat', 25);
            $table->string('alamat', 120);
            $table->string('nama_pemimpin', 25);
            $table->smallInteger('jumlah_karyawan');
            $table->string('jam_operasional', 50);
            $table->string('tahun_berdiri', 4);
            $table->string('no_izin_usaha', 30);
            $table->text('foto')->nullable();
            $table->text('pajak')->nullable();
            $table->text('nilai')->nullable();
            $table->text('total_nilai')->nullable();
            $table->boolean('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()  
    {
        Schema::dropIfExists('salons');
    }
}
