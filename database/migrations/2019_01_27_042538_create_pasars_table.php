<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tempat', 35);
            $table->string('alamat', 120);
            $table->string('nama_pengelola', 35);
            $table->smallInteger('jumlah_kios');
            $table->smallInteger('jumlah_kios_kosong');
            $table->smallInteger('jumlah_kios_terisi');
            $table->smallInteger('jumlah_pedagang');
            $table->smallInteger('jumlah_asosiasi');
            $table->string('jam_operasional', 50);
            $table->string('no_telp', 16);
            $table->string('koordinat', 40);
            $table->text('foto')->nullable();
            $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
            $table->tinyInteger('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasars');
    }
}
