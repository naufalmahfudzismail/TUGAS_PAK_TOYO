<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuskesmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puskesmas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_tempat', 35);
            $table->string('alamat', 120);
            $table->string('nama_pemimpin', 35);
            $table->string('no_registrasi', 30)->nullable();
            $table->smallInteger('jumlah_karyawan')->nullable();
            $table->string('jam_operasional', 50);
            $table->string('no_telp', 16);
            $table->string('koordinat', 40);
            $table->text('foto')->nullable();
            $table->string('pajak', 50)->nullable();
            $table->datetime('waktu');
            $table->boolean('status')->nullable();
            $table->text('total_nilai')->nullable();
            $table->text('nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puskesmas');
    }
}
