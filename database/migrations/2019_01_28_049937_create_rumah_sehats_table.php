<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRumahSehatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rumah_sehats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('old_id')->nullable();
            $table->string('alamat', 120);
            $table->string('jamban', 15);
            // $table->tinyInteger('jumlah_anggota');
            // $table->tinyInteger('jumlah_laki_laki')->nullable();
            // $table->tinyInteger('jumlah_perempuan')->nullable();
            $table->string('koordinat', 40);
            $table->string('nama_kk',100)->nullable();
            $table->string('no_rumah', 20);
            $table->string('rt', 3);
            $table->string('rw', 3);
            $table->boolean('pjb')->nullable();
            $table->boolean('sampah')->nullable();
            $table->boolean('spal')->nullable();
            $table->boolean('status')->nullable();
            $table->boolean('status_rumah')->nullable();
            $table->datetime('waktu');
            $table->string('no_telp', 16)->nullable();
            $table->text('imb')->nullable();
            $table->text('nilai')->nullable();
            $table->text('foto')->nullable();
            $table->smallInteger('total_nilai')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rumah_sehats');
    }
}
