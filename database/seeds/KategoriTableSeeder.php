<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            "Kesehatan",
            "Pendidikan",
            "Perekonomian",
            "Sosial",
            "Parawisata",
            "Olahraga",
            "Transportasi",
            "Fasilitas Umum",
            "Kependudukan",
            "Instansi",
            "Bahasa"
        ];

        for ($j=0; $j < count($kategori); $j++) {
            DB::table('kategoris')->insert([
                'nama_kategori' => $kategori[$j],
                'created_by' => 'admin'
            ]);
        }
    }
}
