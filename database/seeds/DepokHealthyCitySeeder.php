<?php

use Illuminate\Database\Seeder;

class DepokHealthyCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['skor', 75]
        ];
        DB::table('general_sippkling_dashboards')->truncate();
        for ($i=0; $i < count($data); $i++) { 
            DB::table("general_sippkling_dashboards")->insert([
                'param' => $data[$i][0],
                'value' => $data[$i][1]
            ]);
        }

        $skor = [
            ['Rumah Sehat', 0],
            ['Tempat Ibadah', 0],
            ['Pasar', 0],
            ['Sekolah', 0],
            ['Pesantren', 0],
            ['Kolam Renang', 0],
            ['Puskesmas', 0],
            ['Rumah Sakit', 0],
            ['Klinik', 0],
            ['Hotel', 0],
            ['Hotel Melati', 0],
            ['Salon', 0],
            ['Jasa Boga', 0],
            ['Restoran', 0],
            ['Depot Air Minum', 0],
            ['Pelayanan Kesehatan', 0],
            ['PHBS', 0]
        ];
        
        DB::table('skor_kesehatan_lingkungans')->truncate();
        for ($i=0; $i < count($skor); $i++) { 
            DB::table("skor_kesehatan_lingkungans")->insert([
                'objek' => $skor[$i][0],
                'bobot' => $skor[$i][1]
            ]);
        }
    }
}
