<?php

use Illuminate\Database\Seeder;

class SabTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
{
        \App\Models\Moduls\Sab::create([
            'alamat' => 'ini contoh',
            'golongan' => 'ini golongan',
            'kategori' => 'A',
            'kode_sampel' => 'ini kode sampel',
            'kode_sarana' => 'ini kode sarana',
            'koordinat' => 'ini koordinat',
            'nama_puskesmas' => 'ini nama puskesmas',
            'pemilik_sarana' => 'ini pemilik sarana',
            'sudah_diambil' => 'ini sudah diambil',
            'waktu' => \Carbon\Carbon::now(),
            'status' => 'A',
            'total_nilai' => 100,
            'nilai' => 'ini nilai',
            'petugas_id' => 1,
            'rumah_sehat_id' => 1,
            'created_by' => 'admin'
        ]);
    }
}
