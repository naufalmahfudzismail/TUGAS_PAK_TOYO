<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul', 40);
            $table->text('isi');
            $table->string('foto', 40);
            $table->string('nama_pengirim', 40);
            $table->string('model_hp', 40);
            $table->string('versi_android', 40);
            $table->string('merk_hp', 40);
        });

        Schema::table('reports', function (Blueprint $table) {
            /**
             * @foreign table petugas_sippklings, datas, ops and users
             */
            $table->unsignedInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas_sippklings')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
            $table->string('created_by');
            $table->smallInteger('update_by')->nullable();
            $table->string('deleted_by')->nullable();
        });
    }
}
