<?php

use Illuminate\Database\Seeder;

class TestimoniSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {

        $testimoni =[
            ['/images/landingpage/testimoni/bappelkescikarang.png', 'Dukungan Bappelkes Cikarang'],
            ['/images/landingpage/testimoni/michosan.png', 'Dukungan Michosan Indonesia'],
            ['/images/landingpage/testimoni/who.png', 'Dukungan WHO Wash Cikarang'],
            ['/images/landingpage/testimoni/indohealthcareforum.png', 'Indohealthcare Forum'],
            ['/images/landingpage/testimoni/kotadepok.png', 'Kota Depok']
        ];

        for ($i = 0; $i < count($testimoni); $i++) {
            DB::table('testimonis')->insert([
                'gambar' => $testimoni[$i][0],
                'judul' => $testimoni[$i][1]
            ]);
        }
    }
}