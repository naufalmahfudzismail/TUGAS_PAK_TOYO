<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CleanPayload extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KategoriTableSeeder::class);
        $this->call(DataTableSeeder::class);
        $this->call(KecamatanTableSeeder::class);
        $this->call(KelurahanTableSeeder::class);
        $this->call(SuperUserPetugasSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(DataUserSeeder::class);
        $this->call(PetugasTableSeeder::class);
        $this->call(DirectoryPayload::class);
        $this->call(AchievementSeeder::class);
        $this->call(GeneralSeeder::class);
        $this->call(FiturSeeder::class);
        $this->call(ScreenshotSeeder::class);
        $this->call(TestimoniSeeder::class);
        $this->call(SuperUserPetugasSeeder::class);

        Schema::table('rumah_sehats', function (Blueprint $table) {
            $table->dropColumn('old_id');
            $table->dropColumn('nama_kk');
        });

        Schema::table('petugas_sippklings', function (Blueprint $table) {
            $table->dropColumn('old_id');
        });
    }
}
