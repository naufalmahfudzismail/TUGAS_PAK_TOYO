<?php

use Illuminate\Database\Seeder;

class FiturSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fitur = [
            ['/images/landingpage/fitur/fastload.png', 'Fast Load', true],
            ['/images/landingpage/fitur/nointernet.png', 'Working while no internet', true],
            ['/images/landingpage/fitur/directreport.png', 'Get Report Directly Via App', true],
            ['/images/landingpage/fitur/rewardkader.png', 'Get Reward For Top Kader', true],
            ['/images/landingpage/fitur/pushnotification.png', 'Get Push Notification', true],
            ['/images/landingpage/fitur/accessright.png', 'Access Right', true],
            ['/images/landingpage/fitur/realtimescore.png', 'Realtime Score', true],
            ['/images/landingpage/fitur/history.png', 'Menu History', true],
            ['/images/landingpage/fitur/allinone.png', 'All-in-one Dashboard', true],
            ['/images/landingpage/output/kesling.png', 'Kesehatan Lingkungan', false],
            ['/images/landingpage/output/pasar.png', 'Pasar', false],
            ['/images/landingpage/output/puskesmas.png', 'Puskesmas', false],
            ['/images/landingpage/output/rumahsehat.png', 'Rumah Sehat', false],
            ['/images/landingpage/output/rumahsakit.png', 'Rumah Sakit', false],
            ['/images/landingpage/output/sekolah.png', 'Sekolah', false],
            ['/images/landingpage/output/tempatibadah.png', 'Tempat Ibadah', false],
            ['/images/landingpage/output/kesling.png', 'Depot Air Minum', false],
            ['/images/landingpage/output/lainlain.png', 'Lain Lain', false],
        ];
        for ($i = 0; $i < count($fitur); $i++) {
            DB::table('fiturs')->insert([
                'gambar' => $fitur[$i][0],
                'namafitur' => $fitur[$i][1],
                'param' => $fitur[$i][2],
            ]);
        }

    }
}
