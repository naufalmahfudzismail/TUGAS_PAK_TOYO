<?php

use Illuminate\Database\Seeder;

class ScreenshotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $screenshot =[
            ['/images/landingpage/screenshots/homeresize.png','Home Page',true],
            ['/images/landingpage/screenshots/detailresize.png','Detail Page', true],
            ['/images/landingpage/screenshots/kategoripendataanresize.png','Kategori Pendataan Page',true],
            ['/images/landingpage/screenshots/rewardresize.png','Reward Page',true],
            ['/images/landingpage/screenshots/tampilangrafik.png','Tampilan Grafik',false],
            ['/images/landingpage/screenshots/tampilansekolah.png','Tampilan Sekolah',false]

        ];

        for ($i=0; $i < count($screenshot); $i++) {
            DB::table('screenshots')->insert([
                'gambar' => $screenshot[$i][0],
                'alt' => $screenshot[$i][1],
                'param' => $screenshot[$i][2],
            ]);
        }
    }
}
