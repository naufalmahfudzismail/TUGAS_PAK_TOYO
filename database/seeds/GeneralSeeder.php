<?php

use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data =[
            ['home','SIPP KLING', 'Sistem Informasi Pemetaan Profil Kesehatan Lingkungan', 'Pemetaan Profil Kesehatan Lingkungan dengan menggunakan teknologi smartphone sebagai aplikasi utama yang digunakan oleh Kader untuk mendata dari tempat ke tempat lainnya dan website sebagai pengelola sistem yang digunakan oleh petugas kesehatan lingkungan Dinas Kesehatan.', '','/images/landingpage/tampilanai.png'],
            ['latbel','Permenkes RI no.75 th.2014 Pasal 36 ayat 2', '','Puskesmas menjadi bagian dari upaya kesehatan masyarakat dan salah satunya adalah adanya pelayanan kesehatan lingkungan','','/images/landingpage/icon.png'],
            ['latbel','RPJMN 2020-2024 BAB 4 dan BAB 6', 'BAB 4 Sasaran Pemenuhan Layanan Dasar & BAB 6 Sasaran Pembangunan Infrastruktur Pelayanan Dasar, membahas tentang','Eliminasi malaria, persentase merokok penduduk usia 10 - 18 tahun, Persentase fasilitas kesehatan tingkat pertama terakreditasi, Persentase rumah sakit terakreditasi. Sedangkan BAB 6 Membahas tentang: "Penyediaan akses masyarakat terhadap perumahan dan permukiman layak, aman dan terjangkau" dan  "Penyediaan Akses Air Minum dan Sanitasi yang Layak dan Aman."','','/images/landingpage/ide.png'],
            ['latbel','Tingginya Penyakit Berbasis Lingkungan', '','TB Paru : 168.412 Jiwa, Diare : 7.077.299 Jiwa (hanya 60% ditangani), DBD : 59.047 Jiwa, Malaria : 261.617 Jiwa','','/images/landingpage/penyakit.png']
        ];

        for ($i=0; $i < count($data); $i++) {
            DB::table('generals')->insert([
                'param' => $data[$i][0],
                'title' => $data[$i][1],
                'subtitle' => $data[$i][2],
                'konten' => $data[$i][3],
                'link' => $data[$i][4],
                'gambar' =>$data[$i][5]
            ]);
        }
    }
}
