<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["Apotek", 1],
            ["Rumah Sakit", 1],
            ["Klinik", 1],
            ["Puskesmas", 1],
            ["Bidan", 1],
            ["Sekolah", 2],
            ["Pesantren", 2],
            ["Hotel", 8],
            ["Hotel Melati", 8],
            ["Pelayanan Kesehatan Lingkungan", 9],
            ["SAB", 10],
            ["Perpustakaan", 2],
            ["Mall", 3],
            ["Supermarket", 3],
            ["Pasar", 3],
            ["UMKM", 3],
            ["Panti Asuhan", 4],
            ["Komunitas", 4],
            ["Kuliner", 5],
            ["Tempat Wisata", 5],
            ["Olahraga", 6],
            ["Angkot", 7],
            ["Jasa Pengiriman", 7],
            ["Tempat Ibadah", 8], // 24
            ["TPU", 8],
            ["Taman", 8],
            ["Kelurahan", 9],
            ["Kecamatan", 9],
            ["Jumlah Penduduk", 9],
            ["PDAM", 10],
            ["Pos Polisi", 10],
            ["PLN", 10],
            ["SPBU", 10],
            ["Damkar", 10],
            ["TNI", 10],
            ["Rumah Sehat", 9],
            ["Bank", 3],
            ["Industri", 3],
            ["Perkantoran", 10],
            ["Jasa Boga", 3],
            ["Universitas", 2],
            ["Depot Air Minum", 3],
            ["Kolam Renang", 8],
            ["Restoran", 3],
            ["Salon", 1], // angka 1 ini belum di kordinasikan
            ["PHBS",1],// angka 1 ini belum di kordinasikan
        ];

        for ($i = 0; $i < count($data); $i++) {
            DB::table('datas')->insert([
                'nama_data' => $data[$i][0],
                'kategori_id' => $data[$i][1],
                'created_by' => 'admin'
            ]);
        }
    }
}
