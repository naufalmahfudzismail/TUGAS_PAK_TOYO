<?php

use Illuminate\Database\Seeder;

class AchievementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $achievement = [
            ['/images/landingpage/piala2017.png', '', 'Peringkat 1 - Aplikasi Proyek Perubahan Diklat PIM V se-Jawa Barat'],
            ['/images/landingpage/piala2018.png', '', 'Percontohan dan Narasumber pada Diskusi Kesehatan Lingkungan di Bappelkes Cikarang'],
            ['/images/landingpage/piala2018.png', '', 'Peringkat 1 - Kategori Inovasi ICT Bidang Kesehatan INDOHCF Award'],
            ['/images/landingpage/piala2018.png', '', 'Juara Favorit - Ajang Lomba Nasional INDOHCF AWARD 2'],
            ['/images/landingpage/piala2018.png', '', 'Peringkat 1 - Smart Sanitation Award AKKOPSI'],
            ['/images/landingpage/piala2018.png', '', 'Inovation Walikota Entrepreneur Award'],
            ['/images/landingpage/piala2019.png', '', 'Kolaborasi dengan Smart City Depok']
        ];

        for ($i = 0; $i < count($achievement); $i++) {
            DB::table('achievements')->insert([
                'icon' => $achievement[$i][0],
                'year' => $achievement[$i][1],
                'kategori' => $achievement[$i][2]
            ]);
        }
    }
}
