<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = [
            ["Super Admin","met",'$2y$10$0q.KMIH2fSF.GBwgy3Pge.ZaQZXvXcBQ2mQSqzHuQH.ns24p5qog6'],
            ["Super Admin Dinkes","superadmin",'$2y$10$J2zzTr5LyRAnEMi4uARqkO00.kND6RNsZQg/wGp3hXfHWrsTF0jnu'],
        ];

        for ($i=0; $i < count($super_admin); $i++) {
            DB::table('users')->insert([
                'nama' => $super_admin[$i][0],
                'username' => $super_admin[$i][1],
                'password' => $super_admin[$i][2],
                'role' => 1,
                'created_by' => 'admin'
            ]);
        }
        
        /**
         * dinkes = d1nk3s123
         */
        $adminOPD = [
            ["Dinas Kesehatan","admin_dinkes",'$2y$10$bbZzntfOCNaFCUhnlwrFxuP65NRlOtVbpjNveyAQAPKLXLzsqZRvG'],
            ["Diskominfo","admin_diskominfo",'$2y$10$AE/htV/YTPZlK/XG/gUPcePBWcd18eE7Ubr69IgK9xesLFijHpmVi'],
        ];

        for ($i=0; $i < count($adminOPD); $i++) {
            DB::table('users')->insert([
                'nama' => $adminOPD[$i][0],
                'username' => $adminOPD[$i][1],
                'password' => $adminOPD[$i][2],
                'role' => 2,
                'created_by' => 'admin'
            ]);
        }

        /**
         * password
         * upt_beji = upt_beji
         */
        $adminUPT = [
            ["Admin UPT Puskesmas ","upt",'$2y$10$meHmFry5tpHGZ0h.CqGGgeHfNxtN1fqIOnwrpwg3DcyXGljVSQ1py',1],
            ["Admin UPT Puskesmas ","upt",'$2y$10$eneJzopW2tciqAHiVjbwseRTaZ7kznJqMly3hHlJ7xIxRB0SSZRIC',2],
            ["Admin UPT Puskesmas ","upt",'$2y$10$cV6iQ6RMlugXdsjje.0lOu9RA2D6vSKwUW01IsJ3YyyR5ixQfDeZS',3],
            ["Admin UPT Puskesmas ","upt",'$2y$10$1fLeQy1d/KdOCf58113hdetPkxGzAaJn4uUCnmci/1grky9lOxw0e',4],
            ["Admin UPT Puskesmas ","upt",'$2y$10$IurgTJnrROzsTTf8igIa.uGsZZWQHSg/PLBokS3o3aAoKHmWlLSTi',5],
            ["Admin UPT Puskesmas ","upt","$2y$10$1ykWA50j/c4eejRnFeo4yO7qey2DKuuhRd1z/sHy6LwBsH1J7X3ci",6],
            ["Admin UPT Puskesmas ","upt",'$2y$10$BPIQnMVjC4fP6s.rDdet3u4L0grcEPbiDX7jQpcbFaizOfaqjlrXK',7],
            ["Admin UPT Puskesmas ","upt",'$2y$10$VX4AOO1iZsX785NHAMRhs.VfpsmG0Njd.aTb5w.H/wuRlEYJD8M/G',8],
            ["Admin UPT Puskesmas ","upt",'$2y$10$CPuRI0Q9h01I3aQO0VsD2OGW7LoIEVSxb7Da7UdL5q7BCpcn3zVhC',9],
            ["Admin UPT Puskesmas ","upt",'$2y$10$Y1iloeJGDsvLdF6tAHm1uecdvwnRoGvL3FbhEaai2Aw.Vz1uj7k2O',10],
            ["Admin UPT Puskesmas ","upt",'$2y$10$oGLS6UeZrc63gMJxXHLZ1.prsuLbMWBXclJwbNMM6jKbDpNYNbl.6',11]
        ];

        for ($i=0; $i < count($adminUPT); $i++) {
            DB::table('users')->insert([
                'nama' => $adminUPT[$i][0].''.\getKecamatanNameById($adminUPT[$i][3]),
                'username' => $adminUPT[$i][1].'_'.\optimizeUsernameWord(\getKecamatanNameById($adminUPT[$i][3]), ''),
                'password' => Hash::make($adminUPT[$i][1]),
                'role' => 3,
                'kecamatan_id' => $adminUPT[$i][3],
                'created_by' => 'admin'
            ]);

            $kelurahan = \App\Models\Kelurahan::select('nama','id')->where('kecamatan_id', $adminUPT[$i][3])->get();
            
            for ($j=0; $j < count($kelurahan); $j++) {

                $kader = new \App\Models\PetugasSippkling;
                $kader->nama = $adminUPT[$i][0].''.\getKecamatanNameById($adminUPT[$i][3]).' Kelurahan '.$kelurahan[$j]->nama;
                $kader->username = $adminUPT[$i][1] . '_'.\optimizeUsernameWord(\getKecamatanNameById($adminUPT[$i][3]), '').'_' . \optimizeUsernameWord($kelurahan[$j]->nama, '');
                $kader->password = Hash::make($adminUPT[$i][1]);
                $kader->kecamatan_id = $adminUPT[$i][3];
                $kader->kelurahan_id = $kelurahan[$j]->id;
                $kader->role = false;
                $kader->created_by = 'admin';
                $kader->save();

            }
        }

        /**
         * password
         * puskesmas_pasput = puskesmas_pasput
         */

        $adminUPF = [
            ["Puskesmas Pasir Putih","puskesmas_pasput",'$2y$10$D.TEVNOM0j14syH9tAyQQe3ZBjuR47hFvkucy.6122bDUjTttx1hW', 10, 54],
            ["Puskesmas Kedaung","puskesmas_kedaung","$2y$10$0cKwWHXlDYmu5dfDZRl2XO7g2dcQmGS5Vlc4VzuK7Giy7qHfjmZrq", 10, 49],
            ["Puskesmas Pengasinan","puskesmas_pengasinan","$2y$10$.vkOW2LDmgUAiKAi0slQNu0LQmawFRzztJgX8Susm/4qObaoftzQW", 10, 53],
            ["Puskesmas Duren Seribu","puskesmas_duser",'$2y$10$G58QeCQOCCCD0yKz354Rxegh6237Q7rENsjtTjdxWHyjfKNGw8AfW',11, 59],
            ["Puskesmas Depok Jaya","puskesmas_depokjaya",'$2y$10$OelrE5ihM15bfgwgcZUL2OZeiaidqU7sAxGjbVp11QsTbO5po7/ru',2, 9],
            ["Puskesmas Rangkapan Jaya Baru","puskesmas_rangkapan",'$2y$10$p59S5PkPmSf6PION18gHLeo6MyLd.qwsGXRyngUjeBUb2Y.P3ZGye',2, 10],
            ["Puskesmas Ratu Jaya","puskesmas_ratujaya",'$2y$10$ZBJURyPuGRhRNIL3xeeZu.ECv5blZiPgin44iYllYI4dS1IxYaFva',3, 14],
            ["Puskesmas Abadi Jaya","puskesmas_abadi",'$2y$10$DcCHkxycVvYbS1cXPABx/.2bx.cwXFknimp.W3JqtUo8D/TQ7ur4G',4, 19],
            ["Puskesmas Bakti Jaya","puskesmas_bakti",'$2y$10$heAe6Y0TJRQoUxK9fT4ldeIDZXStPHVJ8V5xQruzdyjpyV/aWUXiq',4, 18],
            ["Puskesmas Pondok Sukmajaya","puskesmas_pdsukmajaya",'$2y$10$S4vEUliITnU8ZrKukz0F2uTdEE4CEFdrsjn05Q5/qc3pCwxkx81/m',4, 16],
            ["Puskesmas Villa Pertiwi","puskesmas_villapertiwi",'$2y$10$rgPXozuKjYE5uXr6WS9F5e/sOiiDM5B9qMxyAd0EDSLuAIqDQgNlC',5, 23],
            ["Puskesmas Kalimulya","puskesmas_kalimulya",'$2y$10$Kqdz8kREpX5p42/9ZG4vLefdI4FS1ztW5nyOy9HDTYFYlf53qYpke',5, 25],
            ["Puskesmas Tugu","puskesmas_tugu","$2y$10$..YWba18Ks42G6m9LKC4cOD7F70J.WSCodH8F0kKBZiJVm1QgjvDK",8, 39],
            ["Puskesmas Harjamukti","puskesmas_harjamukti",'$2y$10$TV1jAX1K3Bq7H/6RPwyXS.UkE3N2W2B9B/8.qjzCdYftlTHwwxSou',8, 40],
            ["Puskesmas Pasir Gunung Selatan","puskesmas_pgs",'$2y$10$zWjNzcgDsUqR/1TJagQv.urtBh3jY4qlUi1IgRh/tgeKimPI9n36W',8, 38],
            ["Puskesmas Mekarsari","puskesmas_mekarsari",'$2y$10$LDILUZFIqxMxupEU8mWscOT8DnuLxZAOnCRJGyF3.CVcVL0VkrPxq',8, 37],
            ["Puskesmas Cisalak Pasar","puskesmas_cipas",'$2y$10$P7ejjNJKs0J.ZWMBMwcjbeq8wIKJy89kwY/0c/wtfjOZKTI2wtTwu',8, 36],
            ["Puskesmas Sukatani","puskesmas_sukatani","$2y$10$/Z6tCIvLWTrCpEBl.YPcGeXW39NKCHeZRu2nZP.1d/l.ZG9yvUwxq",9, 44],
            ["Puskesmas Jatijajar","puskesmas_jatijajar","$2y$10$6GdDvUhSprgrqXe7LuHDjuFqjEvomqN/KQMpsG/dn3fwvajfeAvwy",9, 46],
            ["Puskesmas Cilangkap","puskesmas_cilangkap",'$2y$10$UGkcVYfJLmm8Xzai97QqBeq.PWLRsFV3RT7gl9QFmLjYhziXULX4O',9, 47],
            ["Puskesmas Cimpaeun","puskesmas_cimpaeun",'$2y$10$t5AD0/pFI0KBpVB/pjfgJe6hNM7ysle/8JOAXQrKh68/NX/cwJaTC',9, 48],
            ["Puskesmas Sukamaju Baru","puskesmas_sukamaju",'$2y$10$kWERgEYVekBAs24g7QMrOOkjk8OxZIGsa2xo9BvIGnOubUJoYdOfS',9, 45],
            // ["Puskesmas Tanah Baru","puskesmas_tnhbaru",'$2y$10$Kom9ttsGxtHPPAk0NhPfyOqnBQTAC0JBqB5138zgAy.F/2aLOHuSe',10, 6],
            ["Puskesmas Kemiri Muka","puskesmas_kemiri",'$2y$10$tCHYUay0Mj2F9vqx7DkIy.35hiHvxvHLopbn0bA.J30YLbUXbBWia',1, 3],
        ];

        for ($i=0; $i < count($adminUPF); $i++) {
            DB::table('users')->insert([
                'nama' => $adminUPF[$i][0],
                'username' => $adminUPF[$i][1],
                'password' => Hash::make($adminUPF[$i][1]),
                'kecamatan_id' => $adminUPF[$i][3],
                'kelurahan_id' => $adminUPF[$i][4],
                'role' => 4,
                'created_by' => 'admin'
            ]);

            $rootKader = new \App\Models\PetugasSippkling;
            $rootKader->nama = $adminUPF[$i][0];
            $rootKader->username = $adminUPF[$i][1];
            $rootKader->password = Hash::make($adminUPF[$i][1]);
            $rootKader->kecamatan_id = $adminUPF[$i][3];
            $rootKader->kelurahan_id = $adminUPF[$i][4];
            $rootKader->role = false;
            $rootKader->created_by = 'admin';
            $rootKader->save();
        }

    }
}
