<?php

use Illuminate\Database\Seeder;

class DropColumn extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::table('rumah_sehats', function (Blueprint $table) {
            $table->dropColumn('nama_kk');
        });
    }
}
