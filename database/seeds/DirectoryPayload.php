<?php

use Illuminate\Database\Seeder;
use League\Flysystem\Filesystem;

class DirectoryPayload extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        File::deleteDirectory(storage_path() . '/app/public/images');
        $pathImages = storage_path() . '/app/public/images';
        if (!file_exists($pathImages)) {
            $this->makeDirectory($pathImages, 0777, true, true);
            // Storage::makeDirectory($directory);
        }

        $pathObjek = storage_path() . '/app/public/images/objek';
        if (!file_exists($pathObjek)) {
            $this->makeDirectory($pathObjek, 0777, true, true);
        }

        $pathProfil = storage_path() . '/app/public/images/profil';
        if (!file_exists($pathProfil)) {
            File::makeDirectory($pathProfil, 0777, true, true);
        }
    }

    public function makeDirectory($path, $mode = 0777, $recursive = false, $force = false)
    {
        if ($force)
        {
            return @mkdir($path, $mode, $recursive);
        }
        else
        {
            return mkdir($path, $mode, $recursive);
        }
    }
}
