<?php

use Illuminate\Database\Seeder;

class DataUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_user')->insert([
            'user_id' => 3,
            'data_id' => 36
        ]);

        DB::table('data_user')->insert([
            'user_id' => 3,
            'data_id' => 2
        ]);

        DB::table('data_user')->insert([
            'user_id' => 3,
            'data_id' => 4
        ]);

        DB::table('data_user')->insert([
            'user_id' => 4,
            'data_id' => 2
        ]);
    }
}
