<?php

use Illuminate\Database\Seeder;

class HistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.\
     *
     * @return void
     */
    public function run()
    {
        DB::table('histories')->insert([
            'aktifitas' => "input",
            'waktu_history' => \Carbon\Carbon::now()->toDateTimeString(),
            'tipe_petugas' => 'kader',
            'key_histories' => '1-36-1-1-1',
            'data_id' => 36,
            'objek_id' => 1,
            'petugas_id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'created_by' => 'admin'
        ]);
    }
}
