<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperUserPetugasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ['admin', '', 'admin', "51PPKL1NGd1nk3$", false, null, null, 'admin'],
            ['admin', '', 'admin_beji', "51PPKL1NGd1nk3$", false, 1, 1, 'admin'],
            ['admin', '', 'admin_bejitimur', "51PPKL1NGd1nk3$", false, 1, 2, 'admin'],
            ['admin', '', 'admin_kemirimuka', "51PPKL1NGd1nk3$", false, 1, 3, 'admin'],
            ['admin', '', 'admin_pondokcina', "51PPKL1NGd1nk3$", false, 1, 4, 'admin'],
            ['admin', '', 'admin_kukusan', "51PPKL1NGd1nk3$", false, 1, 5, 'admin'],
            ['admin', '', 'admin_tanahbaru', "51PPKL1NGd1nk3$", false, 1, 6, 'admin'],
            ['admin', '', 'admin_pancoranmas', "51PPKL1NGd1nk3$", false, 2, 7, 'admin'],
            ['admin', '', 'admin_depok', "51PPKL1NGd1nk3$", false, 2, 8, 'admin'],
            ['admin', '', 'admin_depokjaya', "51PPKL1NGd1nk3$", false, 2, 9, 'admin'],
            ['admin', '', 'admin_rangkapanjaya', "51PPKL1NGd1nk3$", false, 2, 10,'admin'],
            ['admin', '', 'admin_mampang', "51PPKL1NGd1nk3$", false, 2, 11, 'admin'],
            ['admin', '', 'admin_cipayung', "51PPKL1NGd1nk3$", false, 3, 12, 'admin'],
            ['admin', '', 'admin_cipayungjaya', "51PPKL1NGd1nk3$", false, 3, 13, 'admin'],
            ['admin', '', 'admin_ratujaya', "51PPKL1NGd1nk3$", false, 3, 14, 'admin'],
            ['admin', '', 'admin_pondokjaya', "51PPKL1NGd1nk3$", false, 3, 15, 'admin'],
            ['admin', '', 'admin_sukmajaya', "51PPKL1NGd1nk3$", false, 4, 16, 'admin'],
            ['admin', '', 'admin_mekarjaya', "51PPKL1NGd1nk3$", false, 4, 17, 'admin'],
            ['admin', '', 'admin_baktijaya', "51PPKL1NGd1nk3$", false, 4, 18, 'admin'],
            ['admin', '', 'admin_abadijaya', "51PPKL1NGd1nk3$", false, 4, 19, 'admin'],
            ['admin', '', 'admin_tirtajaya', "51PPKL1NGd1nk3$", false, 4, 20, 'admin'],
            ['admin', '', 'admin_cisalak', "51PPKL1NGd1nk3$", false, 4, 21, 'admin'],
            ['admin', '', 'admin_cilodong', "51PPKL1NGd1nk3$", false, 5, 22, 'admin'],
            ['admin', '', 'admin_sukamaju', "51PPKL1NGd1nk3$", false, 5, 23, 'admin'],
            ['admin', '', 'admin_kalibaru', "51PPKL1NGd1nk3$", false, 5, 24, 'admin'],
            ['admin', '', 'admin_kalimulya', "51PPKL1NGd1nk3$", false, 5, 25, 'admin'],
            ['admin', '', 'admin_jatimulya', "51PPKL1NGd1nk3$", false, 5, 26, 'admin'],
            ['admin', '', 'admin_limo', "51PPKL1NGd1nk3$", false, 6, 27, 'admin'],
            ['admin', '', 'admin_meruyung', "51PPKL1NGd1nk3$", false, 6, 28, 'admin'],
            ['admin', '', 'admin_grogol', "51PPKL1NGd1nk3$", false, 6, 29, 'admin'],
            ['admin', '', 'admin_krukut', "51PPKL1NGd1nk3$", false, 6, 30, 'admin'],
            ['admin', '', 'admin_cinere', "51PPKL1NGd1nk3$", false, 7, 31, 'admin'],
            ['admin', '', 'admin_pangkalanjatibaru', "51PPKL1NGd1nk3$", false, 7, 32, 'admin'],
            ['admin', '', 'admin_pangkalanjatilama', "51PPKL1NGd1nk3$", false, 7, 33,'admin'],
            ['admin', '', 'admin_gandul', "51PPKL1NGd1nk3$", false, 7, 34, 'admin'],
            ['admin', '', 'admin_pangkalanjati', "51PPKL1NGd1nk3$", false, 7, 35,'admin'],
            ['admin', '', 'admin_cisalakpasar', "51PPKL1NGd1nk3$", false, 8, 36, 'admin'],
            ['admin', '', 'admin_mekarsari', "51PPKL1NGd1nk3$", false, 8, 37, 'admin'],
            ['admin', '', 'admin_pasirgunungselatan', "51PPKL1NGd1nk3$", false, 8, 38,'admin'],
            ['admin', '', 'admin_tugu', "51PPKL1NGd1nk3$", false, 8, 39, 'admin'],
            ['admin', '', 'admin_harjamukti', "51PPKL1NGd1nk3$", false, 8, 40, 'admin'],
            ['admin', '', 'admin_curug', "51PPKL1NGd1nk3$", false, 8, 41, 'admin'],
            ['admin', '', 'admin_tapos', "51PPKL1NGd1nk3$", false, 9, 42, 'admin'],
            ['admin', '', 'admin_leuwinanggung', "51PPKL1NGd1nk3$", false, 9, 43, 'admin'],
            ['admin', '', 'admin_sukatani', "51PPKL1NGd1nk3$", false, 9, 44, 'admin'],
            ['admin', '', 'admin_sukamajubaru', "51PPKL1NGd1nk3$", false, 9, 45, 'admin'],
            ['admin', '', 'admin_jatijajar', "51PPKL1NGd1nk3$", false, 9, 46, 'admin'],
            ['admin', '', 'admin_cilangkap', "51PPKL1NGd1nk3$", false, 9, 47, 'admin'],
            ['admin', '', 'admin_cimpaeun', "51PPKL1NGd1nk3$", false, 9, 48, 'admin'],
            ['admin', '', 'admin_sawangan', "51PPKL1NGd1nk3$", false, 10, 49, 'admin'],
            ['admin', '', 'admin_kedaung', "51PPKL1NGd1nk3$", false, 10, 50, 'admin'],
            ['admin', '', 'admin_cinangka', "51PPKL1NGd1nk3$", false, 10, 51, 'admin'],
            ['admin', '', 'admin_sawanganbaru', "51PPKL1NGd1nk3$", false, 10, 52, 'admin'],
            ['admin', '', 'admin_bedahan', "51PPKL1NGd1nk3$", false, 10, 53, 'admin'],
            ['admin', '', 'admin_pengasinan', "51PPKL1NGd1nk3$", false, 10, 54, 'admin'],
            ['admin', '', 'admin_pasirputih', "51PPKL1NGd1nk3$", false, 10, 55, 'admin'],
            ['admin', '', 'admin_bojongsaribaru', "51PPKL1NGd1nk3$", false, 11, 56, 'admin'],
            ['admin', '', 'admin_serua', "51PPKL1NGd1nk3$", false, 11, 57, 'admin'],
            ['admin', '', 'admin_pondokpetir', "51PPKL1NGd1nk3$", false, 11, 58, 'admin'],
            ['admin', '', 'admin_bojongsari_curug', "51PPKL1NGd1nk3$", false, 11, 59, 'admin'],
            ['admin', '', 'admin_durenmekar', "51PPKL1NGd1nk3$", false, 11, 60, 'admin'],
            ['admin', '', 'admin_bojongpondokterong', "51PPKL1NGd1nk3$", false, 3, 61,'admin'],
            ['admin', '', 'admin_durenseribu', "51PPKL1NGd1nk3$", false, 11, 62,'admin'],
            ['admin', '', 'admin_bojongsarilama', "51PPKL1NGd1nk3$", false, 11, 63,'admin'],
            ['admin', '', 'admin_rangkapanjayabaru', "51PPKL1NGd1nk3$", false, 2, 64,'admin'],
        ];
        // DB::table('petugas_sippklings')->truncate();
        for($i = 0; $i< count($arr); $i++){
            DB::table('petugas_sippklings')->insert([
                'nama' => $arr[$i][0],
                'foto' => $arr[$i][1],
                'username' => $arr[$i][2],
                'password' => Hash::make($arr[$i][3]),
                'role' => $arr[$i][4],
                'kecamatan_id' => $arr[$i][5],
                'kelurahan_id' => $arr[$i][6],
                'created_by' => 'admin'
            ]);
        }
    }
}
