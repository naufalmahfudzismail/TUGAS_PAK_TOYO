<?php

use Illuminate\Database\Seeder;

class FinalUploadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(KategoriTableSeeder::class);
        // $this->call(DataTableSeeder::class);
        $this->call(KecamatanTableSeeder::class);
        $this->call(KelurahanTableSeeder::class);

        $super_admin = [
            ["Super Admin","met",'$2y$10$0q.KMIH2fSF.GBwgy3Pge.ZaQZXvXcBQ2mQSqzHuQH.ns24p5qog6'],
            ["Super Admin","superadmin",'$2y$10$J2zzTr5LyRAnEMi4uARqkO00.kND6RNsZQg/wGp3hXfHWrsTF0jnu'],
        ];

        // DB::table('users')->truncate();
        for ($i=0; $i < count($super_admin); $i++) {
            DB::table('users')->insert([
                'nama' => $super_admin[$i][0],
                'username' => $super_admin[$i][1],
                'password' => $super_admin[$i][2],
                'role' => 1,
                'created_by' => 'admin'
            ]);
        }

        // $this->call(DirectoryPayload::class);
        // $this->call(AchievementSeeder::class);
        // $this->call(GeneralSeeder::class);
        // $this->call(FiturSeeder::class);
        // $this->call(ScreenshotSeeder::class);
        // $this->call(TestimoniSeeder::class);
        $this->call(SuperUserPetugasSeeder::class);

        // Schema::table('rumah_sehats', function (Blueprint $table) {
        //     $table->dropColumn('old_id');
        //     $table->dropColumn('nama_kk');
        // });

        // Schema::table('petugas_sippklings', function (Blueprint $table) {
        //     $table->dropColumn('old_id');
        // });

        $this->call(DepokHealthyCitySeeder::class);
    }
}
