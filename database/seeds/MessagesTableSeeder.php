<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'subject' => 'tester',
            'message' => 'ini pesannya',
            'private' => true,
            'sender_id' => 1,
            'receiver_id' => 1
        ]);

        DB::table('messages')->insert([
            'subject' => 'broadcast tester',
            'message' => 'ini pesannya',
            'private' => false,
            'sender_id' => 1,
            'receiver_id' => null
        ]);
    }
}
