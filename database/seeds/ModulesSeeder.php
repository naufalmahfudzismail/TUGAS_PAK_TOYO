<?php

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // tempat ibadah
        $tempat_ibadah = [
            ['Masjid Jami Baitul Maqdis', 'Djoko Sutejo', 'Jl.Rajawali', '200', '07.00 - 17.00', '021727003613', '3872743, 378278', 'foto.jpg', '2019-08-08', true, 850, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Masjid Masnadi', 'Asnan', 'Jl.Rajawali', '200', '07.00 - 7.10', '021727003613', '3872743, 378278', 'foto.jpg', '2019-08-08', false, 600, '80, 90, 28, 80', 1, 'admin'],
        ];

        DB::table('tempat_ibadahs')->truncate();
        for ($i=0; $i < count($tempat_ibadah); $i++) { 
            DB::table('tempat_ibadahs')->insert([
                'nama_tempat' => $tempat_ibadah[$i][0],
                'nama_pengurus' => $tempat_ibadah[$i][1],
                'alamat' => $tempat_ibadah[$i][2],
                'jumlah_jamaah' => $tempat_ibadah[$i][3],
                'jam_operasional' => $tempat_ibadah[$i][4],
                'no_telp' => $tempat_ibadah[$i][5],
                'koordinat' => $tempat_ibadah[$i][6],
                'foto' => $tempat_ibadah[$i][7],
                'waktu' => $tempat_ibadah[$i][8],
                'status' => $tempat_ibadah[$i][9],
                'total_nilai' => $tempat_ibadah[$i][10],
                'nilai' => $tempat_ibadah[$i][11],
                'petugas_id' => $tempat_ibadah[$i][12],
                'created_by' => $tempat_ibadah[$i][13]
            ]);
        }

        $pasar = [
            ['Pasar Segar', 'Jl. Pasar Segar', 'Kusnadi', 140, 20, 120, 200, 30, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 850, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Pasar Pak Iwan', 'Jl. Pak Iwan', 'Bu Shinta', 140, 20, 120, 200, 30, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 650, '80, 90, 28, 80, 90, 100', 1, 'admin'],
        ];

        DB::table('pasars')->truncate();
        for ($i=0; $i < count($pasar); $i++) { 
            DB::table('pasars')->insert([
                'nama_tempat' => $pasar[$i][0],
                'alamat' => $pasar[$i][1],
                'nama_pengelola' => $pasar[$i][2],
                'jumlah_kios' => $pasar[$i][3],
                'jumlah_kios_kosong' => $pasar[$i][4],
                'jumlah_kios_terisi' => $pasar[$i][5],
                'jumlah_pedagang' => $pasar[$i][6],
                'jumlah_asosiasi' => $pasar[$i][7],
                'jam_operasional' => $pasar[$i][8],
                'no_telp' => $pasar[$i][9],
                'koordinat' => $pasar[$i][10],
                'foto' => $pasar[$i][11],
                'pajak' => $pasar[$i][12],
                'waktu' => $pasar[$i][13],
                'status' => $pasar[$i][14],
                'total_nilai' => $pasar[$i][15],
                'nilai' =>$pasar[$i][16],
                'petugas_id' => $pasar[$i][17],
                'created_by' => $pasar[$i][18]
            ]);
        }

        $sekolahs = [
            ['SMA Negeri Depok 1', 'Suyadi', 'Jl. Pak Mauldy', 140, '1998', 120, 200, 70, 50, 70, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', 'Puskesmas Ciledug', '123456', true, 850, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['SMA Negeri Depok 2', 'Sutinem', 'Jl. Pak Suginem', 143, '1998', 120, 200, 70, 50, 70, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', 'Puskesmas Ciledug', '123456', false, 650, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];

        DB::table('sekolahs')->truncate();
        for ($i=0; $i < count($sekolahs); $i++) { 
            DB::table('sekolahs')->insert([
                'nama_tempat' => $sekolahs[$i][0],
                'nama_kepala_sekolah' => $sekolahs[$i][1],
                'alamat' => $sekolahs[$i][2],
                'jumlah_murid_pertahun' => $sekolahs[$i][3],
                'tahun_berdiri' => $sekolahs[$i][4],
                'luas_bangunan' => $sekolahs[$i][5],
                'luas_tanah' => $sekolahs[$i][6],
                'jumlah_guru_pns' => $sekolahs[$i][7],
                'jumlah_guru_tetap_non_pns' => $sekolahs[$i][8],
                'jumlah_guru_honorer' => $sekolahs[$i][9],
                'jam_operasional' => $sekolahs[$i][10],
                'no_telp' => $sekolahs[$i][11],
                'koordinat' => $sekolahs[$i][12],
                'foto' => $sekolahs[$i][13],
                'pajak' => $sekolahs[$i][14],
                'waktu' => $sekolahs[$i][15],
                'puskesmas' => $sekolahs[$i][16],
                'no_izin_operasional' => $sekolahs[$i][17],
                'status' => $sekolahs[$i][18],
                'total_nilai' => $sekolahs[$i][19],
                'nilai' => $sekolahs[$i][20],
                'petugas_id' => $sekolahs[$i][21],
                'created_by' => $sekolahs[$i][22]
            ]);
        }

        $pesantren = [
            ['Pondok Pesantren Junior', 'Jl. Rajawali', 'Ustad Hamengkue', 200, 100, 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 900, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Pondok Pesantren Senior', 'Jl. Raja Jempol', 'Ustad Hamengkue ver. 2', 200, 100, 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin'],
        ];

        DB::table('pesantrens')->truncate();
        for ($i=0; $i < count($pesantren); $i++) { 
            DB::table('pesantrens')->insert([
                'nama_tempat'=> $pesantren[$i][0],
                'alamat'=> $pesantren[$i][1],
                'nama_pengelola'=> $pesantren[$i][2],
                'jumlah_santri'=> $pesantren[$i][3],
                'jumlah_laki_laki'=> $pesantren[$i][4],
                'jumlah_perempuan'=> $pesantren[$i][5],
                'jam_operasional'=> $pesantren[$i][6],
                'no_telp'=> $pesantren[$i][7],
                'koordinat'=> $pesantren[$i][8],
                'foto'=> $pesantren[$i][9],
                'pajak'=> $pesantren[$i][10],
                'waktu'=> $pesantren[$i][11],
                'status'=> $pesantren[$i][12],
                'total_nilai'=> $pesantren[$i][13],
                'nilai'=> $pesantren[$i][14],
                'petugas_id' => $pesantren[$i][15],
                'created_by' => $pesantren[$i][16]
            ]);
        }

        $kolam_renang = [
            ['Kolam Renang Ceria', 'Jl. Rajawali', 'Daniel Isutin', '30', '2342874', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Kolam Renang Ceria 2', 'Jl. Rajawali 2', 'Daniel Isutin 2', '30', '2342874', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];

        DB::table('kolam_renangs')->truncate();        
        for ($i=0; $i < count($kolam_renang); $i++) { 
            DB::table('kolam_renangs')->insert([
                'nama_tempat' => $kolam_renang[$i][0],
                'alamat' => $kolam_renang[$i][1],
                'nama_pengelola' => $kolam_renang[$i][2],
                'jumlah_karyawan' => $kolam_renang[$i][3],
                'no_izin_usaha' => $kolam_renang[$i][4],
                'jam_operasional' => $kolam_renang[$i][5],
                'no_telp' => $kolam_renang[$i][6],
                'koordinat' => $kolam_renang[$i][7],
                'foto' => $kolam_renang[$i][8],
                'pajak' => $kolam_renang[$i][9],
                'waktu' => $kolam_renang[$i][10],
                'status' => $kolam_renang[$i][11],
                'total_nilai' => $kolam_renang[$i][12],
                'nilai' => $kolam_renang[$i][13],
                'petugas_id' => $kolam_renang[$i][14],
                'created_by' => $kolam_renang[$i][15]
            ]);
        }

        $puskesmas = [
            ['Puskesmas Beji', 'Jl. Rajawali', 'Sujono', '287173', 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Puskesmas Beji', 'Jl. Rajawali', 'Sujono', '298717', 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];
        DB::table('puskesmas')->truncate();
        for ($i=0; $i < count($puskesmas); $i++) { 
            DB::table('puskesmas')->insert([
                'nama_tempat' => $puskesmas[$i][0],
                'alamat' => $puskesmas[$i][1],
                'nama_pemimpin' => $puskesmas[$i][2],
                'no_registrasi' => $puskesmas[$i][3],
                'jumlah_karyawan' => $puskesmas[$i][4],
                'jam_operasional' => $puskesmas[$i][5],
                'no_telp' => $puskesmas[$i][6],
                'koordinat' => $puskesmas[$i][7],
                'foto' => $puskesmas[$i][8],
                'pajak' => $puskesmas[$i][9],
                'waktu' => $puskesmas[$i][10],
                'status' => $puskesmas[$i][11],
                'total_nilai' => $puskesmas[$i][12],
                'nilai' => $puskesmas[$i][13],
                'petugas_id' => $kolam_renang[$i][14],
                'created_by' => $kolam_renang[$i][15]
            ]);
        }

        $rumah_sakit = [
            ['Graha Permata Hijau', 'Jl. Rajawali', 69, 1, 'Demam Berdarah', 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Graha Permata Hijau 2', 'Jl. Rajawali 2', 69, 1, 'Demam Berdara', 100, '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin'],
        ];
        DB::table('rumah_sakits')->truncate();
        for ($i=0; $i < count($rumah_sakit); $i++) { 
            DB::table('rumah_sakits')->insert([
                'nama_tempat' => $rumah_sakit[$i][0],
                'alamat' => $rumah_sakit[$i][1],
                'jumlah_tempat_tidur' => $rumah_sakit[$i][2],
                'kelas_rs' => $rumah_sakit[$i][3],
                'penyakit_terbanyak' => $rumah_sakit[$i][4],
                'rata_rata_rawat_inap' => $rumah_sakit[$i][5],
                'jam_operasional' => $rumah_sakit[$i][6],
                'no_telp' => $rumah_sakit[$i][7],
                'koordinat' => $rumah_sakit[$i][8],
                'foto' => $rumah_sakit[$i][9],
                'pajak' => $rumah_sakit[$i][10],
                'waktu' => $rumah_sakit[$i][11],
                'status' => $rumah_sakit[$i][12],
                'total_nilai' => $rumah_sakit[$i][13],
                'nilai' => $rumah_sakit[$i][14],
                'petugas_id' => $rumah_sakit[$i][15],
                'created_by' => $rumah_sakit[$i][16]
            ]);
        }

        $klinik = [
            ['Klinik Persada Jaya', 'Jl. Rajawali', 'Sujoko', 'ini gambaran umum', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Klinik Persada Jaya', 'Jl. Rajawali', 'Sujoko', 'ini gambaran umum', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];
        DB::table('kliniks')->truncate();
        for ($i=0; $i < count($klinik); $i++) { 
            DB::table('kliniks')->insert([
                'nama_tempat' => $klinik[$i][0],
                'alamat' => $klinik[$i][1],
                'nama_pimpinan' => $klinik[$i][2],
                'gambaran_umum' => $klinik[$i][3],
                'jam_operasional' => $klinik[$i][4],
                'no_telp' => $klinik[$i][5],
                'koordinat' => $klinik[$i][6],
                'foto' => $klinik[$i][7],
                'pajak' => $klinik[$i][8],
                'waktu' => $klinik[$i][9],
                'status' => $klinik[$i][10],
                'total_nilai' => $klinik[$i][11],
                'nilai' => $klinik[$i][12],
                'petugas_id' => $klinik[$i][13],
                'created_by' => $klinik[$i][14]
            ]);
        }

        $hotel = [
            ['Laxton Hotel', 'Jl. Rajawali', 'Junaedi', 100, '29817381', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Laxton Hotel', 'Jl. Rajawali', 'Junaedi', 100, '29817381', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];

        DB::table('hotels')->truncate();
        for ($i=0; $i < count($hotel); $i++) { 
            DB::table('hotels')->insert([
                'nama_tempat' => $hotel[$i][0],
                'alamat' => $hotel[$i][1],
                'nama_pemimpin' => $hotel[$i][2],
                'jumlah_karyawan' => $hotel[$i][3],
                'no_izin_usaha' => $hotel[$i][4],
                'jam_operasional' => $hotel[$i][5],
                'no_telp' => $hotel[$i][6],
                'koordinat' => $hotel[$i][7],
                'foto' => $hotel[$i][8],
                'pajak' => $hotel[$i][9],
                'waktu' => $hotel[$i][10],
                'status' => $hotel[$i][11],
                'total_nilai' => $hotel[$i][12],
                'nilai' => $hotel[$i][13],
                'petugas_id' => $hotel[$i][14],
                'created_by' => $hotel[$i][15]
            ]);
        }

        $hotel_melatis = [
            ['Laxton Hotel', 'Jl. Rajawali', 'Junaedi', 100, '29817381', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Laxton Hotel', 'Jl. Rajawali', 'Junaedi', 100, '29817381', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin']
        ];

        DB::table('hotel_melatis')->truncate();
        for ($i=0; $i < count($hotel_melatis); $i++) { 
            DB::table('hotel_melatis')->insert([
                'nama_tempat' => $hotel_melatis[$i][0],
                'alamat' => $hotel_melatis[$i][1],
                'nama_pemimpin' => $hotel_melatis[$i][2],
                'jumlah_karyawan' => $hotel_melatis[$i][3],
                'no_izin_usaha' => $hotel_melatis[$i][4],
                'jam_operasional' => $hotel_melatis[$i][5],
                'no_telp' => $hotel_melatis[$i][6],
                'koordinat' => $hotel_melatis[$i][7],
                'foto' => $hotel_melatis[$i][8],
                'pajak' => $hotel_melatis[$i][9],
                'waktu' => $hotel_melatis[$i][10],
                'status' => $hotel_melatis[$i][11],
                'total_nilai' => $hotel_melatis[$i][12],
                'nilai' => $hotel_melatis[$i][13],
                'petugas_id' => $hotel_melatis[$i][14],
                'created_by' => $hotel_melatis[$i][15]
            ]);
        }

        $dam = [
            ['Depot Barokah', 'Jl. Rajawali', 'Syamsudin', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Depot Barokah', 'Jl. Rajawali', 'Syamsudin', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin'],
        ];
        DB::table('dam_sipp_klings')->truncate();
        for ($i=0; $i < count($dam); $i++) { 
            DB::table('dam_sipp_klings')->insert([
                'nama_depot' => $dam[$i][0],
                'alamat' => $dam[$i][1],
                'nama_pemilik' => $dam[$i][2],
                'jam_operasional' => $dam[$i][3],
                'no_telp' => $dam[$i][4],
                'koordinat' => $dam[$i][5],
                'foto' => $dam[$i][6],
                'pajak' => $dam[$i][7],
                'waktu' => $dam[$i][8],
                'status' => $dam[$i][9],
                'total_nilai' => $dam[$i][10],
                'nilai' => $dam[$i][11],
                'petugas_id' => $dam[$i][12],
                'created_by' => $dam[$i][13]
            ]);
        }

        $jasa_boga = [
            ['Catering Mbo Yun', 'Jl. Rajawali', 'Syamsudin', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', true, 800, '80, 90, 28, 80, 90, 100', 1, 'admin'],
            ['Catering Mbo Yun', 'Jl. Rajawali', 'Syamsudin', '07.00 - 07.10', '021727003613', '3872743, 378278', 'foto.jpg', 'Ada, 12847287483, Sudah bayar', '2019-08-08', false, 600, '80, 90, 28, 80, 90, 100', 1, 'admin'],
        ];
        DB::table('jasa_bogas')->truncate();
        for ($i=0; $i < count($jasa_boga); $i++) { 
            DB::table('jasa_bogas')->insert([
                'nama_tempat' => $jasa_boga[$i][0],
                'alamat' => $jasa_boga[$i][1],
                'nama_pengusaha' => $jasa_boga[$i][2],
                'jam_operasional' => $jasa_boga[$i][3],
                'no_telp' => $jasa_boga[$i][4],
                'koordinat' => $jasa_boga[$i][5],
                'foto' => $jasa_boga[$i][6],
                'pajak' => $jasa_boga[$i][7],
                'waktu' => $jasa_boga[$i][8],
                'status' => $jasa_boga[$i][9],
                'total_nilai' => $jasa_boga[$i][10],
                'nilai' => $jasa_boga[$i][11],
                'petugas_id' => $jasa_boga[$i][12],
                'created_by' => $jasa_boga[$i][13]
            ]);
        }
    }
}