<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KategoriTableSeeder::class);
        $this->call(DataTableSeeder::class);
        $this->call(KecamatanTableSeeder::class);
        $this->call(KelurahanTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(DataUserSeeder::class);
        $this->call(PetugasTableSeeder::class);
        $this->call(RumahSehatTableSeeder5::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(DirectoryPayload::class);
        $this->call(AchievementSeeder::class);
        $this->call(GeneralSeeder::class);
        $this->call(FiturSeeder::class);
        $this->call(ScreenshotSeeder::class);
        $this->call(TestimoniSeeder::class);
        // $this->call(KartuKeluargasTableSeeder::class);

        Schema::table('rumah_sehats', function (Blueprint $table) {
            $table->dropColumn('old_id');
            $table->dropColumn('nama_kk');
        });

        Schema::table('petugas_sippklings', function (Blueprint $table) {
            $table->dropColumn('old_id');
        });

        $this->call(HistoryTableSeeder::class);
        $this->call(SabTableSeeder::class);
        $this->call(SuperUserPetugasSeeder::class);
        $this->call(ModulesSeeder::class);
        $this->call(DepokHealthyCitySeeder::class);
    }
}
