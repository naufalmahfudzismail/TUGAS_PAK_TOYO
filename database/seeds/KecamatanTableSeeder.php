<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class KecamatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kecamatans')->insert([
            'nama' => 'Beji',
            'alamat' => 'Perum Depok Indah',
            'nama_camat' => 'Drs. Ues Suryadi, M.Pd',
            'koordinat' => '-6.380871, 106.814676',
            'no_telp' => '021-7520233',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Pancoran Mas',
            'alamat' => 'JL. Raya Kartini',
            'nama_camat' => 'Utang Wardaya, AP, M.Si',
            'koordinat' => '-6.404173, 106.818971',
            'no_telp' => '021-77200537',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Cipayung',
            'alamat' => 'JL. Raya Cipayung',
            'nama_camat' => 'Drs. Asep Rahmat,M.Si',
            'koordinat' => '-6.427090, 106.796984',
            'no_telp' => '021-77881499',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Sukmajaya',
            'alamat' => 'JL. Sentosa Raya',
            'nama_camat' => 'Drs. Taufan Abdul Fatah, MH',
            'koordinat' => '-6.396512, 106.836806',
            'no_telp' => '021-77822576',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Cilodong',
            'alamat' => 'JL. M. Nasir Raya Cilodong',
            'nama_camat' => 'Mulyadi, SH, MH',
            'koordinat' => '-6.4248467,106.8468658',
            'no_telp' => '021-87909547',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Limo',
            'alamat' => 'JL. Raya Limo',
            'nama_camat' => 'Drs. Dedi Rosadi, M.Si',
            'koordinat' => '-6.368680, 106.775828',
            'no_telp' => '021-7546982',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Cinere', 
            'alamat' => 'JL. Bukit Cinere No.17',
            'nama_camat' => 'Eko Herwiyanto, AP, M.Si',
            'koordinat' => '-6.3278313, 106.7872649',
            'no_telp' => '021-29415552',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Cimanggis',
            'alamat' => 'JL. Radar Auri No.15',
            'nama_camat' => 'Drs. Henry Mahawan',
            'koordinat' => '-6.372944, 106.872644',
            'no_telp' => '021-8711436',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Tapos',
            'alamat' => 'JL. Raya Tapos',
            'nama_camat' => 'Muchsin Mawardi, S.Ip, S.Sos,',
            'koordinat' => '-6.432343, 106.886582',
            'no_telp' => '021-87903977',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Sawangan',
            'alamat' => 'JL. Raya Muchtar',
            'nama_camat' => '-',
            'koordinat' => '-6.405436, 106.759139',
            'no_telp' => '0251-618177',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);

        DB::table('kecamatans')->insert([
            'nama' => 'Bojong Sari',
            'alamat' => 'JL. Raya Batu No.12 RT 03/04 Kelurahan Bojong Sari',
            'nama_camat' => 'Drs. Usman Haliyana',
            'koordinat' => '-6.394040, 106.744017',
            'no_telp' => '0251-8612040',
            'data_id' => 28,
            'created_by' => 'admin'
        ]);
    }
}
