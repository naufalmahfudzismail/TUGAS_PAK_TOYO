<?php

use Illuminate\Database\Seeder;

class KelurahanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelurahan = [
            ["Beji","Jl. H. Asmawi","Waryono, S.IP","(021) 7773243", 1,"-6.381020, 106.813951"],
            ["Beji Timur","Jl. Raya Pemuda No 3 RT 2 RW 5 Beji Timur","Ahmad Darjat, SH","(021) 7521432", 1,"-6.380649, 106.822917"],
            ["Kemiri Muka","Jalan H.Fatimah No.112, Beji, Kemiri Muka, Beji, Kota Depok, Jawa Barat 16423"," Ramdani, SH","(021) 77203114", 1,"-6.378161, 106.832642"],
            ["Pondok Cina","Jl. H.Yahya Nuih No.24, Pd. Cina, Beji, Kota Depok, Jawa Barat 16424","Hendar Fradesa, SE","(021) 7522352", 1,"-6.370936, 106.836930"],
            ["Kukusan","Jalan Palakali No.67, Kukusan, Beji, Kukusan, Beji, Kota Depok, Jawa Barat 16425","Hasan, S.Pd","(021) 7271404", 1,"-6.367666, 106.819050"],
            ["Tanah Baru","Jl. Palakali, RT.7/RW.12, Kukusan, Beji, Kota Depok, Jawa Barat 16425","Drs. Zulkarnain","(021) 7752860", 1,"-6.374562, 106.803405"],
            ["Pancoran Mas","Jl. Pitara Raya No.49, Pancoran MAS, Kota Depok, Jawa Barat 16436","Dadi Rusmiadi","(021) 7759249", 2,"-6.406584, 106.804116"],
            ["Depok","Jl. Raya Kartini No.26, Depok, Pancoran MAS, Kota Depok, Jawa Barat 16431","Mustakim, S.Sos","(021) 7520659", 2,"-6.403224, 106.818833"],
            ["Depok Jaya","Jl. Nusantara Raya No.1, Depok Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16432","Mahindra, SKM","(021) 7520762", 2,"-6.388976, 106.814141"],
            ["Rangkapan Jaya","Jl. Raya Sawangan No.21, Rangkapan Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16435","Komarudin Daiman, S.Sos, M.Si","(021) 77883634", 2,"-6.394841, 106.785020"],
            ["Mampang","JL. Damai, No. 1 RT. 03 RW. 12, Gandul, Cinere, Kota Depok, Jawa Barat 16436","Aa. Abdul Khoir, SH","(021) 77201335", 2,"-6.390511, 106.795560"],
            ["Cipayung","Jl. Bonang Raya No.14, Cipayung, Kota Depok, Jawa Barat 16437","Herman","(021) 77880516", 3,"-6.419266, 106.795648"],
            ["Cipayung Jaya","Jalan Perum Dep Pertanian, Cipayung, Cipayung Jaya, Cipayung, Kota Depok, Jawa Barat 16437","Suprihatin, S.Pd","(021) 77881451", 3,"-6.441244, 106.796681"],
            ["Ratu Jaya","Jl. Kp. Ratu Jaya No.59, Ratu Jaya, Cipayung, Kota Depok, Jawa Barat 16439","Achmad Subandi, SH","(021) 7758190", 3,"-6.416996, 106.817308"],
            /*15*/["Pondok Jaya","Jl. Mirah, Pd. Jaya, Cipayung, Kota Depok, Jawa Barat 16438","Suryana Yusuf, S.Ag","(021) 32192940", 3,"-6.441645, 106.811410"],
            ["Sukmajaya","Jalan Pondok Sukma Jaya Permai Blok C5 No.24, Kecamatan Sukma Jaya, Sukmajaya, Kota Depok, Jawa Barat 16412","Achmad Munandar, S.Sos","(021) 7706854", 4,"-6.411286, 106.844541"],
            ["Mekar Jaya","Jl. Waru Jaya No.9, Mekar Jaya, Sukmajaya, Kota Depok, Jawa Barat 16411","Ahmad,? S.STP","(021) 7710283", 4,"-6.386828, 106.842530"],
            ["Bakti Jaya","Jl. Jasa Warga, Bakti Jaya, Sukmajaya, Kota Depok, Jawa Barat 16418","M. Saiun, S.Sos","(021) 7709806", 4,"-6.385154, 106.854514"],
            ["Abadi Jaya","Jl. Keadilan Raya, Abadijaya, Sukmajaya, Kota Depok, Jawa Barat 16418","Pairin, SH","(021) 77830676", 4,"-6.389669, 106.851878"],
            /*20*/["Tirta Jaya","Jl. Tirta Bhakti Blk. Tugu Jaya No.C No.7, Tirtajaya, Sukmajaya, Kota Depok, Jawa Barat 16412","Ayi Mukarom, S.Ip","(021) 77822769", 4," -6.411061, 106.822210"],
            ["Cisalak","Cisalak, Sukmajaya, Depok City, West Java 16416","Wiyana, SE","(021) 87708662", 4,"-6.394789, 106.863188"],
            ["Cilodong","Jl. M. Nasir, Cilodong, Kota Depok, Jawa Barat 16415","Sumarna, S.Sos","(021)? 7705292", 5,"-6.425579, 106.846477"],
            ["Sukamaju","Jl. Raya Bogor No.31, Sukamaju, Cilodong, Kota Depok, Jawa Barat 16415","Slamet, SH","(021) 8752270", 5,"-6.414946, 106.859181"],
            ["Kalibaru","Jl. H. Nirun No.7, Kalibaru, Cilodong, Kota Depok, Jawa Barat 16413","Siti Hasanah, S.STP","(021) 8762480", 5,"-6.437817, 106.842965"],
            /*25*/["Kalimulya","Jl. TPU Kalimulya 1 No.2, Kalimulya, Cilodong, Kota Depok, Jawa Barat 16413","Hasan Nurdin, AKS","(021) 8764582", 5,"-6.438224, 106.821597"],
            ["Jatimulya","Jl. Bumi Bukit Pabuaran Indah Blok D4 No.13, Jatimulya, Cilodong, Kota Depok, Jawa Barat 16413","Nyoman Budiarsa, SE","(021) 8753669", 5,"-6.446745, 106.833605"],
            ["Limo","JL. Koman Muin, Limo, Kota Depok, Jawa Barat 16515","Danudi Amin SE?","(021) 75 35 48 1", 6,"-6.359465, 106.776386"],
            ["Meruyung","Jl. Amir Marsiah, Meruyung, Limo, Kota Depok, Jawa Barat 16514","Ir. Raden Galih Purnaman, SP","(021) 75 35 40 3", 6,"-6.375690, 106.771598"],
            ["Grogol","Jl. Kp. Grogol No.7, Grogol, Limo, Kota Depok, Jawa Barat 16514","Mansyur, SE, M.Si","(021) 77 54 82 4", 6,"-6.379769, 106.789971"],
            /*30*/["Krukut","Jl. Krukut Raya, Krukut, Limo, Kota Depok, Jawa Barat 16512","Eddy M","(021) 75 33 33 1", 6,"-6.356362, 106.790874"],
            ["Cinere","Jl. Cinere Raya No.8, Cinere, Kota Depok, Jawa Barat 16514","Pupung Purwawijaya, S.IP","(021) 75 43 34 0", 7,"-6.334875, 106.784009"],
            ["Pangkalan Jati Baru","Jl. Pangkalan Jati Baru","-","-", 7,"-"],
            ["Pangkalan Jati Lama","Jl. Pangkalan Jati Lama","-","-", 7,"-"],
            ["Gandul","Jalan Prapatan No.1, Gandul, Cinere, Kota Depok, Jawa Barat 16512","Mursalim, S.AG","(021) 75 45 08 4", 7,"-6.340024, 106.793587"],
            /*35*/["Pengkalan Jati","Jalan Pangkalan Jati II No. 14, Cinere, Pangkalan Jati, Depok, Kota Depok, Jawa Barat 16514","Tarmuji","(021) 75 91 33 51", 7,"-6.323010, 106.793972"],
            ["Cisalak Pasar","Jl. Zamrud Raya, Cisalak Ps., Cimanggis, Kota Depok, Jawa Barat 16452","Cahyanto, SE","(021) 87 34 13 2", 8,"-6.372198, 106.876216"],
            ["Mekarsari","Jl. Tiga Berlian, Mekarsari, Cimanggis, Kota Depok, Jawa Barat 16452","Suryadi","(021) 87 11 73 4", 8,"-6.366774, 106.865213"],
            ["Pasir Gunung Selatan","Jalan Garuda II, Cimanggis, RW.2, Pasir Gn. Sel., Depok, Kota Depok, Jawa Barat 16451","Tedi Mulyono","(021) 87 25 61 1", 8,"-6.343017, 106.849206"],
            ["Tugu","Jl. Tugu Raya, Tugu, Cimanggis, Kota Depok, Jawa Barat 16451","Syaiful Hidayat","(021) 87720940", 8,"-6.366186, 106.850148"],
            /*40*/["Harjamukti","Jl. Putri Tunggal, Harjamukti, Cimanggis, Kota Depok, Jawa Barat 16454","Lamin. S.Pd","(021) 87 31 21 7", 8,"-6.382678, 106.885879"],
            ["Curug","Jl. Sinar Matahari Km. 32, Curug, Cimanggis, Curug, Cimanggis, Kota Depok, Jawa Barat 16453","Purwadi, S.Sos","(021) 87 41 67 2", 8,"-6.386409, 106.873849"],
            ["Tapos","Jl. Tapos Raya, Tapos, Kota Depok, Jawa Barat 16957","Mohammad Imron, S.Pd","(021) 87 57 71 4", 9,"-6.432494, 106.886131"],
            ["Leuwinanggung","Jl. Raya Kramat No. 125, RT. 04 / RW. 02, Leuwinanggung, Tapos, Leuwinanggung, Tapos, Kota Depok, Jawa Barat 16456","Asam, S.Ag","(021) 84 49 22", 9,"-6.407500, 106.905218"],
            ["Sukatani","Jl. Gas Alam No.15, Tapos, Sukarani, Depok, Sukatani, Tapos, Jawa Barat, 16454","Jarkasih","(021) 87 74 20 75", 9,"-6.386307, 106.888527"],
            /*45*/["Sukamaju Baru","Gang Makmur Raya, Sukamaju Baru, Tapos, Sukamaju Baru, Tapos, Kota Depok, Jawa Barat 16455","Samiya, S.Sos","(021) 70782035", 9,"-6.401107, 106.871556"],
            ["Jatijajar","Jalan Kelurahan Jatijajar, Jatijajar, Tapos, Jatijajar, Depok, Kota Depok, Jawa Barat 16455","Sugino, S.Sos","(021) 87 25 92 3", 9,"-6.418035, 106.865550"],
            ["Cilangkap","Jl. Cilangkap No 1, Cilangkap, Tapos, Kota Depok, Jawa Barat 16458","Suaibun","(021) 87 51 83 8", 9,"-6.445135, 106.856833"],
            ["Cimpaeun","Cimpaeun, Tapos, Depok City, West Java 16459","Tri Sutanto, S.Sos","(021) 87 57 33 3", 9,"-6.444950, 106.875531"],
            ["Sawangan","Jl. Kopral Daman No.50, Sawangan Lama, Sawangan, Kota Depok, Jawa Barat 16517","Ahmad Rifai, Lc","(0251) 8617531", 10,"-6.403052, 106.759238"],
            /*50*/["Kedaung","JL. Raya Ketapang, No. 5 RT. 1 RW. 2, Kedaung, Sawangan, Kota Depok, Jawa Barat 16516","Wahidin S","(021) 7494350", 10,"-6.366693, 106.746635"],
            ["Cinangka","Jl. Kona 1, Cinangka, Sawangan, Kota Depok, Jawa Barat 16516","Anis Fatoni, S.Sos","(021) 70623458", 10,"-6.367773, 106.757225"],
            ["Sawangan Baru","Jl. Sawangan Permai No.57, Sawangan Baru, Sawangan, Kota Depok, Jawa Barat 16511","Suhari, SH","0251 70888683", 10,"-6.405144, 106.773783"],
            ["Bedahan","Jalan Haji Sulaiman No.9, Sawangan, Jl. Jabon, Bedahan, Sawangan, Kota Depok, Jawa Barat 16519","Muhammad Sahal, SE","(0251) 8619159", 10,"-6.421460, 106.766798"],
            ["Pengasinan","Pengasinan, Sawangan, Depok City, West Java 16518","Fahlun, S.Sos, M.Si","(0251) 8619521", 10,"-6.422046, 106.752394"],
            /*55*/["Pasir Putih","Jl. Raya Pasir Putih No.19, RT.4/RW.6, Pasir Putih, Sawangan, Kota Depok, Jawa Barat 16519","Sudadih","0251 8610872", 10,"-6.422261, 106.782649"],
            ["Bojongsari Baru","Jl. Rotan No.1, Bojongsari Baru, Bojongsari, Kota Depok, Jawa Barat 16516","?","(0251) 8610108", 11,"-6.384923, 106.744362"],
            ["Serua","Jl. Serua Raya, Serua, Bojongsari, Kota Depok, Jawa Barat 16517","Dede Hidayat, SE","(021) 7443855", 11,"-6.369277, 106.740078"],
            ["Pondok Petir","Jl. Raya Pd. Petir, Pd. Petir, Bojongsari, Kota Depok, Jawa Barat 16517","Anwar Nasihin, S.Ag. MM","(021) 7421409", 11,"-6.366714, 106.725943"],
            ["Curug","Jl. Curug No.3, Curug, Bojongsari, Kota Depok, Jawa Barat 16517","Juanda, SH","(0251) 8610044", 11,"-6.393575, 106.732960"],
            /*60*/["Duren Mekar","Jl. Duren Mekar No.59, Duren Mekar, Bojongsari, Kota Depok, Jawa Barat 16518","Ujang Salahudin, S.Pd","(0251) 8613522", 11,"-6.416713, 106.740371"],
            ["Bojong Pondok Terong","Jl. Masjid Al Ittihad No.9, Bojong Pd. Terong, Cipayung, Kota Depok, Jawa Barat 16436","Bachtiar Satria Buana, S.Sos","(021) 77213277", 3,"-6.432784, 106.803934"],
            ["Duren Seribu","Jl. ARCO Raya Kec. Bojongsari Kota Depok 16518","Ade Ahyadi","(0251) 8613522", 11,"-6.4350951,106.7463663"],
            ["Bojongsari Lama","-","-","0", 11,"-"],
            ["Rangkapan Jaya Baru","-","-","0", 2,"-"]
        ];

        // DB::table('kelurahans')->truncate();
        for ($i=0; $i < count($kelurahan); $i++) { 
            DB::table('kelurahans')->insert([
                'nama' => $kelurahan[$i][0], 
                'alamat' => $kelurahan[$i][1],
                'nama_lurah' => $kelurahan[$i][2],
                'koordinat' => $kelurahan[$i][5],
                'no_telp' => $kelurahan[$i][3],
                'kecamatan_id' => $kelurahan[$i][4],
                'data_id' => 27,
                'created_by' => 'admin'
            ]);
        }
    }
}
